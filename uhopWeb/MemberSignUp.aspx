﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MemberSignUp.aspx.cs" Inherits="uhopWeb.MemberSignUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>u-Hop : Member Sign Up</title>

    <link rel="shortcut icon" href="Images/uhop_icon_beta-min.png" type="image/png" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    <asp:PlaceHolder ID="PlaceHolder2" runat="server">
        <%: Styles.Render("~/bundle/style") %>
    </asp:PlaceHolder>

    <link href="Styles/loginsignup.min.css" rel="stylesheet" />
    
</head>
<body id="mSignUpBG">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" EnablePageMethods="true" runat="server"></asp:ScriptManager>
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server"></asp:UpdatePanel>
        <!-- Successfully Registered Popup -->
        <div class="divCover SignupComplete" id="DivSignupCoplete" runat="server">
            <div style="max-width: 100%; min-height: 100%; width: 100%; text-align: center; color: white; margin-left: auto; margin-right: auto">
                <br /><br /><br /><br /><br /><br />
                <h5 style="color: green"><b>You have successfully registered!</b><br />
                    <span style="color: orange; font-size: 20px;">We've sent a confirmation in your email <span class="pink-text" runat="server" id="spnEmail"></span>. Please validate your account by clicking the link we've provided in your mail. Thank you.</span></h5>
                <a href="MemberSignUp.aspx" class="btn waves-effect waves-light pink">Close &nbsp;<i class="material-icons right" style="margin:0;">&#xE5CD;</i></a>
            </div>
        </div>
        <!-- End of Successfully Registered Popup  -->
        <!-- Start of container -->
        <div class="containerSignUp container">
            <asp:HiddenField ID="HdfIsOperator" runat="server" />

            <div class="card-panel z-depth-5">

                <h2 class="H2Title center black-text signUpHeader">Member Sign up</h2>
                <div class="divider pink accen-t2" style="height: 5px"></div>

                <div class="row red-text">
                    <p class="col s12 right-align redNote">NOTE: fields with * are required</p>
                </div>

                <!-- First name and last name row -->
                <div class="row">

                    <!-- First Name Input Field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE8A6;</i>
                        <asp:TextBox ID="txtFirstName" CssClass="large validate text-color" style="text-transform: capitalize;" runat="server" MaxLength="30" autocomplete="off"></asp:TextBox>
                        <label for="txtFirstName">
                            First Name <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="First name is required" SetFocusOnError="true" ControlToValidate="txtFirstName" Display="Dynamic" ValidationGroup="SignUp" Text="First name is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <!-- END of First Name Input Field -->

                    <!-- Last Name input field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE8A6;</i>
                        <asp:TextBox ID="txtLastName" CssClass="large validate text-color txtLastName" style="text-transform: capitalize;" runat="server" MaxLength="30" autocomplete="off"></asp:TextBox>
                        <label for="txtLastName">
                            Last Name <span class="red-text">*</span>
                        </label>
                        <div class=" col s12 center">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Last name is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtLastName" Text="Last name is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-- END of Last Name input field -->
                </div>
                <!-- First name and last name row -->

                <!-- Email Row -->
                <div class="row">
                    <!-- Email Input Field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE0BE;</i>
                        <asp:TextBox ID="txtEmailSignUp" CssClass="large validate text-color errorLabel" Style="text-transform: lowercase" runat="server" onchange="ConfirmEmail();" autocomplete="off" MaxLength="50"></asp:TextBox>
                        <label for="txtEmailSignUp">
                            Email Address <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center">
                            <span style="display: none; color: red" class="spanEmailExists" id="spanEmailExists" runat="server">Email already exist!</span>
                        </div>
                        <div class="col s12 center">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid email" ValidationExpression='^[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+(\.[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$'  SetFocusOnError="true" ControlToValidate="txtEmailSignUp" Text="Invalid email" Display="Dynamic" ValidationGroup="SignUp" CssClass="ValidationSummary"></asp:RegularExpressionValidator>&nbsp;
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Email is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtEmailSignUp" Text="Email is required" CssClass="ValidationSummary">Email is required</asp:RequiredFieldValidator>&nbsp;                                
                        </div>
                    </div>
                    <!-- End of Email Input Field -->
                    
                    <!-- Re enter email input field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE0BE;</i>
                        <asp:TextBox ID="txtSignUpRetypeEmail" CssClass="large validate text-color errorLabel" runat="server" Style="text-transform: lowercase" onchange="ConfirmEmail();" autocomplete="off" MaxLength="50"></asp:TextBox>
                        <div class="col s12 center">
                            <span style="display: none; color: red" class="spanEmailConfirm">Email do not match!</span>
                        </div>
                        <label for="txtSignUpRetypeEmail">
                            Re-enter email <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Invalid email" ValidationExpression='^[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+(\.[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$'  SetFocusOnError="true" ControlToValidate="txtSignUpRetypeEmail" Text="Invalid email" Display="Dynamic" ValidationGroup="SignUp" CssClass="ValidationSummary"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Re-enter email is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtSignUpRetypeEmail" Text="Re-enter email is required" CssClass="ValidationSummary">Re-enter Email is required</asp:RequiredFieldValidator>                                
                        </div>

                    </div>
                    <!-- END of Re enter email input field -->
                </div>
                <!-- END of Email Row -->

                <!-- Password Row -->
                <div class="row">
                    <!-- Password Input Field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE897;</i>
                        <asp:TextBox ID="txtPasswordSignUp" TextMode="Password" CssClass="large validate text-color" runat="server" onchange="ConfirmPasswordRetype();" MaxLength="20" autocomplete="off"></asp:TextBox>
                        <label for="txtPasswordSignUp">
                            Password (6 characters above) <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center red-text">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Password is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtPasswordSignUp" Text="Password is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator> 
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" SetFocusOnError="true" ControlToValidate="txtPasswordSignUp" ErrorMessage="Password must be Alphanumeric!" ValidationExpression="^[A-Za-z0-9]+[A-Za-z0-9][A-Za-z0-9]*$" ValidationGroup="SignUp"></asp:RegularExpressionValidator><br />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" SetFocusOnError="true" ControlToValidate="txtPasswordSignUp" ErrorMessage="Password must be atleast 6 characters!" ValidationExpression="^[\s\S]{6,}" ValidationGroup="SignUp"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <!-- End of Password Input Field -->

                    <!-- Confirm password field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE897;</i>
                        <asp:TextBox ID="txtSignUpRetypePassword" TextMode="Password" CssClass="large validate text-color" runat="server" onchange="ConfirmPasswordRetype();" MaxLength="20" autocomplete="off"></asp:TextBox>
                        
                        <label for="txtSignUpRetypePassword">
                            Confirm Password <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center red-text">
                            <span style="display: none;" class="spanConfirmPassword red-text">Password do not match!</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Password is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtSignUpRetypePassword" Text="Password is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" SetFocusOnError="true" ControlToValidate="txtSignUpRetypePassword" ErrorMessage="Password must be atleast 6 characters!" ValidationExpression="^[\s\S]{6,}" ValidationGroup="SignUp"></asp:RegularExpressionValidator><br />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" SetFocusOnError="true" ControlToValidate="txtSignUpRetypePassword" ErrorMessage="Password must be Alphanumeric!" ValidationExpression="^[A-Za-z0-9 _]+[A-Za-z0-9][A-Za-z0-9 _]*$" ValidationGroup="SignUp"></asp:RegularExpressionValidator>
                            
                        </div>
                    </div>
                    <!-- END of Confirm password field -->
                </div>
                <!-- END of Password Row -->

                <!-- Gender & Birthday Row -->
                <div class="row">
                    <!-- Birthday Date Picker -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE8DF;</i>
                        <asp:TextBox class="datepicker txtBirthday errorLabel" ID="txtBirthday" runat="server" MaxLength="1"></asp:TextBox>
                        <label for="txtBirthday">
                            Birthday <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Birthday is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtBirthday" Text="Birthday is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-- END of Birthday Date Picker -->


                    <div class="input-field col l6 s12">
                        <div class="col s12">
                            <label class="grey-text">Gender <span class="red-text">*</span></label>
                            <br />
                        </div>
                        <!-- Male Radio Button -->
                        <div class="col s3 offset-s2">
                            <asp:RadioButton ID="optMale" runat="server" groupname="gender"/>
                            <label for="optMale">Male</label>
                        </div>
                        <!-- END of Male Radio Button -->

                        <!-- Female Radio Button -->
                        <div class="col s6 offset-s1 left">
                            <asp:RadioButton ID="optFemale" runat="server" groupname="gender"/>
                            <label for="optFemale">Female</label>
                        </div>
                        <!-- END of Female Radio Button -->
                        <div class="col s12 center" style="visibility:hidden;">-----------</div>

                        <div class="col s12 center">
                            <asp:TextBox ID="txtGender" CssClass="large validate text-color" runat="server" MaxLength="1" Style="display:none;"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvGender" runat="server" ErrorMessage="Gender is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtGender" Text="Gender is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    
                </div>

                <!-- END of Gender & Birthday Row -->

                <!-- Contact Information -->
                <div class="row">
                    <!-- Mobile Number Input Field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE0DD;</i>
                        <asp:TextBox ID="txtMobileNumber" CssClass="large validate text-color" runat="server" MaxLength="30"></asp:TextBox>
                        <label for="txtMobileNumber">
                            Mobile Number <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Mobile Number is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtMobileNumber" Text="Mobile Number is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-- End of Password Input Field -->

                    <%-- Telephone Number Input Field --%>
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE0B0;</i>
                        <asp:TextBox ID="txtTelephoneNumber" CssClass="large validate text-color" runat="server" MaxLength="30"></asp:TextBox>
                        <label for="txtTelephoneNumber">
                            Telephone Number                   
                        </label>
                    </div>
                    <!-- END of Confirm password field -->
                </div>
                
                <br /><div class="divider"></div><br />

                <!-- Home address section -->
                <div class="row">
                    <div class="col s12">
                        <p style="font-size:20px;margin-bottom:-20px;margin-top:0px;">Origin Address</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col l4 s12 center">
                        <label>Province<span class="red-text">*</span></label>
                        <select Class="browser-default ddlOriginProvince" ID="ddlOriginProvince" runat="server"></select>
                        <asp:TextBox ID="txtOriginProvince" CssClass="large validate text-color" runat="server" MaxLength="1" Style="display:none;"></asp:TextBox>
                        <asp:HiddenField ID="hdftxtOriginProvince" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvOProvince" runat="server" ErrorMessage="Origin Province is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtOriginProvince" Text="Origin Province is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                    </div>

                    <div class="col l4 s12 center">
                        <label>City / Municipality<span class="red-text">*</span></label>
                        <%--<asp:DropDownList CssClass="browser-default ddlOriginCityMunicipality" ID="ddlOriginCityMunicipality" runat="server"></asp:DropDownList>--%>
                        <select Class="browser-default ddlOriginCityMunicipality" ID="ddlOriginCityMunicipality" runat="server"></select>
                        <asp:TextBox ID="txtOriginCityMunicipality" CssClass="large validate text-color" runat="server" MaxLength="1" Style="display:none;"></asp:TextBox>
                        <asp:HiddenField ID="hdfOriginCityMunicipality" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvOCity" runat="server" ErrorMessage="Origin City / Municipality is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtOriginCityMunicipality" Text="Origin City / Municipality is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                    </div>

                     <div class="input-field col l4 s12 center">
                          <input id="Ozipcode" type="text" runat="server" class="validate Ozipcode" maxlength="5" style="margin:0; margin-top:19px;">
                         <label class="active lblOzipcode" for="Ozipcode" style="margin:0; margin-top:19px;">Zip Code<span class="red-text spnOZipcode">*</span></label>  
                         <div class="active lblOzipcodeError red-text" for="Ozipcode" style="margin:0; margin-top:5px; display:none">Invalid Zipcode!</div>
                         <asp:RequiredFieldValidator ID="rfvOzipcode" runat="server" style="margin:0; margin-top:19px;" ErrorMessage="Zipcode is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="Ozipcode" Text="Zipcode is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                     </div>
                </div>
                
                <div class="row">
                    <div class="input-field col l8 s12">
                          <input id="Ostreet" type="text" runat="server" class="validate Ostreet" style="margin:0; text-transform: capitalize;" maxlength="150" autocomplete="off">
                          <label class="active" for="Ostreet" style="margin:0">Street</label>
                    </div>
                    <div class="input-field col l4 s12">
                       <input id="Obarangay" type="text" runat="server" class="validate Obarangay" style="margin:0;" maxlength="100" autocomplete="off">
                          <label class="active" for="Obarangay" style="margin:0;">Barangay</label>
                    </div>
                </div>
                <!-- END of Home address section -->

               

                 <!--  Work address section -->
                <div class="row">
                    <div class="col s12">
                        <p style="font-size:20px;">Destination Address</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col l4 s12 center">
                        <label>Destination Type <span class="red-text">*</span></label>
                        <%--<asp:DropDownList CssClass="browser-default ddlAddressType" ID="ddlAddressType" runat="server"></asp:DropDownList>--%>
                        <select Class="browser-default ddlAddressType" ID="ddlAddressType" runat="server"></select>
                        <asp:TextBox ID="txtDestinationType" CssClass="large validate text-color" runat="server" MaxLength="1" Style="display:none;"></asp:TextBox>
                        <asp:HiddenField ID="hdfDestinationType" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvDestinationType" runat="server" ErrorMessage="Destination type is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtDestinationType" Text="Destination type is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="row">
                    <div class="col l4 s12 center">
                        
                        <select id="Select1">
                            <option></option>
                        </select>

                        <label>Province<span class="red-text">*</span></label>
                        <%--<asp:DropDownList CssClass="browser-default ddlDestinationProvince" ID="ddlDestinationProvince" runat="server"></asp:DropDownList>--%>
                        <select Class="browser-default ddlDestinationProvince" ID="ddlDestinationProvince" runat="server"></select>
                        <asp:TextBox ID="txtDestinationProvince" CssClass="large validate text-color" runat="server" MaxLength="1" Style="display:none;"></asp:TextBox>
                        <asp:HiddenField ID="hdfDestinationProvince" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvDProvince" runat="server" ErrorMessage="Destination Province is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtDestinationProvince" Text="Destination Province is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                    </div>

                    <div class="col l4 s12 center">
                        <label>City / Municipality<span class="red-text">*</span></label>
                        <%--<asp:DropDownList CssClass="browser-default ddlDestinationCityMunicipality" ID="ddlDestinationCityMunicipality" runat="server"></asp:DropDownList>--%>
                        <select class="browser-default ddlDestinationCityMunicipality" ID="ddlDestinationCityMunicipality" runat="server"></select>
                        <asp:TextBox ID="txtDestinationCityMunicipality" CssClass="large validate text-color" runat="server" MaxLength="1" Style="display:none;"></asp:TextBox>
                        <asp:HiddenField ID="hdfDestinationCityMunicipality" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvDCity" runat="server" ErrorMessage="Destination City / Municipality is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtDestinationCityMunicipality" Text="Destination City / Municipality is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                    </div>

                     <div class="input-field col l4 s12 center">
                         <input id="Dzipcode" type="text" runat="server" class="validate Dzipcode" maxlength="5" style="margin:0; margin-top:19px;">
                         <label class="active lblDzipcode" for="Dzipcode" style="margin:0; margin-top:19px;">Zip Code<span class="red-text spnDZipcode">*</span></label>
                         <div class="active lblDzipcodeError red-text" for="Dzipcode" style="margin:0; margin-top:5px; display:none">Invalid Zipcode!</div>
                         <asp:RequiredFieldValidator ID="rfvDzipcode" runat="server" ErrorMessage="Zipcode is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="Dzipcode" Text="Zipcode is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col l8 s12">
                          <input id="Dstreet" type="text" runat="server" class="validate Dstreet" style="margin:0; text-transform: capitalize;" maxlength="150" autocomplete="off">
                          <label class="active" for="Dstreet" style="margin:0">Street</label>
                    </div>
                    <div class="input-field col l4 s12">
                       <input id="Dbarangay" type="text" runat="server" class="validate Dbarangay" style="margin:0; text-transform: capitalize;" maxlength="100">
                          <label class="active" for="Dbarangay" style="margin:0;">Barangay</label>
                    </div>
                </div>
                <!--  END of barangay section -->

                
                <div class="row">
                    <div class="col s12 center">
                        <a href="Index.aspx" class="btn waves-effect waves-light pink accent-2">Home</a>
                        <asp:LinkButton ID="lbtnSubmit" runat="server" class="btn btnSubmit blue darken-2 lbtnSubmit" ValidationGroup="SignUp" OnClick="lbtnSubmit_Click" >Sign Up</asp:LinkButton>
                    </div>
                </div>
                <div class="row center">
                    <a href="OperatorSignUp.Aspx">Switch to Operator Sign Up</a><br />
                    <a href="Login.aspx?UT=Member" class="pink-text"><u>LOGIN AS MEMBER</u></a>
                </div>


                <div class="divider pink accent-2" style="height: 5px"></div>
                <br />
                
            </div>
            <!-- END of card panel -->
        </div>
        <!-- END of container -->
    </form>

    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/script") %>
    </asp:PlaceHolder>

    <script src="Scripts/minified/loginsignup.min.js"></script>

    <script>
        var c = document.getElementById("<%=txtSignUpRetypeEmail.ClientID %>");
        c.select =
        function (event, ui)
        { this.value = ""; return false; }


        $(function()
        {
            $("#txtOriginProvince,#txtOriginCityMunicipality,#txtDestinationType,#txtDestinationProvince,#txtDestinationCityMunicipality,#txtGender").bind("contextmenu", function (e)
            {
                e.preventDefault();
            });
        });
        
        $(document).ready(function () {

            $('#lbtnSubmit').click(function () {
                var blnreturn = true;
                //spnDZipcode
                if ($(".spnOZipcode").css("visibility") == "hidden") {
                    blnreturn = false;
                }
                if ($(".spnDZipcode").css("visibility") == "hidden") {
                    blnreturn = false;
                }
                return blnreturn;
            });

            //Numbers Only (Mobile, Telephone) Validation
            $("#txtMobileNumber,#txtTelephoneNumber,.Dzipcode,.Ozipcode").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
            if (e.keyCode == 189 || e.keyCode == 109) {
                return;
            }    
            else if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                else {
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                }

            });
            //End Validation


            $("#txtOriginProvince,#txtOriginCityMunicipality,#txtDestinationType,#txtDestinationProvince,#txtDestinationCityMunicipality,#txtGender").keydown(function (e) {
                e.preventDefault();
                $(this).css("display", "none");
            });

            $('.ddlOriginProvince').change(function () {
                var SelectedText = $(this).find(":selected").text();
                $("#txtOriginProvince").val($(this).find(":selected").val());
                $("#hdfOriginProvince").val($(this).find(":selected").val());
                if ($("#txtOriginProvince").val() == "") {
                    $("#rfvOProvince").css("display", "block");
                }
                else { $("#rfvOProvince").css("display", "none"); }

                $.ajax({
                    type: "POST",
                    url: "MemberSignUp.aspx/GetOriginMunicipality",
                    data: "{'pOriginProvince':'" + SelectedText + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        try {
                            var res = response.d;
                            $(".def1,.D1,.SelectOriginProvince").remove();
                            $(".ddlOriginCityMunicipality").append($("<option selected='true' class='D1'></option>").val("1").html("Choose your City / Municipality"));
                            $('.ddlOriginCityMunicipality option:contains("Choose your City / Municipality")').attr("disabled", "disabled");
                            $.each(res, function (index, MemberOriginMunicipality) {
                                $(".ddlOriginCityMunicipality").append($("<option class='def1'></option>").val(MemberOriginMunicipality.OriginMunicipalityZipCode).html(MemberOriginMunicipality.OriginMunicipality));
                            });
                            $(".lblOzipcodeError").css("display", "none");
                            $(".Ozipcode").val("");
                            $(".lblOzipcode").removeClass("active");
                            $(".lblOzipcodeError").removeClass("active");
                            $(".Ozipcode").removeClass("validate");
                            $(".Ozipcode").removeClass("valid");
                        }
                        catch (e) {

                        }

                    }
                });
            });

            var strOrigin,strDestination;
            
            $('.ddlOriginCityMunicipality').change(function () {
                $("#txtOriginCityMunicipality").val($(this).find(":selected").val());
                
                if ($("#txtOriginCityMunicipality").val() == "") {
                    $("#rfvOCity").css("display", "block");
                }
                else { $("#rfvOCity").css("display", "none"); }

                var SelectedText = $(this).find(":selected").text();
                strOrigin = SelectedText;
                 $("input[id*='hdfOriginCityMunicipality']").val(SelectedText);
                $(".Ozipcode").val($(this).find(":selected").val());
                $(".Ozipcode").addClass("validate");
                $(".Ozipcode").addClass("valid");
                $(".lblOzipcodeError").css("display", "none");
                $(".Ozipcode").focus();
                $(".Ostreet").focus();
               
                
            });

            $('.Ozipcode').blur(function () {
                $.ajax({
                    type: "POST",
                    url: "MemberSignUp.aspx/GetDestinationMunicipalityViaZipcode",
                    data: "{'pZipCode':'" + $(".Ozipcode").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        try {
                            var res = response.d;
                            var zipcode = $(".Ozipcode").val();

                            if (res.length > 0) {
                                if ($("select[id$='ddlOriginProvince'] option:contains('" + res[0].DestinationProvince + "')").length > 0) {
                                    $(".lblOzipcodeError").css("display", "none");
                                    $(".ddlOriginProvince").val(res[0].DestinationProvince);
                                    $(".def1,.D1,.SelectOriginProvince").remove();
                                    $(".ddlOriginCityMunicipality").append($("<option selected='true' class='D1'></option>").val("1").html("Choose your City / Municipality"));
                                    $('.ddlOriginCityMunicipality option:contains("Choose your City / Municipality")').attr("disabled", "disabled");
                                    $.each(res, function (index, MemberDestinationMunicipality) {
                                        $(".ddlOriginCityMunicipality").append($("<option class='def1'></option>").val(MemberDestinationMunicipality.DestinationMunicipalityZipCode).html(MemberDestinationMunicipality.DestinationMunicipality));
                                    });



                                    
                                   if ($("#txtOriginCityMunicipality").val() == $(".Ozipcode").val()) {
                                    //$(".ddlOriginCityMunicipality").text("Bahay Toro");
                                       //document.getElementById("ddlOriginCityMunicipality").text = "Bahay Toro";
                                       var el = document.getElementById("ddlOriginCityMunicipality");
                                       for (var i = 0; i < el.options.length; i++) {
                                           if (el.options[i].text == strOrigin) {
                                               el.selectedIndex = i;
                                               break;
                                           }
                                       }
                                    }
                                    else {
                                       $(".ddlOriginCityMunicipality").val($(".Ozipcode").val());
                                   }

                                    $("#txtOriginCityMunicipality").val(zipcode);
                                    $("#txtOriginProvince").val(res[0].DestinationProvince);
                                    $("#rfvOzipcode").css("display", "none");
                                }

                            }
                            else {
                                $("#txtOriginCityMunicipality").val("");
                                $("#txtOriginProvince").val("");
                                $(".lblOzipcodeError").css("display", "block");
                                $(".ddlOriginProvince").val("");
                                $(".def1,.D1,.SelectOriginProvince").remove();
                                $(".ddlOriginCityMunicipality").append($("<option selected='true' class='D1'></option>").val("1").html("Choose your City / Municipality"));
                                $('.ddlOriginCityMunicipality option:contains("Choose your City / Municipality")').attr("disabled", "disabled");
                            }

                            if ($("#txtOriginProvince").val() == "") {
                                $("#rfvOProvince").css("display", "block");
                            }
                            else { $("#rfvOProvince").css("display", "none"); }


                            if ($("#txtOriginCityMunicipality").val() == "") {
                                $("#rfvOCity").css("display", "block");
                            }
                            else { $("#rfvOCity").css("display", "none"); }

                        }
                        catch (e) {

                        }

                    }
                });
            });

            $('.ddlAddressType').change(function () {
                var SelectedText = $(this).find(":selected").text();

                $("#txtDestinationType").val($(this).find(":selected").val());
                $("#hdfDestinationType").val($(this).find(":selected").val());
                if ($("#txtDestinationType").val() == "") {
                    $("#rfvDestinationType").css("display", "block");
                }
                else { $("#rfvDestinationType").css("display", "none"); }

            });

            $('.ddlDestinationProvince').change(function () {
                var SelectedText = $(this).find(":selected").text();

                $("#txtDestinationProvince").val($(this).find(":selected").val());
                $("#hdfDestinationProvince").val($(this).find(":selected").val());
                if ($("#txtDestinationProvince").val() == "") {
                    $("#rfvDProvince").css("display", "block");
                }
                else { $("#rfvDProvince").css("display", "none"); }

                $.ajax({
                    type: "POST",
                    url: "MemberSignUp.aspx/GetOriginMunicipality",
                    data: "{'pOriginProvince':'" + SelectedText + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        try {
                            var res = response.d;
                            $(".def11,.D11,.SelectDestinationProvince").remove();
                            $(".ddlDestinationCityMunicipality").append($("<option selected='true' class='D11'></option>").val("1").html("Choose your City / Municipality"));
                            $('.ddlDestinationCityMunicipality option:contains("Choose your City / Municipality")').attr("disabled", "disabled");
                            $.each(res, function (index, MemberOriginMunicipality) {
                                $(".ddlDestinationCityMunicipality").append($("<option class='def11'></option>").val(MemberOriginMunicipality.OriginMunicipalityZipCode).html(MemberOriginMunicipality.OriginMunicipality));
                            });
                            $(".lblDzipcodeError").css("display", "none");
                            $(".Dzipcode").val("");
                            $(".lblDzipcode").removeClass("active");
                            $(".lblDzipcodeError").removeClass("active");
                            $(".Dzipcode").removeClass("validate");
                            $(".Dzipcode").removeClass("valid");
                        }
                        catch (e) {

                        }

                    }
                });
            });

            $('.ddlDestinationCityMunicipality').change(function () {
                $("#txtDestinationCityMunicipality").val($(this).find(":selected").val());
                
                if ($("#txtDestinationCityMunicipality").val() == "") {
                    $("#rfvDCity").css("display", "block");
                }
                else { $("#rfvDCity").css("display", "none"); }

                var SelectedText = $(this).find(":selected").text();
                strDestination = SelectedText;
                $("#hdfDestinationCityMunicipality").val(SelectedText);
                $(".Dzipcode").val($(this).find(":selected").val());
                $(".Dzipcode").addClass("validate");
                $(".Dzipcode").addClass("valid");
                $(".lblDzipcodeError").css("display", "none");
                $(".Dzipcode").focus();
                $(".Dstreet").focus();
            });

            $('.Dzipcode').blur(function () {
                $.ajax({
                    type: "POST",
                    url: "MemberSignUp.aspx/GetDestinationMunicipalityViaZipcode",
                    data: "{'pZipCode':'" + $(".Dzipcode").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        try {
                            var res = response.d;
                            var zipcode = $(".Dzipcode").val();

                            if (res.length > 0) {
                                if ($("select[id$='ddlDestinationProvince'] option:contains('" + res[0].DestinationProvince + "')").length > 0) {
                                    $(".lblDzipcodeError").css("display", "none");
                                    $(".ddlDestinationProvince").val(res[0].DestinationProvince);
                                    $(".def11,.D11,.SelectDestinationProvince").remove();
                                    $(".ddlDestinationCityMunicipality").append($("<option selected='true' class='D11'></option>").val("1").html("Choose your City / Municipality"));
                                    $('.ddlDestinationCityMunicipality option:contains("Choose your City / Municipality")').attr("disabled", "disabled");
                                    $.each(res, function (index, MemberDestinationMunicipality) {
                                        $(".ddlDestinationCityMunicipality").append($("<option class='def11'></option>").val(MemberDestinationMunicipality.DestinationMunicipalityZipCode).html(MemberDestinationMunicipality.DestinationMunicipality));
                                    });
                                    $(".ddlDestinationCityMunicipality").val(zipcode);
                                    
                                    $("#txtDestinationCityMunicipality").val(zipcode);
                                    $("#txtDestinationProvince").val(res[0].DestinationProvince);
                                    $("#rfvDzipcode").css("display", "none");



                                    $("#txtOriginCityMunicipality").val(zipcode);
                                    $("#txtOriginProvince").val(res[0].DestinationProvince);
                                    $("#rfvOzipcode").css("display", "none");



                                    
                                    if ($("#txtDestinationCityMunicipality").val() == $(".Dzipcode").val()) {
                                        //$(".ddlOriginCityMunicipality").text("Bahay Toro");
                                        //document.getElementById("ddlOriginCityMunicipality").text = "Bahay Toro";
                                        var el = document.getElementById("ddlDestinationCityMunicipality");
                                        for (var i = 0; i < el.options.length; i++) {
                                            if (el.options[i].text == strDestination) {
                                                el.selectedIndex = i;
                                                break;
                                            }
                                        }
                                    }
                                    else {
                                        $(".ddlOriginCityMunicipality").val($(".Ozipcode").val());
                                    }

                                

                                }
                            }
                            else {
                                $("#txtDestinationCityMunicipality").val("");
                                $("#txtDestinationProvince").val("");
                                $(".lblDzipcodeError").css("display", "block");
                                $(".ddlDestinationProvince").val("");
                                $(".def11,.D11,.SelectDestinationProvince").remove();
                                $(".ddlDestinationCityMunicipality").append($("<option selected='true' class='D11'></option>").val("1").html("Choose your City / Municipality"));
                                $('.ddlDestinationCityMunicipality option:contains("Choose your City / Municipality")').attr("disabled", "disabled");
                            }
                            if ($("#txtDestinationProvince").val() == "") {
                                $("#rfvDProvince").css("display", "block");
                            }
                            else { $("#rfvDProvince").css("display", "none"); }


                            if ($("#txtDestinationCityMunicipality").val() == "") {
                                $("#rfvDCity").css("display", "block");
                            }
                            else { $("#rfvDCity").css("display", "none"); }

                        }
                        catch (e) {

                        }

                    }
                });
            });
        });
    </script>

</body>
</html>
