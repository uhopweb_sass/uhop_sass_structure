﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnderConstruction.aspx.cs" Inherits="uhopWeb.UnderConstruction" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" id="underCPage">
<head runat="server">
    <link rel="shortcut icon" href="Images/uhop_icon_beta.png" type="image/png" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />    
    <link href="Content/materialize/css/materialize.min.css" rel="stylesheet" />
    <link href="Content/wow/animate.min.css" rel="stylesheet" />
    <link href="Styles/global.min.css" rel="stylesheet" />

    <title>u-Hop Transport Network Vehicle System Inc.</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <!-- Dropdown Structure -->
            <!-- Booking main nav dropdowns -->
            <ul id="profile" class="dropdown-content profile" runat="server">
                <li><a href="#">Member page</a></li>                
                <li class="divider"></li>
                <li><a href="LogOut.aspx" id="">Logout</a></li>
            </ul>

            <ul id="booking" class="dropdown-content">
                <li><a href="#" onclick="#!">Future Booking</a></li>
                <li class="divider"></li>
                <li><a href="#" onclick="#!">Book Now</a></li>
            </ul>

            <ul id="history" class="dropdown-content">
                <li><a href="#" onclick="#!">Fulfilled Trips</a></li>
                <li class="divider"></li>
                <li><a href="#" onclick="#!">Subscriptions</a></li>
            </ul>
            <!-- End of booking nav dropdowns -->
            
            <!-- Booking side nav -->        
            <ul id="bookingSideNav" class="dropdown-content">
                <li class="sideNavDropDown"><a href="#" onclick="#!">Future Booking</a></li>
                <li class="divider"></li>
                <li class="sideNavDropDown"><a href="#" onclick="#!">Book Now</a></li>
            </ul>       
            
            <ul id="historySideNav" class="dropdown-content">
                <li class="sideNavDropDown"><a href="#" onclick="#!">Fulfilled Trips</a></li>
                <li class="divider"></li>
                <li class="sideNavDropDown"><a href="#" onclick="#!">Subscriptions</a></li>
            </ul>

            <ul id="profileSideNav" class="dropdown-content profile" runat="server">
                <li class="sideNavDropDown"><a href="#">Member page </a></li>                
                <li class="divider"></li>
                <li class="sideNavDropDown"><a href="LogOut.aspx" id="A1">Logout</a></li>
            </ul>
            <!-- End of booking side nav --> 

            <div class="navbar-fixed">
                <nav class="grey darken-3" role="navigation">
                    <div class="nav-wrapper">
                        <ul class="right hide-on-med-and-down">
                            <li><a href="#">PAYMENT</a></li> 
                            <li><a href="#" class="memberNav">SUBSCRIPTION</a></li>
                            <!-- Booking Trigger -->
                            <li><a class="dropdown-button memberNav" href="#!" data-activates="booking">BOOKING<i class="material-icons right">arrow_drop_down</i>&nbsp;&nbsp;&nbsp;&nbsp;</a></li>  
                            <!-- End of Booking trigger -->
                            <!-- Booking History Trigger -->           
                            <li><a class="dropdown-button memberNav" href="#!" data-activates="history">HISTORY<i class="material-icons right">arrow_drop_down</i>&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                            <!-- End of Booking history trigger -->
                            <li><a href="#locate" class="memberNav">HELP</a></li>
                            <!-- Profile Trigger -->
                            <li><a class="dropdown-button memberNav" href="#!" data-activates="profile"><i class="material-icons left">perm_identity</i>
                                <asp:Label ID="lblNavBarName" CssClass="" runat="server" ></asp:Label><i class="material-icons right">arrow_drop_down</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                            <!-- End of Profile trigger -->
                        </ul>

                        <ul id="slide-out" class="side-nav">
                           <li id="menuImg" onclick="CloseSideNav()"><p>&nbsp;</p></li>                       
                            <li><a href="#"><strong>PAYMENT</strong></a></li>
                            <li><a href="#"><strong>SUBSCRIPTION</strong></a></li>
                            <li><a class="dropdown-button" href="#!" data-activates="bookingSideNav"><strong>BOOKING</strong><i class="material-icons right">arrow_drop_down</i>&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                            <li><a class="dropdown-button" href="#!" data-activates="historySideNav"><strong>HISTORY</strong><i class="material-icons right">arrow_drop_down</i>&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                            <li><a href="#locate"><strong>HELP</strong></a></li>
                            <li><a class="dropdown-button memberNav" href="#!" data-activates="profileSideNav"><i class="material-icons left">perm_identity</i>
                                <asp:Label ID="lblNavBarName2" runat="server" ></asp:Label><i class="material-icons right">arrow_drop_down</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                        </ul>
                        <a href="#" data-activates="slide-out" class="button-collapse show-on-large" id="menu">&nbsp;</a>                        

                    </div>
                </nav>
            </div>
            <!-- END OF NAVBAR -->
        <br>
        <center>
            <img style="margin-top:50px;" class="responsive-img wow pulse animated center-align noMargin" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="3s" src="Images/u-hop_logo.png" alt="Alternate Text" />
        </center>
        <img src="Images/finallast.png" class="responsive-img"/>
        <div class="center" style="margin-top:-145px;">
            <h3>This page is currently under construction.</h3>
            <h4>We're sorry for the inconvenience.</h4>

            <a  class="waves-effect waves-light btn pink accent-2" href="LogOut.aspx"><i class="material-icons"></i>Log out and back to site</a>
        </div>
    </div>
    </form>

    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/script") %>
    </asp:PlaceHolder>

    <script>
        /* WOW.js Initialization */
        new WOW().init();
        /* END of WOW.js initialization */

        /* Side Nav Toggle */
        $('.button-collapse').sideNav();
        /* END of Side Nav Toggle */
    </script>
</body>
</html>
