﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="uhopWeb.Index" %>

<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="shortcut icon" href="Images/uhop_icon_beta-min.png" type="image/png"/>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320"/> 
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

    <asp:PlaceHolder ID="PlaceHolder2" runat="server">
        <%: Styles.Render("~/bundle/style") %>
    </asp:PlaceHolder>
    <link href="Styles/index.min.css" rel="stylesheet" />
    <!--<link href="Styles/ninja-slider.css" rel="stylesheet" type="text/css" />-->

    <title>u-Hop Transport Network Vehicle System Inc.</title>
</head>

<body id="homeBg">
     <!-- WEBSITE PRELOADER --> 
    <div id="load" class="responsive-img" style="background-image:url('../Images/<% PreLoaderImage(); %>')">
        <div class="container center-align">
            <div class="col s12">
                <br /><br /><br />
                <p><img src="Images/u-hop_logo-min.png" class="responsive-img"/></p>
                <div class="preloader-wrapper big active">
                  <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                      <div class="circle"></div>
                    </div><div class="gap-patch">
                      <div class="circle"></div>
                    </div><div class="circle-clipper right">
                      <div class="circle"></div>
                    </div>
                  </div>

                  <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                      <div class="circle"></div>
                    </div><div class="gap-patch">
                      <div class="circle"></div>
                    </div><div class="circle-clipper right">
                      <div class="circle"></div>
                    </div>
                  </div>

                  <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                      <div class="circle"></div>
                    </div><div class="gap-patch">
                      <div class="circle"></div>
                    </div><div class="circle-clipper right">
                      <div class="circle"></div>
                    </div>
                  </div>

                  <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                      <div class="circle"></div>
                    </div><div class="gap-patch">
                      <div class="circle"></div>
                    </div><div class="circle-clipper right">
                      <div class="circle"></div>
                    </div>
                  </div>
                </div>
                <p class="white-text">Please wait while the u-Hop website loads...</p>
            </div><!--  END OF COL -->
        </div> <!-- END of container -->
    </div>
    <!-- END OF WEBSITE LOADER -->

    
    <div id="divForm">
    <form id="form1" runat="server">
        <div>
<!--============================================================================= MODALS ===============================================================================================-->
            <!-- Modal Structure for Shuttle Service Read More -->
              <div id="modalShuttle" class="modal modalShuttle">
                <div class="modal-content" style="padding-bottom:0px;">
                    <div class="row center">
                        <h4><img src="Images/u-hop_logo-min.png" alt="u-Hop" width="350" height="90" class="responsive-img"/></h4>
                        <p class="center-align" style="margin-top:-20px;">"Because everyone deserves better"</p>
                    </div>
                    <div class="row">
                        <div class="col s12 center">
                            <h5>Already a <span class="blue-text">Member</span>/<span class="pink-text">Partner</span>?</h5>
                        </div>
                        <div class="col s12 center">
                            <a href="Login.aspx?UT=Member" class="btn pink accent-2" style="margin-bottom:5px;">Log in as Member</a> &nbsp;
                            <a href="Login.aspx?UT=Operator" class="btn black" style="margin-bottom:5px;">Log in as Partner</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 center">
                            <h5>New to <span class="blue-text">u</span>-<span class="pink-text">Hop</span>?</h5>
                        </div>
                        <div class="col s12 center">
                            <a href="MemberSignUp.aspx" class="btn pink accent-2" style="margin-bottom:5px;">Sign up as Member</a> &nbsp;
                            <a href="OperatorSignUp.aspx" class="btn black" style="margin-bottom:5px;">Sign up as Partner</a>
                        </div>
                    </div>
                </div> <!-- END of modal content-->
                <div class="modal-footer">
                    <a onclick="continueScroll()" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat dismiss" id="A1" style="padding:10px !important;">Dismiss</a>
                </div>
              </div>

            <!-- Modal Structure for About Our Team Read More -->
            <div id="readAbout" class="modal modal-fixed-footer firstModal">
                <div class="modal-content">
                    <h4>OUR TEAM</h4>
                    <blockquote>
                        <p>U-HOP prides itself on having experienced and professional individuals. Each of us represent the spirit and values of our company: Honor, Courage, Commitment, and bravery. The team is acutely aware of U-HOP’s limitless potential to grow.</p>
                        <p>Our competitiveness allows us to deliver the most reasonable rates without having the quality of service to be provided at stake. </p>
                    </blockquote>
                    <h4>OUR MISSION & VISION</h4>
                    <blockquote>
                        <p><i>To provide alternative solutions to daily transportation service across the globe!</i></p>
                        <p>U-HOP aims to resolve the current struggle in commuting by providing a member-based interconnected shuttle service globally. U-HOP aims to lessen the traffic volume by utilizing shuttle vans thus limiting private vehicles on the road.</p>
                        <p>U-HOP aims to offer a better interface using the latest technologies over the internet. U-HOP aims to expand its business over wider areas by joining hands with our domestic clients. U-HOP has its legacy of working hand-in-hand with our clients at each phase of the Application Development process to blend the interests of the client and our technical expertise to deliver the best and quality results and services.</p>
                    </blockquote>
                </div>
                <div class="modal-footer">
                    <a onclick="continueScroll()" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat dismiss" id="dismiss1">Dismiss</a>
                </div>
            </div>

            <!-- Modal Structure for about Our Members Read More -->
            <div id="readEnrollment" class="modal modal-fixed-footer secondModal">
                <div class="modal-content">
                    <h4>MEMBER BENEFITS AND PRIVILEGES</h4>
                    <blockquote>
                        <p><b>SAFETY</b> - All members are subjected to verification and check. Every member will have peace of mind that u-Hop passengers are legitimate office workers or students.</p>
                        <p><b>SAVINGS</b> - Members will enjoy huge savings by availing the unlimited pass. members can hop on and hop off transfer to any u-Hop shuttles without additional charge. Our rate is 70% less than those who offers private car or deluxe taxi.</p>
                        <p><b>SUPPORT</b> - u-Hop Command Center is open 24/7 to provide emergency service such as Police, Fire, Medical and Road Side Assistance. Our Staff can even give you directions and status of the shuttles by calling our hotline numbers.</p>
                        <p><b>CONVENIENCE</b> - You never have to worry how or when are you able to get to your office, school or back home. With u-Hop you can choose the time you want to be pick up either in your own driveway, school or office.</p>
                        <p><b>COMFORT</b> - Our members will enjoy 1 to 1 seating allocation. No more pushing, squeezing and fighting for transport. </p>
                        <p><b>CONTRIBUTION</b> - By using u-Hop, Members are maximizing the whole seats. Unlike others, they got themselves the entire vehicle thus made the traffic much worst.</p>
                        <p><b>TECHNOLOGY</b> - Members will  be able to track the shuttle real time. No more waiting on the street specially during rainy days. </p>
                        <p><b>FAST</b> - u-Hop will issue a permanent RFID Tags to each member. The tag will be read by the shuttle receiver to validate the member status. Member can recharge this tags online. No more waiting on the line.</p>
                        <p><b>PRESENTATION</b> - All u-Hop drivers are uniformed and well groomed. The shuttles are less than 5 year old.</p>
                        <p><b>GIVING BACK</b> - u-Hop utilizes thousands of mini-vans, SUVs and mini-bus operators. The members create great opportunities for operators and drivers as well.</p>

                    </blockquote>
                </div>
                <div class="modal-footer">
                    <a onclick="continueScroll()" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat dismiss" id="dismiss2">Dismiss</a>
                </div>
            </div>

            <!-- Modal Structure for Subscription Read More -->
            <div id="readJoin" class="modal modal-fixed-footer thirdModal">
                <div class="modal-content">
                    <h4>OUR SERVICES</h4>
                    <blockquote>
                        <p><b>Members can avail the following Services: </b></p>
                        <ul>
                            <li>Round Trip - P693.00 Weekly</li>
                            <li>Round Trip - P2,970.00 Monthly</li>
                            <li>Unli Ride - P1,400 Weekly</li>
                            <li>Unli Ride - P6,000 Monthly</li>
                        </ul>

                        <p><b>And choose the following service areas:</b></p>
                        <ul>
                            <li>Metro Manila</li>
                            <li>Bulacan</li>
                            <li>Rizal</li>
                            <li>Laguna</li>
                            <li>Metro Cebu</li>
                        </ul>
                        <p>
                            <i>Avail unlimited rides daily! Membership starts at <s class="red-text">P199</s> P99/day only!<br />
                            More service areas to open monthly!</i>
                        </p>

                    </blockquote>
                </div>
                <div class="modal-footer">
                    <a onclick="continueScroll()" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat dismiss" id="dismiss3">Dismiss</a>
                </div>
            </div>
<!--======================================================================== END of MODALS ===============================================================================================-->

            <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" ScriptMode="Release" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
            <!-- Download App Cover -->
            <div class="divCover AppDL" style="display: none;">
                 <!--Start of application Download background container-->

                <div class="divAppDLBackGround">
                    <a class="CloseDLApp" onclick="CloseDLApp();" href="#">&nbsp;</a>
                    <!--<h1 class="wow fadeIn noMargin" id="dlHeader">Download our app.</h1>-->
                    <div id="dlImages">
                        <span id="SamsungImage"><a runat="server" onserverclick='btnDL_Click' class="DllLinkAndroid wow pulse">
                            <img src="Images/index/gplay-min.png" id="imgLinkGplay"/></a></span>
                        <span id="IphoneImage"><a href="#" class="DlLikAppStore wow pulse">
                            <img src="Images/index/ios-min.png" id="imgLinkIos"/></a></span>
                    </div>
                </div>
                <!--End of application Download background container-->
            </div>
            <!-- END of Download App Cover-->

            <!-- Dropdown Structure for navbar -->
            <ul id="services" class="dropdown-content">
                <li class="sideNavDropDown"><a href="#">Shuttle</a></li>
                <li class="sideNavDropDown"><a href="Luxury.Aspx" class="white-text">Luxury</a></li>
                <%--<li class="sideNavDropDown"><a href="#" class="black-text">Cars</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Rental</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Tram</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Diner</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Hotel</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Beauty & Wellness</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Express</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Plane</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Medicine</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Money</a></li>--%>
            </ul>

            <ul id="member" class="dropdown-content">
                <li class="sideNavDropDown"><a href="Login.aspx?UT=Member" class="white-text">As Member?</a></li>
                <li class="sideNdavDropDown"><a href="Login.aspx?UT=Operator" class="white-text">As Operator?</a></li>
            </ul>

            <ul id="operator" class="dropdown-content">
                <li class="sideNavDropDown"><a href="MemberSignUp.aspx" class="white-text">As Member?</a></li>
                <li class="sideNavDropDown"><a href="OperatorSignUp.aspx" class="white-text">As Operator?</a></li>
            </ul>
            <!-- END of Dropdown Structure for navbar -->

            <!-- Dropdown Structure for sideNav -->
            <ul id="servicesSN" class="dropdown-content">
                <li class="sideNavDropDown"><a href="#" class="white-text">Shuttle</a></li>
                <li class="sideNavDropDown"><a href="Luxury.Aspx" class="white-text">Luxury</a></li>
                <%--<li class="sideNavDropDown"><a href="#" class="black-text">Cars</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Rental</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Tram</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Diner</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Hotel</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Beauty & Wellness</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Express</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Plane</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Medicine</a></li>
                <li class="sideNavDropDown"><a href="#" class="black-text">Money</a></li>--%>
            </ul>

            <ul id="memberSN" class="dropdown-content">
                <li class="sideNavDropDown"><a href="Login.aspx?UT=Member" class="white-text">As Member?</a></li>
                <li class="sideNavDropDown"><a href="Login.aspx?UT=Operator" class="white-text">As Operator?</a></li>
            </ul>

            <ul id="operatorSN" class="dropdown-content">
                <li class="sideNavDropDown"><a href="MemberSignUp.aspx" class="white-text">As Member?</a></li>

                <li class="sideNavDropDown"><a href="OperatorSignUp.aspx" class="white-text">As Operator?</a></li>
            </ul>
            <!-- END of Dropdown Structure for sideNav -->

            <div class="navbar-fixed">
                <nav class="black" role="navigation" id="indexNav" style="top:0;left:0;">
                    <div class="nav-wrapper">
                        <ul class="right hide-on-med-and-down">
                            <li class="currentActive"><a href="#juan" class="navText">Home</a></li>
                            <li><a class="dropdown-button navText" href="#!" data-activates="services">Our Services&ensp;<i class="material-icons right">&#xE5C5;</i></a></li>
                            <li class="currentActive"><a href="#aboutUs" class="navText">About Us</a></li>
                            <li class="currentActive"><a href="#contactUs" class="navText">Contact Us</a></li>
                            <li class="currentActive"><a href="#history" class="navText" >How we started</a></li>
                            
                            <!--<li class="currentActive">
                                <div id="divLuxury" runat="server" class="wow flash animated" data-wow-iteration="infinite" data-wow-duration="10s">
                                    <a href="#" style="color:#FFD700 !important;" class="aLuxury wow pulse animated" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="3s">LUXURY</a>
                                </div>
                            </li>-->

                            <!-- Dropdown Trigger -->
                            <li><a class="dropdown-button navText" href="#!" data-activates="member">Log in &ensp;<i class="material-icons right">&#xE5C5;</i></a></li>
                            <li><a class="dropdown-button navText" href="#!" data-activates="operator">Sign up &ensp;<i class="material-icons right">&#xE5C5;</i></a></li>
                        </ul>

                        <ul class="left hide-on-med-and-down show-on-large">
                            <li><a href="#juan"><img class="responsive-img left uhopLogo" src="Images/u-hop_logo-min.png" alt="Alternate Text" width="250" height="200"/></a></li>
                        </ul>

                        <ul id="slide-out" class="side-nav">
                            <li id="menuImg" onclick="CloseSideNav()">
                                <p>&nbsp;</p>
                            </li>
                            <li class="currentActive"><a href="#juan" class="active">Home</a></li>
                            <li><a class="dropdown-button" href="#!" data-activates="memberSN">Log in &ensp;<i class="material-icons right">&#xE5C5;</i></a></li>
                            <li><a class="dropdown-button" href="#!" data-activates="operatorSN">Sign up &ensp;<i class="material-icons right">&#xE5C5;</i></a></li>
                            <li><a class="dropdown-button" href="#!" data-activates="servicesSN">Our Services &ensp;<i class="material-icons right">&#xE5C5;</i></a></li>
                            <li class="currentActive"><a href="#aboutUs">About Us</a></li>
                            <li class="currentActive"><a href="#contactUs">Contact Us</a></li>
                            <li class="currentActive"><a href="#history">How we started</a></li>
                            <!-- Dropdown Trigger -->
                        </ul>
                        <a href="#" data-activates="slide-out" class="button-collapse hide-on-large-only show-on-medium-and-down" id="menu">&nbsp;</a>

                    </div>
                </nav>
            </div>
            <!-- END OF NAVBAR -->
                
            <!-- FIRST HEADER -->
            <div id="index-banner" class="parallax-container">
                <div class="section no-pad-bot">
                    <div class="container wow bounceInUp scrollspy" id="juan">
                        <div class="row center">
                            <img class="responsive-img wow pulse animated" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="3s" src="Images/u-hop_logo-min.png" alt="Alternate Text" align="center" />
                            <h5 class="header col s12 light" id="H1">"Because everyone deserves better"</h5>
                        </div>
                        <div class="row center quickLoadButtons">
                            <a class="buttonTextSize tooltipped waves-effect waves-light pink accent-2 btn-large z-depth-5" data-position="bottom" data-delay="10" data-tooltip="Ride now!" href="MemberSignUp.aspx">Become a member</a>
                            <a class="buttonTextSize textSizeButtons tooltipped waves-effect waves-dark black btn-large z-depth-5" id="op" data-position="bottom" data-delay="10" data-tooltip="Earn up to 35k monthly!" href="OperatorSignUp.aspx">Become a partner</a>
                            <a class="buttonTextSize tooltipped waves-effect waves-light blue darken-2 btn btn-large z-depth-5" data-position="bottom" data-delay="10" data-tooltip="Its free, and will always be!" onclick="ShowDLApp();"><i class="material-icons right" style="margin:0;">&#xE884;</i>Download App! &nbsp;</a>
                        </div>
                    </div>
                </div>
                <div class="parallax" id="parallax1">
                    <img src="Images/index/u-hop_streetdim-min.jpg" alt="Unsplashed background img 1" />
                </div>
            </div>
            <!-- END OF FIRST HEADER -->


            <!-- SECOND SECTION -->
            <div class="container scrollspy white-text">
                <div class="section">
                    <div class="row">
                        <div class="col s12 center">
                            <h4 class="wow bounceInDown  wow bounce">Our Services</h4>   
                            <div class="divider"></div>
                            <div class="row"></div>                         
                            <!-- FIRST ROW -->
                            <div class="row center">
                                <div class="col s6 m4 l2 wow bounceInDown">
                                    <div class="icon-block hoverable center white-text">
                                        <a href="#modalShuttle" class="modal-trigger" onclick="stopScroll()"><img src="Images/uhop_services/shuttle-min.png" class="responsive-img" height="100" width="100"/></a>
                                        <h6>Shuttle</h6>
                                    </div>
                                </div>
                                <div class="col s6 m4 l2 wow bounceInDown">
                                    <div class="icon-block hoverable center white-text">
                                        <a href="Luxury.aspx?LT=Plane"><img src="Images/uhop_services/fly-min.png" class="responsive-img" height="100" width="100" /></a>
                                        <h6>u-Fly</h6>
                                    </div>
                                </div>
                                <div class="col s6 m4 l2 wow bounceInDown">
                                    <div class="icon-block hoverable center white-text">
                                        <a href="Luxury.aspx?LT=Chopper"><img src="Images/uhop_services/glide-min.png" class="responsive-img" height="100" width="100" /></a>
                                        <h6>u-Glide</h6>
                                    </div>
                                </div>
                                <div class="col s6 m4 l2 wow bounceInDown">
                                    <div class="icon-block hoverable center white-text">
                                        <a href="Luxury.aspx?LT=Yacht"><img src="Images/uhop_services/cruise-min.png" class="responsive-img" height="100" width="100" /></a>
                                        <h6>u-Cruise</h6>
                                    </div>
                                </div>
                                <div class="col s6 m4 l2 wow bounceInDown">
                                    <div class="icon-block hoverable center white-text">
                                        <a href="Luxury.aspx?LT=Jetski"><img src="Images/uhop_services/speed-min.png" class="responsive-img" height="100" width="100" /></a>
                                        <h6>u-Speed</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="divider"></div>
                            <div class="row"></div>
                        </div>
                     </div><!-- END of MAIN row -->
                     <br />
               </div> <!-- END of  SECTION -->
             </div><!--EMD of CONTAINER -->
 
    

        <!-- THIRD PARALLAX BG -->
        <div class="parallax-container valign-wrapper">
            <div class="section no-pad-bot">
                <div class="container">
                    <div class="row center">
                        <h5 class="header col s12 light wow swing" style="color:#e3f2fd;">We are starting the following loop service areas every 30 minutes. </h5>
                    </div>
                </div>
            </div>
            <div class="parallax">
                <img src="Images/index/riyadh-min.jpg" alt="Unsplashed background img 3" />
            </div>
        </div>
        <!-- END OF THIRD PARALLAX BG -->

            <!-- FIRST SECTION -->
            <div class="container scrollspy" id="aboutUs">
                <div class="section">
                    <h4 class="center wow bounce white-text">ABOUT US</h4>
                    <div class="row">
                        <div class="col m8 offset-m2 s12 wow fadeIn" data-wow-delay=".5s">
                            <img src="Images/index/Team_u-Hop-min.jpg" class="responsive-img materialboxed" data-caption="Team u-Hop" style="border-radius: 20%;" height="700" width="1400"/>
                        </div>
                    </div>
                    <!--   Icon Section   -->
                    <div class="row" style="margin-bottom:0px;">
                                
                        <!-- First Box -->
                        <div class="col s12 m6 l4  wow bounceInDown">
                            <div class="icon-block hoverable center white-text">
                                <h2 class="center"><i class="material-icons" style="color: #ff9800;">&#xE7EF;</i></h2>
                                <h5 class="center">About our team</h5>
                                <p class="light center">Living out the principles and values of the company, U-HOP team work together to provide exemplary services to its partners and clients.<br /><br /></p>
                                <!-- Modal Trigger -->
                                <a onclick="stopScroll()" class="rMore btn waves-effect waves-light modal-trigger pink accent-2 modals show-on-medium-and-down hide-on-med-and-up" href="#readAbout">Read more...</a>
                            </div>
                        </div>
                        <!-- End of First Box -->

                        <!-- Second Box -->
                        <div class="col s12 m6 l4 wow bounceInDown">
                            <div class="icon-block hoverable center white-text">
                                <h2 class="center" style="z-index:0;"><i class="material-icons" style="color: #ff9800;">&#xE8E8;</i></h2>
                                <h5 class="center">Our members</h5>
                                <p class="light center">Members and clients are guaranteed with benefits and privileges that maintain a pleasant yet professional relationship with the company.<br /><br /></p>                                     
                                <!-- Modal Trigger -->
                                <a onclick="stopScroll()" class="rMore waves-effect waves-light btn modal-trigger pink accent-2 modals show-on-medium-and-down hide-on-med-and-up" href="#readEnrollment">Read more ...</a>
                            </div>
                        </div>
                        <!-- End of Second Box -->

                        <!-- Third Box -->
                        <div class="col s12 m6 l4  wow bounceInDown">
                            <div class="icon-block hoverable center white-text">
                                <h2 class="center brown-text"><i class="material-icons" style="color: #ff9800;z-index:auto;">&#xE531;</i></h2>
                                <h5 class="center">Subscription</h5>
                                <p class="light center">No more waiting, wasting gas and maintenance issues. Your income is certain with us. You will have daily regular trips and fixed daily, weekly or monthly income. Because we are better. Join us now!</p>                               
                                <!-- Modal Trigger -->
                                <a onclick="stopScroll()" class="rMore waves-effect waves-light btn modal-trigger pink accent-2 modals show-on-medium-and-down hide-on-med-and-up" href="#readJoin">Read more ...</a>
                            </div>                            
                        </div>
                        <!-- End of Third Box -->
                    </div>
                    <!-- END OF ROW -->

                    <!-- Read More on desktop -->
                    <div class="row center">
                        <div class="col s12 l4 wow bounceInDown">
                            <!-- Modal Trigger -->
                                <a onclick="stopScroll()" class="rMore waves-effect waves-light btn modal-trigger pink accent-2 modals hide-on-med-and-down" href="#readAbout">Read more ...</a>
                        </div>
                        <div class="col s12 l4 wow bounceInDown">
                            <!-- Modal Trigger -->
                                <a onclick="stopScroll()" class="rMore waves-effect waves-light btn modal-trigger pink accent-2 modals hide-on-med-and-down" href="#readEnrollment">Read more...</a>
                        </div>

                        <div class="col s12 l4 wow bounceInDown">
                            <!-- Modal Trigger -->
                                <a onclick="stopScroll()" class="rMore waves-effect waves-light btn modal-trigger pink accent-2 modals hide-on-med-and-down" href="#readJoin">Read more...</a>
                        </div>
                    </div>
                    <!-- END of Read More on desktop -->

                    <%--<div class="slider">
                        <ul class="slides">
                            <% PopulateIndexAds(); %>
                        </ul>
                    </div>--%>

                    <br /><br />

                    <%--<h4 class="white-text  wow bounce">Interactive Videos</h4>
                    <div class="row">
                        <div class="col l4 s12 wow fadeIn">
                            <!--<video src="Videos/u-Hop_Members.mp4" controls="controls" class="responsive-video wow fadeIn" data-wow-delay=".5s" />-->
                            <div class="video-container no-controls">
                              <iframe width="853" height="480" src="//www.youtube.com/embed/57LfNN5TZvg?rel=0;autohide=1" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col l4 s12 wow fadeIn" data-wow-delay=".1s">
                            <!--<video src="Videos/u-Hop_Operators.mp4" controls="controls" class="responsive-video wow fadeIn" data-wow-delay="1s" />-->
                            <div class="video-container no-controls">
                              <iframe width="853" height="480" src="//www.youtube.com/embed/bvkVKDh5v9I?rel=0;autohide=1" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col l4 s12 center wow fadeIn" data-wow-delay=".2s">
                            <!--<video src="Videos/u-Hop_Ad.mp4" controls="controls" class="responsive-video wow fadeIn" data-wow-delay="1.5s" />-->
                            <div class="video-container no-controls">
                              <iframe width="853" height="480" src="//www.youtube.com/embed/t7SKzw2IYMs?rel=0;autohide=1" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div> <!-- END of row -->--%>
                </div>
                <!-- END OF FIRST SECTION -->
            </div>


            <!-- SECOND BACKGROUND -->
            <div class="parallax-container valign-wrapper">
                <div class="section no-pad-bot">
                    <div class="container">
                        <div class="row center">
                            <h5 class="valign header col s12 light wow flipInY" style="color:#1a237e; -webkit-text-stroke:.1px;">More service areas to open weekly! </h5>
                            <!--<h5 class="valign header col s12 light wow slideInRight" data-wow-delay="1s">Track your shuttle thru your phone or call our  command center 24/7.</h5>-->
                        </div>
                    </div>
                </div>
                <div class="parallax">
                    <img src="Images/index/u-hop_road-min.jpg" alt="Unsplashed background img 2" />
                </div>
            </div>
            <!-- END OF SECOND BACKGROUND -->

            <!-- SECOND SECTION -->
            <div class="container scrollspy white-text" id="contactUs">
                <div class="section">
                    <div class="row">
                        <div class="col s12 center hoverable" data-wow-delay="1s">
                            <h4 class="wow fadeInDownBig  wow bounce">Contact Us</h4>
                            <!--<h5 class="wow fadeInDownBig">Home - Office/School - Home! We will pick you up at your doorstep and Office/School driveway!</h5>-->
                            <div class="row">
                                <div class="col l6 s12 wow slideInLeft">
                                    <div class="col s12">
                                        <div class="col s12">
                                            <h5 class="center-align" style="margin-bottom:0px;">Our office locations:</h5>
                                        </div>

                                        <div class="col m6 offset-m3 s12">
                                            <p class="center">
                                                <asp:DropDownList ID="ddlBranch" runat="server" cssclass="browser-default left blue darken-1"></asp:DropDownList></p>
                                        </div>
                                    </div>
                                    <div class="col s12 center-align">
                                        <br /> 
                                        <a id="aMap" runat="server" target="_blank">
                                            <img id="imgMap" runat="server" scrolling="no" class="responsive-img" width="600"/>
                                        </a>
                                    </div>
                                    <div class="col s12 center-align">
                                        <div class="col s12">
                                            <h5>Site map (u-Hop <span id="spBranchName" runat="server"></span>)</h5>
                                        </div>
                                        <div class="col s12">
                                            <asp:Label ID="lblBranchAddress" runat="server"></asp:Label>
                                        </div>
                                        <br /><br />
                                    </div>
                                </div>

                                <div class="col l6 s12 wow slideInRight">
                                    <div class="col s12">
                                       
                                        <h5 class="center-align">Feel unsafe with the existing transportation option? Can't afford private car/taxi hailing services? No Worries!<br /> Contact us at:</h5>
                                         
                                        <div class="divider"></div><br />

                                        <div class="col s12">
                                            <h5>FOR MEMBERSHIP </h5>
                                        </div>

                                        <div class="col s12">
                                            <b>LANDLINE </b>
                                        </div>
                                         <div class="col s12">
                                             Tel #: 02 (504-47-57)<br /><br />
                                        </div>

                                        <div class="col s12">
                                            <b>MOBILE</b>
                                        </div>
                                        <div class="col s12" style="padding-bottom:5px;">
                                            <b>For Rizal Area (to any destination):</b> <br />0908-230-5125/ 0916-245-3579/ 0906-575-6209/ 0977-644-1612
                                        </div>
                                        <div class="col s12"  style="padding-bottom:5px;">
                                            <b>For Novaliches/ North Avenue/ Quezon City Area (to any destination):</b> <br />0915-249-3939/ 0943-334-8604
                                        </div>
                                        <div class="col s12"  style="padding-bottom:5px;">
                                            <b>For Cavite/ Parañaque/ Las Piñas (to any destination):</b> <br />0915-249-3939/ 0943-334-8604
                                        </div>
                                        <div class="col s12"  style="padding-bottom:5px;">
                                            <b>For Makati Area (to any destination):</b> <br />0912-661-3582/ 0908-230-5125
                                        </div>
                                        <div class="col s12"  style="padding-bottom:5px;">
                                            <b>For Pasig Area (to any destination):</b> <br />0906-575-6209/ 0912-661-3582
                                        </div>
                                        <div class="col s12" style="padding-bottom:5px;">
                                            <b>For BGC (to any destination):</b> <br />0908-230-5125/ 0906-575-6209
                                        </div>
                                        <div class="col s12">
                                            <br />
                                            <div class="divider"></div>
                                        </div>
                                        <br />

                                        <div class="col s12">
                                            <h5>FOR OPERATORS</h5>
                                        </div>

                                         <div class="col s12">
                                            <b>LANDLINE </b>
                                        </div>
                                         <div class="col s12">
                                             Tel #: 904-84-39/ 504-47-54<br /><br />
                                        </div>
                                        <div class="col s12">
                                            <b>MOBILE</b>
                                        </div>
                                        <div class="col s12" style="padding-bottom:5px;">
                                            <b>0998-461-05-11/ 0917-970-5000/ 0915-132-45-27/ 0925-491-5503/ 0915-132-46-57/ 0908-220-0771/ 0977-353-7511/ 0998-354-54-07/ 0916-245-3089/ 0925-491-55-07</b>
                                        </div>

                                        <div class="col s12">
                                            <br />
                                            <div class="divider"></div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- END of MAIN row -->
                            <br />
                      </div> <!-- END of col s12 center -->

                      
                            
                        <div class="col s12 center ">
                            <br />
                            <div class="divider" style="height: 4px;"></div>
                            <br />
                            <div class="hoverable">
                                <div class="row">
                                    <div class="col l8 offset-l2 s12">
                                        <h5 class="wow fadeIn">Reach us through mail</h5>
                                        <br />
                                        <br />
                                        <div class="input-field wow fadeIn" data-wow-delay=".2s">
                                            <i class="material-icons prefix">&#xE853;</i> <!-- account_circle -->
                                            <input id="txtContactUsFullName" type="text" runat="server" class="validate txtContactUsFullName" autocomplete="off">
                                            <label for="icon_prefix">Name</label>
                                        </div>
                                        <div class="input-field wow fadeIn" data-wow-delay=".4s">
                                            <i class="material-icons prefix">&#xE0BE;</i> <!-- email -->
                                            <input id="txtContactUsEmail" runat="server" type="email" class="validate txtContactUsEmail" autocomplete="off">
                                            <label for="email" data-error="wrong" data-success="right">Email</label>
                                        </div>
                                        <div class="input-field wow fadeIn" data-wow-delay=".6s">
                                            <i class="material-icons prefix">&#xE8D2;</i><!-- subject -->
                                            <input id="txtContactUsSubject" runat="server" type="text" class="validate txtContactUsSubject" autocomplete="off">
                                            <label for="subject" data-error="wrong" data-success="right">Subject</label>
                                        </div>
                                        <div class="input-field wow fadeIn" data-wow-delay=".8s">
                                            <i class="material-icons prefix">&#xE254;</i> <!-- mode_edit -->
                                            <textarea id="txtContactUsBody" runat="server" class="materialize-textarea txtContactUsBody" autocomplete="off"></textarea>
                                            <label for="icon_prefix2">Message</label>
                                        </div>
                                        <asp:LinkButton ID="lbtnContactSend" runat="server" CssClass="waves-effect waves-light btn wow fadeIn pink accent-2"  data-wow-delay="1s" OnClick="lbtnContactSend_Click">Submit &nbsp;<i class="material-icons right"  style="margin:0;">&#xE163;</i></asp:LinkButton> <!-- send icon-->
                                    </div>
                                </div>
                                <br />
                            </div><!-- END of hoverable2 -->
                        </div><!-- END of col s12 center -->
                    </div> <!--EMD of row -->
                </div> <!-- END OF SECOND SECTION -->
                </div> <!-- END of container -->
    

        <!-- THIRD PARALLAX BG -->
        <div class="parallax-container valign-wrapper">
            <div class="section no-pad-bot">
                <div class="container">
                    <div class="row center">
                        <h5 class="header col s12 light wow swing" id="backToTopKey" style="color:#ff4081;">More service areas to open monthly! </h5>
                    </div>
                </div>
            </div>
            <div class="parallax">
                <img src="Images/index/u-hop_sh-min.jpg" alt="Unsplashed background img 3" />
            </div>
        </div>
        <!-- END OF THIRD PARALLAX BG -->


        <!-- FOOTER SECTION -->
        <footer class="page-footer" id="footerBg">

            <div id="history" class="container scrollspy" data-wow-delay="1s">
                <div class="row">
                    <div class="col s12">
                        <div class="divider wow fadeIn" ></div><br />
                        <h4 class="white-text center-align wow fadeIn">Company History</h4>
                        <div class="row">
                            <div class="col s12 companyHistory">

                                <div class="wow fadeInLeft">
                                    <p class="grey-text text-lighten-4 center-align">
                                        &nbsp;&nbsp;&nbsp;We are a group of professionals with more than 20 years of experience in transportation, business and information technology. We have been researching about a transport solution since 2001 and we have studied the Philippine market thoroughly to bring you a service that’s affordable, efficient and visionary.
                                    </p>
                                    <p class="grey-text text-lighten-4 center-align">
                                        We are all Filipinos. This is an all-Filipino corporation that strives to do public good by means of transportation.
                                    </p>
                                    <p class="grey-text text-lighten-4 center-align">
                                        U-Hop is a pilot project of Shangrilacars, and we’re bringing in our expertise and love for cars to help improve the way Filipinos commute. We care about your convenience and we care about your safety.

                                    </p>
                                </div>
                                <div class="wow fadeInRight"> 
                                    <p class="grey-text text-lighten-4 center-align">
                                       Our team of IT professionals have spent years developing an android and IOS-based app system that is strategically linked to our command center to bring you the most efficient online transport system there is. 
                                       Our app is also able to monitor the computer box of each of our vehicles so we can check fuel, door lock, location and speed. If our vehicles are stolen, we can control the computer box to stop the vehicles’ functions. 
                                       We have partnered with over 39 banks to make it easy for you to pay for our services.
                                    </p>
                                    <p class="grey-text text-lighten-4 center-align">
                                        Our other providers are of no other: Smart Card IT Solutions Singapore and All Cards for our cards, Aurum Pay for payment and business processing, Petron for fuel and roadside assistance, Highway Assistance Patrol for roadside assistance, Globe and Avaya for communications, HP for data center and Skycable for video solutions.
                                    </p>
                                    <p class="grey-text text-lighten-4 center-align">
                                       U-HOP can offer an alternative public transport system in terms of market or demand niches, and from the viewpoint of the public authority and the commercial operator. Our organizations aims to determine and serve new journeys that are not currently well served by public transport.
                                    </p>
                                </div>                           
                            </div>

                        </div>
                    </div>
                </div> <!-- END of ROW -->

                <div class="divider wow fadeIn"></div>
                <br />
                 <div class="row">
                        <div class="col s12 center white-text partnership">
                            <div class="wow fadeInDown" data-wow-delay="1s" data-wow-duration="1s">
                            <h4>Partnership</h4>
                            
                                <p class="grey-text text-lighten-4 center-align">u-Hop builds strong partnerships.</p>
                                <p class="grey-text text-lighten-4 center-align">u-Hop provides opportunities for convergence, co-development, and other synergistic arrangements with companies, groups or 
                                    individuals who share the same interest and vision.</p>
                                <p class="grey-text text-lighten-4 center-align">u-Hop has the solid competency needed to match its unending interest in unlimited possibilities with the partners.</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s6 m6 l3 noPadding2 wow  animate flip" data-wow-duration="1s" style="text-align:center;">
      			            <ul class="flipPic" style="margin-bottom:10px;">
      			                <li>
      			                    <figure>
      			                        <img src="Images/index/AllCard-min.jpg" class="responsive-img" alt="" width="240" height="100"/>
      			                        <figcaption>
      			                            <h2 style="margin-left:-18px;">AllCard</h2>
      			                        </figcaption>
      			                    </figure>
      			                </li>
      			            </ul>
	                    </div>

	                    <div class="col s6 m6 l3 noPadding2 wow  animate flip" data-wow-duration="1s">
      			            <ul class="flipPic" style="margin-bottom:10px;">
      			                <li class="center">
      			                    <figure>
      			                        <img src="Images/index/AurumPay-min.jpg" class="responsive-img" alt="" width="240" height="100"/>
      			                        <figcaption>
      			                            <h2 style="margin-left:-30px;">AurumPay</h2>
      			                        </figcaption>
      			                    </figure>
      			                </li>
      			            </ul>
	                    </div>
                         
                          <div class="col s6 m6 l3 noPadding2 wow  animate flip" data-wow-duration="1s">
      			                <ul class="flipPic">
    			                    <li>
    			                        <figure>
    			                            <img src="Images/index/Petron2-min.jpg" class="responsive-img" alt="" width="240" height="100"/>
    			                            <figcaption>
    			                                <h2 style="margin-left:-12px;margin-bottom:0;">Petron</h2>
    			                            </figcaption>
    			                        </figure>
    			                    </li>
    			                </ul>
	                      </div>
                      <div class="col s6 m6 l3 noPadding2 center wow  animate flip" data-wow-duration="1s">
      			            <ul class="flipPic">
    			                <li class="center">
    			                    <figure>
    			                        <img src="Images/index/Automobile-Assoc-min.jpg" class="responsive-img"  width="240" height="100"/>
    			                        <figcaption>
    			                            <h2  style="margin-left:-35px;margin-bottom:0;">Automobile</h2>
                                            <h2  style="margin-left:-35px;margin-top:-30px;">Association</h2>
    			                        </figcaption>
    			                    </figure>
    			                </li>
    			            </ul>
	                  </div>
                   </div> <!-- END of row -->

                <div class="footer-copyright">
                    <i>u-Hop Transport Network Vehicle System Inc. &copy; 2015. All rights reserved.</i> &nbsp; 
                    <div class="right">
                        <a href="https://www.facebook.com/uhopnow/" target="_blank"><img class="socialBtns" src="Images/index/fb-min1.png" height="30" width="30" /></a> &nbsp; <a href="https://twitter.com/uhoptheworld" target="_blank"><img class="socialBtns" src="Images/index/twitter-min1.png"height="30" width="30"/></a>
                    </div>
                </div>
            </div> <!-- END of div id="history" -->
        </footer>

        <a href="#juan" id="backToTop" class="btn-floating btn-large waves-effect waves-light pink accent-2"><i class="material-icons">&#xE55D;</i></a> <!-- Navigation -->
    </form>
    </div>

    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/script") %>
    </asp:PlaceHolder>
    <script src="Scripts/minified/index.min.js"></script>

    <script>
        /* Loading Icon and disappears when the HTML page loads completely */
        document.onreadystatechange = function () {
            
            var state = document.readyState
            if (state == 'complete') {
                setTimeout(function () {
                    document.getElementById('load').style.visibility = "hidden";
                    //document.getElementById('divForm').style.visibility = "visible";
                    $('#divForm').hide().css({ 'visibility' : 'visible'}).fadeIn(3000);                    
                    $("html, body").css({ "overflow": "auto" });
                    /* WOW.js Initialization */
                    new WOW().init();
                    /* END of WOW.js initialization */
                }, 1000);
            }
        }

        //$('.aLuxury').click(function () {
        //    window.location.href = "Luxury.aspx";            
        //});
    </script>

</body>
</html>