﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using uhopWeb.Classes;
using System.Security.Cryptography;
using System.Text;

namespace uhopWeb
{
    public partial class ForgotPasswordApproved : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["xRT"] != null)
                    {
                        string strEmail = ClsEncryptor.Decrypt(Request.QueryString["xRT"].Replace(" ", "+"), "uh0p");

                        using(ClsMembers objMembers=new ClsMembers())
                        {
                            objMembers.FillByEmail(strEmail);

                            if (objMembers.TblMembers.Rows.Count > 0)
                            {
                                if (!objMembers.TblMembers.Rows[0].IsNull("Email"))
                                {
                                    if (objMembers.TblMembers.Rows[0].GetString("IsOperator") == "1")
                                    {
                                        lnklogin.HRef = "Login.aspx?UT=Operator";
                                    }
                                    else
                                    {
                                        lnklogin.HRef = "Login.aspx?UT=Member";
                                    }

                                    string NewPassword = "";
                                    string rndomPassword = clsForgotPassword.RandomText();
                                    using (SHA1Managed sha1 = new SHA1Managed())
                                    {
                                        var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(rndomPassword));
                                        var sb = new StringBuilder(hash.Length * 2);
                                        foreach (byte b in hash)
                                        {
                                            sb.Append(b.ToString("x2"));
                                        }
                                        NewPassword = sb.ToString().ToLower();
                                    }

                                    objMembers.TblMembers.Rows[0].BeginEdit();
                                    objMembers.TblMembers.Rows[0]["Password"] = NewPassword;
                                    objMembers.TblMembers.Rows[0]["ModifiedOn"] = DateTime.Now;
                                    objMembers.TblMembers.Rows[0].EndEdit();
                                    objMembers.Update();
                                    ClsSendMail objSendMail=new ClsSendMail();
                                    objSendMail.SendEmailNotify(strEmail, objMembers.TblMembers.Rows[0].IsNull("Firstname").ToString(), "u-Hop Member New Password",
                                      "Change password successfully done!<br /> New Password :" + rndomPassword);
                                }
                            }
                        }                        
                    }
                }
            }
            catch
            {
                Response.Redirect("index.aspx");
            }
        }
    }
}