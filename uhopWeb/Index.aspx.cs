﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;
using System.Data;
using System.Web.Services;
using System.Security.Cryptography;
using System.Text;
using System.Reflection;

namespace uhopWeb
{
    public partial class Index : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) 
            {
                ClsBranch objBranch=new ClsBranch();
                objBranch.Fill(objBranch.TblBranch);

                foreach (DataRow drw in objBranch.TblBranch.Rows)
                {
                    ddlBranch.Items.Add(new ListItem(drw.GetString("BranchName").Split('[')[0], drw.GetString("BranchID")));
                }

                lblBranchAddress.Text = objBranch.GetBranchDetails("Address", ddlBranch.SelectedValue.ToString());
                spBranchName.InnerText = objBranch.GetBranchDetails("BranchName", ddlBranch.SelectedValue.ToString());
                imgMap.Src = UHopCore.GetPageURL() + "UploadedImages/Branch/" + objBranch.GetBranchDetails("FileName", ddlBranch.SelectedValue.ToString());
                aMap.HRef = objBranch.GetBranchDetails("MapCode", ddlBranch.SelectedValue.ToString());
            }
        }



        public string AndroidApp()
        {
            return UHopCore.GetPageURL() + "/DownloadApplication/u-Hop.apk";
        }

        protected void btnDL_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", "attachment; filename=u-Hop.apk");
            Response.Flush();
            Response.TransmitFile(Server.MapPath("~/DownloadApplication/u-Hop.apk"));
            Response.End();
        }

        public void PreLoaderImage()
        {
            var strImgName = new List<string> { "LuxuryChopperNowAvailable.png", "LuxuryPlaneNowAvailable-min.png", "LuxuryJetSkiNowAvailable-min.png","LuxuryYachtNowAvailable-min.png"};
            System.Random random = new System.Random();
            int index = random.Next(strImgName.Count);
            string name = strImgName[index];
            Response.Write(name);
        }

        [WebMethod]
        public static string GetBranchDetails(String pBranchID ,String pFieldName)
        {
            ClsBranch objBranch = new ClsBranch();
            return objBranch.GetBranchDetails(pFieldName, pBranchID);
        }
        public class BranchDetails
        {
            private string _BranchAddress;
            public string BranchAddress
            {
                get { return _BranchAddress; }
                set { _BranchAddress = value; }
            }
            private string _BranchMap;
            public string BranchMap
            {
                get { return _BranchMap; }
                set { _BranchMap = value; }
            }
            private string _BranchFileName;
            public string BranchFileName
            {
                get { return _BranchFileName; }
                set { _BranchFileName = value; }
            }

        }
        [WebMethod]
        public static List<BranchDetails> GetBranchDetails(String pBranchCode)
        {
            using (ClsBranch objBranch = new ClsBranch())
            {
                List<BranchDetails> lstBranchDetails = new List<BranchDetails>();
                DataTable tbl = new DataTable();
                objBranch.Fill(objBranch.TblBranch);
                tbl = objBranch.TblBranch.Where(t => t.BranchID == pBranchCode).CopyToDataTable();
                foreach (DataRow drw in tbl.Rows)
                {
                    string BranchAddress = drw["Address"].ToString();
                    string BranchMap = drw["Mapcode"].ToString();
                    string BranchFileName = UHopCore.GetPageURL() + "UploadedImages/Branch/" + drw["FileName"].ToString();

                    lstBranchDetails.Add(new BranchDetails
                    {
                        BranchAddress = BranchAddress,
                        BranchMap = BranchMap,
                        BranchFileName = BranchFileName
                    });
                }
                return lstBranchDetails;
            }
        }







        protected void lbtnContactSend_Click(object sender, EventArgs e)
        {
            string strSubject = txtContactUsFullName.Value + " - " + txtContactUsSubject.Value;
            ClsSendMail objSendMail = new ClsSendMail();
            objSendMail.SendMailContactUs(txtContactUsEmail.Value, strSubject, txtContactUsBody.Value);

            txtContactUsBody.Value = "";
            txtContactUsEmail.Value = "";
            txtContactUsFullName.Value = "";
            txtContactUsSubject.Value = "";
        }


    }
}