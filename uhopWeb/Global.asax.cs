﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Optimization;
using uhopWeb.Classes;
//using ASPSnippets.FaceBookAPI;

namespace uhopWeb
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);            
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            //FaceBookConnect.API_Key = "400078423534584";
            //FaceBookConnect.API_Secret = "9338fd510efa16ee3a682ee7af689a54";

            ClsContactType objContactType = new ClsContactType();
            objContactType.Fill();
          
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}