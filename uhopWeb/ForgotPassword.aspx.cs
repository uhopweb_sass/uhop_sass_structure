﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;
using System.Data;

namespace uhopWeb
{
    public partial class Forgot : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            if (txtEmail.Text != "")
            {

                using (ClsMembers objMembers = new ClsMembers())
                {
                    objMembers.FillByEmail(txtEmail.Text);

                    if (objMembers.TblMembers.Rows.Count > 0)
                    {
                        ClsSendMail objSendMail=new ClsSendMail();
                        objSendMail.SendRequestForgotPassword(txtEmail.Text, objMembers.GetMemberDetailsViaEmail(txtEmail.Text, "Firstname").ToString());
                        spnForgot.Style["display"] = "none";
                        spnResetRequestSent.Style["display"] = "block";
                    }
                    else
                    {
                        spnForgot.Style["display"] = "block";
                        spnResetRequestSent.Style["display"] = "none";
                    }
                }
            }            
        }
    }
}