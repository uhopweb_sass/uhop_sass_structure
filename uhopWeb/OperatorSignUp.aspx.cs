﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using uhopWeb.Classes;
using System.Security.Cryptography;
using System.Text;
using System.Web.Services;



namespace uhopWeb
{
    public partial class OperatorSignUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadYearModel();
            }
        }

        public void LoadYearModel()
        {
            int y = 0;
            for (int x = 2013; x <= DateTime.Now.Year + 1; x++)
            {
                ddlYearModel.Items.Insert(y, new ListItem(Convert.ToString(x), Convert.ToString(x)));
                ddlYearModel.SelectedIndex = 0;
                y += 1;
            }
        }

        protected void lbtnSubmit_Click(object sender, EventArgs e)
        {

            try
            {
                using (ClsMembers objMembers = new ClsMembers())
                {
                    ClsAutoNumber objAutoNumber=new ClsAutoNumber();
                    objMembers.FillByEmail(txtEmailSignUp.Text);

                    if (objMembers.TblMembers.Rows.Count == 0)
                    {
                        DataRow drw = objMembers.TblMembers.NewRow();
                        string MemberID = objAutoNumber.GenerateAutoNumber("MemberID", "000000").ToString();
                        DateTime dtNow = DateTime.Now;
                        drw.SetString("MemberID", MemberID);
                        drw.SetString("FirstName", System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(txtFirstName.Text));
                        drw.SetString("LastName", System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(txtLastName.Text));
                        drw.SetString("Gender", txtGender.Text);
                        drw.SetString("IsOperator", "1");
                        drw.SetString("IsPWD", "0");
                        drw.SetString("IsSeniorCitizen", "0");
                        drw.SetString("Email", txtEmailSignUp.Text.ToLower().Trim());
                        drw.SetDateTime("Birthdate", Convert.ToDateTime(txtBirthday.Text));
                        drw.SetString("ProfilePicture", "Default_Profile.png");
                        string encryptedPassword = "";
                        using (SHA1Managed sha1 = new SHA1Managed())
                        {
                            var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(txtPasswordSignUp.Text));
                            var sb = new StringBuilder(hash.Length * 2);
                            foreach (byte b in hash)
                            {
                                sb.Append(b.ToString("x2"));
                            }
                            encryptedPassword = sb.ToString().ToLower();
                        }

                        drw.SetString("Password", encryptedPassword);
                        drw.SetString("Enabled", "1");
                        drw.SetString("Status", "0");
                        drw.SetString("IsVerified", "0");
                        drw.SetDateTime("CreatedOn", dtNow);
                        drw.SetString("ModifiedBy", MemberID);
                        drw.SetDateTime("ModifiedOn", dtNow);

                        if (txtBrandName.Text != "" || txtModelName.Text != "" || txtPlateNo.Text != "" || txtMaxCap.Text != "")
                        {
                        string strVehicleID = objAutoNumber.GenerateAutoNumber("VehicleID", "000000").ToString();
                        objMembers.FillAttachmentType();

                        if (FUFrontView.HasFile)
                        {
                            DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000003'");
                            DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                            string strFileName = "O" + MemberID + drwAttachmentType[0].GetString("Prefix") + "." + FUFrontView.PostedFile.FileName.Split('.')[(FUFrontView.PostedFile.FileName.Split('.').Length - 1)];
                            drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                            drwAttachment.SetString("AttachmentTypeID", "20150923000003");
                            drwAttachment.SetString("ReferenceID", strVehicleID);
                            drwAttachment.SetString("AttachmentFileName", strFileName);
                            drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                            drwAttachment.SetString("Status", "1");
                            drwAttachment.SetString("IsForDriver", "0");
                            FUFrontView.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                            objMembers.TblAttachment.Rows.Add(drwAttachment);
                            objMembers.UpdateAttachment();
                        }


                        if (FURearView.HasFile)
                        {
                            DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000004'");
                            DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                            string strFileName = "O" + MemberID + drwAttachmentType[0].GetString("Prefix") + "." + FURearView.PostedFile.FileName.Split('.')[(FURearView.PostedFile.FileName.Split('.').Length - 1)];
                            drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                            drwAttachment.SetString("AttachmentTypeID", "20150923000004");
                            drwAttachment.SetString("ReferenceID", strVehicleID);
                            drwAttachment.SetString("AttachmentFileName", strFileName);
                            drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                            drwAttachment.SetString("Status", "1");
                            drwAttachment.SetString("IsForDriver", "0");
                            FURearView.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                            objMembers.TblAttachment.Rows.Add(drwAttachment);
                            objMembers.UpdateAttachment();
                        }

                        if (FULeftView.HasFile)
                        {
                            DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000005'");
                            DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                            string strFileName = "O" + MemberID + drwAttachmentType[0].GetString("Prefix") + "." + FULeftView.PostedFile.FileName.Split('.')[(FULeftView.PostedFile.FileName.Split('.').Length - 1)];
                            drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                            drwAttachment.SetString("AttachmentTypeID", "20150923000005");
                            drwAttachment.SetString("ReferenceID", strVehicleID);
                            drwAttachment.SetString("AttachmentFileName", strFileName);
                            drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                            drwAttachment.SetString("Status", "1");
                            drwAttachment.SetString("IsForDriver", "0");
                            FULeftView.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                            objMembers.TblAttachment.Rows.Add(drwAttachment);
                            objMembers.UpdateAttachment();
                        }
                        
                        if (FURightView.HasFile)
                        {
                            DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000006'");
                            DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                            string strFileName = "O" + MemberID + drwAttachmentType[0].GetString("Prefix") + "." + FURightView.PostedFile.FileName.Split('.')[(FURightView.PostedFile.FileName.Split('.').Length - 1)];
                            drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                            drwAttachment.SetString("AttachmentTypeID", "20150923000006");
                            drwAttachment.SetString("ReferenceID", strVehicleID);
                            drwAttachment.SetString("AttachmentFileName", strFileName);
                            drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                            drwAttachment.SetString("Status", "1");
                            drwAttachment.SetString("IsForDriver", "0");
                            FURightView.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                            objMembers.TblAttachment.Rows.Add(drwAttachment);
                            objMembers.UpdateAttachment();
                        }
                        
                        if (FUFrontInterior.HasFile)
                        {
                            DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000007'");
                            DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                            string strFileName = "O" + MemberID + drwAttachmentType[0].GetString("Prefix") + "." + FUFrontInterior.PostedFile.FileName.Split('.')[(FUFrontInterior.PostedFile.FileName.Split('.').Length - 1)];
                            drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                            drwAttachment.SetString("AttachmentTypeID", "20150923000007");
                            drwAttachment.SetString("ReferenceID", strVehicleID);
                            drwAttachment.SetString("AttachmentFileName", strFileName);
                            drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                            drwAttachment.SetString("Status", "1");
                            drwAttachment.SetString("IsForDriver", "0");
                            FUFrontInterior.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                            objMembers.TblAttachment.Rows.Add(drwAttachment);
                            objMembers.UpdateAttachment();
                        }
                        
                        if (FURearInterior.HasFile)
                        {
                            DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000008'");
                            DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                            string strFileName = "O" + MemberID + drwAttachmentType[0].GetString("Prefix") + "." + FURearInterior.PostedFile.FileName.Split('.')[(FURearInterior.PostedFile.FileName.Split('.').Length - 1)];
                            drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                            drwAttachment.SetString("AttachmentTypeID", "20150923000008");
                            drwAttachment.SetString("ReferenceID", strVehicleID);
                            drwAttachment.SetString("AttachmentFileName", strFileName);
                            drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                            drwAttachment.SetString("Status", "1");
                            drwAttachment.SetString("IsForDriver", "0");
                            FURearInterior.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                            objMembers.TblAttachment.Rows.Add(drwAttachment);
                            objMembers.UpdateAttachment();
                        }

                        if (FUOr.HasFile)
                        {
                            DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000009'");
                            DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                            string strFileName = "O" + MemberID + drwAttachmentType[0].GetString("Prefix") + "." + FUOr.PostedFile.FileName.Split('.')[(FUOr.PostedFile.FileName.Split('.').Length - 1)];
                            drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                            drwAttachment.SetString("AttachmentTypeID", "20150923000009");
                            drwAttachment.SetString("ReferenceID", strVehicleID);
                            drwAttachment.SetString("AttachmentFileName", strFileName);
                            drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                            drwAttachment.SetString("Status", "1");
                            drwAttachment.SetString("IsForDriver", "0");
                            drwAttachment.SetString("ExpirationDate", txtExpiryDateOR.Text);
                            FUOr.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                            objMembers.TblAttachment.Rows.Add(drwAttachment);
                            objMembers.UpdateAttachment();
                         }
                        
                        if (FUCR.HasFile)
                        {
                            DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000010'");
                            DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                            string strFileName = "O" + MemberID + drwAttachmentType[0].GetString("Prefix") + "." + FUCR.PostedFile.FileName.Split('.')[(FUCR.PostedFile.FileName.Split('.').Length - 1)];
                            drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                            drwAttachment.SetString("AttachmentTypeID", "20150923000010");
                            drwAttachment.SetString("ReferenceID", strVehicleID);
                            drwAttachment.SetString("AttachmentFileName", strFileName);
                            drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                            drwAttachment.SetString("Status", "1");
                            drwAttachment.SetString("IsForDriver", "0");
                            FUCR.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                            objMembers.TblAttachment.Rows.Add(drwAttachment);
                            objMembers.UpdateAttachment();
                        }
                        
                        if (FUInsurance.HasFile)
                        {
                            DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000011'");
                            DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                            string strFileName = "O" + MemberID + drwAttachmentType[0].GetString("Prefix") + "." + FUInsurance.PostedFile.FileName.Split('.')[(FUInsurance.PostedFile.FileName.Split('.').Length - 1)];
                            drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                            drwAttachment.SetString("AttachmentTypeID", "20150923000011");
                            drwAttachment.SetString("ReferenceID", strVehicleID);
                            drwAttachment.SetString("AttachmentFileName", strFileName);
                            drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                            drwAttachment.SetString("Status", "1");
                            drwAttachment.SetString("IsForDriver", "0");
                            drwAttachment.SetString("ExpirationDate", txtExpiryDateInsurance.Text);
                            FUInsurance.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                            objMembers.TblAttachment.Rows.Add(drwAttachment);
                            objMembers.UpdateAttachment();
                        }

                        ClsMemberVehicle objMemberVehicle = new ClsMemberVehicle();
                        DataRow drwVehicle = objMemberVehicle.TblMemberVehicle.NewRow();

                        
                        string strVehicleTypeID = "";
                        if (txtMaxCap.Text != "")
                        {
                            objMemberVehicle.FillType();
                            DataRow[] drwVehicleType = objMemberVehicle.TblVehicleType.Select("MinimumCapacity<='" + txtMaxCap.Text + "' AND MaximumCapacity >='" + txtMaxCap.Text + "'");
                            if (drwVehicleType.Length > 0)
                            {
                                strVehicleTypeID = drwVehicleType[0].GetString("VehicleTypeID");
                            }
                            else
                            {
                                DataRow[] drwVehicleTypeMax = objMemberVehicle.TblVehicleType.Select("", "MaximumCapacity DESC");
                                strVehicleTypeID = drwVehicleTypeMax[0].GetString("VehicleTypeID");
                            }
                        }
                        else
                        {
                            txtMaxCap.Text = "0";
                        }
                            
                        drwVehicle.SetString("VehicleID", strVehicleID);
                        drwVehicle.SetString("MemberID", MemberID);
                        drwVehicle.SetString("VehicleTypeID", strVehicleTypeID);
                        drwVehicle.SetString("BrandName", txtBrandName.Text);
                        drwVehicle.SetString("VehicleColor", txtVehicleColor.Text);
                        drwVehicle.SetString("Model", txtModelName.Text);
                        drwVehicle.SetString("PlateNumber", txtPlateNo.Text);
                        drwVehicle.SetString("YearModel", ddlYearModel.SelectedValue.ToString());
                        drwVehicle.SetString("VehicleType", ddlVehicelType.SelectedValue.ToString());
                        drwVehicle.SetInt32("MaximumCapacity", Convert.ToInt32(txtMaxCap.Text));
                        drwVehicle.SetString("Enabled", "1");
                        drwVehicle.SetString("Status", "1");
                        drwVehicle.SetString("CreatedBy", MemberID);
                        drwVehicle.SetString("ModifiedBy", MemberID);
                        drwVehicle.SetDateTime("ModifiedOn", DateTime.Now);
                        drwVehicle.SetDateTime("CreatedOn", DateTime.Now);
                        
                        //saving member vehicle
                        objMemberVehicle.TblMemberVehicle.Rows.Add(drwVehicle);
                        objMemberVehicle.Update();
                        }

                        DataRow drwMobile = objMembers.TblContact.NewRow();
                        drwMobile.SetString("ContactID", objAutoNumber.GenerateAutoNumber("ContactNumberID", "000000").ToString());
                        drwMobile.SetString("ContactNumberTypeID", "20150923000002");
                        drwMobile.SetString("ReferenceID", MemberID);
                        drwMobile.SetString("ContactNumber", txtMobileNumber.Text);
                        drwMobile.SetString("Enabled", "1");
                        drwMobile.SetString("IsForDriver", "0");
                        objMembers.TblContact.Rows.Add(drwMobile);
                        objMembers.UpdateContact();

                        if (txtTelephoneNumber.Text.Trim() != "")
                        {
                            DataRow drwLandline = objMembers.TblContact.NewRow();
                            drwLandline.SetString("ContactID", objAutoNumber.GenerateAutoNumber("ContactNumberID", "000000").ToString());
                            drwLandline.SetString("ContactNumberTypeID", "20150923000001");
                            drwLandline.SetString("ReferenceID", MemberID);
                            drwLandline.SetString("ContactNumber", txtTelephoneNumber.Text);
                            drwLandline.SetString("Enabled", "1");
                            drwLandline.SetString("IsForDriver", "0");
                            objMembers.TblContact.Rows.Add(drwLandline);
                            objMembers.UpdateContact();
                        }
                                                
                        //saving operator details
                        objMembers.TblMembers.Rows.Add(drw);
                        objMembers.Update();
                        ClsSendMail objSendMail = new ClsSendMail();
                        objSendMail.SendMailSignUp(txtEmailSignUp.Text.ToLower().Trim(), txtFirstName.Text);

                        spnEmail.InnerText = txtEmailSignUp.Text.ToLower().Trim();
                        txtEmailSignUp.Text = "";
                        txtSignUpRetypeEmail.Text = "";
                        DivSignupComplete.Style["display"] = "block";
                        DivSignupComplete.Style["overflow"] = "hidden";
                    }
                    else {
                        DivSignupComplete.Style["display"] = "none";
                        spanEmailExists.Style["display"] = "none";
                        spanEmailExists.Focus();

                    }
                }
            }

            catch (Exception ex)
            {
                DivSignupComplete.Style["display"] = "none";
            }
        }


        [WebMethod]
        public static string CheckExistingMail(string email_address)
        {
            try
            {
                if (email_address.Trim() != "")
                {
                    ClsMembers objMembers=new ClsMembers();
                    DataRow drw = objMembers.TblMembers.Select("Email='" + email_address + "'")[0];
                    return "Invalid";
                }
                else
                {
                    return "valid";
                }
            }
            catch
            {
                return "Valid";
            }
        }
    }
}