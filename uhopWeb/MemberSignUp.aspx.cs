﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using uhopWeb.Classes;
using System.Security.Cryptography;
using System.Text;
using System.Web.Services;

namespace uhopWeb
{
    public partial class MemberSignUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateOriginProvince();
                PopulateDestinationProvince();
                PopulateAddressType();
            }
        }

        public void PopulateAddressType()
        {
            using (clsAddressType objAddressType = new clsAddressType())
            {
                ddlAddressType.Items.Clear();
                objAddressType.Fill();
                DataTable tblTemp = clsAddressType.TblAddressType;
                var qry = from row in clsAddressType.TblAddressType.AsEnumerable()
                              where row.Field<string>("IsOrigin").Trim() == "0"
                              orderby row["AddressTypeDescription"] ascending
                              select row;
                tblTemp = qry.CopyToDataTable();
                ddlAddressType.DataSource = tblTemp;
                ddlAddressType.DataTextField = "AddressTypeDescription";
                ddlAddressType.DataValueField = "AddressTypeID";
                ddlAddressType.DataBind();
                ddlAddressType.Items.Insert(0, new ListItem("Choose your Address Type", String.Empty));
                ddlAddressType.Items[0].Attributes.Add("disabled", "disabled");
                //ddlAddressType.Items[0].Attributes.Add("selected", "true");
                ddlAddressType.SelectedIndex = 0;
            }
        }

        public void PopulateOriginProvince()
        {
            using (ClsMembers objMembers = new ClsMembers())
            {
                ddlOriginProvince.Items.Clear();
                objMembers.FilluHopLocation();

                DataTable tblTemp = objMembers.TbluHopLocation;
                var qryGroupBy = from rows in tblTemp.AsEnumerable()
                                         orderby rows["Province"] ascending
                                         group rows by new { Province = rows["Province"] } into grp
                                         select grp.First();
                tblTemp = qryGroupBy.CopyToDataTable();
                ddlOriginProvince.DataSource = tblTemp;
                ddlOriginProvince.DataTextField = "Province";
                ddlOriginProvince.DataValueField = "Province";
                ddlOriginProvince.DataBind();
                ddlOriginProvince.Items.Insert(0, new ListItem("Choose your Province", String.Empty));
                ddlOriginProvince.Items[0].Attributes.Add("disabled", "disabled");
                //ddlOriginProvince.Items[0].Attributes.Add("selected", "true");
                ddlOriginProvince.SelectedIndex = 0;
                ddlOriginCityMunicipality.Items.Insert(0, new ListItem("Choose Province first", String.Empty));
                ddlOriginCityMunicipality.Items[0].Attributes.Add("disabled", "disabled");
                //ddlOriginCityMunicipality.Items[0].Attributes.Add("selected", "true");
                ddlOriginCityMunicipality.SelectedIndex = 0;
                ddlOriginCityMunicipality.Items[0].Attributes.Add("class", "SelectOriginProvince");
            }
        }

        public void PopulateDestinationProvince()
        {
            using (ClsMembers objMembers = new ClsMembers())
            {
                ddlDestinationProvince.Items.Clear();
                objMembers.FilluHopLocation();
                DataTable tblTemp = objMembers.TbluHopLocation;
                var qryGroupBy = from rows in tblTemp.AsEnumerable()
                                 orderby rows["Province"] ascending
                                 group rows by new { Province = rows["Province"] } into grp
                                 select grp.First();
                tblTemp = qryGroupBy.CopyToDataTable();
                ddlDestinationProvince.DataSource = tblTemp;
                ddlDestinationProvince.DataTextField = "Province";
                ddlDestinationProvince.DataValueField = "Province";
                ddlDestinationProvince.DataBind();
                ddlDestinationProvince.Items.Insert(0, new ListItem("Choose your Province", String.Empty));
                ddlDestinationProvince.Items[0].Attributes.Add("disabled", "disabled");
                ddlDestinationProvince.SelectedIndex = 0;
                ddlDestinationCityMunicipality.Items.Insert(0, new ListItem("Choose Province first", String.Empty));
                ddlDestinationCityMunicipality.Items[0].Attributes.Add("disabled", "disabled");
                ddlDestinationCityMunicipality.SelectedIndex = 0;
                ddlDestinationCityMunicipality.Items[0].Attributes.Add("class", "SelectDestinationProvince");
            }
        }
        
        [WebMethod]
        public static string CheckExistingMail(string email_address)
        {
            try
            {
                ClsMembers objMembers = new ClsMembers();
                objMembers.FillByEmail(email_address);
                if (email_address.Trim() != "")
                {
                    DataRow drw = objMembers.TblMembers.Rows[0];
                    return "Invalid";
                }
                else
                {
                    return "valid";
                }
            }
            catch
            {
                return "Valid";
            }
        }

        public class MemberOriginMunicipality
        {
            private string _OriginMunicipality;
            public string OriginMunicipality
            {
                get { return _OriginMunicipality; }
                set { _OriginMunicipality = value; }
            }

            private string _OriginMunicipalityZipCode;
            public string OriginMunicipalityZipCode
            {
                get { return _OriginMunicipalityZipCode; }
                set { _OriginMunicipalityZipCode = value; }
            }

            private string _OriginProvince;
            public string OriginProvince
            {
                get { return _OriginProvince; }
                set { _OriginProvince = value; }
            }
        }

        public class MemberDestinationMunicipality
        {
            private string _DestinationMunicipality;
            public string DestinationMunicipality
            {
                get { return _DestinationMunicipality; }
                set { _DestinationMunicipality = value; }
            }

            private string _DestinationMunicipalityZipCode;
            public string DestinationMunicipalityZipCode
            {
                get { return _DestinationMunicipalityZipCode; }
                set { _DestinationMunicipalityZipCode = value; }
            }

            private string _DestinationProvince;
            public string DestinationProvince
            {
                get { return _DestinationProvince; }
                set { _DestinationProvince = value; }
            }
        }

        [WebMethod]
        public static List<MemberOriginMunicipality> GetOriginMunicipality(String pOriginProvince)
        {
            using (ClsMembers objMembers = new ClsMembers())
            {
                List<MemberOriginMunicipality> lstOriginMunicipality = new List<MemberOriginMunicipality>();
                objMembers.FilluHopLocation();
                DataTable tbl = new DataTable();
                var qry = from row in objMembers.TbluHopLocation.AsEnumerable()
                          where row.Field<string>("Province").Trim() == pOriginProvince
                          orderby row["Description"] ascending
                          select row;
                tbl = qry.CopyToDataTable();
                foreach (DataRow drw in tbl.Rows)
                {
                    string OriginMunicipality = drw["Description"].ToString();
                    string OriginMunicipalityZipCode = drw["zipcode"].ToString();


                    lstOriginMunicipality.Add(new MemberOriginMunicipality
                    {
                        OriginMunicipality = OriginMunicipality,
                        OriginMunicipalityZipCode = OriginMunicipalityZipCode
                    });
                }
                return lstOriginMunicipality;
            }
        }

        [WebMethod]
        public static List<MemberDestinationMunicipality> GetDestinationMunicipalityViaZipcode(String pZipCode)
        {
            using (ClsMembers objMember = new ClsMembers())
            {
                List<MemberDestinationMunicipality> lstDestination = new List<MemberDestinationMunicipality>();
                DataTable tblq = new DataTable();
                objMember.FilluHopLocation();
                DataRow[] drw1 = objMember.TbluHopLocation.Select("zipcode='" + pZipCode + "'");
                if (drw1.Length != 0)
                {
                    var qryq = from row in objMember.TbluHopLocation.AsEnumerable()
                               where row.Field<string>("Province").Trim() == drw1[0].GetString("Province")
                               orderby row["Description"] ascending
                               select row;
                    tblq = qryq.CopyToDataTable();
                    foreach (DataRow drw in tblq.Rows)
                    {
                        string xDestinationMunicipality = drw["Description"].ToString();
                        string xDestinationMunicipalityZipCode = drw["zipcode"].ToString();
                        string xDestinationProvince = drw["province"].ToString();
                        MemberDestinationMunicipality test = new MemberDestinationMunicipality() { 
                            DestinationMunicipality=xDestinationMunicipality,
                            DestinationMunicipalityZipCode=xDestinationMunicipalityZipCode,
                            DestinationProvince=drw1[0].GetString("Province")
                        };

                        lstDestination.Add(test);
                    }
                }

                return lstDestination;
            }
       }

        protected void lbtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                using (ClsMembers objMembers = new ClsMembers())
                {
                    objMembers.FillByEmail(txtEmailSignUp.Text);
                    DataRow[] drwEmail = objMembers.TblMembers.Select("Email='" + txtEmailSignUp.Text + "'");
                    if (drwEmail.Length == 0)
                    {
                        objMembers.FilluHopLocation();
                        ClsAutoNumber objAutoNumber = new ClsAutoNumber();
                        DataRow drw = objMembers.TblMembers.NewRow();
                        string MemberID = objAutoNumber.GenerateAutoNumber("MemberID", "000000").ToString();
                        DateTime dtNow = DateTime.Now;
                        drw.SetString("MemberID", MemberID);
                        drw.SetString("FirstName", System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(txtFirstName.Text));
                        drw.SetString("LastName", System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(txtLastName.Text));
                        drw.SetString("Gender", (optFemale.Checked ? "Female" : "Male"));
                        drw.SetDateTime("Birthdate", Convert.ToDateTime(txtBirthday.Text));
                        drw.SetString("Email", txtEmailSignUp.Text.ToLower().Trim());
                        string encryptedPassword = "";
                        using (SHA1Managed sha1 = new SHA1Managed())
                        {
                            var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(txtPasswordSignUp.Text));
                            var sb = new StringBuilder(hash.Length * 2);
                            foreach (byte b in hash)
                            {
                                sb.Append(b.ToString("x2"));
                            }
                            encryptedPassword = sb.ToString().ToLower();
                        }

                        drw.SetString("Password", encryptedPassword);
                        drw.SetString("ProfilePicture", "Default_Profile.png");
                        drw.SetString("IsOperator", "0");
                        drw.SetString("IsPWD", "0");
                        drw.SetString("IsSeniorCitizen", "0");
                        drw.SetString("Enabled", "1");
                        drw.SetString("Status", "0");
                        drw.SetString("IsVerified", "0");
                        drw.SetDateTime("CreatedOn", dtNow);
                        drw.SetString("ModifiedBy", MemberID);
                        drw.SetDateTime("ModifiedOn", dtNow);

                        DataRow drwAddress1 = objMembers.TblAddress.NewRow();
                        drwAddress1.SetString("AddressID", objAutoNumber.GenerateAutoNumber("AddressID", "000000").ToString());
                        drwAddress1.SetString("AddressTypeID", "20150923000001"); // home address
                        drwAddress1.SetString("ReferenceID", MemberID);
                        drwAddress1.SetString("Street", Ostreet.Value);
                        drwAddress1.SetString("Barangay", Obarangay.Value);
                        drwAddress1.SetString("ZipCode", Ozipcode.Value);
                        drwAddress1.SetString("CityMunicipality", objMembers.GetLocationDetails(Request.Form["ddlOriginCityMunicipality"], "Description"));//);
                        drwAddress1.SetString("Province", ddlOriginProvince.Items[ddlOriginProvince.SelectedIndex].Value);
                        drwAddress1.SetString("Country", "Philippines");
                        drwAddress1.SetString("IsForDriver", "0");

                        DataRow drwAddressBilling = objMembers.TblAddress.NewRow();
                        drwAddressBilling.SetString("AddressID", objAutoNumber.GenerateAutoNumber("AddressID", "000000").ToString());
                        drwAddressBilling.SetString("AddressTypeID", "20150923000003"); // billing address
                        drwAddressBilling.SetString("ReferenceID", MemberID);
                        drwAddressBilling.SetString("Street", Ostreet.Value);
                        drwAddressBilling.SetString("Barangay", Obarangay.Value);
                        drwAddressBilling.SetString("ZipCode", Ozipcode.Value);
                        drwAddressBilling.SetString("CityMunicipality", objMembers.GetLocationDetails(Request.Form["ddlOriginCityMunicipality"], "Description"));//Request.Form["ddlOriginCityMunicipality"]);
                        drwAddressBilling.SetString("Province", ddlOriginProvince.Items[ddlOriginProvince.SelectedIndex].Value);
                        drwAddressBilling.SetString("Country", "Philippines");
                        drwAddressBilling.SetString("IsForDriver", "0");

                        DataRow drwAddress2 = objMembers.TblAddress.NewRow();
                        drwAddress2.SetString("AddressID", objAutoNumber.GenerateAutoNumber("AddressID", "000000").ToString());
                        drwAddress2.SetString("AddressTypeID", hdfDestinationType.Value);
                        drwAddress2.SetString("ReferenceID", MemberID);
                        drwAddress2.SetString("Street", Dstreet.Value);
                        drwAddress1.SetString("Barangay", Dbarangay.Value);
                        drwAddress2.SetString("ZipCode", Dzipcode.Value);
                        drwAddress2.SetString("CityMunicipality", objMembers.GetLocationDetails(Request.Form["ddlDestinationCityMunicipality"], "Description"));//Request.Form["ddlDestinationCityMunicipality"]);
                        drwAddress2.SetString("Province", ddlDestinationProvince.Items[ddlDestinationProvince.SelectedIndex].Value);
                        drwAddress2.SetString("Country", "Philippines");
                        drwAddress2.SetString("IsForDriver", "0");

                        DataRow drwMobile = objMembers.TblContact.NewRow();
                        drwMobile.SetString("ContactID", objAutoNumber.GenerateAutoNumber("ContactNumberID", "000000").ToString());
                        drwMobile.SetString("ContactNumberTypeID", "20150923000002");
                        drwMobile.SetString("ReferenceID", MemberID);
                        drwMobile.SetString("ContactNumber", txtMobileNumber.Text);
                        drwMobile.SetString("Enabled", "1");
                        drwMobile.SetString("IsForDriver", "0");

                        if (txtTelephoneNumber.Text.Trim() != "")
                        {
                            DataRow drwLandline = objMembers.TblContact.NewRow();
                            drwLandline.SetString("ContactID", objAutoNumber.GenerateAutoNumber("ContactNumberID", "000000").ToString());
                            drwLandline.SetString("ContactNumberTypeID", "20150923000001");
                            drwLandline.SetString("ReferenceID", MemberID);
                            drwLandline.SetString("ContactNumber", txtTelephoneNumber.Text);
                            drwLandline.SetString("Enabled", "1");
                            drwLandline.SetString("IsForDriver", "0");
                            objMembers.TblContact.Rows.Add(drwLandline);
                            objMembers.UpdateContact();
                        }

                        objMembers.TblContact.Rows.Add(drwMobile);
                        objMembers.UpdateContact();
                        objMembers.TblAddress.Rows.Add(drwAddress2);
                        objMembers.UpdateAddress();
                        objMembers.TblAddress.Rows.Add(drwAddress1);
                        objMembers.UpdateAddress();
                        objMembers.TblAddress.Rows.Add(drwAddressBilling);
                        objMembers.UpdateAddress();
                        objMembers.TblMembers.Rows.Add(drw);
                        objMembers.Update();
                        ClsSendMail objSendMail = new ClsSendMail();
                        objSendMail.SendMailSignUp(txtEmailSignUp.Text.ToLower().Trim(), txtFirstName.Text);
                        spnEmail.InnerText = txtEmailSignUp.Text.ToLower().Trim();
                        DivSignupCoplete.Style["display"] = "block";
                    }
                    else
                    {
                        DivSignupCoplete.Style["display"] = "none";
                        spanEmailExists.Style["display"] = "block";
                        spanEmailExists.Focus();

                    }
                }
            }
            catch
            {
                Response.Redirect("MemberSignup.aspx");
            }
        }


     






    }
}