﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using uhopWeb.Classes;
using System.Data;

namespace uhopWeb
{
    public partial class Luxury : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadLuxuryType();
            }
        }

        //public bool IsRefresh { get; private set; }
        //protected override void OnInit(EventArgs e)
        //{
        //    base.OnInit(e);
        //    if (Session["IsRefresh" + Page.GetType().Name] == null)
        //    {
        //        Session["IsRefresh" + Page.GetType().Name] = true;
        //        IsRefresh = false;
        //    }
        //    else
        //    {
        //        IsRefresh = true;
        //    }
        //}

 
        public class LuxuryDetails
        {
            private string _LuxuryID;
            public string LuxuryID
            {
                get { return _LuxuryID; }
                set { _LuxuryID = value; }
            }

            private string _LuxuryType;
            public string LuxuryType
            {
                get { return _LuxuryType; }
                set { _LuxuryType = value; }
            }

            private string _ProviderID;
            public string ProviderID
            {
                get { return _ProviderID; }
                set { _ProviderID = value; }
            }

            private string _Description;
            public string Description
            {
                get { return _Description; }
                set { _Description = value; }
            }

            private string _Capacity;
            public string Capacity
            {
                get { return _Capacity; }
                set { _Capacity = value; }
            }

            private string _Specification;
            public string Specification
            {
                get { return _Specification; }
                set { _Specification = value; }
            }

            private string _LuxuryPicture;
            public string LuxuryPicture
            {
                get { return _LuxuryPicture; }
                set { _LuxuryPicture = value; }
            }
        }
        [WebMethod]
        //public static List<LuxuryDetails> GetLuxuryTypeDetails(String pLuxuryTypeID)
        //{
        //    using (clsLuxury objLuxury = new clsLuxury())
        //    {
        //        List<LuxuryDetails> lstLuxuryDetails = new List<LuxuryDetails>();
        //        DataTable tbl = new DataTable();
        //        objLuxury.FillLuxury(clsLuxury.TblLuxury);
        //        tbl = clsLuxury.TblLuxury.Where(t => t.LuxuryTypeID == pLuxuryTypeID && t.Enabled == "1").CopyToDataTable();
        //        DataView dv = new DataView(tbl);
        //        dv.Sort = "Description, Specification ASC";
        //        tbl = dv.ToTable();
        //        foreach (DataRow drw in tbl.Rows)
        //        {
        //            string LuxuryID = drw["LuxuryID"].ToString();
        //            string LuxuryType = drw["LuxuryTypeID"].ToString(); ;
        //            string ProviderID = drw["ProviderID"].ToString(); ;
        //            string Description = drw["Description"].ToString(); ;
        //            string Capacity = drw["Capacity"].ToString(); ;
        //            string Specification = drw["Specification"].ToString(); ;
        //            string LuxuryPicture = UHopCore.GetPageURL() + "UploadedImages/Attachment/" + drw["LuxuryPicture"].ToString(); ;

        //            lstLuxuryDetails.Add(new LuxuryDetails
        //            {
        //                LuxuryID = LuxuryID,
        //                LuxuryType = LuxuryType,
        //                ProviderID = ProviderID,
        //                Description = Description,
        //                Capacity = Capacity,
        //                Specification = Specification,
        //                LuxuryPicture = LuxuryPicture
        //            });
        //        }
        //        return lstLuxuryDetails;
        //    }
        //}
        public static List<LuxuryDetails> GetLuxuryDetails(String pLuxuryID)
        {
            using (clsLuxury objLuxury = new clsLuxury())
            {
                List<LuxuryDetails> lstLuxuryDetails = new List<LuxuryDetails>();
                DataTable tbl = new DataTable();
                objLuxury.FillLuxury(objLuxury.TblLuxury);
                tbl = objLuxury.TblLuxury.Where(t => t.LuxuryID == pLuxuryID && t.Enabled == "1").CopyToDataTable();
                DataView dv = new DataView(tbl);
                dv.Sort = "Description, Specification ASC";
                tbl = dv.ToTable();
                foreach (DataRow drw in tbl.Rows)
                {
                    string LuxuryID = drw["LuxuryID"].ToString();
                    string LuxuryType = drw["LuxuryTypeID"].ToString(); ;
                    string ProviderID = drw["ProviderID"].ToString(); ;
                    string Description = drw["Description"].ToString(); ;
                    string Capacity = drw["Capacity"].ToString(); ;
                    string Specification = drw["Specification"].ToString(); ;
                    string strImg = string.IsNullOrEmpty(drw["LuxuryPicture"].ToString()) == false? drw["LuxuryPicture"].ToString(): "Default_Luxury.gif";
                    string LuxuryPicture = UHopCore.GetPageURL() + "UploadedImages/LuxuryModel/" + strImg;

                    lstLuxuryDetails.Add(new LuxuryDetails
                    {
                        LuxuryID = LuxuryID,
                        LuxuryType = LuxuryType,
                        ProviderID = ProviderID,
                        Description = Description,
                        Capacity = Capacity,
                        Specification = Specification,
                        LuxuryPicture = LuxuryPicture
                    });
                }
                return lstLuxuryDetails;
            }
        }

        protected void btnReserve_Click(object sender, EventArgs e)
        {
            using (clsLuxury objLuxury = new clsLuxury())
            {
                if (txtfirstname.Text != "" && txtlastname.Text != "" && txtEmail.Text != "" && txtMobileNo.Text != "" && txtDepatureDateTime.Text != "" && txtNumberOfPax.Text != "")
                {
                    ClsAutoNumber objAutoNumber = new ClsAutoNumber();
                    DataTable tbl = new DataTable();
                    objLuxury.FillLuxury(objLuxury.TblLuxury);
                    tbl = objLuxury.TblLuxury.Where(t => t.LuxuryTypeID == hdfLuxuryTypeID.Value).CopyToDataTable();
                    DataRow drwLuxuryBooking = objLuxury.TblLuxuryBooking.NewRow();
                    string strLuxuryBookingID = objAutoNumber.GenerateAutoNumber("LuxuryBookingID", "000000").ToString();
                    drwLuxuryBooking.SetString("LuxuryBookingID", strLuxuryBookingID);
                    
                    string strLuxuryID = "";
                    if (hdfLuxuryTypeID.Value == "20151117000003") { strLuxuryID = ddlPlaneLuxury.SelectedValue.ToString(); }
                    else if (hdfLuxuryTypeID.Value == "20151117000001") { strLuxuryID = ddlChopperLuxury.SelectedValue.ToString(); }
                    else if (hdfLuxuryTypeID.Value == "20151117000002") { strLuxuryID = ddlJetskiLuxury.SelectedValue.ToString(); }
                    else if (hdfLuxuryTypeID.Value == "20151117000004") { strLuxuryID = ddlYachtLuxury.SelectedValue.ToString(); }
                        
                    drwLuxuryBooking.SetString("LuxuryID", strLuxuryID);
                    drwLuxuryBooking.SetString("FirstName", txtfirstname.Text);
                    drwLuxuryBooking.SetString("LastName", txtlastname.Text);
                    drwLuxuryBooking.SetString("MiddleName", txtmiddlename.Text);
                    drwLuxuryBooking.SetString("Email", txtEmail.Text);
                    drwLuxuryBooking.SetString("MobileNumber", txtMobileNo.Text);
                    if (txtLandNo.Text.Trim() != "")
                    {
                        drwLuxuryBooking.SetString("LandlineNumber", txtLandNo.Text);
                    }
                    drwLuxuryBooking.SetDateTime("DepartureDatetime", Convert.ToDateTime(txtDepatureDateTime.Text));
                    drwLuxuryBooking.SetInt16("NumberOfPax", Convert.ToInt16(txtNumberOfPax.Text.Replace('-','.')));
                    if (txtRemarks.Text.Trim() != "")
                    {
                        drwLuxuryBooking.SetString("Remarks", txtRemarks.Text);
                    }
                    drwLuxuryBooking.SetString("Status", "0");
                    drwLuxuryBooking.SetString("IsVerify", "0");
                    drwLuxuryBooking.SetDateTime("CreatedOn", DateTime.Now);
                    objLuxury.TblLuxuryBooking.Rows.Add(drwLuxuryBooking);
                    objLuxury.UpdateLuxuryBooking();

                    objLuxury.FillLuxuryType(objLuxury.TblLuxuryType);
                    string ServiceType = objLuxury.GetLuxuryTypeDetails(hdfLuxuryTypeID.Value,"Description");
                    string strLuxuryDescription  = objLuxury.GetLuxuryDetails(strLuxuryID,"Description");
                    string strLuxurySpecification = objLuxury.GetLuxuryDetails(strLuxuryID,"Specification");
                    DateTime dtBookingDate = DateTime.Now;
                    DateTime dtDepartureDate = Convert.ToDateTime(txtDepatureDateTime.Text);
                    string strLink = UHopCore.GetPageURL() + "LuxuryConfirmation.aspx?ULUX=" + ClsEncryptor.Encrypt(strLuxuryBookingID,"uLUX");
                    string strRemarks = txtRemarks.Text == "" ? "N/A" : txtRemarks.Text;
                    ClsSendMail objSendMail = new ClsSendMail();
                    objSendMail.SendMailLuxuryConfirm(txtEmail.Text, txtfirstname.Text + " " + txtlastname.Text, Convert.ToString(dtBookingDate.ToString("MMM dd, yyyy hh:mm:ss tt")), Convert.ToString(dtDepartureDate.ToString("MMM dd, yyyy hh:mm:ss tt")), ServiceType, strLuxuryDescription, strLuxurySpecification, txtNumberOfPax.Text, strLink, strLink, strRemarks);
                    spnEmail.InnerText = txtEmail.Text.ToLower().Trim();
                    DivLuxuryCover.Style["display"] = "block";

                    txtDepatureDateTime.Text = ""; txtfirstname.Text = ""; txtLandNo.Text = ""; txtNumberOfPax.Text = "";
                    txtEmail.Text = ""; txtlastname.Text = ""; txtmiddlename.Text = ""; txtMobileNo.Text = ""; txtRemarks.Text = "";
                }

            }
        }

        public void LoadLuxuryType()
        { 
        using (clsLuxury objLuxury = new clsLuxury())
        {
            DataTable tbl = new DataTable();
            objLuxury.FillLuxury(objLuxury.TblLuxury);
            tbl = objLuxury.TblLuxury.Where(t => t.LuxuryTypeID == "20151117000003" && t.Enabled == "1").CopyToDataTable();
            DataView dv = new DataView(tbl);
            dv.Sort = "Description, Specification ASC";
            tbl = dv.ToTable();
            tbl.Columns.Add("Descriptions", typeof(string), "Description + ' - [' + Specification + ']'");
            ddlPlaneLuxury.DataSource = tbl;
            ddlPlaneLuxury.DataTextField = "Descriptions";
            ddlPlaneLuxury.DataValueField = "LuxuryID";
            ddlPlaneLuxury.DataBind();

            tbl = objLuxury.TblLuxury.Where(t => t.LuxuryTypeID == "20151117000001" && t.Enabled == "1").CopyToDataTable();
            dv = new DataView(tbl);
            dv.Sort = "Description, Specification ASC";
            tbl = dv.ToTable();
            tbl.Columns.Add("Descriptions", typeof(string), "Description + ' - [' + Specification + ']'");
            ddlChopperLuxury.DataSource = tbl;
            ddlChopperLuxury.DataTextField = "Descriptions";
            ddlChopperLuxury.DataValueField = "LuxuryID";
            ddlChopperLuxury.DataBind();

            tbl = objLuxury.TblLuxury.Where(t => t.LuxuryTypeID == "20151117000002" && t.Enabled == "1").CopyToDataTable();
            dv = new DataView(tbl);
            dv.Sort = "Description, Specification ASC";
            tbl = dv.ToTable();
            tbl.Columns.Add("Descriptions", typeof(string), "Description + ' - [' + Specification + ']'");
            ddlJetskiLuxury.DataSource = tbl;
            ddlJetskiLuxury.DataTextField = "Descriptions";
            ddlJetskiLuxury.DataValueField = "LuxuryID";
            ddlJetskiLuxury.DataBind();

            tbl = objLuxury.TblLuxury.Where(t => t.LuxuryTypeID == "20151117000004" && t.Enabled == "1").CopyToDataTable();
            dv = new DataView(tbl);
            dv.Sort = "Description, Specification ASC";
            tbl = dv.ToTable();
            tbl.Columns.Add("Descriptions", typeof(string), "Description + ' - [' + Specification + ']'");
            ddlYachtLuxury.DataSource = tbl;
            ddlYachtLuxury.DataTextField = "Descriptions";
            ddlYachtLuxury.DataValueField = "LuxuryID";
            ddlYachtLuxury.DataBind();
        }
        }      

    }
}