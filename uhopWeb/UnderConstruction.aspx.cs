﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;


namespace uhopWeb
{
    public partial class UnderConstruction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clsAuthenticator.Authenticate();
                UHopCore.Title = "Under Construction";
                string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                ViewState["MemberID"] = strDecrypted;
                using (ClsMembers objMembers = new ClsMembers(strDecrypted))
                {
                    lblNavBarName.Text = objMembers.GetMemberDetails("FirstName");
                    lblNavBarName2.Text = objMembers.GetMemberDetails("FirstName");
                }
            }

        }
    }
}