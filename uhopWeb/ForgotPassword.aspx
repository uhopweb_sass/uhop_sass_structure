﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="uhopWeb.Forgot" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" id="forgotBg">
<head runat="server">
    <link rel="shortcut icon" href="Images/uhop_icon_beta.png" type="image/png" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" /> 
    <link href="Content/materialize/css/materialize.min.css" rel="stylesheet" />
    <link href="Content/wow/animate.min.css" rel="stylesheet" />
    <link href="Styles/global.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    <link href="Styles/media.min.css" rel="stylesheet" />
    <title>Reset your password</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="sectionReset center">
            <div class="wow flash animated" data-wow-iteration="infinite" data-wow-duration="10s">
                <a href="Index.aspx"><img class="responsive-img wow pulse animated" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="3s" src="Images/u-hop_logo.png" alt="u-Hop" height="300" width="400"/></a>
            </div>
           <div class="row white-text">
                <div class="col s12">
                    <div class="divider"></div>
                    <h3>Find your account</h3>
                    <p class="center">Type in your email address and we will send you a link to reset your password.</p>
                    <br />
                </div>
                <div class="input-field col l8 offset-l2 s12">
                    <i class="large material-icons prefix"><i class="material-icons">&#xE0BE;</i></i>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass-="large validate white-text" type="email" Text=""></asp:TextBox>
                    <label for="email" data-error="wrong" data-success="right">Email</label>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" SetFocusOnError="true" ForeColor="Red" ErrorMessage="Email Address is required!"></asp:RequiredFieldValidator>
                               <span id="spnForgot" runat="server" class="red-text" style="display:none">Invalid Email Address!</span>
               <span id="spnResetRequestSent" class="green-text" runat="server" style="display:none;">Reset password request successfully sent to your email!<br /> Kindly check your email.</span>
                </div>

            </div>
            <div class="row">
                <asp:Button ID="btnSendEmail" class="btn btn-danger btn btn-large blue darken-2 white-text" runat="server" Text="Send Reset Password" OnClick="btnSendEmail_Click" />
                <br /><br />
                <a href="Login.aspx?UT=<% string qstr = Request.QueryString["UT"]; var qstring = qstr; Response.Write(qstring); %>">Back to u-Hop login</a>
            </div>
        </div>
    </form>

     <script src="Scripts/jquery-2.1.4.min.js"></script>
    <script src="Scripts/materialize/materialize.min.js"></script>
    <script src="Scripts/wow/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
</body>
</html>
