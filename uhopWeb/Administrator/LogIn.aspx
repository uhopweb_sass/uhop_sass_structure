﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogIn.aspx.cs" Inherits="uhopWeb.Administrator.LogIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Administrator Log-In</title>

    <link rel="shortcut icon" href="../Images/uhop_icon_beta.png" type="image/png" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <!--<link href="Styles/index.css" rel="stylesheet" />-->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css' />
    <asp:PlaceHolder ID="PlaceHolder2" runat="server">
        <%: Styles.Render("~/bundle/AdminLoginStyle") %>
    </asp:PlaceHolder>
</head>
<body id="adminBG">
    <form id="form1" runat="server">
        <table>
            <tr>
                <td>
                    <div class="container white-text">
                        <br /><br />
                        <div class="row">
                            <div class="input-field col s12 l6">
                                <asp:TextBox ID="txtUserName" runat="server" CssClass="validate"></asp:TextBox>
                                <label for="txtUserName">User Name</label>
                            </div>
                            <div class="input-field col s12 l6">
                                <asp:TextBox ID="txtPassword" runat="server" CssClass="validate" TextMode="Password"></asp:TextBox>
                                <label for="txtPassword">password</label>
                            </div>
                        </div>
                        <div class="row" style="text-align:center">
                            <asp:LinkButton ID="btnLogIn" CssClass="waves-effect waves-light btn blue darken-2" runat="server">Log-In</asp:LinkButton>
                            <a href="#" class="btn pink accent-2">Cancel</a>
                        </div>
                    </div>
                </td>
            </tr>
        </table>


    </form>

    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/AdminLoginScript") %>
    </asp:PlaceHolder>
</body>

</html>
