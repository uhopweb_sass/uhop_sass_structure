﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Luxury.aspx.cs" Inherits="uhopWeb.Luxury" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Images/uhop_icon_beta.png" type="image/png"/>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320"/> 
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <asp:PlaceHolder ID="PlaceHolder2" runat="server">
        <%: Styles.Render("~/bundle/style") %>
    </asp:PlaceHolder>
    <link href="Styles/index.min.css" rel="stylesheet" />
    <link href="Styles/jquery.datetimepicker.css" rel="stylesheet" />
    <link href="Styles/luxury.css" rel="stylesheet" />
     <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    <!--<link href="Styles/ninja-slider.css" rel="stylesheet" type="text/css" />-->

    <title>u-Hop Transport Network Vehicle System Inc.</title>

</head>
<body>
    <!-- Successfully Reservation Popup -->
    <div class="divCover LuxuryComplete" id="DivLuxuryCover" runat="server"  style="display: none;margin-top:-20px; background-color:black; opacity:.9; z-index:99999;">
    <div style="max-width: 100%; min-height: 100%; width: 100%; text-align: center; color: white; margin-left: auto; margin-right: auto">
        <br /><br /><br /><br /><br /><br />
        <h5 style="color: green"><b>You have successfully finished your request!</b><br />
            <span style="color: orange; font-size: 20px;">We've sent a confirmation in your email <span class="pink-text" runat="server" id="spnEmail"></span>. Please validate your request account by clicking the link we've provided in your mail. Thank you.</span></h5>
        <a href="Luxury.aspx" class="btn waves-effect waves-light pink">Close &nbsp;<i class="material-icons right" style="margin:0;">&#xE5CD;</i></a>
    </div>
    </div>
    <!-- End of Successfully Reservation Popup  -->
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" EnablePageMethods="true" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server"></asp:UpdatePanel>
        <asp:HiddenField ID="hdfLuxuryTypeID" runat="server" />
        <asp:HiddenField ID="hdfLux" runat="server" />
      <!-- Dropdown Structure for navbar -->
           <div class="navbar-fixed">
                <nav class="transparent" role="navigation" id="indexNav" style="top:0;left:0;border-bottom: 2px solid transparent;">
                    <div class="nav-wrapper">
                        <ul class="right hide-on-med-and-down">
                            <li class="currentActive"><a href="Index.aspx"><i class="material-icons">&#xE88A;</i></a></li>
                        </ul>
                        <ul class="left show-on-medium-and-down hide-on-large-only">
                            <li class="currentActive" id="homeIcon"><a href="Index.aspx"><i class="material-icons">&#xE88A;</i></a></li>
                        </ul>

                        <ul class="left hide-on-med-and-down">
                            <li><a href="Index.aspx"><img class="responsive-img left" src="Images/u-hop_logo.png" alt="Alternate Text" width="250" height="200"/></a></li>
                        </ul>

                    </div>
                </nav>
            </div>
            <!-- END OF NAVBAR -->


      <!-- Modal Structure -->
      <div id="modalPlane" class="modal">
          <div class="modal-content">
              <div class="row noMargin">
                  <div class="col l6 s12">
                    <h4 id="luxuryTextHeader"><i class="material-icons medium luxuryIcon">&#xE55F;</i> &nbsp; Reserve a <span id="spnTypeofService" class="spnTypeofService"></span></h4>
                  </div>
                  <div class="col l6 s12 right-align luxuryImg">
                      <img id="imgModal" class="imgModal responsive-img materialboxed"  style="height:100px;width:200px;"/>
                  </div>
                  <form class="col s12">
                      <div class="row">
                          <div class="col l8 s10 offset-s1">
                              <label><span id="Span1" class="spnTypeofService"></span>Model</label><br />
                              <asp:DropDownList ID="ddlPlaneLuxury" runat="server" CssClass="ddlPlaneLuxury dropdown-button btn left blue darken-1"></asp:DropDownList>
                              <asp:DropDownList ID="ddlChopperLuxury" runat="server" CssClass="ddlChopperLuxury dropdown-button btn left blue darken-1"></asp:DropDownList>
                              <asp:DropDownList ID="ddlJetskiLuxury" runat="server" CssClass="ddlJetskiLuxury dropdown-button btn left blue darken-1"></asp:DropDownList>
                              <asp:DropDownList ID="ddlYachtLuxury" runat="server" CssClass="ddlYachtLuxury dropdown-button btn left blue darken-1"></asp:DropDownList>
                          </div>
                      </div>

                      <div class="row">
                          <div class="input-field col l4 s10 offset-s1">
                              <asp:TextBox ID="txtfirstname" CssClass="large validate text-color" Style="text-transform: capitalize;" runat="server" MaxLength="30" autocomplete="off"></asp:TextBox>
                              <%--<input id="first_name" runat="server" type="text" style="text-transform: capitalize;" />--%>
                              <label for="txtfirstname">First Name</label>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="First name is required" SetFocusOnError="true" ControlToValidate="txtfirstname" Display="Dynamic" ValidationGroup="LuxuryReserve" Text="First name is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                          </div>
                          <div class="input-field col l4 s10 offset-s1">
                              <asp:TextBox ID="txtlastname" CssClass="large validate text-color" Style="text-transform: capitalize;" runat="server" MaxLength="30" autocomplete="off"></asp:TextBox>
                              <%--<input id="last_name" runat="server" type="text" style="text-transform: capitalize;" />--%>
                              <label for="txtlastname">Last Name</label>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Last name is required" SetFocusOnError="true" ControlToValidate="txtlastname" Display="Dynamic" ValidationGroup="LuxuryReserve" Text="Last name is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                          </div>
                          <div class="input-field col l4 s10 offset-s1">
                              <asp:TextBox ID="txtmiddlename" CssClass="large validate text-color" Style="text-transform: capitalize;" runat="server" MaxLength="30" autocomplete="off"></asp:TextBox>
                              <%--<input id="middle_name" runat="server" type="text" style="text-transform: capitalize;" />--%>
                              <label for="txtmiddlename">Middle Name</label>
                          </div>
                      </div>
                      <div class="row">
                          <div class="input-field col l4 s10 offset-s1">
                              <asp:TextBox ID="txtEmail" CssClass="large validate text-color" runat="server" MaxLength="300" autocomplete="off"></asp:TextBox>
                              <%--<input id="Email_add" runat="server" type="email" />--%>
                              <label for="txtEmail">E-mail</label>
                              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid email" ValidationExpression='^[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+(\.[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$' SetFocusOnError="true" ControlToValidate="txtEmail" Text="Invalid email" Display="Dynamic" ValidationGroup="LuxuryReserve" CssClass="ValidationSummary"></asp:RegularExpressionValidator>&nbsp;
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="E-mail is required" SetFocusOnError="true" ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="LuxuryReserve" Text="E-mail is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                          </div>
                          <div class="input-field col l4 s5 offset-s1">
                              <asp:TextBox ID="txtMobileNo" CssClass="large validate text-color" runat="server" MaxLength="30" autocomplete="off"></asp:TextBox>
                              <%--<input id="mobile_no" runat="server" type="text" />--%>
                              <label for="txtMobileNo">Mobile #</label>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Mobile # is required" SetFocusOnError="true" ControlToValidate="txtMobileNo" Display="Dynamic" ValidationGroup="LuxuryReserve" Text="Mobile # is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                          </div>
                          <div class="input-field col l4 s5">
                              <asp:TextBox ID="txtLandNo" CssClass="large validate text-color" runat="server" MaxLength="30" autocomplete="off"></asp:TextBox>
                              <%--<input id="landline_no" runat="server" type="text" />--%>
                              <label for="txtLandNo">Landline #</label>
                          </div>
                      </div>
                      <div class="row">
                          <div class="input-field col l4 s10 offset-s1">
                              <asp:TextBox ID="txtDepatureDateTime" CssClass="large validate text-color" runat="server" MaxLength="16" autocomplete="off"></asp:TextBox>
                              <%--<input id="departure_date" runat="server" type="date" class="datepicker" />--%>
                              <label for="txtDepatureDateTime">Departure Date</label>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Departure Date is required" SetFocusOnError="true" ControlToValidate="txtDepatureDateTime" Display="Dynamic" ValidationGroup="LuxuryReserve" Text="Departure Date is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                          </div>
                          <div class="input-field col l4 s10 offset-s1">
                              <asp:TextBox ID="txtNumberOfPax" CssClass="large validate text-color" runat="server" MaxLength="2" autocomplete="off"></asp:TextBox>
                              <%--<input id="number_of_Pax" runat="server" type="text" />--%>
                              <label for="txtNumberOfPax">Number of pax</label>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Number of Pax is required" SetFocusOnError="true" ControlToValidate="txtNumberOfPax" Display="Dynamic" ValidationGroup="LuxuryReserve" Text="Number of Pax is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                          </div>
                      </div>
                      <div class="row">
                          <div class="input-field col s10 l12 offset-s1">
                              <asp:TextBox ID="txtRemarks" CssClass="large validate text-color" runat="server" MaxLength="800" autocomplete="off"></asp:TextBox>
                              <label for="txtRemarks">Remarks</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>

        <div class="modal-footer"> 
            <div class="divider"></div>  
            <div class="col s12">
                <%--<a runat="server" id="btnReserve" onserverclick="btnReserve_Click" validationgroup="LuxuryReserve" class="modal-action waves-effect waves-green btn blue darken-2">Reserve Now!</a>--%> 
                <a href="#!" class=" modal-action modal-close waves-effect waves-green btn red darken-2" style="margin-left:5px;">Cancel</a> &nbsp;
                <asp:LinkButton ID="btnReserve" runat="server" class="modal-action waves-effect waves-green btn blue darken-2" ValidationGroup="LuxuryReserve" OnClick="btnReserve_Click">Reserve Now!</asp:LinkButton>                    
            </div>
        </div>
      </div>
        
        <div class="slider fullscreen">
        <ul class="slides">
          <li class="sliderContent">
            <img src="Images/u-hop_plane-min.png"  class="responsive-img"/>
            <div class="caption center-align">
              <h2>u-Hop Plane</h2>
              <%--<h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>--%>
              <h5><b><span class="pink-text accent-2">u</span>-<span class="blue-text darken-2">Fly</span></b> in comfort and luxuries. Your most affordable charter plane. Over 10 fleet to choose from.</h5>
              <%--<a href="#modalPlane" runat="server" id="aPlane" class="LuxuryPlane modal-trigger" style="display:none;">Reserve Now!</a>--%>
                <a id="A1" href="#modalPlane" runat="server" class="LuxuryPlane btn pink accent-2 waves-effect waves-light modal-trigger wow tada" data-wow-delay="10000ms">Reserve Now!</a>
              <%--<asp:LinkButton ID="LinkButton1" runat="server" class="LuxuryPlane btn pink accent-2 waves-effect waves-light wow tada" data-wow-delay="1000ms" OnClick="btnLuxuryType_Click">Reserve Now!</asp:LinkButton>--%>
            </div>
          </li>
          <li class="sliderContent">            
            <img src="Images/u-hop_chopper2-min.jpg" />
            <div class="caption left-align">
              <h2>u-Hop Chopper</h2>
              <h5 class="light grey-text text-lighten-3"><b><span class="pink-text accent-2">u</span>-<span class="blue-text darken-2">Glide</span></b> on Chopper wherever and whenever. Newest range of choppers from 3-6 seaters. Arrived in style.</h5>
              <a href="#modalPlane" runat="server" class="LuxuryChopper btn pink accent-2 waves-effect waves-light modal-trigger wow tada" data-wow-delay="10000ms">Reserve Now!</a>
            </div>
          </li>
          <li class="sliderContent">            
            <img src="Images/u-hop_yatch-min.png" />
            <div class="caption right-align">
              <h2>u-Hop Yacht</h2>
              <h5 class="light grey-text text-lighten-3"><b><span class="pink-text accent-2">u</span>-<span class="blue-text darken-2">Cruise</span></b> to your sea adventure, Yacht Party or romantic sunset dinner.</h5>
              <a href="#modalPlane" runat="server" class="LuxuryYacht btn pink accent-2 waves-effect waves-light modal-trigger wow tada" data-wow-delay="19000ms">Reserve Now!</a>
            </div>
          </li>
          <li class="sliderContent">            
            <img src="Images/u-hop_JetSki-min.png" />
            <div class="caption center-align">
              <h2>u-Hop Jet Ski</h2>
              <h5 class="light grey-text text-lighten-3"><b><span class="pink-text accent-2">u</span>-<span class="blue-text darken-2">Speed</span></b> on sea adventure anytime and anywhere. Water sport gears and recreational equipment. Rent on-demand.</h5>
              <a href="#modalPlane" runat="server" class="LuxuryJetski btn pink accent-2 waves-effect waves-light modal-trigger wow tada" data-wow-delay="27000ms" >Reserve Now!</a>
            </div>
          </li>
        </ul>

      </div>

    </form>
    
    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/script") %>
    </asp:PlaceHolder>
        
    <script src="Scripts/jquery.datetimepicker.full.js"></script>
    <script type="text/javascript" src="Scripts/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
        var MinDate = new Date();
        var ToDate = new Date();
        MinDate.setDate(MinDate.getDate() + 1);

        var dateToDisable = new Date();
        dateToDisable.setDate(dateToDisable.getDate());

        $('#txtDepatureDateTime').datetimepicker({
        lang: 'en',
        minDate: MinDate
        });

        //$('#txtDepatureDateTime').datetimepicker({ value: dateToDisable });

        //$('#txtDepatureDateTime').datetimepicker({
        //    beforeShowDay: function(date) {
        //        if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
        //            return [false, ""]
        //        }

        //        return [true, ""];
        //    }
        //});
               
        $(document).keyup(function (e) {
            if (e.keyCode == 27) { 
                $('#txtDepatureDateTime').datetimepicker('hide');
            }
        });

        $('#txtDepatureDateTime').val("");
        
        $("#txtDepatureDateTime").focusout(function () {
            $('#txtDepatureDateTime').datetimepicker('hide');
        });

    </script>





    <script>
        /* WOW.js Initialization */
        new WOW().init();
        /* END of WOW.js initialization */
        //$('.datepicker').pickadate({
        //    selectMonths: true, // Creates a dropdown to control month
        //    selectYears: true,
        //    min: 1,
        //    max: 10000
        //});

        $('#btnReserve').click(function () {
            var blnreturn = true;
            $.each(Page_Validators, function (index, validator) {
                if (!validator.isvalid) {
                    //$(validator).closest("div").css({ "display": "block" });
                }
            });
            return blnreturn;
        });

        //Letters Only
        $("#txtfirstname,#txtlastname,#txtmiddlename").keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a letter and stop the keypress
            if ((e.keyCode < 65 || e.keyCode > 90) && (e.keyCode < 97 || e.keyCode > 122)) {
                e.preventDefault();
            }
        });
        //End Letters Only
        
        
        //Numbers Only (Mobile, Telephone) Validation
        $("#txtMobileNo,#txtLandNo,#txtNumberOfPax").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if (e.keyCode == 189 || e.keyCode == 109) {
                return;
            }
            else if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
                // Ensure that it is a number and stop the keypress
            else {
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }

        });
        //End Validation

        function AdjustWidth(ddl) {
            var maxWidth = 0;
            for (var i = 0; i < ddl.length; i++) {
                if (ddl.options[i].text.length > maxWidth) {
                    maxWidth = ddl.options[i].text.length;
                }
            }
            ddl.style.width = maxWidth * 8 + "px";
            //-->
        }

        var LuxID;
        function LoadLuxury() {
            $.ajax({
                type: "POST",
                url: "Luxury.aspx/GetLuxuryDetails",
                data: "{'pLuxuryID':'" + LuxID + "'}",
                contentType: "application/json",
                dataType: "json",
                success: function (response) {
                    try {
                        // var res = response.d;
                        $(".imgModal").attr('src', response.d[0].LuxuryPicture);

                    }
                    catch (e) {
                        alert(e);
                    }
                }
            });
        }


        $(".LuxuryPlane").click(function () {
            $(".spnTypeofService").text("Plane");
            $("input[id*='hdfLuxuryTypeID']").val("20151117000003");
           //$(".spnTypeofServiceIco").text("&#xE539;");
            $("#ddlPlaneLuxury").css("display", "block");
            $("#ddlChopperLuxury,#ddlJetskiLuxury,#ddlYachtLuxury").css("display", "none");
            LuxID = $("#ddlPlaneLuxury").find(":selected").val();
            LoadLuxury();
        });

        $(".LuxuryChopper").click(function () {
            $(".spnTypeofService").text("Chopper");
            $("input[id*='hdfLuxuryTypeID']").val("20151117000001");
           // $(".spnTypeofServiceIco").text("&#xE539;");
            $("#ddlChopperLuxury").css("display", "block");
            $("#ddlPlaneLuxury,#ddlJetskiLuxury,#ddlYachtLuxury").css("display", "none");
            LuxID = $("#ddlChopperLuxury").find(":selected").val();
            LoadLuxury();
        });

        $(".LuxuryYacht").click(function () {
            $(".spnTypeofService").text("Yacht");
            $("input[id*='hdfLuxuryTypeID']").val("20151117000004");
           // $(".spnTypeofServiceIco").text("&#xE532;");
            $("#ddlYachtLuxury").css("display", "block");
            $("#ddlChopperLuxury,#ddlJetskiLuxury,#ddlPlaneLuxury").css("display", "none");
            LuxID = $("#ddlYachtLuxury").find(":selected").val();
            LoadLuxury();
        });

        $(".LuxuryJetski").click(function () {
            $(".spnTypeofService").text("Jet Ski");
            $(".spnTypeofServiceIco").text("&#xE532;");
           // $("input[id*='hdfLuxuryTypeID']").val("20151117000002");
            $("#ddlJetskiLuxury").css("display", "block");
            $("#ddlChopperLuxury,#ddlPlaneLuxury,#ddlYachtLuxury").css("display", "none");
            LuxID = $("#ddlJetskiLuxury").find(":selected").val();
            LoadLuxury();

        });


        $(".ddlPlaneLuxury,.ddlChopperLuxury,.ddlJetskiLuxury,.ddlYachtLuxury").change(function () {
            var pLuxuryID = $(this).find(":selected").val();
            $.ajax({
                type: "POST",
                url: "Luxury.aspx/GetLuxuryDetails",
                data: "{'pLuxuryID':'" + pLuxuryID + "'}",
                contentType: "application/json",
                dataType: "json",
                success: function (response) {
                    try {
                       // var res = response.d;
                       $(".imgModal").attr('src', response.d[0].LuxuryPicture);
                        
                    }
                    catch (e) {
                        alert(e);
                    }
                }
            });
        });

        $(document).ready(function () {

            $('.slider').slider({ full_width: true });
            var qrystr = "<%= Request.QueryString["LT"]%>";
            if (qrystr != "") {
               $(".Luxury" + qrystr).click();
            }
        });
    </script>
</body>
</html>
