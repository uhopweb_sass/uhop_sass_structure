﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using uhopWeb.Classes;

namespace uhopWeb
{
    public partial class LuxuryConfirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["ULUX"] != null)
                    {
                        
                        string strID = ClsEncryptor.Decrypt(Request.QueryString["ULUX"].Replace(" ", "+"), "uLUX");
                        if (strID != "")
                        {
                            using (clsLuxury objLuxury = new clsLuxury())
                            {
                                if (ViewState["TblLuxuryBooking"] == null)
                                {
                                    objLuxury.FillLuxuryBooking(strID);
                                    ViewState["TblLuxuryBooking"] = objLuxury.TblLuxuryBooking;
                                }

                                objLuxury.TblLuxuryBooking = (DataSets.DSConfigs.LuxuryBookingDataTable)ViewState["TblLuxuryBooking"];

                                DataRow drw = objLuxury.TblLuxuryBooking.Rows[0];
                                if (!drw.IsNull("LuxuryBookingID"))
                                {
                                    if (drw.GetString("IsVerify") == "0")
                                    {
                                        drw.BeginEdit();
                                        drw["IsVerify"] = 1;
                                        drw.EndEdit();
                                        objLuxury.UpdateLuxuryBooking();

                                        string strRecipient = "";
                                        objLuxury.FillEmailRecipient();
                                        DataTable tbl = objLuxury.TblEmailRecipient.Where(t => t.Enabled == "1").CopyToDataTable();
                                        foreach (DataRow dr in tbl.Rows)
                                        {
                                            strRecipient += dr["Email"].ToString() + ";";
                                        }
                                        string strMobileNumber = drw["MobileNumber"].ToString();
                                        string strFullname = drw["FirstName"].ToString() + " " + drw["LastName"].ToString();
                                        string strEmail = drw["Email"].ToString();
                                        string strLandlineNumber = drw["LandlineNumber"].ToString() == "" ? "N/A" : drw["LandlineNumber"].ToString();
                                        objLuxury.FillLuxuryType(objLuxury.TblLuxuryType);
                                        objLuxury.FillLuxury(objLuxury.TblLuxury);
                                        string strLuxuryTypeID = objLuxury.GetLuxuryDetails(drw["LuxuryID"].ToString(), "LuxuryTypeID");
                                        string strServiceType = objLuxury.GetLuxuryTypeDetails(strLuxuryTypeID, "Description");
                                        string strDescription = objLuxury.GetLuxuryDetails(drw["LuxuryID"].ToString(), "Description") + " - [" + objLuxury.GetLuxuryDetails(drw["LuxuryID"].ToString(), "Specification") + "]";
                                        DateTime dtDepartureDate = Convert.ToDateTime(drw["DepartureDateTime"].ToString());
                                        string strRemarks = drw["Remarks"].ToString() == "" ? "N/A" : drw["Remarks"].ToString();
                                        ClsSendMail objSendMail = new ClsSendMail();
                                        objSendMail.SendMailLuxuryReservation(strRecipient, strFullname, strMobileNumber, strLandlineNumber, strServiceType, strDescription, Convert.ToString(dtDepartureDate.ToString("MMM dd, yyyy hh:mm:ss tt")), drw["NumberOfPax"].ToString(), strRemarks, strEmail);

                                        spnTop.InnerHtml = "Thank you for confirming your reservation!";
                                        spnBottom.InnerHtml = "Please wait for a call or E-mail from us!";

                                    }
                                    else
                                    {
                                        spnTop.InnerHtml = "You're requested service is already confirmed!";
                                        spnBottom.InnerHtml = "Please wait for a call or E-mail from us!";
                                    }
                                }
                            }

                            
                        }
                    }
                }
            }
            catch
            { 
                Response.Redirect("luxury.aspx"); 
            }
        }
    }
}