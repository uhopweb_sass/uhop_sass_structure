﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;
using System.Data;

namespace uhopWeb
{
    public partial class EmailVerification : System.Web.UI.Page
    {
        public static string strUserType;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
            if (!IsPostBack)
            {
                if (Request.QueryString["ND54662"] != null)
                {

                    using (ClsMembers objMembers = new ClsMembers())
                    {

                        string strID = ClsEncryptor.Decrypt(Request.QueryString["ND54662"].Replace(" ", "+"), "uh0p");
                        objMembers.FillByEmail(strID);


                        if (!objMembers.TblMembers.Rows[0].IsNull("Email"))
                        {
                            if (objMembers.TblMembers.Rows[0].GetString("Isverified") == "0")
                            {
                                if (objMembers.TblMembers.Rows[0].GetString("IsOperator") == "1")
                                {
                                    lnklogin.HRef = "Login.aspx?UT=Operator";
                                }
                                else
                                {
                                    lnklogin.HRef = "Login.aspx?UT=Member";
                                }
                                objMembers.TblMembers.Rows[0].BeginEdit();
                                objMembers.TblMembers.Rows[0]["IsVerified"] = 1;
                                objMembers.TblMembers.Rows[0].EndEdit();
                                objMembers.Update();

                                spnTop.InnerHtml = "Alright! You're account was successfully verified!";
                                spnBottom.InnerHtml = "Thank you! You're now part of our family";

                            }
                            else
                            {
                                spnTop.InnerHtml = "You're Email is Already verified!";
                                spnBottom.InnerHtml = "You can now Proceed to Log in";
                                if (objMembers.TblMembers.Rows[0].GetString("IsOperator") == "1")
                                {
                                    lnklogin.HRef = "Login.aspx?UT=Operator";
                                }
                                else
                                {
                                    lnklogin.HRef = "Login.aspx?UT=Member";
                                }
                            }
                        }

                    }                    
                }
            }
        }          
        catch
            {
                Response.Redirect("index.aspx");
            }
        }
    }
}