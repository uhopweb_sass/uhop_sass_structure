﻿$(document).ready(function () {
    $('.lblFor').click(function () {
        var labelID = $(this).attr('id');
        $('#ContentPlaceHolder1_txt' + labelID).focus();
    });

    $('.lblDate').click(function () {
        var labelID = $(this).attr('id');
        $('#ContentPlaceHolder1_txt' + labelID).trigger("click");
    });

    $('#ContentPlaceHolder1_btnSubmit').click(function () {
        var blnreturn = true;

        $.each(Page_Validators, function (index, validator) {
            if (!validator.isvalid) {
                $(validator).closest("div").css({ "display": "block" });
            }

        });
        return blnreturn;
    });

    $("#ContentPlaceHolder1_txtMaxCap").keydown(function (e) {
        if ($.inArray(e.keyCode, [8, 9, 27, 13]) !== -1 ||
            // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up

            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || e.keyCode == 46) {
            e.preventDefault();
        }
    });

});