﻿

$(document).ready(function () {

    var dtNow = new Date();
    var hrNow = (dtNow.getHours() > 12) ? dtNow.getHours()-12 : dtNow.getHours();
    var minNow = dtNow.getMinutes();
    var ttNow = (dtNow.getHours() > 12) ? "PM" : "AM";


    $("#txthour,#txtMin").keypress(function (e) {
        var allowChar = "1234567890";
        var char = String.fromCharCode(e.keyCode);

        if (allowChar.indexOf(char) < 0)
        { return false;}
    });

    $("#txthour,#txtMin").keyup(function () {
        if ($(this).val().trim() == "") {
            if ($(this).attr("id") == "txthour") {
                $(this).val("01");
            }
            else {
                $(this).val("00");
            }
            $(this).select();
        }

        if ($(this).attr("id") == "txthour")
        {
            if (parseInt($(this).val()) > 12)
            { $(this).val("01"); $(this).select(); }
            else if(parseInt($(this).val())<1)
            { $(this).val("12"); $(this).select(); }
            
        }
        else
        {
            if (parseInt($(this).val()) > 59)
            { $(this).val("00"); $(this).select(); }
            else if (parseInt($(this).val()) < 0)
            { $(this).val("59"); $(this).select(); }
        }

        $("input[id*='hdfSelectedHr']").val($("#txthour").val());
        $("input[id*='hdfSelectedMin']").val($("#txtMin").val());

    });

    $("#txthour").val(((hrNow.toString().length > 1) ? hrNow.toString() : "0" + hrNow.toString()));
    $("#txtMin").val(((minNow.toString().length > 1) ? minNow.toString() : "0" + minNow.toString()));
    $("#divTT").html(ttNow);

    $("input[id*='hdfSelectedHr']").val($("#txthour").val());
    $("input[id*='hdfSelectedMin']").val($("#txtMin").val());
    $("input[id*='hdftt']").val($("#divTT").html());

    $("#tblTimePicker tr:first-child td:first-child div").mousedown(function (e) {
        var hour = parseInt($("#txthour").val());
        hour++;
        if (hour > 12)
        { hour = 1; }
        $("#txthour").val(((hour.toString().length > 1) ? hour.toString() : "0" + hour.toString()));
    });

    $("#tblTimePicker tr:first-child td:first-child div").mouseup(function () {
        $("input[id*='hdfSelectedHr']").val($("#txthour").val());
    });

    $("#tblTimePicker tr:first-child td:nth-child(2) div").mousedown(function () {
        var min = parseInt($("#txtMin").val());
        min++;
        if (min > 59)
        { min = 0; }
        $("#txtMin").val(((min.toString().length > 1) ? min.toString() : "0" + min.toString()));
    });

    $("#tblTimePicker tr:first-child td:nth-child(2) div").mouseup(function () {
        $("input[id*='hdfSelectedMin']").val($("#txtMin").val());
    });

    $("#tblTimePicker tr:first-child td:last-child div").mousedown(function () {
        var tt = $("#divTT").html().toString().toUpperCase();
        if (tt == "AM") {
            tt = "PM";
        }
        else { tt = "AM"; }
        parseInt($("#divTT").html(tt.toString()));
    });

    $("#tblTimePicker tr:first-child td:last-child div").mouseup(function () {
        $("input[id*='hdftt']").val($("#divTT").html());
    });






    $("#tblTimePicker tr:last-child td:first-child div").mousedown(function () {

        var hour = parseInt($("#txthour").val());
        hour--;
        if (hour < 1)
        { hour = 12; }

        $("#txthour").val(((hour.toString().length > 1) ? hour.toString() : "0" + hour.toString()));
    });

    $("#tblTimePicker tr:last-child td:first-child div").mouseup(function () {
        $("input[id*='hdfSelectedHr']").val($("#txthour").val());
    });



    $("#tblTimePicker tr:last-child td:nth-child(2) div").mousedown(function () {

        var min = parseInt($("#txtMin").val());
        min--;
        if (min < 0)
        { min = 59; }

        $("#txtMin").val(((min.toString().length > 1) ? min.toString() : "0" + min.toString()));
    });

    $("#tblTimePicker tr:last-child td:nth-child(2) div").mouseup(function () {
        $("input[id*='hdfSelectedMin']").val($("#txtMin").val());
    });


    $("#tblTimePicker tr:last-child td:last-child div").mousedown(function () {

        var tt = $("#divTT").html().toString().toUpperCase();

        if (tt == "AM") {
            tt = "PM";
        }
        else { tt = "AM"; }

        parseInt($("#divTT").html(tt.toString()));
    });

    $("#tblTimePicker tr:last-child td:last-child div").mouseup(function () {
        $("input[id*='hdftt']").val($("#divTT").html());
    });
});