﻿$(document).ready(function () {
    //datepicker for birthdate
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 101,
        selectMonths: true,
        max: -1
    });

    /* For dropdown button */
    $(".dropdown-button").dropdown({
        hover: true // Activates on hover
    });


    $('.button-collapse').sideNav(); // Side Nav Toggle

    /* For closing the sidenav by clicking on the image */
    function CloseSideNav() {
        $('#sidenav-overlay').velocity({ opacity: 0 }, {
            duration: 200, queue: false, easing: 'easeOutQuad',
            complete: function () {
                $(this).remove();
            }
        });
        $(".side-nav").animate({ left: '-250px' });
    }

    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal-trigger').leanModal();

    // Collapsible Uploading image list
    $('.collapsible').collapsible({
        accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
});