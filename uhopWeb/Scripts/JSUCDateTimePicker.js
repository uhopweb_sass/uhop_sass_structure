﻿var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var curMonthIndex = 0;
var curYearSelected = 0;
var hour = 1;
var min = 0;
var TT = "AM";
var selectedDate = new Date();

function ComputeNumberOfDaysInAMonth() {
    var noOfDays = 0;
    var year=parseInt($(".bYear").data("selected"));
    var month=parseInt($(".bMonth").data("selected"));

    var curDate = new Date(year, month, 1);
    month++;
    if (month > 11) { month = 0; year++; }

    var nextDate = new Date(year, month, 1);
    var monthLen = (nextDate - curDate) / (1000 * 60 * 60 * 24);

    return monthLen;
}

function PopulateTable()
{


    $("#tblCalendar tbody").html("<tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>");

    var year = parseInt($(".bYear").data("selected"));
    var month = parseInt($(".bMonth").data("selected"));
    var dayOfWeek = 0;
    var totalDays = 0;
    var xDate = new Date(year, month, 1);
    var td = "";
    var trTD = "";
    var minDate = new Date();
    var maxDate = new Date();

    totalDays = ComputeNumberOfDaysInAMonth();

    




    for(var day=1,row=0;day<totalDays;row++)
    {
        td = "";
        //For First Row Only
        for (var dw = 0; dw < 7 && row == 0; dw++) {
            if (dw >= xDate.getDay()) {
                td += "<td><div data-value=" + day + ">" + day + "</div></td>";
                day++;
            }
            else {
                td += "<td>&nbsp;</td>";
            }
            
        }
        //End

        for (var dw = 0;(dw < 7 && day<=totalDays) && row > 0; dw++) {
            td += "<td><div data-value="+ day +">" + day + "</div></td>";
            day++;
        }
        trTD += "<tr>"+td+"</tr>";
    }

    $("#tblCalendar tbody").append(trTD);


    $("#tblCalendar td div").click(function () {
        try
        {
            try
            {
                $("#tblCalendar td div").removeClass("selected");
                $(this).addClass("selected");
                selectedDate.setMonth(curMonthIndex);
                selectedDate.setDate($(this).data("value"));
                selectedDate.setFullYear(curYearSelected);
                $(".selectedDate").data("selected", selectedDate.getFullYear().toString() + "-" + (selectedDate.getMonth() + 1).toString() + "-" + selectedDate.getDate().toString() + " " + selectedDate.getHours().toString() + ":" + selectedDate.getMinutes().toString());
                $(".selectedDate").html(months[curMonthIndex] + " " + $(this).data("value") + ", " + curYearSelected.toString() +" " + ((hour.toString().length > 1) ? hour : "0" + hour) + ":" + ((min.toString().length > 1) ? min : "0" + min) + " " + TT);
            }
            catch (e) { alert(e);}
        }
        catch (e)
        { alert(e);}
    });
}

function PopulateMonthList() {
    var ul = "";

    for (var i = 0; i < 12; i++) {
        ul += "<li>" + months[i] + "</li>";
    }


    $(".MonthList").html(ul);
}

function ShowMonth(monthIndex) {
    $(".bMonth").html(months[monthIndex]);
    $(".bMonth").data("selected", (curMonthIndex).toString());

    PopulateTable();
}

function NextYear() {

    curYearSelected++;
    ShowYear(curYearSelected);
}

function PrevYear() {

    var dtNow=new Date();

    curYearSelected--;
    if (curYearSelected < dtNow.getFullYear())
    { curYearSelected = dtNow.getFullYear(); }

    ShowYear(curYearSelected);

}

function ShowYear() {
    $(".bYear").html(curYearSelected.toString());
    $(".bYear").data("selected", curYearSelected.toString());

    PopulateTable();
}

$(document).ready(function () {
    try
    {
        var dtNow = new Date();

        hour = (dtNow.getHours() > 12) ? dtNow.getHours() - 12 : dtNow.getHours();
        hour = (hour == 0) ? 12 : hour;
        min = dtNow.getMinutes();
        TT = (dtNow.getHours() > 11) ? "PM" : "AM";

        selectedDate.setFullYear(dtNow.getFullYear());
        selectedDate.setMonth(dtNow.getMonth());
        selectedDate.setDate(dtNow.getDate());
        selectedDate.setHours(dtNow.getHours());
        selectedDate.setMinutes(dtNow.getMinutes());

        curYearSelected = selectedDate.getFullYear();
        ShowYear();

        curMonthIndex = selectedDate.getMonth();
        ShowMonth(curMonthIndex);

        ShowYear();
        PopulateMonthList();

        $(".bMonth").click(function () {
            var parentList = $(".MonthList").parent();

            if ($(parentList).data("toggle").trim() != "") {
                $(parentList).addClass("hide");
                $(parentList).data("toggle", "");
            }
            else {
                $(parentList).removeClass("hide");
                $(parentList).data("toggle", "toggle");
            }
        });

        $(".MonthList > li").click(function () {
            curMonthIndex=$(this).index().toString();
            var parentList = $(".MonthList").parent();
            $(".bMonth").data("selected", curMonthIndex);
            $(".bMonth").html(months[curMonthIndex]);
            $(parentList).addClass("hide");
            $(parentList).data("toggle", "");

            ShowMonth(curMonthIndex);        
        });


        $("#divHour").html((hour.toString().length>1)?hour.toString():"0"+hour.toString());
        $("#divMin").html((min.toString().length > 1) ? min.toString() : "0" + min.toString());
        $("#divTT").html(TT);
        $("#tblCalendar td div[data-value='" + selectedDate.getDate() + "']").addClass("selected");
        
        $(".selectedDate").data("selected", selectedDate.getFullYear().toString() + "-" + (selectedDate.getMonth() + 1).toString() + "-" + selectedDate.getDate().toString() + " " + selectedDate.getHours().toString() + ":" + selectedDate.getMinutes().toString());

        $(".selectedDate").html(months[curMonthIndex] + " " + selectedDate.getDate() + ", " + curYearSelected.toString() + " " + ((hour.toString().length > 1) ? hour : "0" + hour.toString()) + ":" + ((min.toString().length>1)?min:"0"+min)+" "+TT);

    }
    catch (e)
    { alert(e);}
});



/*For Time Picker*/
function IncrementMin() {

    try {

        min = parseInt($("#divMin").html());

        min++;
        if (min > 59)
        { min = 0; }

        $("#divMin").html((min.toString().length > 1) ? min.toString() : "0" + min.toString());
        selectedDate.setMinutes(min);
        $(".selectedDate").data("selected", selectedDate.getFullYear().toString() + "-" + (selectedDate.getMonth() + 1).toString() + "-" + selectedDate.getDate().toString() + " " + selectedDate.getHours().toString() + ":" + selectedDate.getMinutes().toString());
        $(".selectedDate").html(months[curMonthIndex] + " " + selectedDate.getDate() + ", " + curYearSelected.toString() + " " + ((hour.toString().length > 1) ? hour : "0" + hour.toString()) + ":" + ((min.toString().length > 1) ? min : "0" + min) + " " + TT);

    }
    catch (e)
    { alert(e); }
}

function DecrementMin() {
    min = parseInt($("#divMin").html());

    min--;
    if (min < 0)
    { min = 59; }

    $("#divMin").html((min.toString().length > 1) ? min.toString() : "0" + min.toString());
    selectedDate.setMinutes(min);
    $(".selectedDate").data("selected", selectedDate.getFullYear().toString() + "-" + (selectedDate.getMonth() + 1).toString() + "-" + selectedDate.getDate().toString() + " " + selectedDate.getHours().toString() + ":" + selectedDate.getMinutes().toString());
    $(".selectedDate").html(months[curMonthIndex] + " " + selectedDate.getDate() + ", " + curYearSelected.toString() + " " + ((hour.toString().length > 1) ? hour : "0" + hour.toString()) + ":" + ((min.toString().length > 1) ? min : "0" + min) + " " + TT);

}

function IncrementHours() {

    try {

        hour = parseInt($("#divHour").html());

        hour++;
        if (hour > 12)
        { hour = 1; }

        $("#divHour").html((hour.toString().length > 1) ? hour.toString() : "0" + hour.toString());

        if (hour == 12 && TT == "AM")
        { selectedDate.setHours(0); }
        else if ((hour >= 1 && hour < 12) && TT == "PM")
        { selectedDate.setHours((hour + 12)); }
        else
        { selectedDate.setHours(hour);}

        $(".selectedDate").data("selected", selectedDate.getFullYear().toString() + "-" + (selectedDate.getMonth() + 1).toString() + "-" + selectedDate.getDate().toString() + " " + selectedDate.getHours().toString() + ":" + selectedDate.getMinutes().toString());
        $(".selectedDate").html(months[curMonthIndex] + " " + selectedDate.getDate() + ", " + curYearSelected.toString() + " " + ((hour.toString().length > 1) ? hour : "0" + hour.toString()) + ":" + ((min.toString().length > 1) ? min : "0" + min) + " " + TT);
    }
    catch (e)
    { alert(e);}
}

function DecrementHours() {
    hour = parseInt($("#divHour").html());

    hour--;
    if (hour <=0)
    { hour = 12; }

    $("#divHour").html((hour.toString().length > 1) ? hour.toString() : "0" + hour.toString());

    if (hour == 12 && TT == "AM")
    { selectedDate.setHours(0); }
    else if ((hour >= 1 && hour < 12) && TT == "PM")
    { selectedDate.setHours((hour + 12)); }
    else
    { selectedDate.setHours(hour); }

    $(".selectedDate").data("selected", selectedDate.getFullYear().toString() + "-" + (selectedDate.getMonth() + 1).toString() + "-" + selectedDate.getDate().toString() + " " + selectedDate.getHours().toString() + ":" + selectedDate.getMinutes().toString());
    $(".selectedDate").html(months[curMonthIndex] + " " + selectedDate.getDate() + ", " + curYearSelected.toString() + " " + ((hour.toString().length > 1) ? hour : "0" + hour.toString()) + ":" + ((min.toString().length > 1) ? min : "0" + min) + " " + TT);
}

function ChangeTT() {
    TT = $("#divTT").html();
    try{
        if (TT.toString().toUpperCase() == "AM")
        { TT = "PM"; }
        else
        { TT = "AM"; }

        $("#divTT").html(TT);

        if (hour == 12 && TT == "AM")
        { selectedDate.setHours(0); }
        else if ((hour >= 1 && hour < 12) && TT == "PM")
        { selectedDate.setHours((hour + 12)); }
        else
        { selectedDate.setHours(hour); }

        $(".selectedDate").data("selected", selectedDate.getFullYear().toString() + "-" + (selectedDate.getMonth()+1).toString() + "-" + selectedDate.getDate().toString() + " " + selectedDate.getHours().toString() + ":" + selectedDate.getMinutes().toString());
        $(".selectedDate").html(months[curMonthIndex] + " " + selectedDate.getDate() + ", " + curYearSelected.toString() + " " + ((hour.toString().length > 1) ? hour : "0" + hour.toString()) + ":" + ((min.toString().length > 1) ? min : "0" + min) + " " + TT);

    }
    catch (e) { alert(e);}
}