﻿$(document).ready(function () {
    //Letters Only
    $("#txtFirstName,#txtLastName,#txtContactUsFullName").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
            // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a letter and stop the keypress
        if ((e.keyCode < 65 || e.keyCode > 90) && (e.keyCode < 97 || e.keyCode > 122)) {
            e.preventDefault();
        }
    });
    //End Letters Only

    // Email Validator
    $("#txtEmailSignUp").focusout(function (e) {
        $.ajax({
            type: "POST",
            url: "MemberSignUp.aspx/CheckExistingMail",
            data: "{'email_address':'" + $("#txtEmailSignUp").val() + "'}",
            contentType: "application/json",
            dataType: "json",
            success: function (result) {
                console.log(result["d"]);
                if (result["d"] == "NullValue") {
                    $(".spanEmailExists").css({ "display": "none" });
                }
                else if (result["d"] == "Invalid") {
                    $(".spanEmailExists").css({ "display": "block" });
                } else {
                    $(".spanEmailExists").css({ "display": "none" });
                }
            }
        });
    });
    // End Email validator

    // For radio buttons
    $('#optFemale,#optMale').click(function () {
        if ($('#optFemale').is(':checked')) { $("#txtGender").val("Female"); }
        if ($('#optMale').is(':checked')) { $("#txtGender").val("Male"); }

        if ($("#txtGender").val() == "") {
            $("#rfvGender").css("display", "block");
        }
        else { $("#rfvGender").css("display", "none"); }
    });
    // END of for radio buttons
});

/* FUNCTIONS */
function ConfirmEmail() {
    if ($("input[id*='txtSignUpRetypeEmail']").val() == "") {

    }
    else if ($("input[id*='txtEmailSignUp']").val() != $("input[id*='txtSignUpRetypeEmail']").val()) {
        $(".spanEmailConfirm").css({ "display": "block" });
    }
    else {
        $(".spanEmailConfirm").css({ "display": "none" });
    }

}

function ConfirmPasswordRetype() {
    if ($("input[id*='txtSignUpRetypePassword']").val() == "") {
    }
    else if ($("input[id*='txtPasswordSignUp']").val() != $("input[id*='txtSignUpRetypePassword']").val()) {
        $(".spanConfirmPassword").css({ "display": "block" });
    }
    else {
        $(".spanConfirmPassword").css({ "display": "none" });
    }
}
/* END of FUNCTIONS */