﻿var olatitude, olongitude, dlatitude, dlongitude, lat, lng, address;
var infowindow = new google.maps.InfoWindow();


//window.onload = function () {
    //if (navigator.geolocation) {
    //    navigator.geolocation.getCurrentPosition(showPosition, showError);
    //}
//}



function showPosition(position) {

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition;
    }

    olatitude = position.coords.latitude;
    olongitude = position.coords.longitude;
    dlatitude = position.coords.latitude;
    dlongitude = position.coords.longitude;

    var locations = [
  [
  "mOrigin", "",
  olatitude,
  olongitude
  ],
  [
  "mDestination", "",
  dlatitude,
  dlongitude
  ],
    ];

    gmarkers = [];




    var map = new google.maps.Map(document.getElementById('DvmapAreaOrigin'), {
        zoom: 13,
        center: new google.maps.LatLng(14.582299799999998, 121.06150439999999),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    for (var i = 0; i < 2; i++) {
        gmarkers[locations[i][0]] = createMarker(new google.maps.LatLng(locations[i][2], locations[i][3]));
    }

    function createMarker(latlng) {
        var geocoder = new google.maps.Geocoder();
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            icon: (i == 0) ? "/Images/Origin.png" : "/Images/Destination.png",
            id: "m" + i
        });

        lat = marker.getPosition().lat();
        lng = marker.getPosition().lng();
        address = results[0].formatted_address;
        var html = (marker.id == "m0") ? "<b>Origin (Pickup Location)</b> <br/>" + address : "<b>Destination (Drop off Location)</b> <br/>" + address;
        infowindow.setContent(html);
        infowindow.open(map, marker);

        if (marker.id == "m0") {
            $("input[id*='hdfLatO']").val(lat);
            $("input[id*='hdfLngO']").val(lng);
            $("input[id*='hdfAddressO']").val(address);
            
        }
        else if (marker.id == "m1") {

            $("input[id*='hdfLatD']").val(lat);
            $("input[id*='hdfLngD']").val(lng);
            $("input[id*='hdfAddressD']").val(address);
        }


        google.maps.event.addListener(marker, 'click', function () {
            geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                // This is checking to see if the Geoeode Status is OK before proceeding
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = marker.getPosition().lat();
                    lng = marker.getPosition().lng();
                    address = results[0].formatted_address;
                    var html = (marker.id == "m0") ? "<b>Origin (Pickup Location)</b> <br/>" + address : "<b>Destination (Drop off Location)</b> <br/>" + address;
                    infowindow.setContent(html);
                    infowindow.open(map, marker);

                    if (marker.id == "m0") {
                        $("input[id*='hdfLatO']").val(lat);
                        $("input[id*='hdfLngO']").val(lng);
                        $("input[id*='hdfAddressO']").val(address);
                        
                    }
                    else if (marker.id == "m1") {
                        
                        $("input[id*='hdfLatD']").val(lat);
                        $("input[id*='hdfLngD']").val(lng);
                        $("input[id*='hdfAddressD']").val(address);
                    }
                }
            });
        });

        google.maps.event.addListener(marker, 'dragend', function (e) {
            geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                // This is checking to see if the Geoeode Status is OK before proceeding
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = marker.getPosition().lat();
                    lng = marker.getPosition().lng();
                    address = results[0].formatted_address;
                    var html = (marker.id == "m0") ? "<b>Origin (Pickup Location)</b> <br/>" + address : "<b>Destination (Drop off Location)</b> <br/>" + address;
                    infowindow.setContent(html);
                    infowindow.open(map, marker);

                    if (marker.id == "m0") {
                        $("input[id*='hdfLatO']").val(lat);
                        $("input[id*='hdfLngO']").val(lng);
                        $("input[id*='hdfAddressO']").val(address);
                    }
                    else if (marker.id == "m1") {
                        $("input[id*='hdfLatD']").val(lat);
                        $("input[id*='hdfLngD']").val(lng);
                        $("input[id*='hdfAddressD']").val(address);
                    }
                }
            });
        });



        return marker;
    }

}

function showError(error) {

    var locations = [
  [
  "mOrigin", "",
  "14.582299799999998",
  "121.06150439999999"
  ],
  [
  "mDestination", "",
  "14.582299799999998",
  "121.06150439999999"
  ],
    ];

    gmarkers = [];

    var map = new google.maps.Map(document.getElementById('DvmapAreaOrigin'), {
        zoom: 13,
        center: new google.maps.LatLng(14.582299799999998, 121.06150439999999),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    for (var i = 0; i < 2; i++) {
        gmarkers[locations[i][0]] = createMarker(new google.maps.LatLng(locations[i][2], locations[i][3]));
    }

    function createMarker(latlng) {
        var geocoder = new google.maps.Geocoder();
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            icon: (i == 0) ? "/Images/Origin.png" : "/Images/Destination.png",
            id: "m" + i
        });

 if (marker.id == "m0") {
     geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
         if (status == google.maps.GeocoderStatus.OK) {
             lat = marker.getPosition().lat();
             lng = marker.getPosition().lng();
             address = results[0].formatted_address;
             $("input[id*='hdfLatO']").val(lat);
             $("input[id*='hdfLngO']").val(lng);
             $("input[id*='hdfAddressO']").val(address);
             
         }
     });
        }
        else if (marker.id == "m1") {
            geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = marker.getPosition().lat();
                    lng = marker.getPosition().lng();
                    address = results[0].formatted_address;
                    $("input[id*='hdfLatD']").val(lat);
                    $("input[id*='hdfLngD']").val(lng);
                    $("input[id*='hdfAddressD']").val(address);
}
            });
            
        }



        google.maps.event.addListener(marker, 'click', function () {
            geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                // This is checking to see if the Geoeode Status is OK before proceeding
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = marker.getPosition().lat();
                    lng = marker.getPosition().lng();
                    address = results[0].formatted_address;
                    var html = (marker.id == "m0") ? "<b>Origin (Pickup Location)</b> <br/>" + address : "<b>Destination (Drop off Location)</b> <br/>" + address;
                    infowindow.setContent(html);
                    infowindow.open(map, marker);

                    if (marker.id == "m0") {
                        $("input[id*='hdfLatO']").val(lat);
                        $("input[id*='hdfLngO']").val(lng);
                        $("input[id*='hdfAddressO']").val(address);
                    }
                    else if (marker.id == "m1") {
                        
                        $("input[id*='hdfLatD']").val(lat);
                        $("input[id*='hdfLngD']").val(lng);
                        $("input[id*='hdfAddressD']").val(address);
                    }
                }
            });
        });

        google.maps.event.addListener(marker, 'dragend', function (e) {
            geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                // This is checking to see if the Geoeode Status is OK before proceeding
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = marker.getPosition().lat();
                    lng = marker.getPosition().lng();
                    address = results[0].formatted_address;
                    var html = (marker.id == "m0") ? "<b>Origin (Pickup Location)</b> <br/>" + address : "<b>Destination (Drop off Location)</b> <br/>" + address;
                    infowindow.setContent(html);
                    infowindow.open(map, marker);
      
                    if (marker.id == "m0") {
                        $("input[id*='hdfLatO']").val(lat);
                        $("input[id*='hdfLngO']").val(lng);
                        $("input[id*='hdfAddressO']").val(address);
                    }
                    else if (marker.id == "m1") {
                       
                        $("input[id*='hdfLatD']").val(lat);
                        $("input[id*='hdfLngD']").val(lng);
                        $("input[id*='hdfAddressD']").val(address);
                    }
                }
            });
        });



        return marker;
    }
}


