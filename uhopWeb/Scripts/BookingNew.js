﻿
var map;

function displayTopOptions() {
    var origHeight = $("#divTopOptions").css({ "height": "auto" }).height() + "px";
    $("#divSearchServiceArea").addClass("hide");
    $("#divTopOptions").height("0");    
    $("#divTopOptions").animate({
        height: origHeight
    }, {
        duration: 1000, queue: false, complete: function () {
            $("#divTopOptions").removeAttr("style");
            $("#divTopOptions").removeClass("hideContent");
            
        }
    });

    $("#divTopOptions").removeClass("hideContent");
}

function HideTopOptions() {
    $("#divTopOptions").animate({ height: '0' }, {
        duration: 1000, complete: function () {
            $("#divTopOptions").addClass("hideContent");

            $("#divSearchServiceArea").removeClass("hide");
        }
    });
}

function CloseBookingReturnCover() {
    $("#divCoverReturn").fadeOut("slow");
}

function CloseBookingNewCover() {
    $("#divCover").fadeOut("slow");
}

function ShowBookingNewCover() {
    $("#divCover").removeClass("hide").hide().fadeIn("slow");
}




/*For Booking Map*/

function RepopulateStation(selectedID,target) {

    try {
        var ServiceAreaIDFrom = $("select[id*='ddlServiceAreaFrom']").val();
        var ServiceAreaIDTo = $("select[id*='ddlServiceAreaTo']").val();
        var currentSelectedVal = $("select[id*='" + target + "']").val();
        if (!ServiceAreaIDTo || !ServiceAreaIDFrom)
        { return; }

        PageMethods.RepopulateStations(ServiceAreaIDFrom, ServiceAreaIDTo, selectedID,target, function (result) {
            var opt = document.createElement("option");
            var ddl = $("select[id*='" + target + "']");
            opt.value = "";
            $(ddl).empty();
            if (target == "ddlPickup")
            { opt.text = "Select your pickup point"; }
            else
            { opt.text = "Select your dropoff point"; }
            opt.disabled = true;
            document.getElementById($(ddl).prop("id")).options.add(opt);

            for (var i = 0; i < result.length; i++) {
                var opt2 = document.createElement("option");
                opt2.value = result[i].MajorStopID;
                opt2.text = result[i].StationName;
                
                if (currentSelectedVal)
                {
                    if (currentSelectedVal == result[i].MajorStopID) {
                        opt2.selected = true;
                        document.getElementById($(ddl).prop("id")).options.add(opt2);
                    }
                    else {
                        document.getElementById($(ddl).prop("id")).options.add(opt2);
                    }

                }
                else
                {
                    document.getElementById($(ddl).prop("id")).options.add(opt2);
                    document.getElementById($(ddl).prop("id")).options[0].selected = true;
                }
            }

            if ($("select[id*='ddlPickup']").val() && $("select[id*='ddlDropOff']").val()) {
                $("#btnShowSummary").prop("disabled", false);
            }

        });
    }
    catch (e)
    { alert(e); }

}

function PopulateDropOffAndPickup() {

    try
    {
        $("#btnShowSummary").prop("disabled", true);

        var ServiceAreaIDFrom = $("select[id*='ddlServiceAreaFrom']").val();
        var ServiceAreaIDTo = $("select[id*='ddlServiceAreaTo']").val();
        
        if (!ServiceAreaIDTo || !ServiceAreaIDFrom)
        { return; }

        PageMethods.PopulateStations(ServiceAreaIDFrom, ServiceAreaIDTo, function (result) {

            var opt = document.createElement("option");
            var opt2 = document.createElement("option");
            var ddlPickUp = document.getElementById("ContentPlaceHolder1_ddlPickup");
            var ddlDropOff = document.getElementById("ContentPlaceHolder1_ddlDropOff");
            opt.value = "";
            opt.text = "Select your pickup point";
            opt.disabled = true;
            opt.selected = true;
            opt2.value = "";
            opt2.text = "Select your dropoff point";
            opt2.disabled = true;
            opt2.selected = true;

            ddlPickUp.options.add(opt);
            ddlDropOff.options.add(opt2);


            if (result.length < 3) {

                var opt3 = document.createElement("option");
                opt3.text = $("select[id*='ddlServiceAreaFrom'] option:selected").text();
                opt3.value = "S01";
                opt3.selected = true;
                ddlPickUp.options.add(opt3);

                    var opt4 = document.createElement("option");
                    opt4.text = $("select[id*='ddlServiceAreaTo'] option:selected").text();
                    opt4.value = "S02";
                    opt4.selected = true;
                    ddlDropOff.options.add(opt4);
                    ShowMarkersOnMap();
                    $("#btnShowSummary").prop("disabled", false);

            }
            else {
                for (var i = 0; i < result.length; i++) {                   
                    if (result[i].MajorStopID != "S02")
                    {
                        var opt3 = document.createElement("option");
                        opt3.text = result[i].StationName;
                        opt3.value = result[i].MajorStopID;
                        ddlPickUp.options.add(opt3);
                    }
                    if (result[i].MajorStopID != "S01")
                    {
                        var opt4 = document.createElement("option");
                        opt4.text = result[i].StationName;
                        opt4.value = result[i].MajorStopID;
                        ddlDropOff.options.add(opt4);
                    }
                }
            }

        });
    }
    catch (e)
    { alert(e);}

}

function PopulateReturnTable(hour) {
    try {

        PageMethods.PopulateReturnTrip($("select[id*='ddlPickup']").val(), $("select[id*='ddlDropOff']").val(), $("select[id*='ddlServiceAreaFrom']").val(), $("select[id*='ddlServiceAreaTo']").val(), hour, function (result) {
            $("#tblDepartureScheduleList tbody").html("<tr><th colspan='3'>Departure</th><th colspan='3'>Return</th></tr><tr><th>Date Time</th><th>Pickup</th><th>Drop-off</th><th>Date Time</th><th>Pickup</th><th>Drop-off</th></tr>");
            var trData = "";
            for (var i = 0; i < result.length; i++) {
                trData += "<tr class='trData'><td>" + result[i].DepartureDateTime + "</td><td>" + result[i].DeparturePickUp + "</td><td>" + result[i].DepartureDropOff + "</td><td>" + result[i].ReturnDateTime + "</td><td>" + result[i].ReturnPickUp + "</td><td>" + result[i].ReturnDropOff + "</td></tr>";
            }
            $("#tblDepartureScheduleList tbody").append(trData);
            $("#divCover").addClass("hide");
            $("#divCoverReturn").removeClass("hide");
        });
    }
    catch (e)
    { alert(e); }
}

window.onload = function () {
    
    try
    {
        
        
        

        var mapOptions = {
            center: new google.maps.LatLng(14.5457370, 121.1767660),
            zoom: 7,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        
        map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        
    }
    catch (e)
    { alert(e); }


}

function SubmitReturnBooking(obj) {
    var msg = "";
    var hasError = false;
    $(obj).prop("disabled", false);
    $("#ulMsgReturn").addClass("hide");

    if (!$("#txtNumberOfHrs").val().indexOf("1234567890")<0) {
        msg += "<li>Number of hours only accept numeric values</li>";
        hasError = true;
    }
    if ($("#tblDepartureScheduleList tr").length < 2) {
        msg += "<li>Please select your pick-up/drop-off point</li>";
        hasError = true;
    }

    if (!hasError) {
        $(obj).prop("disabled", true);
        __doPostBack('Return', $("input[id*='txtNumberOfHrs']").val() + "~" + $("select[id*='ddlPickup']").val() + "~" + $("select[id*='ddlDropOff']").val());
    }
    else {
        $("#ulMsg").html(msg);
        $("#ulMsg").removeClass("hide");
    }
}

function SubmitMe(obj) {
    try
    {
        var msg = "";
        var hasError = false;
        $(obj).prop("disabled", false);
        $("#ulMsg").addClass("hide");
        if (!$("#h4Origin").html().trim() || !$("#h4Destination").html().trim()) {
            msg += "<li>Please select origin and destination.</li>";
            hasError = true;
        }
        if ($("#tblScheduleList tr").length < 2) {
            msg += "<li>Please select your pick-up/drop-off point</li>";
            hasError = true;
        }

        if (!hasError) {
            $(obj).prop("disabled", true);
            __doPostBack('Booking', $("input[id*='hdfSelectedHr']").val() + ":" + $("input[id*='hdfSelectedMin']").val() + " " + $("input[id*='hdftt']").val() + "~" + $("select[id*='ddlPickup']").val() + "~" + $("select[id*='ddlDropOff']").val());
        }
        else {

            $("#ulMsg").html(msg);
            $("#ulMsg").removeClass("hide");
        }
    }
    catch (e)
    { alert(e);}
    
}

function ResetMe() {
    window.location.reload();
}


function ShowCover() {

    var hour = 0;
    var min = 0;

    var isReturn = "";

    $("#ulMsgReturn").addClass("hide");
    $("#ulMsg").addClass("hide");
    

    PageMethods.IsReturnTrip($("select[id*='ddlServiceAreaFrom']").val(), $("select[id*='ddlServiceAreaTo']").val(), function (result) {
        if (result == "1")
        {
            hour = $("#txtNumberOfHrs").val();
            PopulateReturnTable(hour);
            $("#divCover").addClass("hide");
            $("#divCoverReturn").fadeIn("slow");
        }
        else
        {
            var dtNow = new Date();
            hour = dtNow.getHours();
            min = dtNow.getMinutes();
            $("#divHeader #h4Origin").html($("select[id*='ddlServiceAreaFrom']").val());
            $("#divHeader #h4Destination").html($("select[id*='ddlServiceAreaTo']").val());
            $("#tblScheduleList tbody").html("<tr><th>Departure Date Range</th><th>Departure Time</th><th>Pickup Point</th><th>Drop-Off Point</th></tr>");
            try {
                PageMethods.PopulateGV($("select[id*='ddlPickup']").val(), $("select[id*='ddlDropOff']").val(), $("select[id*='ddlServiceAreaFrom']").val(), $("select[id*='ddlServiceAreaTo']").val(), hour, min, function (result) {

                    var trData = "";

                    for (var i = 0; i < result.length; i++) {
                        trData += "<tr><td>" + result[i].DepartureDate + "</td><td>" + result[i].Time + "</td><td>" + result[i].PickUp + "</td><td>" + result[i].DropOff + "</td></tr>";
                    }

                    $("#tblScheduleList tbody").append(trData);
                    $("#divCoverReturn").addClass("hide");
                    $("#divCover").removeClass("hide");
                });
            }
            catch (e)
            { alert(e); }

            $("#divCoverReturn").addClass("hide");
            $("#divCover").fadeIn("slow");
        }
        $("#btnShowSummary").prop("disabled", false);
    });
}

function ShowMarkersOnMap() {

    try
    {
        var pickup = $("select[id*='ddlPickup']").val();
        var dropoff = $("select[id*='ddlDropOff']").val();
        var pickupText = $("select[id*='ddlPickup'] option:selected").text();
        var dropoffText = $("select[id*='ddlDropOff'] option:selected").text();
        var serviceAreaFrom = $("select[id*='ddlServiceAreaFrom'] option:selected").text();
        var serviceAreaTo = $("select[id*='ddlServiceAreaTo'] option:selected").text();

        PageMethods.GetLatLang(pickup, dropoff, pickupText, dropoffText,serviceAreaFrom,serviceAreaTo, function (result) {

            var mapOptions = {
                center: new google.maps.LatLng(14.5457370, 121.1767660),
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);




            var markers = [];

            for (var i = 0; i < result.length; i++) {
                var objMarkers = new Object();
                objMarkers = {
                    "title": (result[i].IsPickup == true) ? $("select[id*='ddlPickup'] option:selected").text() : $("select[id*='ddlDropOff'] option:selected").text(),
                    "lat": result[i].Latitude,
                    "lng": result[i].Longitude,
                    "description": (result[i].IsPickup == true) ? $("select[id*='ddlPickup'] option:selected").text() : $("select[id*='ddlDropOff'] option:selected").text(),

                };
                markers.push(objMarkers);
            }

            var infoWindow = new google.maps.InfoWindow();
            var latlngbounds = new google.maps.LatLngBounds();
            var geocoder = geocoder = new google.maps.Geocoder();
            try
            {
                for (var i = 0; i < markers.length; i++) {
                    var data = markers[i];
                    var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        title: data.title,
                        draggable: false,
                        icon: (data.title == $("select[id*='ddlPickup'] option:selected").text()) ? "/Images/MARKPOINTERgreen32.png" : "/Images/MARKPOINTERRed32.png",
                        animation: google.maps.Animation.DROP                    
                    });

                    (function (marker, data) {
                        google.maps.event.addListener(marker, "click", function (e) {
                            infoWindow.setContent(data.description);
                            infoWindow.open(map, marker);
                        });
                        
                    })(marker, data);
                    latlngbounds.extend(marker.position);
                }

                var bounds = new google.maps.LatLngBounds();
                map.setCenter(latlngbounds.getCenter());
                map.fitBounds(latlngbounds);
            }
            catch (e)
            { alert(e);}


        });

    }
    catch (e)
    { alert(e);}

}

/*End For Booking Map*/



/*Capturing TimePickerEvent*/
try
{
    $(document).ready(function () {
    
        $(".aServiceAreaNext").click(function () {
            if ($("select[id*='ddlServiceAreaFrom']").val() && $("select[id*='ddlServiceAreaTo']").val()) {
                PopulateDropOffAndPickup();
                $(".ServiceAreaSelectionCover").delay().fadeOut("slow");

            }
            else {
                $(".pErrorServiceAreaSelection").html("Please select ORIGIN and DESTINATION").removeClass("hide");
            }
        });

        $("select[id*='ddlPickup'],select[id*='ddlDropOff']").change(function () {
            ShowMarkersOnMap();
        });

        $("select[id*='ddlPickup']").change(function () {
            RepopulateStation($(this).val(), "ddlDropOff");
        });
        $("select[id*='ddlDropOff']").change(function () {
            RepopulateStation($(this).val(), "ddlPickup");
        });


        $("#btnShowSummary").prop("disabled", true);

        try
        {
            $("#tblTimePicker tr:first-child td:last-child div,#tblTimePicker tr:first-child td:nth-child(2) div,#tblTimePicker tr:first-child td:first-child div,#tblTimePicker tr:last-child td:first-child div,#tblTimePicker tr:last-child td:nth-child(2) div,#tblTimePicker tr:last-child td:last-child div").mouseup(function () {

                var timeSelected = $("#txthour").val() + ":" + $("#txtMin").val() + " " + $("#divTT").html();

                $("#tblScheduleList tr td:nth-child(2)").html(timeSelected);
            });
        }
        catch (e)
        { alert(e); }


        $("#txtNumberOfHrs").keypress(function (e) {
            var xchar = String.fromCharCode(e.keyCode);
            var allowChar = "1234567890" + String.fromCharCode(8);
            if (allowChar.indexOf(xchar) < 0)
            { return false; }        
        });
        $("#txtNumberOfHrs").keyup(function () {
            var hrs = 0;
            if ($("#txtNumberOfHrs").val().trim() == "") {
                $("#txtNumberOfHrs").val("0");
                $("#txtNumberOfHrs").select();
            }
            else {
                hrs = parseInt($("#txtNumberOfHrs").val());
            }
            PopulateReturnTable(hrs);
        });

    
    });
}
catch (e)
{ alert(e);}

/*End*/



