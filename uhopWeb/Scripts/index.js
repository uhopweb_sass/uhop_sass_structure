﻿$(document).ready(function () {
    // Materialize.toast(message, displayLength, className, completeCallback);
    Materialize.toast('Welcome to u-Hop!', 4444); // 4444 is the duration of the toast
    
    $('.parallax').parallax(); // Parallax scrolling

    $('.scrollspy').scrollSpy(); // Materialize Scrollspy 
    
    /* ADDING an Active Class */
    $('ul > li.currentActive').on('click', function (e) {
        e.preventDefault();
        $('ul > li').removeClass('activated');
        $(this).addClass('activated');
    });

    /* For changing the color of the navbar */
    $(window).on('scroll', function () {
        if ($window.scrollTop() <= 81) {
            $('#indexNav').removeClass('blue darken-4').addClass('black').css({ 'opacity': '1', 'border-bottom' : 'none' });
        } else {
            $('#indexNav').removeClass('black').addClass('blue darken-4').css({ 'opacity': '.9', 'border-bottom': '2px solid rgba(255, 64, 129, .9)' });
        }
    });

    /* Materialize select on u-Hop Shuttle*/
    $(document).ready(function () {
        $('select').material_select();
    });

    /* Back to top button scroller */
    var $window = $(window);
    var $b2top = $('#backToTop');
    var endZone = $('#backToTopKey').offset().top - $window.height() - 500;

    /* For showing and hiding the backtotop button */
    $(window).on('scroll', function () {
        if ((endZone) < $window.scrollTop()) {
            $b2top.stop().animate({ 'right': '20px' }, 250);
        } else {
            $b2top.stop().animate({ 'right': '-360px' }, 250);
        }
    });

    $('#ddlBranch').change(function () {
        var SelectedText = $(this).find(":selected").text();
        var SelectedValue = $(this).val();
        $('#spBranchName').text(SelectedText);

        $.ajax({
            type: "POST",
            url: "index.aspx/GetBranchDetails",
            data: "{'pBranchCode':'" + SelectedValue + "'}",
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                try {
                    var res = data.d;
                    $.each(res, function (index, BranchDetails) {
                        $('#lblBranchAddress').text(BranchDetails.BranchAddress);
                        $('#aMap').attr("href", BranchDetails.BranchMap);
                        $('#imgMap').attr("src", BranchDetails.BranchFileName);
                    });
                }
                catch (e) {

                }

            }
        });
    });
});

function stopScroll() {
    $('html, body').css({ 'overflow': 'hidden' });
}

function continueScroll() {
    $('html, body').css({ 'overflow': 'visible' });
}

function ShowDLApp() {
    $("html, body").css({ "overflow": "hidden" });
    $(".AppDL").css({ "display": "block" });
    $("body").addClass("TruncateContent");
}

function CloseDLApp() {
    $("html, body").css({ "overflow": "auto" });
    $(".AppDL").css({ "display": "none" });
    $("body").removeClass("TruncateContent");
}

    //function stopScroll() {
    //    $('.rMore').on('click', function () {
    //        $('html, body').addClass('stop-scrolling');
    //        $('body').bind('touchmove', function (e) { e.preventDefault() })
    //    });
    //}


    //function continueScroll() {
    //    /* FOR MODAL */
    //    $('.onDismissal, #lean-overlay').on('click', function () {
    //        $('html, body').removeClass('stop-scrolling');
    //        $('body').unbind('touchmove');
    //    });
    //}

/* END of FOR MODAL */

/* Smooth Scrolling (smoothscroll.js) initialization */
/*$.srSmoothscroll({
    // defaults
    step: 70,
    speed: 1200,
    ease: 'swing',
    target: $('body'),
    container: $(window)
});*/
/* END of Smooth Scrolling (smoothscroll.js) initialization*/