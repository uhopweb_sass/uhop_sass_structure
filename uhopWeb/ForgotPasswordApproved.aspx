﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPasswordApproved.aspx.cs" Inherits="uhopWeb.ForgotPasswordApproved" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" id="passwordReset">
<head runat="server">
    <title>Reset password successful!</title>
    <link rel="shortcut icon" href="Images/uhop_icon_beta.png" type="image/png" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="../Content/materialize/css/materialize.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link href="Styles/global.min.css" rel="stylesheet" />
    <link href="../Content/wow/animate.min.css" rel="stylesheet" />  
</head>
<body>
    <form id="form1" runat="server">
        <div class="container center" id="verifiedEmailContainer">
            <div class="white-text" id="verifiedEmailText">
                
                <h3>You're Password was successfully reset!</h3>
                <h4>Kindly check your Email</h4>
                <img src="Images/u-hop_logo.png" class="responsive-img wow pulse" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="3s"/>
            </div>
            <div class="verifiedBtns">
                <a href="Index.aspx" class="waves-effect waves-light btn blue darken-2"><i class="material-icons right">web</i>Go to site </a>
                <a runat="server" id="lnklogin" class="waves-effect waves-light btn pink accent-2"><i class="material-icons right">trending_flat</i>Proceed to login</a>  
            </div> 
        </div>
        <!-- FOOTER SECTION -->
        <footer class="page-footer indigo darken-4" id="verifiedEmailFooter">

        </footer>
        
    </form>

     <script src="Scripts/jquery-2.1.4.min.js"></script>
    <script src="Scripts/materialize/materialize.min.js" charset="utf8"></script>
    <script src="Scripts/wow/wow.min.js"></script>
    <script>
        /* WOW.js Initialization */
        new WOW().init();
        /* END of WOW.js initialization */
    </script>



</body>
</html>
