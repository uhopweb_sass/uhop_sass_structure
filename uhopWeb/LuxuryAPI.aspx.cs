﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;

namespace uhopWeb
{
    public partial class LuxuryAPI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Form["Email"] != "" && Request.Form["FullName"] != "" && Request.Form["BookingDate"] != "" && Request.Form["DepartureDate"] != "" && Request.Form["ServiceType"] != "" && Request.Form["Description"] != "" && Request.Form["Specification"] != "" && Request.Form["NumberOfPax"] != "" && Request.Form["Remarks"] != "" && Request.Form["LuxuryLink"] != "")
                {
                    ClsSendMail objMail = new ClsSendMail();
                    string strLink = UHopCore.GetPageURL() + "LuxuryConfirmation.aspx?ULUX=" + Request.Form["LuxuryLink"];
                    objMail.SendMailLuxuryConfirm(Request.Form["Email"], Request.Form["FullName"], Request.Form["BookingDate"], Request.Form["DepartureDate"], Request.Form["ServiceType"], Request.Form["Description"], Request.Form["Specification"], Request.Form["NumberOfPax"], strLink, strLink, Request.Form["Remarks"]);
                    Response.StatusCode = 200;
                }
                else
                {
                    Response.StatusCode = 500;
                    
                }
                Response.End();
            }
        }
    }
}