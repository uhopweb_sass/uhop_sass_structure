﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundle/style").Include("~/Content/materialize/css/materialize.min.css")
                .Include("~/Styles/global.css")
                .Include("~/Styles/media.min.css")
                .Include("~/stylesheets/main.css")
                .Include("~/Content/wow/animate.min.css"));

            bundles.Add(new ScriptBundle("~/bundle/script").Include("~/Scripts/minified/jquery-2.1.4.min.js")
                /*.Include("~/Scripts/smoothscrolling/mousewheel/jquery.mousewheel.min.js")
                .Include("~/Scripts/smoothscrolling/smoothscroll/jquery.simplr.smoothscroll.min.js")*/
                .Include("~/Scripts/materialize/materialize.min.js")
                .Include("~/Scripts/wow/wow.min.js")
                .Include("~/Scripts/minified/global.min.js"));

            bundles.Add(new StyleBundle("~/bundle/BookingStyle").Include("~/Styles/booking.css")
                .Include("~/Styles/UCTimePicker.min.css"));

            bundles.Add(new ScriptBundle("~/bundle/BookingScript")
                .Include("~/Scripts/jquery-2.1.4.js")
                .Include("~/Scripts/BookingNew.min.js")
                .Include("~/Scripts/JSUCTimePicker.min.js"));

            bundles.Add(new ScriptBundle("~/bundle/ProfileScript").Include("~/Scripts/userprofile.js"));

            bundles.Add(new ScriptBundle("~/bundle/AdminLoginScript").Include("~/Scripts/jquery-2.1.4.js")
                .Include("~/Scripts/materialize/materialize.min.js")
                .Include("~/Scripts/wow/wow.min.js"));

            bundles.Add(new ScriptBundle("~/bundle/BookingSuccessScript")
                .Include("~/Scripts/jquery-2.1.4.js")
                .Include("~/Scripts/BookingScuccessScript.min.js")
                .Include("~/Scripts/wow/wow.min.js"));

            bundles.Add(new StyleBundle("~/bundle/BookingSuccessStyle").Include("~/Content/materialize/css/materialize.min.css")
                .Include("~/Styles/global.min.css")
                .Include("~/Content/wow/animate.min.css")
                .Include("~/Styles/BookingSuccessStyle.min.css"));

            bundles.Add(new StyleBundle("~/bundle/AdminLoginStyle").Include("~/Content/materialize/css/materialize.min.css")
                .Include("~/Styles/global.min.css")
                .Include("~/Content/wow/animate.min.css")
                .Include("~/Styles/AdminLogIn.css"));

            bundles.Add(new StyleBundle("~/bundle/bookingHistoryStyle").Include("~/Styles/BookingHistory.css"));

            bundles.Add(new StyleBundle("~/bundle/SubscriptionHistoryStyle").Include("~/Styles/SubscriptionList.css"));

          }
    }
