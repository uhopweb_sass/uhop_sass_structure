﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="uhopWeb.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" id="loginBG">
<head id="Head1" runat="server">
    <link rel="shortcut icon" href="Images/uhop_icon_beta-min.png" type="image/png" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" /> 
    
    <asp:PlaceHolder ID="PlaceHolder2" runat="server">
        <%: Styles.Render("~/bundle/style") %>
    </asp:PlaceHolder>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    <link href="Styles/loginsignup.min.css" rel="stylesheet" />
    <title>Login</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container white-text center loginContainer">
            <div class="row">
                <div><img src="Images/u-hop_logo.png" class="responsive-img wow pulse animated" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="3s" id="loginImg" width="450"/></div>
            </div>
            <asp:HiddenField ID="HdfIsOperatorLogIn" runat="server" />
            <h2 class="white-text"><span id="spnLogin" runat="server"></span></h2>
            <div class="row white-text">
                <div class="input-field col l6 s12">
                    <i class="large material-icons prefix">&#xE853;</i>
                    <asp:TextBox ID="txtEmail1" runat="server" CssClass-="large validate white-text" type="email" Text="" AutoComplete="false"></asp:TextBox>
                    <label for="email" data-error="wrong" data-success="right">E-mail</label>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid email" ValidationExpression='^[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+(\.[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$' SetFocusOnError="true" ControlToValidate="txtEmail1" Text="Invalid email format!" Display="Dynamic" ValidationGroup="Login" CssClass="ValidationSummary"></asp:RegularExpressionValidator>&nbsp;
                </div>

                <div class="input-field col l6 s12">
                    <i class="large material-icons prefix">&#xE0DA;</i>
                    <asp:TextBox ID="txtPassword2" runat="server" CssClass-="large validate white-text" type="password" Text=""></asp:TextBox>
                    <label for="password" data-error="wrong" data-success="right">Password</label>
                </div>
                <br /><br /><br /><br />
                <div class="col s12">
                    <a href="Index.aspx" class="btn btn-danger btn btn-lg pink accent-2 white-text">Home</a>
                    <asp:Button ID="BtnogIn" runat="server" Text="Log In" CssClass="btn btn-primary btn btn-lg" ValidationGroup="Login" OnClick="BtnogIn_Click" /> <br />
                </div>
                <div class="col s12 center" style="margin-top: 10px; color: white;"><a class="white-text" href="ForgotPassword.aspx?UT=<% string qstr = Request.QueryString["UT"]; var qstring = qstr; Response.Write(qstring); %>" style="font-size: 20px;"><u>Forgot password?</u></a></div>
                <div class="col s12">
                    <ul class="Ulerr red-text" id="Ulerr" runat="server"></ul>
                    <span runat="server" id="spnVerifyEmail" class="red-text" style="display: none">You're Email is not yet verified! Click the link to re-send Email <a runat="server" onserverclick="btnResendMail_Click" class="blue-text">Click Here!</a></span>
                    <span runat="server" id="spnResendSuccess" class="green-text" style="display: none"></span>
                </div>
<%--                <div class="row">
                    <div class="col s6 offset-s3" style="margin-top:-20px;">
                        <br />
                        <h4 class="noMargin">OR</h4>
                    </div>
                </div>
                 <div class="row">
                    <div class="col s12">
                        <asp:Button ID="btnLoginFacebook" runat="server" Text="Connect with Facebook" CssClass="btn small" style="background-color:#3b5998;" OnClick="btnLoginFacebook_Click"/>
                    </div>
                </div>--%>
                <div class="col s12 center">
                    <p class="noMargin center"><a id="switchLogin" href="">Switch to <span id="spnLogin2" runat="server"></span> login</a></p>
                </div>
            </div>
        </div>
    </form>

    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/script") %>
    </asp:PlaceHolder>

    <script>
        /* For switching log in */
        if ($("#spnLogin2").text() == "Member") {
            $("#switchLogin").attr("href", "<%=uhopWeb.Classes.UHopCore.GetPageURL() %>Login.aspx?UT=Member");
            $("#BtnogIn").removeClass("blue darken-3").addClass("black");
        } else if ($("#spnLogin2").text() == "Operator") {
            $("#switchLogin").attr("href",  "<%=uhopWeb.Classes.UHopCore.GetPageURL() %>Login.aspx?UT=Operator");
            $("#BtnogIn").removeClass("black").addClass("blue darken-3");
        } else {
            alert("None of the above");
            $("#switchLogin").attr("href", "");
        }

        new WOW().init();
    </script>
</body>
</html>
    