﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCDateTimePicker.ascx.cs" Inherits="uhopWeb.UserControls.UCDateTimePicker" %>


<div id="divDTContent">
    <ul id="ulCalendarContainer">
        <li>
            <ul style="display: inline-block;">
                <li class="bMonth" data-selected="" data-title="Click To Change Month">Month</li>
                <li class="hide" data-toggle="">
                    <ul class="MonthList">
                    </ul>
                </li>
            </ul>
            &nbsp;&nbsp;&nbsp;&nbsp;<b class="Clickable" data-title="Previous Year" onclick="PrevYear();"><</b><b class="bYear" data-selected="">Year</b><b class="Clickable" data-title="Next Year" onclick="NextYear();">></b>
        </li>
        <li>
            <table id="tblCalendar">
                <tbody>
                </tbody>
            </table>
        </li>
        <li><b class="selectedDate" data-selected="">&nbsp;</b></li>
    </ul>

    <table id="tblTimePicker">
        <tr>
            <td>
                <div onclick="IncrementHours();">&nbsp;</div>
            </td>
            <td>
                <div onclick="IncrementMin();">&nbsp;</div>
            </td>
            <td>
                <div onclick="ChangeTT();">&nbsp;</div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="divHour">01</div>
            </td>
            <td>
                <div id="divMin">00</div>
            </td>
            <td>
                <div id="divTT">AM</div>
            </td>
        </tr>
        <tr>
            <td>
                <div  onclick="DecrementHours();">&nbsp;</div>
            </td>
            <td>
                <div onclick="DecrementMin();">&nbsp;</div>
            </td>
            <td>
                <div onclick="ChangeTT();">&nbsp;</div>
            </td>
        </tr>

    </table>

</div>
