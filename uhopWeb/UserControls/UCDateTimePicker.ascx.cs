﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace uhopWeb.UserControls
{
    public partial class UCDateTimePicker : System.Web.UI.UserControl
    {

        private static DateTime _dtMaxDate = new DateTime();
        private static DateTime _dtMinDate = new DateTime();

        public DateTime MaxDate { get { return _dtMaxDate; } set { _dtMaxDate = value; } }
        public DateTime MinDate { get { return _dtMinDate; } set { _dtMinDate = value; } }



        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}