﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCTimePicker.ascx.cs" Inherits="uhopWeb.UserControls.UCTimePicker" %>
<div class="TimePickerContainer">

   <table id="tblTimePicker">
        <tr>
            <td>
                <div>&nbsp;</div>
            </td>
            <td>
                <div>&nbsp;</div>
            </td>
            <td>
                <div>&nbsp;</div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" id="txthour" value="01" maxlength="2"/>
            </td>
            <td>
                <input type="text" id="txtMin" value="00" maxlength="2"/>
            </td>
            <td>
                <div id="divTT">AM</div>
            </td>
        </tr>
        <tr>
            <td>
                <div>&nbsp;</div>
            </td>
            <td>
                <div>&nbsp;</div>
            </td>
            <td>
                <div>&nbsp;</div>
            </td>
        </tr>

    </table>
    <asp:HiddenField ID="hdfSelectedHr" runat="server" />
    <asp:HiddenField ID="hdfSelectedMin" runat="server" />
    <asp:HiddenField ID="hdftt" runat="server" />
</div>