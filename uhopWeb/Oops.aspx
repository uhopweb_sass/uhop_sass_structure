﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Oops.aspx.cs" Inherits="uhopWeb.Oops" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" id="underCPage">
<head runat="server">
     <link rel="shortcut icon" href="Images/uhop_icon_beta-min.png" type="image/png"/>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320"/> 
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <asp:PlaceHolder ID="PlaceHolder2" runat="server">
        <%: Styles.Render("~/bundle/style") %>
    </asp:PlaceHolder>

    <title>u-Hop Transport Network Vehicle System Inc.</title>

    <style>
        html, body {
            height: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <div class="navbar-fixed blue darken-2">
        <nav>
          <div class="nav-wrapper pink accent-2">
          </div>
        </nav>
      </div>
        <br />
        <div class="container">
        <div class="row center">
            <div class="wow hinge">
                <div class="wow flash animated" data-wow-iteration="infinite" data-wow-duration="10s" data-wow-delay="3s">
                    <img style="margin-top:50px;" class="wrongImg responsive-img wow pulse animated center-align noMargin" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="3s" src="Images/u-hop_logo.png" alt="Alternate Text"/>
                </div>
            </div>
        </div>
        <div class="row center">
            <img src="Images/wrong-icon.png" class="responsive-img"/>
        </div>
        <div class="center" style="margin-top:-70px;">
            <h3>Oops!</h3>
            <h4>Something went wrong, we're sorry for the inconvenience.</h4>

            <a  class="waves-effect waves-light btn pink accent-2" href="LogOut.aspx"><i class="material-icons"></i>Back to site</a>
        </div>
        </div>
    </div>
    </form>

    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/script") %>
    </asp:PlaceHolder>

    <script>
        $('.wrongImg').hide(3000);
        /* WOW.js Initialization */
        new WOW().init();
        /* END of WOW.js initialization */
    </script>
</body>
</html>
