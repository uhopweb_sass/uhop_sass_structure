﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OperatorSignUp.aspx.cs" Inherits="uhopWeb.OperatorSignUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>u-Hop : Operator Sign Up</title>

    <link rel="shortcut icon" href="Images/uhop_icon_beta-min.png" type="image/png" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

    <asp:PlaceHolder ID="PlaceHolder2" runat="server">
        <%: Styles.Render("~/bundle/style") %>
    </asp:PlaceHolder>
    <link href="Styles/loginsignup.min.css" rel="stylesheet" />

</head>
<body id="oSignUpBG">
    <form id="form1" runat="server">
        <!-- Successfully Registered Popup -->
        <div class="divCover SignupComplete" id="DivSignupComplete" runat="server">
            <div style="max-width: 100%; min-height: 100%; width: 100%; text-align: center; color: white; margin-left: auto; margin-right: auto">
                <br /><br /><br /><br /><br /><br />
                <h5 style="color: green"><b>You have successfully registered!</b><br />
                    <span style="color: orange; font-size: 20px;">We've sent a confirmation in your e-mail <span class="pink-text" runat="server" id="spnEmail"></span>. Please validate your account by clicking the link we've provided in your mail. Thank you.</span></h5>
                <a href="OperatorSignUp.aspx" class="btn waves-effect waves-light pink">Close &nbsp;<i class="material-icons right" style="margin:0;">&#xE5CD;</i></a>
            </div>
        </div>
        <!-- End of Successfully Registered Popup  -->

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="container containerSignUp">
            <div class="card-panel z-depth-5">

                <h2 class="center signUpHeader">Operator Sign up</h2>

                <div class="divider black" style="height: 5px"></div>
                <div class="row red-text">
                    <p class="col s12 right-align redNote">NOTE: fields with * are required</p>
                </div>
                <!-- First name and last name row -->
                <div class="row">
                    <!-- First Name Input Field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE8A6;</i>
                        <asp:TextBox ID="txtFirstName" CssClass="large validate text-color" Style="text-transform: capitalize;" runat="server" MaxLength="30" autocomplete="off"></asp:TextBox>
                        <label for="txtFirstName">
                            First Name <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="First name is required" SetFocusOnError="true" ControlToValidate="txtFirstName" Display="Dynamic" ValidationGroup="SignUp" Text="First name is required" CssClass="ValidationSummary "></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <!-- END of First Name Input Field -->

                    <!-- Last Name input field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE8A6;</i>
                        <asp:TextBox ID="txtLastName" CssClass="large validate text-color txtLastName" Style="text-transform: capitalize;" runat="server" MaxLength="30" autocomplete="off"></asp:TextBox>
                        <label for="txtLastName">
                            Last Name <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Last name is required" SetFocusOnError="true" ValidationGroup="SignUp" Display="Dynamic" ControlToValidate="txtLastName" Text="Last name is required" CssClass="ValidationSummary  "></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-- END of Last Name input field -->
                </div>
                <!-- First name and last name row -->

                <!-- Email Row -->
                <div class="row">
                    <!-- Email Input Field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE0BE;</i>
                        <asp:TextBox ID="txtEmailSignUp" CssClass="large validate text-color  " runat="server" onchange="ConfirmEmail();" Style="text-transform: lowercase" autocomplete="off" MaxLength="50"></asp:TextBox>
                        <label for="txtEmailSignUp">
                            E-mail <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center">
                            <span style="display: none; color: red" id="spanEmailExists" runat="server" class="spanEmailExists">Email already exist!</span>
                        </div>

                        <div class="col s12 center">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email" SetFocusOnError="true" ValidationExpression='^[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+(\.[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$' ControlToValidate="txtEmailSignUp" Text="Invalid E-Mail" Display="Dynamic" ValidationGroup="SignUp" CssClass="ValidationSummary"></asp:RegularExpressionValidator>&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Email is required" SetFocusOnError="true" ValidationGroup="SignUp" Display="Dynamic" ControlToValidate="txtEmailSignUp" Text="Email is required" CssClass="ValidationSummary">Email is required</asp:RequiredFieldValidator>&nbsp;                                
                        </div>
                    </div>
                    <!-- End of Email Input Field -->

                    <!-- Re enter email input field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE0BE;</i>
                        <asp:TextBox ID="txtSignUpRetypeEmail" CssClass="large validate text-color" runat="server" onchange="ConfirmEmail();" Style="text-transform: lowercase" autocomplete="off" MaxLength="50"></asp:TextBox>
                        <div class="col s12 center">
                            <span style="display: none; color: red" class="spanEmailConfirm">Email do not match!</span>
                        </div>
                        <label for="txtSignUpRetypeEmail">
                            Re-enter E-mail <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" SetFocusOnError="true" ErrorMessage="Invalid Email" ValidationExpression='^[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+(\.[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$' ControlToValidate="txtSignUpRetypeEmail" Text="Invalid E-Mail" Display="Dynamic" ValidationGroup="SignUp" CssClass="ValidationSummary"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" SetFocusOnError="true" ErrorMessage="Re-enter Email is required" ValidationGroup="SignUp" Display="Dynamic" ControlToValidate="txtSignUpRetypeEmail" Text="Re-enter Email is required" CssClass="ValidationSummary">Re-enter Email is required</asp:RequiredFieldValidator>&nbsp;                                
                        </div>

                    </div>
                    <!-- END of Re enter email input field -->
                </div>
                <!-- END of Email Row -->

                <!-- Password Row -->
                <div class="row">
                    <!-- Password Input Field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE897;</i>
                        <asp:TextBox ID="txtPasswordSignUp" TextMode="Password" CssClass="large validate text-color" runat="server" onchange="ConfirmPasswordRetype();" MaxLength="20" autocomplete="off"></asp:TextBox>
                        <label for="txtPasswordSignUp">
                            Password <span class="red-text">*</span>
                        </label>

                        <div class="col s12 center red-text">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" SetFocusOnError="true" runat="server" ErrorMessage="Password is required" ValidationGroup="SignUp" Display="Dynamic" ControlToValidate="txtPasswordSignUp" Text="Password is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" SetFocusOnError="true" ControlToValidate="txtPasswordSignUp" ErrorMessage="Password must be Alphanumeric!" ValidationExpression="^[A-Za-z0-9 _]+[A-Za-z0-9][A-Za-z0-9 _]*$" ValidationGroup="SignUp"></asp:RegularExpressionValidator><br />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" SetFocusOnError="true" ControlToValidate="txtPasswordSignUp" ErrorMessage="Password must be atleast 6 characters!" ValidationExpression="^[\s\S]{6,}" ValidationGroup="SignUp"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <!-- End of Password Input Field -->

                    <!-- Confirm password field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE897;</i>
                        <asp:TextBox ID="txtSignUpRetypePassword" TextMode="Password" CssClass="large validate text-color" runat="server" onchange="ConfirmPasswordRetype();" MaxLength="20" autocomplete="off"></asp:TextBox>
                        <div class="col s12 center">
                            <span style="display: none; color: red" class="spanConfirmPassword">Password do not match!</span>
                        </div>
                        <label for="txtSignUpRetypePassword">
                            Confirm Password <span class="red-text">*</span>
                        </label>

                        <div class="col s12 center red-text">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" SetFocusOnError="true" runat="server" ErrorMessage="Password is required" ValidationGroup="SignUp" Display="Dynamic" ControlToValidate="txtSignUpRetypePassword" Text="Confirm Password is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" SetFocusOnError="true" ControlToValidate="txtSignUpRetypePassword" ErrorMessage="Password must be atleast 6 characters!" ValidationExpression="^[\s\S]{6,}" ValidationGroup="SignUp"></asp:RegularExpressionValidator><br />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" SetFocusOnError="true" ControlToValidate="txtSignUpRetypePassword" ErrorMessage="Password must be Alphanumeric!" ValidationExpression="^[A-Za-z0-9 _]+[A-Za-z0-9][A-Za-z0-9 _]*$" ValidationGroup="SignUp"></asp:RegularExpressionValidator>
                            
                        </div>
                    </div>
                    <!-- END of Confirm password field -->
                </div>
                <!-- END of Password Row -->

                <!-- Gender & Birthday Row -->
                <div class="row">
                    <!-- Birthday Date Picker -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE8DF;</i>
                        <asp:TextBox class="datepicker txtBirthday" ID="txtBirthday" runat="server" MaxLength="1"></asp:TextBox>
                        <label for="txtBirthday">
                            Birthday <span class="red-text">*</span>
                        </label>
                        <div class="col s12 center">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" SetFocusOnError="true" ErrorMessage="Birthday is required" ValidationGroup="SignUp" Display="Dynamic" ControlToValidate="txtBirthday" Text="Birthday is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-- END of Birthday Date Picker -->


                    <div class="input-field col l6 s12">
                        <div class="col s12">
                            <label class="grey-text">Gender <span class="red-text">*</span></label>
                            <br />
                        </div>
                        <!-- Male Radio Button -->
                        <div class="col s3 offset-s2">
                            <asp:RadioButton ID="optMale" runat="server" groupname="gender"/>
                            <label for="optMale">Male</label>
                        </div>
                        <!-- END of Male Radio Button -->

                        <!-- Female Radio Button -->
                        <div class="col s6 offset-s1 left">
                            <asp:RadioButton ID="optFemale" runat="server" groupname="gender"/>
                            <label for="optFemale">Female</label>
                        </div>
                        <!-- END of Female Radio Button -->
                        <div class="col s12 center" style="visibility:hidden;">-----------</div>

                        <div class="col s12 center">
                            <asp:TextBox ID="txtGender" CssClass="large validate text-color" runat="server" MaxLength="1" Style="display:none;"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvGender" runat="server" ErrorMessage="Gender is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtGender" Text="Gender is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <!-- END of Female Radio Button -->
                </div>

                <!-- END of Gender & Birthday Row -->

                <!-- Contact Information -->
                <div class="row">
                    <!-- Mobile Number Input Field -->
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE0DD;</i>
                        <asp:TextBox ID="txtMobileNumber" CssClass="large validate text-color" runat="server" MaxLength="30"></asp:TextBox>
                        <label for="txtMobileNumber">
                            Mobile Number <span class="red-text">*</span>
                        </label>

                        <div class="col s12 center">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Mobile Number is required" SetFocusOnError="true" ValidationGroup="SignUp" Display="Dynamic" ControlToValidate="txtMobileNumber" Text="Mobile Number is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-- End of Password Input Field -->

                    <%-- Telephone Number Input Field --%>
                    <div class="input-field col l6 s12">
                        <i class="material-icons prefix">&#xE0B0;</i>
                        <asp:TextBox ID="txtTelephoneNumber" CssClass="large validate text-color" runat="server" MaxLength="30"></asp:TextBox>
                        <label for="txtTelephoneNumber">
                            Telephone Number                   
                        </label>
                    </div>
                    <!-- END of Confirm password field -->
                </div>
                <!-- End Contact Information -->

                <br />

                <!-- Vehicle upload form-->
                <div class="VehicleUpload">
                    <div class="divider black" style="height: 5px"></div>
                    <!-- Vehicle Information -->
                    <div class="row">
                        <div class="col l6 s12">
                            <h5 class="left" id="vehicleInfo">Your vehicle Information: </h5>
                            <br />
                        </div>
                         
                    </div>
                    <!-- END of  Vehicle Information -->

                    <!-- Brand name, Model Name & Plate Number Row -->
                    <div class="row">
                        <!-- Brand Name Input Field -->
                        <div class="input-field col l4 s12">
                            <i class="material-icons prefix">&#xE88E;</i>
                            <asp:TextBox ID="txtBrandName" TextMode="SingleLine" CssClass="large validate text-color" runat="server" MaxLength="20" autocomplete="off"></asp:TextBox>
                            <label for="txtBrandName">
                                Brand Name
                            </label>

                        </div>
                        <!-- END of Brand Name Input Field -->

                        <!-- Model Name Input Field -->
                        <div class="input-field col l4 s12">
                            <i class="material-icons prefix">&#xE88E;</i>
                            <asp:TextBox ID="txtModelName" TextMode="SingleLine" CssClass="large validate text-color" runat="server" MaxLength="20" autocomplete="off"></asp:TextBox>
                            <label for="txtModelName">
                                Model Name
                            </label>

                        </div>
                        <!-- END of  Model Name Input Field -->

                        <!-- Plate number Input Field -->
                        <div class="input-field col l4 s12">
                            <i class="material-icons prefix">&#xE88E;</i>
                            <asp:TextBox ID="txtPlateNo" TextMode="SingleLine" CssClass="large validate text-color" runat="server" MaxLength="8"></asp:TextBox>
                            <label for="txtPlateNo">
                                Plate Number
                            </label>

                        </div>
                        <!-- END OF Plate number Input Field -->
                    </div>
                    <!-- END of Brand name, Model Name & Plate Number Row -->

                    <div class="row">
                        <!-- Vehicle type Dropdown -->
                        <div class="input-field col l3 s12">
                            <h6 class="type left grey-text">Vehicle type</h6>
                            <asp:DropDownList ID="ddlVehicelType" runat="server" CssClass="browser-default black-text">
                                <asp:ListItem Text="Car" Value="Car"></asp:ListItem>
                                <asp:ListItem Text="Van" Value="Van"></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                        </div>
                        <!-- END of Vehicle type Dropdown -->

                        <!-- Vehicle year model -->
                        <div class="input-field col l3 s12">
                            <h6 class="type left grey-text">Model year</h6>
                            <asp:DropDownList ID="ddlYearModel" runat="server" CssClass="browser-default black-text">
                            </asp:DropDownList>
                        </div>
                        <!-- End of Vehicle year model -->

                        <!-- Max Capacity Input Field -->
                        <div style="padding-top:20px;">
                            <div class="input-field col l3 s12" style="padding-left: 0px;">
                                <i class="material-icons prefix maxCapIcon">&#xE88E;</i>
                                <asp:TextBox ID="txtMaxCap" TextMode="SingleLine" CssClass="large validate text-color" runat="server" MaxLength="2"></asp:TextBox>
                                <label for="txtMaxCap">
                                    Capacity
                                </label>
                            </div>
                        </div>
                        <!-- END of Max Capacity Input Field -->

                        <!-- Color Input Field -->
                            <div class="input-field col l3 s12" style="padding-left: 0px;">
                                <i class="material-icons prefix maxCapIcon">&#xE88E;</i>
                                <asp:TextBox ID="txtVehicleColor" TextMode="SingleLine" CssClass="large validate text-color" runat="server" MaxLength="30"></asp:TextBox>
                                <label for="txtVehicleColor">
                                    Vehicle Color
                                </label>
                            </div>
                        <!-- END of Max Capacity Input Field -->

                    </div>
                    <!-- END of Vehicle type Dropdown & Max Capacity Row -->

                    <!-- One whole row for collapsible accordion --->
                    <div class="row black-text">
                        <h6 class="center"><u>Upload an image of your vehicle:</u></h6>
                        <div class="col s12 center">
                            <h6><span class="red-text redNote">NOTE: Maximum file size is <b>2 MB</b>.</span></h6>
                        </div>
                        <div class="col s12 m6 l4 center">
                            <ul class="collapsible" data-collapsible="accordion">
                                <!-- Front View Upload -->
                                <li>
                                    <div class="collapsible-header">Front View<i class="material-icons">&#xE5C5;</i></div>
                                    <div class="collapsible-body">
                                        <p>
                                            <asp:FileUpload ID="FUFrontView" runat="server" class="black-text" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                                                ControlToValidate="FUFrontView" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                                Display="Dynamic" ValidationGroup="SignUp" />
                                            <asp:CustomValidator runat="server" ID="custPrimeCheck" ForeColor="Red" ControlToValidate="FUFrontView" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="SignUp" />

                                        </p>
                                    </div>
                                </li>
                                <!-- END of Front View Upload -->

                                <!-- Rear View Upload -->
                                <li>
                                    <div class="collapsible-header">Rear View<i class="material-icons">&#xE5C5;</i></div>
                                    <div class="collapsible-body">
                                        <p>
                                            <asp:FileUpload ID="FURearView" runat="server" class="black-text" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                                                ControlToValidate="FURearView" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                                Display="Dynamic" ValidationGroup="SignUp" />
                                            <asp:CustomValidator runat="server" ID="CustomValidator1" ForeColor="Red" ControlToValidate="FURearView" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="SignUp" />
                                        </p>
                                    </div>
                                </li>
                                <!-- END of Rear View Upload -->

                                <!-- Left View Upload -->
                                <li>
                                    <div class="collapsible-header">Left View<i class="material-icons">&#xE5C5;</i></div>
                                    <div class="collapsible-body">
                                        <p>
                                            <asp:FileUpload ID="FULeftView" runat="server" class="black-text" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                                                ControlToValidate="FULeftView" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                                Display="Dynamic" ValidationGroup="SignUp" />
                                            <asp:CustomValidator runat="server" ID="CustomValidator2" ForeColor="Red" ControlToValidate="FULeftView" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="SignUp" />
                                        </p>
                                    </div>
                                </li>
                                <!-- End of Left View Upload -->


                            </ul>
                        </div>
                        <div class="col s12 m6 l4 center">
                            <ul class="collapsible" data-collapsible="accordion">
                                <!-- Right View Upload -->
                                <li>
                                    <div class="collapsible-header">Right View<i class="material-icons">&#xE5C5;</i></div>
                                    <div class="collapsible-body">
                                        <p>
                                            <asp:FileUpload ID="FURightView" runat="server" class="black-text" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                                                ControlToValidate="FURightView" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                                Display="Dynamic" ValidationGroup="SignUp" />
                                            <asp:CustomValidator runat="server" ID="CustomValidator3" ForeColor="Red" ControlToValidate="FURightView" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="SignUp" />
                                        </p>
                                    </div>
                                </li>
                                <!-- Right View Upload -->

                                <!-- Front Interior Upload -->
                                <li>
                                    <div class="collapsible-header">Front Interior<i class="material-icons">&#xE5C5;</i></div>
                                    <div class="collapsible-body">
                                        <p>
                                            <asp:FileUpload ID="FUFrontInterior" runat="server" class="black-text" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                                                ControlToValidate="FUFrontInterior" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                                Display="Dynamic" ValidationGroup="SignUp" />
                                            <asp:CustomValidator runat="server" ID="CustomValidator4" ForeColor="Red" ControlToValidate="FUFrontInterior" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="SignUp" />
                                        </p>
                                    </div>
                                </li>
                                <!-- END of Front Interior Upload -->

                                <!-- Rear Interior Upload -->
                                <li>
                                    <div class="collapsible-header">Rear Interior<i class="material-icons">&#xE5C5;</i></div>
                                    <div class="collapsible-body">
                                        <p>
                                            <asp:FileUpload ID="FURearInterior" runat="server" class="black-text" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                                                ControlToValidate="FURearInterior" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                                Display="Dynamic" ValidationGroup="SignUp" />
                                            <asp:CustomValidator runat="server" ID="CustomValidator5" ForeColor="Red" ControlToValidate="FURearInterior" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="SignUp" />
                                        </p>
                                    </div>
                                </li>
                                <!-- END of Rear Interior Upload -->

                            </ul>
                        </div>

                        <!-- ACCORDION COLLAPSIBLE FOR UPLOAD -->
                        <div class="col s12 m6 l4 center">
                            <ul class="collapsible" data-collapsible="accordion">

                                <!-- Official Receipt Upload -->
                                <li>
                                    <div class="collapsible-header">Official Receipt<i class="material-icons">&#xE5C5;</i></div>
                                    <div class="collapsible-body">
                                        <p>
                                            <asp:FileUpload ID="FUOr" runat="server" CssClass="black-text" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator11" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                                                ControlToValidate="FUOr" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                                Display="Dynamic" ValidationGroup="SignUp" />
                                            <asp:CustomValidator runat="server" ID="CustomValidator6" ForeColor="Red" ControlToValidate="FUOr" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="SignUp" />

                                <asp:TextBox class="dpexpiration txtExpiryDateOR errorLabel" ID="txtExpiryDateOR" runat="server" MaxLength="1" placeholder="Official Receipt Expiration Date"></asp:TextBox>
                                
                                        </p>
                                    </div>
                                </li>
                                <!-- End of Official Receipt Upload -->

                                <!-- Registration Certificate Upload -->
                                <li>
                                    <div class="collapsible-header">Registration Cert.<i class="material-icons left">&#xE5C5;</i></div>
                                    <div class="collapsible-body">
                                        <p>
                                            <asp:FileUpload ID="FUCR" runat="server" class="black-text" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator9" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                                                ControlToValidate="FURearInterior" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                                Display="Dynamic" ValidationGroup="SignUp" />
                                            <asp:CustomValidator runat="server" ID="CustomValidator7" ForeColor="Red" ControlToValidate="FUCR" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="SignUp" />
                                        </p>
                                    </div>
                                </li>
                                <!-- END of Registration Certificate Upload -->

                                <!-- Insurance Upload -->
                                <li>
                                    <div class="collapsible-header">Insurance<i class="material-icons">&#xE5C5;</i></div>
                                    <div class="collapsible-body">
                                        <p>
                                            <asp:FileUpload ID="FUInsurance" runat="server" class="black-text" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                                                ControlToValidate="FURearInterior" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                                Display="Dynamic" ValidationGroup="SignUp" />
                                            <asp:CustomValidator runat="server" ID="CustomValidator8" ForeColor="Red" ControlToValidate="FUInsurance" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="SignUp" />

                                            <asp:TextBox class="dpexpiration txtExpiryDateInsurance errorLabel" ID="txtExpiryDateInsurance" runat="server" MaxLength="1" placeholder="Insurance Expiration Date"></asp:TextBox>
                                        </p>
                                    </div>
                                </li>
                                <!-- END of Insurance Upload -->
                            </ul>
                        </div>
                        <!-- END OF ACCORDION COLLAPSIBLE FOR UPLOAD -->
                    </div>
                    <!--  END of One whole row for collapsible accordion -->
                </div>
                <!-- END of div Vehicle Upload Form -->


                <div class="row">
                    <div class="col s12 center">
                        <a href="Index.aspx" class="btn waves-effect waves-light pink accent-2">Home</a>
                        <asp:LinkButton ID="lbtnSubmit" runat="server" class="btn btnSubmit black lbtnSubmit" ValidationGroup="SignUp" OnClick="lbtnSubmit_Click">Sign Up</asp:LinkButton>
                    </div>
                </div>
                <div class="row center">
                    <a href="MemberSignUp.Aspx">Switch to Member Sign Up</a><br />
                    <a href="Login.aspx?UT=Operator" class="black-text"><u>LOGIN AS OPERATOR</u></a>
                </div>                
                <div class="divider black" style="height: 5px"></div>
                <br />
            </div> <!-- END of card-panel -->
        </div>
        <!-- END of container -->
    </form>

    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/script") %>
    </asp:PlaceHolder>

    <script src="Scripts/minified/loginsignup.min.js"></script>

    <script>
        /* Date picker for Expiration Date */
        $('.dpexpiration').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: true,
            min: -1,
            max: 10000
        });

        //Operator Vehicle
        function Upload(sender, args) {
            var fileUpload = document.getElementById(sender.controltovalidate);
            if (typeof (fileUpload.files) != "undefined") {
                var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
                var maxFileSize = 1000
                if (size < maxFileSize) {
                    args.IsValid = true;
                } else {
                    $(fileUpload).closest("div").css({ "display": "block" });
                    args.IsValid = false;
                    return;
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        }

        $('#lbtnSubmit').click(function () {
            var blnreturn = true;
            $.each(Page_Validators, function (index, validator) {
                if (!validator.isvalid) {
                    $(validator).closest("div").css({ "display": "block" });
                }
            });
            return blnreturn;
        });

        //Numbers Only (Mobile, Telephone) Validation
        $("#txtMobileNumber,#txtTelephoneNumber,.Dzipcode,.Ozipcode,#txtMaxCap").keydown(function (e) {
            if ($.inArray(e.keyCode, [8, 9, 27, 13]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            if (e.keyCode == 189 || e.keyCode == 109 || e.keyCode == 107) {
            return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) || e.keyCode == 46) {
                e.preventDefault();
            }
        });
        //End Validation


    </script>
</body>
</html>
