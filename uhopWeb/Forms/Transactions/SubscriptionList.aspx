﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Site1.Master" AutoEventWireup="true" CodeBehind="SubscriptionList.aspx.cs" Inherits="uhopWeb.Forms.Transactions.SubscriptionList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div id="messagePrompt" style="background-color:green;color:black;text-align:center;padding:10px; display:none;" hide></div>
    <br />

    <asp:PlaceHolder ID="PlaceHolder2" runat="server">
        <%: Styles.Render("~/bundle/SubscriptionHistoryStyle") %>
    </asp:PlaceHolder>
    <link href="../../Styles/jquery.datetimepicker.css" rel="stylesheet" />
    <style>
        .customContainer {
            max-width: 1600px;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
    <br />
    <div class="customContainer">
        <div class="row">
            <div class="col s12">
                <asp:HiddenField ID="hdfMemberSubscription" runat="server" />
                <h4><i class="small material-icons">list</i>&nbsp;&nbsp;&nbsp;&nbsp;My Subscription History</h4>
            </div>
        </div>
        <div class="divider"></div>
        <br />


        <div class="row">
            <div class="col s12">
                <ul class="filterOptions hoverable">
                    <li>Filter Options<span><asp:LinkButton CssClass="waves-effect waves-light btn" ID="btnSearch" runat="server" OnClick="btnSearch_Click"><i class="material-icons left">search</i>Search</asp:LinkButton></span></li>
                    <li><b>Status:</b></li>
                    <li>
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="browser-default">
                            <asp:ListItem Value="A">All</asp:ListItem>
                        </asp:DropDownList></li>
                </ul>
            </div>
        </div>


        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvSubscription" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="gvSubscription_PageIndexChanging" OnRowCommand="gvSubscription_RowCommand" PageSize="5" ShowHeaderWhenEmpty="True" CssClass="hoverable responsive-table">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%--<%# Eval("IsUsed").ToString()=="0"?"<a href='#' id='" + Eval("MemberSubscriptionID").ToString() + ">Edit</a>":""%>--%>
                                <a href="#" id='<%# Eval("MemberSubscriptionID").ToString() %>' style="display: <%# Eval("IsUsed").ToString()=="0"?"block":"none"%>;" onclick="showCover()" class="editSubscriptiondateDate">Edit</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label2" runat="server" Text="Date Purchased"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblBookingDate" runat="server" Text='<%# Convert.ToDateTime(Eval("CreatedOn").ToString()).ToString("MMM dd, yyyy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label4" runat="server" Text="Service Area"></asp:Label>
                            </HeaderTemplate>
                            <ItemStyle VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label ID="lblServiceAreaOrigin" runat="server" Text='<%# Eval("ServiceAreaLocationOrigin").ToString() %>'></asp:Label>
                                <i class="material-icons">&#xE040;</i>
                                <asp:Label ID="lblServiceAreaDestination" runat="server" Text='<%# Eval("ServiceAreaLocationDestination").ToString() %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label6" runat="server" Text="Effectivity Date"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblPickup" runat="server" Text='<%# Convert.ToDateTime(Eval("DateSubscribed").ToString()).ToString("MMM dd, yyyy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label8" runat="server" Text="Expiration Date"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDropOffPoint" runat="server" Text='<%# Convert.ToDateTime(Eval("DateExpired").ToString()).ToString("MMM dd, yyyy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label10" runat="server" Text="Price & Type"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblPreferedDepartureDateTime" runat="server" Text='<%# "Php " + Eval("PriceType").ToString() %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label14" runat="server" Text="Status"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblScheduledDepartureDateTime" runat="server" Text='<%# System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(Eval("Status").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%--<%# Eval("IsUsed").ToString()=="0"?"<a href='#' id='" + Eval("MemberSubscriptionID").ToString() + ">Edit</a>":""%>--%>
                                <a href="../AurumPay/AurumModesOfPayment.aspx?ND54653=<%# uhopWeb.Classes.ClsEncryptor.Encrypt(Eval("ServiceAreaSubscriptionID").ToString(),"u-h0p") %>" id='<%# Eval("ServiceAreaSubscriptionID").ToString() %>' style="display: <%# Eval("IsUsed").ToString()=="1"?"block":"none"%>;">Renew</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" Text="No Record\s Found!"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div id="divCover" class="divCover" runat="server" hide>
        <div class="container">
            <div class="card">
                <div class="row">
                    <div class="col s12 center">
                        <h4>Subscription effectivity date</h4>
                    </div>
                </div>
                <div class="row center noMargin">
                    <div class="input-field col m6 offset-m3 s12">
                        <i class="Large material-icons prefix">&#xE8A3;</i>
                        <input type="date" id="txtDepatureDateTime" runat="server" class="Subscriptiondatepicker txtDepatureDateTime" value="" />
                        <label for="txtDepatureDateTime">Set the date Here</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" class="rfvRequired" runat="server" ErrorMessage="Complete Date is required" ControlToValidate="txtDepatureDateTime" Display="Dynamic" ValidationGroup="EditSubscriptionDate" Text="Effectivity Date is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row center noMargin">
                    <div class="input-field col m6 offset-m3 s12">
                        <span runat="server" id="spnSubscribe" class="red-text" style="display: none;"></span>
                    </div>
                </div>

                <div class="row center">
                    <div class="col l2 offset-l5 m6 offset-m3 s8 offset-s2">
                        <a runat="server" id="btnProceed" class="waves-effect waves-blue btn green darken-2" validationgroup="PurchasedSubscription" style="display: none;" onserverclick="btnProceed_Click">PROCEED</a>
                    </div>
                </div>

                <div class="row center">
                    <%--<a runat="server" id="A2" href="#!" class="waves-effect waves-green btn blue darken-2" validationgroup="EditSubscriptionDate" onserverclick="btnUpdate_Click">Save Changes</a>--%>
                    <%--<button type="button" class="waves-effect waves-green btn blue darken-2" validationgroup="EditSubscriptionDate">Save Changes</button>--%>
                    <%--<asp:Button ID="btnEditSubscription" class="waves-effect waves-green btn blue darken-2" validationgroup="EditSubscriptionDate" runat="server" Text="Save Changes" OnClick="CheckIfExist()" />--%>
                    <asp:Button ID="btnEditSubscription" runat="server" OnClick="btnEditSubscription_Click" Style="display: none" />
                    <button type="button" class="waves-effect waves-green btn blue darken-2" validationgroup="EditSubscriptionDate" onclick="CheckIfExist()" style="margin-bottom: 5px;">Save Effectivity Date</button>
                    <%--<asp:LinkButton ID="btnUpdate" runat="server" class="modal-action waves-effect waves-green btn blue darken-2" ValidationGroup="EditSubscriptionDate" OnClick="btnUpdate_Click">Save Changes</asp:LinkButton>--%>
                    <button type="button" class="btn btn-primary btn btn-lg red accent-2" onclick="HideCoverSList()" style="margin-bottom: 5px;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

<%--    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Styles.Render("~/bundle/style") %>
    </asp:PlaceHolder>--%>
    <script src="../../Scripts/jquery-2.1.4.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //var today = new Date();
            //today.setDate(today.getDate() + 1);
            //var dd = today.getDate();
            //var mm = today.getMonth() + 1; //January is 0!

            //var yyyy = today.getFullYear();
            //if (dd < 10) {
            //    dd = '0' + dd
            //}
            //if (mm < 10) {
            //    mm = '0' + mm
            //}
            //var today = yyyy + '-' + mm + '-' + dd;

            //$(".txtDepatureDateTime").attr('min', today);


            $('.Subscriptiondatepicker').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: true,
                min: 1,
                max: 10000
            });
        });





        function CheckIfExist(sender, args) {
            
            if ($(".txtDepatureDateTime").val() != "") {
                var blnReturn = false;
                var EffectivityDate = $(".txtDepatureDateTime").val();
                var hdfMemberSubscription = $("input[id*='hdfMemberSubscription']").val();
                if (EffectivityDate != "" && hdfMemberSubscription != "") {
                    $.ajax({
                        type: "POST",
                        url: "SubscriptionList.aspx/CheckMembershipDate",
                        data: "{'pMemberSubscription':'" + hdfMemberSubscription + "','pEffectivityDate':'" + EffectivityDate + "'}",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (response) {
                            try {
                                if (response.d[0].MemberSubscription == "Exist") {
                                    // alert("Exist");
                                    document.getElementById("ContentPlaceHolder1_spnSubscribe").textContent = "Currently you have existing subscription with the same Service Area and will be expire on : " + response.d[0].ExpirationDate;
                                    $("#ContentPlaceHolder1_spnSubscribe").css({ "display": "block" });
                                    $("#ContentPlaceHolder1_btnProceed").css({ "display": "block" });
                                    blnReturn = false;
                                }
                                else {
                                    //alert("NotExist");
                                    blnReturn = true;
                                    document.getElementById('<%= btnEditSubscription.ClientID %>').click();
                                }
                            }
                            catch (e) {

                            }
                        }
                    });
                }

            }
            else {
                $("#ContentPlaceHolder1_RequiredFieldValidator5").css({"display": "block"});
             
            }
            return blnReturn;
        };

        function HideCoverSList() {
            $(".divCover").removeClass("show");
            $(".divCover").addClass("hide");
        }

        function showCover() {
            $(".divCover").removeClass("hide");
            $(".divCover").addClass("show");
        }

        $('.editSubscriptiondateDate').click(function () {
            $("input[id*='hdfMemberSubscription']").val($(this).attr('id'));
            $("#ContentPlaceHolder1_spnSubscribe").css({ "display": "none" });
            $("#ContentPlaceHolder1_btnProceed").css({ "display": "none" });
            $(".txtDepatureDateTime").val("");
            $("#ContentPlaceHolder1_RequiredFieldValidator5").css({ "display": "none" });
        });

        function stopScroll() {
            $('.vDetails').on('click', function () {
                $('html, body').addClass('stop-scrolling');
                $('body').bind('touchmove', function (e) { e.preventDefault() })
            });
        }

        function continueScroll() {
            /* FOR MODAL */
            $('.onDismissal, #lean-overlay').on('click', function () {
                $('html, body').removeClass('stop-scrolling');
                $('body').unbind('touchmove');
            });
        }

    </script>


</asp:Content>
