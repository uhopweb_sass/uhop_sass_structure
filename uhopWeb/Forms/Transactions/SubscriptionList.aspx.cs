﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;
using System.Data;
using System.Web.Services;

namespace uhopWeb.Forms.Transactions
{
    public partial class SubscriptionList : System.Web.UI.Page
    {
        public static string strMemberID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack){
                clsAuthenticator.Authenticate();
                UHopCore.Title = "Subscription List";
                strMemberID = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                ViewState["MemberID"] = strMemberID;

                ClsMembers objMembers = new ClsMembers(strMemberID);
                objMembers.Fill(ViewState["MemberID"].ToString());

                if (objMembers.TblMembers.Rows[0].GetString("Status") == "0")
                {
                    Response.Redirect("../Maintenance/UserProfile.aspx");
                }
                
                //if (ViewState["TblMembers"] == null)
                //{
                //    objMembers.Fill(ViewState["MemberID"].ToString());
                //    ViewState["TblMembers"] = objMembers.TblMembers;
                //}
                //objMembers.TblMembers = (DataSets.DSConfigs.MemberDataTable)objMembers.TblMembers;

                //var MemberDetails = objMembers.TblMembers.FirstOrDefault();

                //if (MemberDetails.Status == "0")
                //{
                //    Response.Redirect("../Maintenance/UserProfile.aspx");
                //}
                LoadStatus();
                LoadGridView(ddlStatus.SelectedItem.ToString());

            }
            else { 

            }
        }


        public void LoadStatus()
        {
            ClsSubscription objMemberSubscription = new ClsSubscription();
            objMemberSubscription.FillMemberSubscriptionList(strMemberID);
            if (objMemberSubscription.TblMemberSubscriptionList.Count > 0)
            {
                DataTable dt = objMemberSubscription.TblMemberSubscriptionList.AsEnumerable()
                               .GroupBy(r => new { Col1 = r["Status"] })
                               .Select(g => g.OrderBy(r => r["Status"]).First())
                               .CopyToDataTable();
                int x = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    ddlStatus.Items.Insert(x, new ListItem(System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(dr["Status"].ToString()), dr["MemberSubscriptionID"].ToString()));
                    ddlStatus.SelectedIndex = 0;
                    x += 1;
                }
            }
        }

        private void LoadGridView(string status)
        {
            //ClsViews.ViewBookingHistory objViewBookingHistory = new ClsViews.ViewBookingHistory(ViewState["MemberID"].ToString());
            ClsSubscription objMemberSubscription = new ClsSubscription();
            objMemberSubscription.FillMemberSubscriptionList(strMemberID);
            if (status == "All") { status = "%"; }

            //DateTime dtBookingDate = Convert.ToDateTime(date);
            string filterBy = String.Format("Status LIKE '{0}'", status);
            DataRow[] drw =  objMemberSubscription.TblMemberSubscriptionList.Select(filterBy, "DateSubscribed ASC");

            if (drw.Length > 0)
            {
                gvSubscription.DataSource = drw.CopyToDataTable();
                lblMessage.Visible = false;
            }
            else
            {
                gvSubscription.DataSource = null;
                lblMessage.Visible = true;
            }
            gvSubscription.DataBind();

        }


        protected void gvSubscription_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSubscription.PageIndex = e.NewPageIndex;
            LoadGridView(ddlStatus.SelectedItem.ToString());
        }

        protected void gvSubscription_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView(ddlStatus.SelectedItem.ToString());
        }

        protected void btnProceed_Click(object sender, EventArgs e)
        {
            if (txtDepatureDateTime.Value.Trim() != "" && hdfMemberSubscription.Value.Trim() != "")
            {
                if (Convert.ToDateTime(txtDepatureDateTime.Value) >= DateTime.Today.AddDays(1))
                {
                    EditSubscriptionDate();
                    
                }
            }
        }

        public void EditSubscriptionDate()
        {
            using (ClsMemberSubscription objMemberSubscription = new ClsMemberSubscription())
            {
                DateTime dtSubscribed = Convert.ToDateTime(txtDepatureDateTime.Value);
                objMemberSubscription.Fill(objMemberSubscription.TblMemberSubscription, strMemberID);
                DataTable dt = objMemberSubscription.TblMemberSubscription;
                DataRow[] drw = dt.Select("MemberSubscriptionID='" + hdfMemberSubscription.Value + "'");
                ClsTransaction objTransaction = new ClsTransaction();
                objTransaction.FillTransactionSubscriptionDetails(drw[0].GetString("ServiceAreaSubscriptionID"));
                DataRow[] drwSubscription = objTransaction.TblTransactionSubscriptionDetails.Select();
                
                drw[0].BeginEdit();
                drw[0].SetDateTime("DateSubscribed", dtSubscribed);
                drw[0].SetDateTime("DateExpired", dtSubscribed.AddDays(drwSubscription[0].GetInt32("ValidForNoOfDays") -1));
                drw[0].EndEdit();

                objMemberSubscription.Update();
                LoadGridView(ddlStatus.SelectedItem.ToString());
                divCover.Attributes.Remove("show");
                System.Text.StringBuilder sbs = new System.Text.StringBuilder();
                sbs.Append("<script type = 'text/javascript'>");
                sbs.Append("window.onload=function(){");
                sbs.Append("$('#messagePrompt').text('");
                sbs.Append("Subscription Effective date successfully Updated");
                sbs.Append("');");
                sbs.Append("$(\"#messagePrompt\").show(0).delay(3000).fadeOut(\"slow\");");
                sbs.Append("};");
                sbs.Append("</script>");
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sbs.ToString());
            }
        }

        public class SubscriptionListValidate
        {
            private string _MemberSubscription;
            public string MemberSubscription
            {
                get { return _MemberSubscription; }
                set { _MemberSubscription = value; }
            }

            private string _ExpirationDate;
            public string ExpirationDate
            {
                get { return _ExpirationDate; }
                set { _ExpirationDate = value; }
            }
        }

        [WebMethod]
        public static List<SubscriptionListValidate> CheckMembershipDate(string pMemberSubscription,string pEffectivityDate)
        {

            using (ClsMemberSubscription objMemberSubscription = new ClsMemberSubscription())
            {
                List<SubscriptionListValidate> lstMemberSubscription = new List<SubscriptionListValidate>();
                DataTable tbl = new DataTable();
                ClsSubscription objSubscription = new ClsSubscription();
                objSubscription.FillMemberSubscriptionList(strMemberID);
                if (pEffectivityDate.Trim() != "")
                {
                    DataRow[] drwSID = objSubscription.TblMemberSubscriptionList.Select("MemberSubscriptionID='" + pMemberSubscription + "'");
                    objMemberSubscription.FillMemberSubscriptionValidation(strMemberID, drwSID[0].GetString("ServiceAreaSubscriptionID"), pEffectivityDate);
                    DataRow[] drwMemberSubscription = objMemberSubscription.TblMemberSubscriptionValidation.Select("MemberSubscriptionID <> '" + pMemberSubscription + "'");
                    if (drwMemberSubscription.Count() > 0)
                    {
                        tbl = objMemberSubscription.TblMemberSubscriptionValidation;
                        if (tbl.Rows.Count > 0)
                        {
                            foreach (DataRow drw in tbl.Rows)
                            {
                                var Expirationdate = Convert.ToDateTime(drw["DateExpired"].ToString()).ToString("MMM dd, yyyy");
                                lstMemberSubscription.Add(new SubscriptionListValidate
                                {
                                    MemberSubscription = "Exist",
                                    ExpirationDate = Expirationdate
                                });
                            }
                        }
                    }
                    else
                    {
                        lstMemberSubscription.Add(new SubscriptionListValidate
                            {
                                MemberSubscription = "NotExist"
                            });
                    }
                }
                return lstMemberSubscription;
            }

        }

        protected void btnEditSubscription_Click(object sender, EventArgs e)
        {
         if (txtDepatureDateTime.Value.Trim() != "" && hdfMemberSubscription.Value.Trim() != "")
            {
                using (ClsMemberSubscription objMemberSubscriptionSubscription = new ClsMemberSubscription())
                {
                    ClsSubscription objMemberSubscription = new ClsSubscription();
                    objMemberSubscription.FillMemberSubscriptionList(strMemberID);
                    DataRow[] drwSID = objMemberSubscription.TblMemberSubscriptionList.Select("MemberSubscriptionID='" + hdfMemberSubscription.Value + "'");
                    if (drwSID.Count() > 0)
                    {
                        if (drwSID[0].GetString("IsUsed") == "0")
                        {
                            objMemberSubscriptionSubscription.FillMemberSubscriptionValidation(strMemberID, drwSID[0].GetString("ServiceAreaSubscriptionID"), txtDepatureDateTime.Value);
                            DataRow[] drwMemberSubscription = objMemberSubscriptionSubscription.TblMemberSubscriptionValidation.Select("MemberSubscriptionID <> '" + hdfMemberSubscription.Value + "'");
                            if (drwMemberSubscription.Count() <= 0)
                            {
                            
                                if (Convert.ToDateTime(txtDepatureDateTime.Value) >= DateTime.Today.AddDays(1))
                                {
                                    spnSubscribe.Attributes.Add("style", "display:none");
                                    btnProceed.Attributes.Add("style", "display:none");
                                    EditSubscriptionDate();
                                }
                            
                            }
                            else
                            {
                                                                
                                    spnSubscribe.Attributes.Add("style", "display:block;margin-bottom:10px");
                                    btnProceed.Attributes.Add("style", "display:block");
                                    spnSubscribe.InnerText = "You already have an existing subscription with the same Service Area and that will expire on : " + Convert.ToDateTime(drwMemberSubscription[0].GetString("DateExpired")).ToLongDateString();
                                    //btnEditSubscription.Attributes.Add("onclick", "return false");
                               
                            }
                        }
                    }

                }
            }
        }

       //public void Show


    }
}