﻿<%@ Page Title="" EnableEventValidation="true" Language="C#" MasterPageFile="~/Forms/Site1.Master" AutoEventWireup="true" CodeBehind="BookingNew.aspx.cs" Inherits="uhopWeb.Forms.Transactions.BookingNew" %>

<%@ Register Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc2" %>
<%@ Register Src="~/UserControls/UCTimePicker.ascx" TagPrefix="uc1" TagName="UCTimePicker" %>





<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:PlaceHolder ID="PlaceHolder2" runat="server">
        <%: Styles.Render("~/bundle/BookingStyle") %>
    </asp:PlaceHolder>

    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>--%>

    <style>
        #ContentPlaceHolder1_UpdatePanel3 {
            display: inline-block;
        }

        body {
            overflow-x: hidden;
            overflow-y: hidden;
            display:initial;
        }
    </style>

    <!--For ServiceArea Selection Cover-->

    <div id="ServiceAreaSelectionCover" class="ServiceAreaSelectionCover">
        <div class="container">
        <div class="col l4 offset-l4 s12">
        <h1 class="title center">Select your service area</h1>
        </div>
        <div class="ServiceAreaSelectionCoverContent">
            <div class="row">
                
                <p class="pErrorServiceAreaSelection hide"></p>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <asp:DropDownList ID="ddlServiceAreaFrom" runat="server" AutoPostBack="true"  CssClass="browser-default ddlServiceArea" OnSelectedIndexChanged="ddlServiceAreaFrom_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlServiceAreaTo" runat="server" AutoPostBack="False" CssClass="browser-default ddlServiceArea">
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlServiceAreaFrom" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                </div>
            </div>    
            
            <div class="row">
                <a href="#" class="pink accent-2 btn btn-primary btn btn-lg aServiceAreaNext">Next</a>
            </div>        
        </div>
        </div>
    </div>

    <!--End ServiceArea Selection Cover-->

    <!--For return-->
    <div id="divCoverReturn" class="hide">
        <ul id="ulMsgReturn" class="hide">
        </ul>
        <div class="input-field inputBoxHolder">
            <input type="text" id="txtNumberOfHrs" value="8" maxlength="2" />
            <label for="txtNumberOfHrs">Number of hours</label>
        </div>
        <div id="divDepartureScheduleListContainer">
            <table id="tblDepartureScheduleList">
                <tbody>
                    <tr>
                        <th colspan="3">Departure</th>
                        <th colspan="3">Return</th>
                    </tr>
                    <tr>
                        <th>Date Time</th>
                        <th>Pickup</th>
                        <th>Drop-off</th>
                        <th>Date Time</th>
                        <th>Pickup</th>
                        <th>Drop-off</th>
                    </tr>

                </tbody>
            </table>
        </div>
        <div id="divReturnButtonContainer">
            <button type="button" class="pink accent-2 btn btn-primary btn btn-lg" id="btnReturnSubmit" onclick="CloseBookingReturnCover();">Close</button>
            <button type="button" class="pink accent-2 btn btn-primary btn btn-lg" id="btnReturnClose" onclick="SubmitReturnBooking(this);">Submit</button>
        </div>
    </div>
    <!--End return-->

    <!--For new booking-->
    <div id="divCover" class="hide">
        <div id="divHeader">
            <ul id="ulMsg" class="hide">
            </ul>
            <h4 id="h4Origin"></h4>
            <i class="Arrow"></i>
            <h4 id="h4Destination"></h4>
        </div>
        <div id="divContainer">


            <div class="row" id="divScheduleList">
                <table id="tblScheduleList">
                    <tbody>
                    </tbody>
                </table>
            </div>

            <div class="row">
                <uc1:UCTimePicker runat="server" ID="UCTimePicker" />
            </div>

            <div class="row">
                <div class="col s12 l12" id="divNewBookingButtonContainer">
                    <button type="button" class="pink accent-2 btn btn-primary btn btn-lg" id="btnCloseBooking" onclick="CloseBookingNewCover();">Close</button>
                    <button type="button" class="pink accent-2 btn btn-primary btn btn-lg" id="btnSubmitBooking" onclick="SubmitMe(this);">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <!--End For new booking-->
    <div id="divTopOptions">
        <div class="row">
                <div class="input-field col s12 l3">
                    <br />
                    <select id="ddlPickup" runat="server" class="browser-default ddlServiceArea"></select>

                </div>
                <div class="input-field col s12 l3">
                    <br />
                    <select id="ddlDropOff" runat="server" class="browser-default ddlServiceArea"></select>
                </div>
            <div class="input-field col s12 l6">
                <button type="button" class="pink accent-2 btn btn-primary btn btn-lg OptionButtons" onclick="ResetMe();" id="Button1">Reset</button>
                <button type="button" class="pink accent-2 btn btn-primary btn btn-lg OptionButtons" onclick="ShowCover();" id="btnShowSummary">Submit Booking</button>
            </div>

        </div>
        <a class="white-text" id="bCloseTopOptions" onclick="HideTopOptions();" style="bottom:10px !important;"><i class="material-icons">&#xE5CE;</i></a>
    </div>

    <div id="divSearchServiceArea" class="hide tooltipped" onclick="displayTopOptions();" style="top:70px !important;left:120px !important;" data-position="right" data-delay="10" data-tooltip="Click to show the origin and destination picker again">&nbsp;</div>
    <div id="dvMap"></div>

    <script async defer src="https://maps.googleapis.com/maps/api/js?signed_in=true&callback=initMap"></script>

    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/BookingScript") %>
    </asp:PlaceHolder>

</asp:Content>
