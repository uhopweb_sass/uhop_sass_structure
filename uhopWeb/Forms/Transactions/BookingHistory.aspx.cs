﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;
using System.Data;

namespace uhopWeb.Forms.Transactions
{
    public partial class BookingHistory : System.Web.UI.Page
    {
        private void LoadGridView(string status,string date) {
            ClsViews.ViewBookingHistory objViewBookingHistory = new ClsViews.ViewBookingHistory(ViewState["MemberID"].ToString());

            if (status == "A") { status = "%"; }

            DateTime dtBookingDate = Convert.ToDateTime(date);
            string filterBy = String.Format("Status LIKE '{0}' AND CreatedOn<=#{1}#",status,new DateTime(dtBookingDate.Year,dtBookingDate.Month,dtBookingDate.Day,23,59,59));
            DataRow[] drw = objViewBookingHistory.TblBookingHistory.Select(filterBy, "CreatedOn DESC");

            if (drw.Length > 0)
            {
                gvBookingHistory.DataSource = drw.CopyToDataTable();
                lblMessage.Visible = false;
            }
            else
            {
                gvBookingHistory.DataSource = null;
                lblMessage.Visible = true;
            }
            gvBookingHistory.DataBind();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UHopCore.Title = "Booking History";
                clsAuthenticator.Authenticate();
                string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                ViewState["MemberID"] = strDecrypted;

                ClsMembers objMembers = new ClsMembers();
                if (ViewState["TblMembers"] == null)
                {
                    objMembers.Fill(ViewState["MemberID"].ToString());
                    ViewState["TblMembers"] = objMembers.TblMembers;
                }
                objMembers.TblMembers = (DataSets.DSConfigs.MemberDataTable)objMembers.TblMembers;

                var MemberDetails = objMembers.TblMembers.FirstOrDefault();

                if (MemberDetails.Status == "0")
                {
                    Response.Redirect("../Maintenance/UserProfile.aspx");
                }


                txtBookingDate.Value = DateTime.Now.ToString("dd MMMM, yyyy");

                LoadGridView(ddlStatus.SelectedValue.ToString(),txtBookingDate.Value);

            }
        }

        protected void gvBookingHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvBookingHistory.PageIndex = e.NewPageIndex;
            LoadGridView(ddlStatus.SelectedValue.ToString(), txtBookingDate.Value);
        }

        protected void gvBookingHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView(ddlStatus.SelectedValue.ToString(), txtBookingDate.Value);

        }
    }
}