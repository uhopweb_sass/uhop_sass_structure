﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Site1.Master" AutoEventWireup="true" CodeBehind="BookingSuccess.aspx.cs" Inherits="uhopWeb.Forms.Transactions.BookingSuccess" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:PlaceHolder ID="PlaceHolder2" runat="server">
    <%: Styles.Render("~/bundle/BookingSuccessStyle") %>
</asp:PlaceHolder>


    <p>You have successfully booked your <span class="pink-text">t</span><span class="blue-text">rip!</span><br /><span>Would you like to book another?</span></p>
    <div id="btnContainer"><a class="waves-effect waves-light btn blue darken-2" id="btnYes">Yes</a>&nbsp;<a class="waves-effect waves-light btn" id="btnNo">No</a></div>


<asp:PlaceHolder ID="PlaceHolder1" runat="server">
    <%: Scripts.Render("~/bundle/BookingSuccessScript") %>
</asp:PlaceHolder>
</asp:Content>
