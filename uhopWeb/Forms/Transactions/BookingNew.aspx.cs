﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;
using System.Data;
using System.Web.Services;

namespace uhopWeb.Forms.Transactions
{
    public partial class BookingNew : System.Web.UI.Page
    {
        public static string strMemberID;

        public class Coordinates{
            public decimal Latitude{get;set;}
            public decimal Longitude{get;set;}
            public Boolean IsPickup { get; set; }
        }

        public class Station
        {
            public string MajorStopID { get; set; }   
            public string StationName {get;set;}
        }

        public class Schedule
        {
            public string DepartureDate { get; set; }
            public string Time { get; set; }
            public string PickUp{get;set;}
            public string DropOff{get;set;}
        }

        public class ReturnSchedule 
        {
            public string DepartureDateTime { get; set; }            
            public string DeparturePickUp { get; set; }
            public string DepartureDropOff { get; set; }
            public string ReturnDateTime { get; set; }
            public string ReturnPickUp { get; set; }
            public string ReturnDropOff { get; set; }        
        }

        DataTable tblLimited = new DataTable();
        DataTable tblUnlimited = new DataTable();

        [WebMethod(EnableSession=true)]
        public static string IsReturn(string Origin, string Destination)
        {

            string strRetrun = "";
            clsAuthenticator.Authenticate();
            string strDecrypted = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
            string memberID =strDecrypted;
            string serviceAreaID = ClsServiceAreaLocation.GetServiceAreaID(Origin, Destination);
            ClsBooking objBooking = new ClsBooking();
            objBooking.Fill(objBooking.tblBooking, memberID);
            strRetrun = ((objBooking.tblBooking.Select("ServiceAreaID='" + serviceAreaID + "'").Length > 0) ? "0" : "1");           
            return strRetrun;
        }

        [WebMethod]
        public static List<ReturnSchedule> PopulateReturnTrip(string PickUpID, string DropOffID, string origin, string destination, int hour)
        {
            string serviceAreaID = ClsServiceAreaLocation.GetServiceAreaID(origin, destination);

            new ClsServiceAreaSubscription().Fill(serviceAreaID);
            List<ReturnSchedule> scheduleList = new List<ReturnSchedule>();
            ClsServiceAreaMajorStopDataTable objServiceAreaMajorStopDataTable = new ClsServiceAreaMajorStopDataTable();
            objServiceAreaMajorStopDataTable.Fill(serviceAreaID, ClsServiceAreaLocation.GetServiceAreaLocationID(origin, destination));
            ClsBooking objBooking = new ClsBooking();
            string strDecrypted = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
            objBooking.Fill(objBooking.tblBooking, strDecrypted);
            IEnumerable<DataRow> query = objBooking.tblBooking.Where(x => x.ServiceAreaID == serviceAreaID && x.IsCancelled == "0" && x.IsFulfilled == "0" && x.IsRebook == "0" && x.IsVoid == "0").OrderBy(y => y.DepartureDateTime);

            foreach (DataRow drw in query)
            {
                if (Convert.ToDateTime(drw.GetDateTime("DepartureDateTime").AddDays(-1).ToString("yyyy-MM-dd")) > Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")))
                {
                    if (objServiceAreaMajorStopDataTable.GetStationName(PickUpID) == drw.GetString("PickupLocation") && objServiceAreaMajorStopDataTable.GetStationName(DropOffID) == drw.GetString("DropOffLocation"))
                    {
                        string strTemp = PickUpID;
                        PickUpID = DropOffID;
                        DropOffID = strTemp;
                    }
                    scheduleList.Add(new ReturnSchedule()
                    {
                        DepartureDateTime = drw.GetDateTime("DepartureDateTime").ToString("MMM dd, yy hh:mm tt"),
                        DeparturePickUp = drw.GetString("PickupLocation"),
                        DepartureDropOff = drw.GetString("DropOffLocation"),
                        ReturnDateTime = drw.GetDateTime("DepartureDateTime").AddHours(hour).ToString("MMM dd, yy hh:mm tt"),
                        ReturnPickUp = PickUpID.Substring(0,1)=="S"?origin : objServiceAreaMajorStopDataTable.GetStationName(PickUpID),
                        ReturnDropOff =DropOffID.Substring(0,1)=="S"?destination : objServiceAreaMajorStopDataTable.GetStationName(DropOffID)
                    });
                }
            }
            return scheduleList;
        }

        [WebMethod(EnableSession = true)]
        public static string IsReturnTrip(string origin,string destination)
        {
            string strReturn = "";
            clsAuthenticator.Authenticate();
            string strDecrypted = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
            string memberID = strDecrypted;
            string serviceAreaID = ClsServiceAreaLocation.GetServiceAreaID(origin, destination);
            ClsBooking objBooking = new ClsBooking();
            objBooking.Fill(objBooking.tblBooking, memberID);
            strReturn = ((objBooking.tblBooking.Select("ServiceAreaID='" + serviceAreaID + "'").Length > 0) ? "1" : "0");

            return strReturn;
        }

        [WebMethod]
        public static List<Schedule> PopulateGV(string PickUpID, string DropOffID,string Origin,string Destination,int hour,int min)
        {
            string pickUp = "";
            string dropOff = "";
            List<Schedule> scheduleList = new List<Schedule>();
            ClsServiceAreaMajorStopDataTable objServiceAreaMajorStopDataTable = new ClsServiceAreaMajorStopDataTable();
            ClsMemberSubscription objMemberSubscription = new ClsMemberSubscription();
            string strDecrypted = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
            objMemberSubscription.Fill(objMemberSubscription.TblMemberSubscription, strDecrypted);
            objServiceAreaMajorStopDataTable.Fill(ClsServiceAreaLocation.GetServiceAreaID(Origin, Destination), ClsServiceAreaLocation.GetServiceAreaLocationID(Origin, Destination));


            foreach (DataRow drw in objMemberSubscription.TblMemberSubscription.Rows)
            {
                DateTime dtSubscriptionStart = drw.GetDateTime("DateSubscribed");
                DateTime dtExpiration = drw.GetDateTime("DateExpired");
                DateTime dtStart = new DateTime(dtSubscriptionStart.Year, dtSubscriptionStart.Month, dtSubscriptionStart.Day, hour, min, 0);
                DateTime dtEnd = new DateTime(dtExpiration.Year, dtExpiration.Month, dtExpiration.Day, hour, min, 0);
                pickUp=PickUpID.Substring(0,1)=="S"?Origin:objServiceAreaMajorStopDataTable.GetStationName(PickUpID);
                dropOff=DropOffID.Substring(0,1)=="S"?Destination:objServiceAreaMajorStopDataTable.GetStationName(DropOffID);


                if (Convert.ToDateTime(dtSubscriptionStart.AddDays(-1).ToString("yyyy-MM-dd")) < Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")))
                {
                    dtStart = dtStart.AddDays(1);
                }
                scheduleList.Add(new Schedule()
                {
                    DepartureDate = string.Format("{0} - {1}",dtStart.ToString("MMM dd, yy"),dtEnd.ToString("MMM dd, yy")),
                    Time=dtStart.ToString("hh:mm tt"),
                    PickUp = pickUp,
                    DropOff = dropOff
                });
            }

            return scheduleList;
        }

        [WebMethod]
        public static List<Station> RepopulateStations(string Origin, string Destination, string SelectedStopID,string TargetName)
        {
            List<Station> stationList = new List<Station>();
            clsAuthenticator.Authenticate();
            string strDecrypted = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
            string memberID = strDecrypted;
            ClsServiceAreaMajorStopDataTable objServiceAreaMajorStopDataTable = new ClsServiceAreaMajorStopDataTable();
            ClsViews.ViewServiceAreaOrigin objViewServiceAreaOrigin = new ClsViews.ViewServiceAreaOrigin(memberID, Origin);
            ClsViews.ViewServiceAreaDestination objViewServiceAreaDestination = new ClsViews.ViewServiceAreaDestination(memberID, Origin, Destination);
            objServiceAreaMajorStopDataTable.Fill(ClsServiceAreaLocation.GetServiceAreaID(Origin, Destination), ClsServiceAreaLocation.GetServiceAreaLocationID(Origin, Destination));


                foreach (DataRow drw in objServiceAreaMajorStopDataTable.TblCityMajorStop.Rows)
                {
                    if (SelectedStopID != drw.GetString("MajorStopID"))
                    {
                        Station objStation = new Station();
                        objStation.MajorStopID = drw.GetString("MajorStopID");
                        objStation.StationName = drw.GetString("StationName");
                        stationList.Add(objStation);                    
                    }
                }

                if (TargetName == "ddlPickup")
                {
                    stationList.Add(new Station()
                    {
                        MajorStopID = "S01",
                        StationName = objViewServiceAreaOrigin.Origin
                    });
                }
                else
                {
                    stationList.Add(new Station()
                    {
                        MajorStopID = "S02",
                        StationName = objViewServiceAreaDestination.Destination
                    });  
                }
            
            return stationList;
        }

        [WebMethod(EnableSession=true)]
        public static List<Station> PopulateStations(string Origin,string Destination)
        {
            List<Station> stationList = new List<Station>();
            clsAuthenticator.Authenticate();
            string strDecrypted = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
            string memberID = strDecrypted;
            ClsServiceAreaMajorStopDataTable objServiceAreaMajorStopDataTable = new ClsServiceAreaMajorStopDataTable();
            ClsViews.ViewServiceAreaOrigin objViewServiceAreaOrigin = new ClsViews.ViewServiceAreaOrigin(memberID,Origin);
            ClsViews.ViewServiceAreaDestination objViewServiceAreaDestination = new ClsViews.ViewServiceAreaDestination(memberID, Origin,Destination);
            objServiceAreaMajorStopDataTable.Fill(ClsServiceAreaLocation.GetServiceAreaID(Origin, Destination), ClsServiceAreaLocation.GetServiceAreaLocationID(Origin, Destination));

            foreach (DataRow drw in objServiceAreaMajorStopDataTable.TblCityMajorStop.Rows)
            {
                Station objStation = new Station();
                objStation.MajorStopID = drw.GetString("MajorStopID");
                objStation.StationName = drw.GetString("StationName");
                stationList.Add(objStation);
            }

            stationList.Add(new Station() 
            { MajorStopID = "S01", 
                StationName = objViewServiceAreaOrigin.Origin
            });

            stationList.Add(new Station()
            {
                MajorStopID = "S02",
                StationName = objViewServiceAreaDestination.Destination
            });

            return stationList;
        }

        [WebMethod(EnableSession = true)]
        public static List<Coordinates> GetLatLang(string pickup,string dropoff,string pickupText,string dropoffText,string ServiceAreaFrom,string ServiceAreaTo)
        {
            List<Coordinates> lstCoordinates = new List<Coordinates>();

            string strPickup = string.IsNullOrEmpty(pickup) ? "" : pickup;
            string strDropoff = string.IsNullOrEmpty(dropoff) ? "" : dropoff;
            string strPickupText=string.IsNullOrEmpty(pickupText)?"":pickupText;
            string strDropoffText = string.IsNullOrEmpty(dropoffText) ? "" : dropoffText;

            string memberID = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");

            if (strPickup.IndexOf("S")>=0)
            {
                ClsViews.ViewServiceAreaOrigin objViewServiceAreaOrigin = new ClsViews.ViewServiceAreaOrigin(memberID, strPickupText);
                lstCoordinates.Add(new Coordinates() { Latitude=objViewServiceAreaOrigin.Latitude,Longitude=objViewServiceAreaOrigin.Longitude,IsPickup=true});
            }
            else
            {
                ClsServiceAreaMajorStopDataTable objServiceAreaMajorStopDataTable = new ClsServiceAreaMajorStopDataTable();
                objServiceAreaMajorStopDataTable.Fill(ClsServiceAreaLocation.GetServiceAreaID(ServiceAreaFrom, ServiceAreaTo), ClsServiceAreaLocation.GetServiceAreaLocationID(ServiceAreaFrom, ServiceAreaTo));
                if (objServiceAreaMajorStopDataTable.TblCityMajorStop.Rows.Count > 0)
                {
                    var row = objServiceAreaMajorStopDataTable.TblCityMajorStop.Where(x => x.MajorStopID == pickup).FirstOrDefault();
                    lstCoordinates.Add(new Coordinates() { Latitude = row.Latitude, Longitude = row.Longtitude, IsPickup = true });
                }                
            }

            if (strDropoff.IndexOf("S") >= 0)
            {
                ClsViews.ViewServiceAreaDestination objViewServiceAreaDestination = new ClsViews.ViewServiceAreaDestination(memberID, ServiceAreaFrom, ServiceAreaTo);
                lstCoordinates.Add(new Coordinates() { Latitude = objViewServiceAreaDestination.Latitude, Longitude = objViewServiceAreaDestination.Longitude, IsPickup = false });
            }
            else
            {
                if (strDropoff != "")
                {
                    ClsServiceAreaMajorStopDataTable objServiceAreaMajorStopDataTable = new ClsServiceAreaMajorStopDataTable();
                    objServiceAreaMajorStopDataTable.Fill(ClsServiceAreaLocation.GetServiceAreaID(ServiceAreaFrom, ServiceAreaTo), ClsServiceAreaLocation.GetServiceAreaLocationID(ServiceAreaFrom, ServiceAreaTo));
                    if (objServiceAreaMajorStopDataTable.TblCityMajorStop.Rows.Count > 0)
                    {
                        var row = objServiceAreaMajorStopDataTable.TblCityMajorStop.Where(x => x.MajorStopID == dropoff).FirstOrDefault();
                        lstCoordinates.Add(new Coordinates() { Latitude = row.Latitude, Longitude = row.Longtitude, IsPickup = false });
                    }    
                }                
            }

            return lstCoordinates;
        }
        

        private void PopulateServiceAreaOrigin()
        {
            ClsViews.ViewServiceAreaOrigin objViewServiceAreaOrigin = new ClsViews.ViewServiceAreaOrigin(ViewState["MemberID"].ToString());
            ddlServiceAreaFrom.DataSource = objViewServiceAreaOrigin.TblServiceAreaOrigin;
            ddlServiceAreaFrom.DataTextField = "Origin";
            ddlServiceAreaFrom.DataValueField = "Origin";
            ddlServiceAreaFrom.DataBind();
            ddlServiceAreaFrom.Items.Insert(0, new ListItem("Select Your Origin", ""));
            ddlServiceAreaFrom.Items[0].Attributes.Add("disabled", "disabled");
            ddlServiceAreaFrom.SelectedIndex = 0;
        }

        private void PopulateServiceAreaDestination(string Origin)
        {
            ClsViews.ViewServiceAreaDestination objViewServiceAreaDestination = new ClsViews.ViewServiceAreaDestination(ViewState["MemberID"].ToString(), Origin);
            ddlServiceAreaTo.DataSource = objViewServiceAreaDestination.TblServiceAreaDestination;
            ddlServiceAreaTo.DataTextField = "Destination";
            ddlServiceAreaTo.DataValueField = "Destination";
            ddlServiceAreaTo.DataBind();
            ddlServiceAreaTo.Items.Insert(0, new ListItem("Select Your Destination", ""));
            ddlServiceAreaTo.Items[0].Attributes.Add("disabled", "disabled");
            ddlServiceAreaTo.SelectedIndex = 0;

        }        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clsAuthenticator.Authenticate();
                string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                ViewState["MemberID"] = strDecrypted;
                    UHopCore.Title = "Create new Booking";
                    ViewState["MemberID"] = strDecrypted;

                    ClsMembers objMembers = new ClsMembers();
                    objMembers.Fill(strDecrypted);

                    strMemberID = strDecrypted;
                    var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                    if (MemberDetails.Status == "0")
                    {
                        Response.Redirect("../Maintenance/UserProfile.aspx");
                    }


                ClsBooking objBooking = new ClsBooking();
                objBooking.Fill(objBooking.tblBooking, ViewState["MemberID"].ToString());

                ClsMemberSubscription clsMemberSubscription = new ClsMemberSubscription();
                clsMemberSubscription.Fill(clsMemberSubscription.TblMemberSubscription, ViewState["MemberID"].ToString());

                PopulateServiceAreaOrigin();
                PopulateServiceAreaDestination(ddlServiceAreaFrom.SelectedValue.ToString());
            }
            else
            {
                if (Request.Form["__EVENTTARGET"] == "Booking")
                {

                    btnSubmitBookingNew(Request.Form["__EVENTARGUMENT"].ToString().Split('~')[0], Request.Form["__EVENTARGUMENT"].ToString().Split('~')[1], Request.Form["__EVENTARGUMENT"].ToString().Split('~')[2]);
                }
                else if(Request.Form["__EVENTTARGET"]=="Return")
                {
                    btnSubmitBookingReturn(Convert.ToInt32(Request.Form["__EVENTARGUMENT"].ToString().Split('~')[0]), Request.Form["__EVENTARGUMENT"].ToString().Split('~')[1], Request.Form["__EVENTARGUMENT"].ToString().Split('~')[2]);
                }
            }
            
        }

        protected void ddlServiceAreaFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateServiceAreaDestination(ddlServiceAreaFrom.SelectedValue.ToString());
        }


        protected void btnSubmitBookingReturn(int hour,string pickupID,string dropoffID)
        {
            string pickUpLocation = "";
            decimal pickupLat = 0;
            decimal pickUpLong = 0;
            string dropOffLocation = "";
            decimal dropOffLat = 0;
            decimal dropOffLong = 0;

            string serviceAreaID = ClsServiceAreaLocation.GetServiceAreaID(ddlServiceAreaFrom.SelectedValue.ToString(), ddlServiceAreaTo.SelectedValue.ToString());
            string memberID = ViewState["MemberID"].ToString();

            string memberSubscriptionID = "";
            string serviceAreaLocationID = ClsServiceAreaLocation.GetServiceAreaLocationID(ddlServiceAreaFrom.SelectedValue.ToString(), ddlServiceAreaTo.SelectedValue.ToString());

            ClsMajorStops objDropOffpPoint = new ClsMajorStops(dropoffID);
            ClsMajorStops objPickUpPoint = new ClsMajorStops(pickupID);
            ClsBooking objBooking = new ClsBooking();
            objBooking.Fill(objBooking.tblBooking,memberID);
            IEnumerable<DataRow> query = objBooking.tblBooking.Where(x => x.ServiceAreaID == serviceAreaID && x.IsCancelled=="0" && x.IsFulfilled=="0" && x.IsRebook=="0" && x.IsVoid=="0").OrderBy(y => y.DepartureDateTime);

            foreach (DataRow drwBooking in query)
            {
                if (Convert.ToDateTime(drwBooking.GetDateTime("DepartureDateTime").AddDays(-1).ToString("yyyy-MM-dd")) > Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")))
                {
                    if(objPickUpPoint.address == drwBooking.GetString("PickupLocation") && objDropOffpPoint.address == drwBooking.GetString("DropOffLocation"))
                    {
                        objDropOffpPoint = new ClsMajorStops(pickupID);
                        objPickUpPoint = new ClsMajorStops(dropoffID);
                    }

                    if (dropoffID.Substring(0, 1) == "S")
                    {
                        ClsViews.ViewServiceAreaDestination objServiceAreaDestination = new ClsViews.ViewServiceAreaDestination(ViewState["MemberID"].ToString(), ddlServiceAreaFrom.SelectedItem.Text, ddlServiceAreaTo.SelectedItem.Text);
                        dropOffLocation = objServiceAreaDestination.Destination;
                        dropOffLat = objServiceAreaDestination.Latitude;
                        dropOffLong = objServiceAreaDestination.Longitude;
                    }
                    else
                    {
                        
                        dropOffLocation = objDropOffpPoint.address;
                        dropOffLat = objDropOffpPoint.latitude;
                        dropOffLong = objDropOffpPoint.longitude;
                    }

                    if (pickupID.Substring(0, 1) == "S")
                    {
                        ClsViews.ViewServiceAreaOrigin objServiceAreaOrigin = new ClsViews.ViewServiceAreaOrigin(ViewState["MemberID"].ToString(), ddlServiceAreaFrom.SelectedItem.Text);
                        pickUpLocation = objServiceAreaOrigin.Origin;
                        pickupLat = objServiceAreaOrigin.Latitude;
                        pickUpLong = objServiceAreaOrigin.Longitude;
                    }
                    else
                    {
                        pickUpLocation = objPickUpPoint.address;
                        pickupLat = objPickUpPoint.latitude;
                        pickUpLong = objPickUpPoint.longitude;
                    }

                    ClsAutoNumber objAutonumber=new ClsAutoNumber();
                    DataRow drwBookingNew = objBooking.tblBooking.NewRow();
                    string strBookingID = objAutonumber.GenerateAutoNumber("BookingID", "000000").ToString();
                    memberSubscriptionID = drwBooking.GetString("MemberSubscriptionID");
                    drwBookingNew.SetString("BookingID", strBookingID);
                    drwBookingNew.SetString("MemberID", memberID);
                    drwBookingNew.SetString("TransactionTypeID", "20150923000002");
                    drwBookingNew.SetString("ServiceAreaID", serviceAreaID);
                    drwBookingNew.SetString("ServiceAreaLocationID", serviceAreaLocationID);
                    drwBookingNew.SetString("MemberSubscriptionID", memberSubscriptionID);
                    drwBookingNew.SetString("TripID", null);
                    drwBookingNew.SetDateTime("DepartureDateTime", drwBooking.GetDateTime("DepartureDateTime").AddHours(hour));
                    drwBookingNew.SetDateTime("SchedulerDepartureDateTime", null);
                    drwBookingNew.SetString("PickupLocation", pickUpLocation);
                    drwBookingNew.SetDecimal("PickUpLatitude",pickupLat );
                    drwBookingNew.SetDecimal("PickUpLongitude",pickUpLong);
                    drwBookingNew.SetString("DropOffLocation", dropOffLocation);
                    drwBookingNew.SetDecimal("DropOffLatitude",dropOffLat);
                    drwBookingNew.SetDecimal("DropOffLongitude", dropOffLong);
                    drwBookingNew.SetString("IsCancelled", "0");
                    drwBookingNew.SetString("IsRebook", "0");
                    drwBookingNew.SetString("IsFulfilled", "0");
                    drwBookingNew.SetString("IsVoid", "0");
                    drwBookingNew.SetDateTime("CreatedOn", DateTime.Now);

                    objBooking.tblBooking.Rows.Add(drwBookingNew);
                }
            }

            objBooking.Update();


            Response.Redirect("BookingSuccess.aspx");
        }

        protected void btnSubmitBookingNew(string time,string pickupID,string dropoffID)
        {

            string serviceAreaID = ClsServiceAreaLocation.GetServiceAreaID(ddlServiceAreaFrom.SelectedValue.ToString(), ddlServiceAreaTo.SelectedValue.ToString());
            string memberID = ViewState["MemberID"].ToString();
            int hr = 0;
            int min = 0;
            string tt = "";
            string memberSubscriptionID = "";
            string serviceAreaLocationID = ClsServiceAreaLocation.GetServiceAreaLocationID(ddlServiceAreaFrom.SelectedValue.ToString(), ddlServiceAreaTo.SelectedValue.ToString());
            string pickUpLocation = "";
            decimal pickupLat = 0;
            decimal pickUpLong = 0;
            string dropOffLocation="";
            decimal dropOffLat = 0;
            decimal dropOffLong = 0;
            ClsMemberSubscription objMemberSubscription = new ClsMemberSubscription();
            ClsBooking objBooking = new ClsBooking();
            ClsAutoNumber objAutoNumber = new ClsAutoNumber();
            objMemberSubscription.Fill(objMemberSubscription.TblMemberSubscription, ViewState["MemberID"].ToString());
            if (pickupID.Substring(0, 1) == "S")
            {
                ClsViews.ViewServiceAreaOrigin objServiceAreaOrigin = new ClsViews.ViewServiceAreaOrigin(ViewState["MemberID"].ToString(),ddlServiceAreaFrom.SelectedItem.Text);
                pickUpLocation = objServiceAreaOrigin.Origin;
                pickupLat = objServiceAreaOrigin.Latitude;
                pickUpLong = objServiceAreaOrigin.Longitude;
            }
            else
            {
                ClsMajorStops objPickUpPoint = new ClsMajorStops(pickupID);
                pickUpLocation = objPickUpPoint.address;
                pickupLat = objPickUpPoint.latitude;
                pickUpLong = objPickUpPoint.longitude;
            }

            if (dropoffID.Substring(0, 1) == "S")
            {
                ClsViews.ViewServiceAreaDestination objServiceAreaDestination = new ClsViews.ViewServiceAreaDestination(ViewState["MemberID"].ToString(),ddlServiceAreaFrom.SelectedItem.Text,ddlServiceAreaTo.SelectedItem.Text);
                dropOffLocation = objServiceAreaDestination.Destination;
                dropOffLat = objServiceAreaDestination.Latitude;
                dropOffLong = objServiceAreaDestination.Longitude;
            }
            else
            {
                ClsMajorStops objDropOffpPoint = new ClsMajorStops(dropoffID);
                dropOffLocation = objDropOffpPoint.address;
                dropOffLat = objDropOffpPoint.latitude;
                dropOffLong = objDropOffpPoint.longitude;
            }
            

            tt = time.Split(' ')[1].ToString().ToUpper();
            if (tt == "PM" && (Convert.ToUInt32(time.Split(' ')[0].Split(':')[0]) >= 1 && Convert.ToUInt32(time.Split(' ')[0].Split(':')[0]) <= 11))
            {
                hr = Convert.ToInt32(time.Split(' ')[0].Split(':')[0])+12;
            }
            else if (tt == "AM" && Convert.ToInt32(time.Split(' ')[0].Split(':')[0]) == 12)
            {
                hr = 0;
            }
            else
            {
                hr = Convert.ToInt32(time.Split(' ')[0].Split(':')[0]);
            }

            min = Convert.ToInt32(time.Split(' ')[0].Split(':')[1]);

            foreach (DataRow drwMemberSubscription in objMemberSubscription.TblMemberSubscription.Rows)
            {
                DateTime dtDateSubscribe = drwMemberSubscription.GetDateTime("DateSubscribed");
                DateTime dtDepDate = new DateTime(dtDateSubscribe.Year, dtDateSubscribe.Month, dtDateSubscribe.Day, hr, min, 0);

                if (Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")) > Convert.ToDateTime(dtDateSubscribe.AddDays(-1).ToString("yyyy-MM-dd")))
                {
                    dtDepDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hr, min, 0).AddDays(1);
                }

                memberSubscriptionID = drwMemberSubscription.GetString("MemberSubscriptionID");
                while (Convert.ToDateTime(dtDepDate.ToString("yyyy-MM-dd")) <= Convert.ToDateTime(objMemberSubscription.GetExpirationDate(memberSubscriptionID).ToString("yyyy-MM-dd")))
                {
                    DataRow drwBooking = objBooking.tblBooking.NewRow();
                    string strBookingID = objAutoNumber.GenerateAutoNumber("BookingID", "000000").ToString();
                    drwBooking.SetString("BookingID", strBookingID);
                    drwBooking.SetString("MemberID", memberID);
                    drwBooking.SetString("TransactionTypeID", "20150923000002");
                    drwBooking.SetString("ServiceAreaID", serviceAreaID);
                    drwBooking.SetString("ServiceAreaLocationID", serviceAreaLocationID);
                    drwBooking.SetString("MemberSubscriptionID", memberSubscriptionID);
                    drwBooking.SetString("TripID", null);
                    drwBooking.SetDateTime("DepartureDateTime", dtDepDate);
                    drwBooking.SetDateTime("SchedulerDepartureDateTime", null);
                    drwBooking.SetString("PickupLocation", pickUpLocation);
                    drwBooking.SetDecimal("PickUpLatitude", pickupLat);
                    drwBooking.SetDecimal("PickUpLongitude", pickUpLong);
                    drwBooking.SetString("DropOffLocation", dropOffLocation);
                    drwBooking.SetDecimal("DropOffLatitude", dropOffLat);
                    drwBooking.SetDecimal("DropOffLongitude", dropOffLong);
                    drwBooking.SetString("IsCancelled", "0");
                    drwBooking.SetString("IsRebook", "0");
                    drwBooking.SetString("IsFulfilled", "0");
                    drwBooking.SetString("IsVoid", "0");
                    drwBooking.SetDateTime("CreatedOn", DateTime.Now);

                    objBooking.tblBooking.Rows.Add(drwBooking);

                    dtDepDate = dtDepDate.AddDays(1);
                }
            }


            if (objBooking.tblBooking.Rows.Count > 0)
            {
                objBooking.Update();
                Response.Redirect("BookingSuccess.aspx");            
            }
        }

    }
}