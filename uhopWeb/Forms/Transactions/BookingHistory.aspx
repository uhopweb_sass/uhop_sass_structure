﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Site1.Master" AutoEventWireup="true" CodeBehind="BookingHistory.aspx.cs" Inherits="uhopWeb.Forms.Transactions.BookingHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:PlaceHolder ID="PlaceHolder2" runat="server">
        <%: Styles.Render("~/bundle/bookingHistoryStyle") %>
    </asp:PlaceHolder>
    <style>
        .customContainer {
            max-width: 1600px;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
    <br />
    <div class="customContainer"> 
        <div class="row">
            <div class="col s12">
                <h4><i class="small material-icons">list</i>&nbsp;&nbsp;&nbsp;&nbsp;Booking History</h4>
            </div>
        </div>
        <div class="divider"></div>
        <br />


        <div class="row">
            <div class="col s12">
               <ul class="filterOptions hoverable">
                    <li>Filter Options<span><asp:LinkButton CssClass="waves-effect waves-light btn" ID="btnSearch" runat="server" OnClick="btnSearch_Click"><i class="material-icons left">search</i>Search</asp:LinkButton></span></li>
                    <li><b>Booking Date:</b></li>
                    <li>
                        <input type="date" class="datepicker" runat="server" id="txtBookingDate"/>
                      </li>   
                    <li><b>Status:</b></li>
                    <li>
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="browser-default">
                            <asp:ListItem Value="A">All</asp:ListItem>
                            <asp:ListItem Value="P">Pending</asp:ListItem>
                            <asp:ListItem Value="F">Fulfilled</asp:ListItem>
                            <asp:ListItem Value="C">Cancelled</asp:ListItem>
                            <asp:ListItem Value="V">Voided</asp:ListItem>
                            <asp:ListItem Value="R">Rebooked</asp:ListItem>
                        </asp:DropDownList></li>
                </ul>
            </div>
        </div>


        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvBookingHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="gvBookingHistory_PageIndexChanging" OnRowCommand="gvBookingHistory_RowCommand" PageSize="5" ShowHeaderWhenEmpty="True" CssClass="hoverable responsive-table">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label2" runat="server" Text="Booking Date"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblBookingDate" runat="server" Text='<%# Convert.ToDateTime(Eval("CreatedOn").ToString()).ToString("MMM dd, yyyy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label4" runat="server" Text="Service Area"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblServiceArea" runat="server" Text='<%# Bind("ServiceArea") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label6" runat="server" Text="Pickup Point"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblPickup" runat="server" Text='<%# Bind("PickupLocation") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label8" runat="server" Text="Dropoff Point"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDropOffPoint" runat="server" Text='<%#Bind("DropOffLocation") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label10" runat="server" Text="Prefered Departure Date Time"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblPreferedDepartureDateTime" runat="server" Text='<%# Convert.ToDateTime(Eval("DepartureDateTime").ToString()).ToString("MMM dd, yyyy hh:mm tt") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label14" runat="server" Text="Scheduled Departure Date Time"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblScheduledDepartureDateTime" runat="server" Text='<%# (Eval("SchedulerDepartureDateTime")!=null?"":Convert.ToDateTime(Eval("SchedulerDepartureDateTime").ToString()).ToString("MMM dd, yyyy hh:mm tt")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label12" runat="server" Text="Status"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status").ToString()=="V"?"Voided":Eval("Status").ToString()=="C"?"Cancelled":Eval("Status").ToString()=="R"?"Rebooked":Eval("Status").ToString()=="F"?"Fulfilled":Eval("Status").ToString()=="P"?"Pending":"" %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            </Triggers>
            </asp:UpdatePanel>
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" Text="No Record\s Found!"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            </Triggers>
            </asp:UpdatePanel>
    </div>
</asp:Content>
