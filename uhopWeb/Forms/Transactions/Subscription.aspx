﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Site1.Master" AutoEventWireup="true" CodeBehind="Subscription.aspx.cs" Inherits="uhopWeb.Forms.Transactions.Subscription" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script src="/../Scripts/jquery-2.1.4.min.js"></script>
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <div class="divCover ServiceAreaPopup hide">

        <div class="divServiceAreaContainer">
            <br />
            <br />
            <br />
            <div class="center">
                <h4 class="card-title white-text">Service Area included for subscription:
                    <br />
                    <span class="subscriptioname"></span></h4>
            </div>
            <div class="row divSubscriptionDetails">
            </div>
            <div class="row center">
                <button type="button" class="waves-effect waves-light btn red darken-2" onclick="HideCover();">Close</button>
            </div>
        </div>
    </div>


    <asp:HiddenField ID="hdfSelectedSubscription" runat="server" />
    <asp:HiddenField ID="hdfSelectedSubscriptionPrice" runat="server" />
    <asp:HiddenField ID="hdfSelectedSubscriptionType" runat="server" />
    <asp:HiddenField ID="hdfSelectedSubscriptionNoOfDays" runat="server" />



    <div class="subscriptionWrapper">
        <div class="row">
            <div class="col s12" id="carEffect">
                <!-- Removed wow lightSpeedOut -->
                <div class="wow lightSpeedOut">
                    <div class="wow bounce" data-wow-delay="1.4s">
                        <img src="../../Images/van-512.png" class="responsive-img hide-on-med-and-down" id="carImg" />
                    </div>
                </div>
                <div class="container center wow fadeIn" data-wow-duration="5s">
                    <!-- Removed id="subscribePlan" -->
                    <h2 style="-webkit-text-stroke: .5px pink;" class="center">Choose your subscription:</h2>
                    <div class="input-field col l6 offset-l3 s12">
                        <div class="input-field">
                            <div class="col m6 s12">
                                <h5 class="green-text hide-on-small-only">ORIGIN</h5>
                            </div>
                            <div class="col m6 s12">
                                <h5 class="red-text hide-on-small-only">DESTINATION</h5>
                            </div>
                            <div class="col s12 show-on-small hide-on-med-and-up">
                                <h5 class="green-text show-on-small hide-on-med-and-up">ORIGIN</h5>
                            </div>
                            <div class="col m6 s12">
                                <asp:DropDownList CssClass="browser-default ddlServiceAreaLocationOrigin" ID="ddlServiceAreaLocationOrigin" runat="server" AutoPostBack="false" OnSelectedIndexChanged="ddlServiceAreaLocationOrigin_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <div class="col s12 show-on-small hide-on-med-and-up">
                                <h5 class="red-text show-on-small hide-on-med-and-up">DESTINATION</h5>
                            </div>
                            <div class="col m6 s12">
                                <asp:DropDownList CssClass="browser-default ddlServiceAreaLocationDestination" ID="ddlServiceAreaLocationDestination" runat="server" AutoPostBack="false"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </div>
    <div class="container center">
        <div class="row divShowCards" style="margin-top: -90px;">
            <br />
            <br />
            <br />
            <br />
            <h1 class="subscribeHeader wow bounceInUp" data-wow-delay="1s">"Because everyone deserves a ride."</h1>
        </div>
    </div>

    <script type="text/javascript">

        /*$("#subscribePlan").addClass("visible").hide().delay(1200).fadeIn(2000);*/

        $('.ddlServiceAreaLocationOrigin').change(function () {
            var SelectedText = $(this).find(":selected").text();

            $.ajax({
                type: "POST",
                url: "Subscription.aspx/GetDestination",
                data: "{'pOriginCityProvince':'" + SelectedText + "'}",
                contentType: "application/json",
                dataType: "json",
                success: function (response) {
                    try {
                        var res = response.d;
                        $(".def1,.D1,.SelectOriginFirst").remove();
                        $(".ddlServiceAreaLocationDestination").append($("<option selected='true' class='D1'></option>").val("1").html("Select your Destination"));
                        $('.ddlServiceAreaLocationDestination option:contains("Select your Destination")').attr("disabled", "disabled");
                        $.each(res, function (index, ServiceAreaLocationDestination) {
                            $(".ddlServiceAreaLocationDestination").append($("<option class='def1'></option>").val(ServiceAreaLocationDestination.DestinationCityProvince).html(ServiceAreaLocationDestination.DestinationCityProvince));
                        });
                    }
                    catch (e) {

                    }

                }
            });
        });

        $('.ddlServiceAreaLocationDestination').change(function () {
            var SelectedTextOrigin = $(".ddlServiceAreaLocationOrigin").find(":selected").text();
            var SelectedTextDestination = $(this).find(":selected").text();

            $.ajax({
                type: "POST",
                url: "Subscription.aspx/PopulateSubscriptionCards",
                data: "{'pOriginCityProvince':'" + SelectedTextOrigin + "','pDestinationCityProvince':'" + SelectedTextDestination + "'}",
                contentType: "application/json",
                dataType: "json",
                success: function (response) {
                    try {
                        $('.divCardtoClear').remove();
                        //document.getElementsByClassName("divCardtoClear").innerHTML = "";
                        var res = response.d;
                        $.each(res, function (index, ServiceAreaSubscriptionToSell) {

                            var IsRecommended = (ServiceAreaSubscriptionToSell.IsRecommended == "1") ? "<div class='card hoverable subscribeCardRecommended'><div class='wow rubberband' data-wow-delay='1s'><div id='divCard'><span class='white-text subscribeTextHeader wow pulse animated' data-wow-delay='300ms' data-wow-iteration='infinite' data-wow-duration='2s'><b>Recommended</b></span></div></div><div class='card-content black-text' style='padding-top:5px; padding-bottom:5px;'>" : "<div class='card hoverable subscribeCardRegular'><div class='card-content black-text' style='padding-bottom:5px;'>";
                            var IsUnlimited = (ServiceAreaSubscriptionToSell.IsUnlimited == "1") ? "UNLIMITED RIDE" : "LIMITED RIDE";
                            var NoOfDays = (ServiceAreaSubscriptionToSell.ValidForNumberOfDays > 1) ? ServiceAreaSubscriptionToSell.ValidForNumberOfDays + " days" : ServiceAreaSubscriptionToSell.ValidForNumberOfDays + " day";

                            $(".divShowCards").append(
                                "<div class='col s12 m6 l6 center divCardtoClear ' id='" + ServiceAreaSubscriptionToSell.ServiceAreaSubscriptionID + "'>"
                                + IsRecommended +
                                "<div class='col s12'>" +
                                "<span class='card-title blue-text cardHeader'>" + ServiceAreaSubscriptionToSell.SubscriptionName + "</span><div class='divider'></div>" +
                                "</div><br /><br />" +
                                "<ul class='subscribeDetails' style='margin-bottom:0px;'>" +
                                "<li style='font-size:14px;'>" + ServiceAreaSubscriptionToSell.ShortDescription + "</li>" +
                                //"<li>" + IsUnlimited + "</b></li>" +
                                "<li><h5 class='blue-text'><b>" + ServiceAreaSubscriptionToSell.Price + " PHP</b></h5></li>" +
                                "<li>valid for " + NoOfDays + "</li>" +
                                "<li>" + ServiceAreaSubscriptionToSell.Origin + "</li>" +
                                "<li style='margin:0px;'><b>to</b></li>" +
                                "<li style='margin:0px;'>" + ServiceAreaSubscriptionToSell.Destination + "</li>" +
                                "<li><a id='" + ServiceAreaSubscriptionToSell.ServiceAreaSubscriptionID + "' class='asdzxc' onclick='showCover(this.id); return false;' href='#'>View more details...</a></li>" +
                                "</ul>" +
                                "</div>" +
                                "<div class='card-action' id='limitedaction'>" +
                                "<a href='../AurumPay/AurumModesOfPayment.aspx?ND54653=" + ServiceAreaSubscriptionToSell.ServiceAreaSubscriptionIDE + "' class='btn waves-effect waves-light blue darken-2 white-text center-align' style='margin-right:0px;'>PURCHASE</a>" +
                                "</div></div></div></div>"
                                );

                        });
                    }
                    catch (e) {

                    }

                }
            });
        });

        function HideCover() {
            $(".ServiceAreaPopup").addClass("hide");
        }

        function showCover(obj) {
            $(".ServiceAreaPopup").removeClass("hide").hide().fadeToggle(244);
            $("input[id*='hdfSelectedSubscription']").val(obj);
            var strsample = [];
            var SelectedSubscription = obj;
            $.ajax({
                type: "POST",
                url: "Subscription.aspx/PopulateOriginDestinationInfo",
                data: "{'pSubscription':'" + SelectedSubscription + "'}",
                contentType: "application/json",
                dataType: "json",
                success: function (response) {
                    try {
                        var x = 1;

                        var res = response.d;
                        $(".divSlist").remove();
                        $.each(res, function (index, ServiceAreaSubscriptionLocation) {
                            strsample[x] = ServiceAreaSubscriptionLocation.ServiceAreaLocationID;
                            var footer = "";
                            if (x == 1) {
                                footer = "<h5><strong>Home <i class='material-icons'>&#xE8E4;</i> Office</strong></h5>"
                            }
                            else if (x == 2) {
                                footer = "<h5><strong>Office <i class='material-icons'>&#xE8E4;</i> Home</strong></h5>"
                            }

                            $(".divSubscriptionDetails").append(
                                 "<div class='col l6 s12 divSlist'><div class='card grey lighten-3'><div class='card-content blue darken-4 white-text'>" +
                                 "<h5>Origin</h5>" +
                            "<p>- " + ServiceAreaSubscriptionLocation.OriginLocation + "</p>" +
                            "<br />" +
                            "<div class='divider'></div>" +
                            "<h5>Major Stop/s</h5>" +
                            "<ul class='majorStops mstop" + x + "'>" +

                            "</ul>" +
                            "<div class='divider'></div>" +
                            "<h5>Destination</h5>" +
                            "<p>- " + ServiceAreaSubscriptionLocation.DestinationLocation + "</p>" +
                    "</div>" +
                    "<div class='card-action pink accent-2 center black-text'>" +
                        footer +
                    "</div></div></div>")
                            $(".subscriptioname").text(ServiceAreaSubscriptionLocation.SubscriptionName);
                            x = x + 1;

                        });
                    }
                    catch (e) {

                    }


                    var y = 1;
                    for (i = 1; i < strsample.length; i++) {
                        $.ajax({
                            type: "POST",
                            url: "Subscription.aspx/PopulateOriginDestinationInfoMajorStop",
                            data: "{'pSubscription':'" + strsample[i] + "'}",
                            contentType: "application/json",
                            dataType: "json",
                            success: function (response) {
                                try {
                                    
                                    var res1 = response.d;
                                    $.each(res1, function (index, ServiceAreaSubscriptionLocationMajorStop) {
                                        $(".mstop" + y).append(
                                           "<li>" + ServiceAreaSubscriptionLocationMajorStop.MajorStopAddress + "</li>")
                                        
                                    });
                                    y = y + 1;

                                }
                                catch (e) {

                                }
                            }
                        });
                    }
                }
            });




        };

        function stopScroll() {
            $('.vDetails').on('click', function () {
                $('html, body').addClass('stop-scrolling');
                $('body').bind('touchmove', function (e) { e.preventDefault() })
            });
        }

        function continueScroll() {
            /* FOR MODAL */
            $('.onDismissal, #lean-overlay').on('click', function () {
                $('html, body').removeClass('stop-scrolling');
                $('body').unbind('touchmove');
            });
        }



    </script>

</asp:Content>
