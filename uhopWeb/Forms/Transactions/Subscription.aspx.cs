﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Web.Services;
using uhopWeb.Classes;

namespace uhopWeb.Forms.Transactions
{
    public partial class Subscription : System.Web.UI.Page
    {

        public static string strMemberID;

        internal class MySubscribeBtn : LinkButton
        {
            public string MemberID { get; set; }
            public string SusbcriptionID {get;set;}
            public string IsLimited {get;set;}
            public decimal price { get; set; }
            public int NoOfDays { get; set; }

            public MySubscribeBtn() :base(){
                MemberID = "";
                SusbcriptionID = "";
                IsLimited = "";
                price=0;
            }
            
        }

        public void PopulateOrigin()
        {
            using (ClsServiceAreas objServiceAreaLocationOrigin = new ClsServiceAreas())
            {
                objServiceAreaLocationOrigin.FillOrigin();
                ddlServiceAreaLocationOrigin.DataSource = objServiceAreaLocationOrigin.TblServiceAreaLocationOrigin;
                ddlServiceAreaLocationOrigin.DataTextField = "OriginCityProvince";
                ddlServiceAreaLocationOrigin.DataValueField = "OriginCityProvince";
                ddlServiceAreaLocationOrigin.DataBind();
                ddlServiceAreaLocationOrigin.Items.Insert(0, new ListItem("Choose your Origin", String.Empty));
                ddlServiceAreaLocationOrigin.Items[0].Attributes.Add("disabled","disabled");
                ddlServiceAreaLocationOrigin.Items[0].Attributes.Add("selected","true");
                ddlServiceAreaLocationDestination.Items.Insert(0, new ListItem("Select origin first", String.Empty));
                ddlServiceAreaLocationDestination.Items[0].Attributes.Add("disabled", "disabled");
                ddlServiceAreaLocationDestination.Items[0].Attributes.Add("selected", "true");
                ddlServiceAreaLocationDestination.Items[0].Attributes.Add("class", "SelectOriginFirst");
            }
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clsAuthenticator.Authenticate();
                strMemberID = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                UHopCore.Title = "Choose a subscription plan";
                ViewState["MemberID"] = strMemberID;


                ClsMembers objMembers = new ClsMembers();
                ClsMemberSubscription objMemberSubscription = new ClsMemberSubscription();
                ClsSubscription objSubscription = new ClsSubscription();

                if (ViewState["TblSubscription"] == null)
                {
                    objSubscription.Fill();
                    ViewState["TblSubscription"] = objSubscription.TblSubscription;
                }

                if (ViewState["TblMembers"] == null)
                {
                    objMembers.Fill(strMemberID);
                    ViewState["TblMembers"] = objMembers.TblMembers;
                }
                if (ViewState["TblMemberSubscription"] == null)
                {
                    objMemberSubscription.Fill(objMemberSubscription.TblMemberSubscription, ViewState["MemberID"].ToString());
                    ViewState["TblMemberSubscription"] = objMembers.TblMembers;
                }


                var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                if (MemberDetails.Status == "0")
                {
                    Response.Redirect("../Maintenance/UserProfile.aspx");
                }


                PopulateOrigin();
                //PopulateDestination(ddlOrigin.SelectedValue.ToString());
            }
               // PopulateShuttleSubscription();
          }

        public void PopulateSubscriptions()
        {
            using (ClsSubscription objSubscription = new ClsSubscription())
            {
                objSubscription.TblSubscription = (DataSets.DSConfigs.SubscriptionDataTable)ViewState["TblSubscription"];
                int cntr=1;
                string strWrite="";
                foreach (DataRow drw in objSubscription.TblSubscription.Rows)
                {
                    strWrite+=  "<div class='col s12 m4 wow fadeIn'>" +
                                "<div class='card hoverable' id='subscribeCard1'>" +
                                (drw["isrecommended"].ToString() == "1" ? "<div id='subscribeHeader1'><span class='white-text subscribeTextHeader wow pulse animated' data-wow-delay='300ms' data-wow-iteration='infinite' data-wow-duration='2s'><b>Recommended</b></span></div>" : "<div><br /><br /></div>") +
                                "<div class='card-content black-text'>" +
                                "<div class='col s12'><span class='card-title pink-text'>" + drw["subscriptionname"].ToString() + "</span><div class='divider'></div></div>" +
                                "<br /><br />" +
                                "<ul class='subscribeDetails'>" +
                                "<li><u><b>"+ (drw["IsUnlimited"].ToString() == "1" ? "UNLIMITED RIDE" : "LIMITED RIDE") +"</b></u></li>" +
                                "<li><u>" + (Convert.ToInt32(drw["validfornoofdays"]) > 1 ? drw["validfornoofdays"].ToString() +" days" :drw["validfornoofdays"].ToString() + " day") + "</u></li>" +
                                "<li><u><b>" + drw["Price"].ToString() + " PHP</b></u></li>" +
                                "<li><u>" + drw["shortdescription"].ToString() + "</u></li>" +
                                "</ul>" +
                                "</div>" +
                                "<div class='card-action'>" +
                                "<a href='#' class='btn waves-effect waves-light black white-text center-align'>PURCHASE</a>" +
                                "</div></div></div>";
                    cntr += 1;
                }
                Response.Write(strWrite);
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
          
        }
        protected void ddlOrigin_SelectedIndexChanged(object sender, EventArgs e)
        {
         //   PopulateDestination(ddlOrigin.SelectedValue.ToString());
        }
        protected void ddlServiceAreaLocationOrigin_SelectedIndexChanged(object sender, EventArgs e)
        {
           // PopulateDestination(ddlServiceAreaLocationOrigin.SelectedValue);
        }

        public class ServiceAreaLocationDestination
        {
            private string _DestinationCityProvince;
            public string DestinationCityProvince 
            {
                get { return _DestinationCityProvince; }
                set { _DestinationCityProvince = value; }
            }
        }
        public class ServiceAreaSubscriptionToSell
        {
            private string _Origin;
            public string Origin
            { get { return _Origin; } set { _Origin = value; } }

            private string _Destination;
            public string Destination
            { get { return _Destination; } set { _Destination = value; } }
            
            private string _ServiceAreaSubscriptionID;
            public string ServiceAreaSubscriptionID
            { get { return _ServiceAreaSubscriptionID; } set { _ServiceAreaSubscriptionID = value; }}

            private string _ServiceAreaSubscriptionIDE;
            public string ServiceAreaSubscriptionIDE
            { get { return _ServiceAreaSubscriptionIDE; } set { _ServiceAreaSubscriptionIDE = value; } }

                        private string _IsRecommended;
            public string IsRecommended
            { get { return _IsRecommended; } set { _IsRecommended = value; }}

                        private string _SubscriptionName;
            public string SubscriptionName
            { get { return _SubscriptionName; } set { _SubscriptionName = value; }}

                        private string _IsUnlimited;
            public string IsUnlimited
            { get { return _IsUnlimited; } set { _IsUnlimited = value; }}
            
            private string _ValidForNumberOfDays;
            public string ValidForNumberOfDays
            { get { return _ValidForNumberOfDays; } set { _ValidForNumberOfDays = value; }}

            private string _Price;
            public string Price
            { get { return _Price; } set { _Price = value; }}

            private string _ShortDescription;
            public string ShortDescription
            { get { return _ShortDescription; } set { _ShortDescription = value; } }

        }
        public class ServiceAreaSubscriptionLocation
        {
            private string _ServiceAreaLocationID;
            public string ServiceAreaLocationID
            {
                get { return _ServiceAreaLocationID; }
                set { _ServiceAreaLocationID = value; }
            }

            private string _OriginLocation;
            public string OriginLocation
            {
                get { return _OriginLocation; }
                set { _OriginLocation = value; }
            }
            
            private string _DestinationLocation;
            public string DestinationLocation
            {
                get { return _DestinationLocation; }
                set { _DestinationLocation = value; }
            }

            private string _SubscriptionName;
            public string SubscriptionName
            {
                get { return _SubscriptionName; }
                set { _SubscriptionName = value; }
            }
        }
        
        [WebMethod]
        public static List<ServiceAreaLocationDestination> GetDestination (String pOriginCityProvince)
        {
            using (ClsServiceAreas objDestination = new ClsServiceAreas())
            {
                List<ServiceAreaLocationDestination> lstDestinationCityProvince = new List<ServiceAreaLocationDestination>();
                DataTable tbl = new DataTable();
                objDestination.FillDestination(pOriginCityProvince);
                tbl = objDestination.TblServiceAreaLocationDestination;
                foreach (DataRow drw in tbl.Rows)
                {
                     string DestinationCityProvince = drw["DestinationCityProvince"].ToString();
                     lstDestinationCityProvince.Add(new ServiceAreaLocationDestination 
                    {
                        DestinationCityProvince = DestinationCityProvince
                    });
                }
                return lstDestinationCityProvince;
            }         
        }
        [WebMethod]
        public static List<ServiceAreaSubscriptionToSell> PopulateSubscriptionCards(String pOriginCityProvince, String pDestinationCityProvince)
        {
            using (ClsSubscription objSubscriptionCards = new ClsSubscription())
            {
                List<ServiceAreaSubscriptionToSell> lstSubscriptionCards = new List<ServiceAreaSubscriptionToSell>();
                DataTable tbl = new DataTable();
                objSubscriptionCards.FillServiceAreaSubscription(pOriginCityProvince, pDestinationCityProvince);
                tbl = objSubscriptionCards.TblServiceAreaSubscription;
                foreach (DataRow drw in tbl.Rows)
                {
                    string ServiceAreaSubscriptionID = drw["ServiceAreaSubscriptionID"].ToString();
                    string ServiceAreaSubscriptionIDE = ClsEncryptor.Encrypt(drw["ServiceAreaSubscriptionID"].ToString(),"u-h0p");
                    string Origin = drw["Origin"].ToString();
                    string Destination = drw["Destination"].ToString();
                    string IsRecommended = drw["IsRecommended"].ToString();
                    string SubscriptionName = drw["SubscriptionName"].ToString();
                    string IsUnlimited = drw["IsUnlimited"].ToString();
                    string ValidForNumberOfDays = drw["ValidForNumberOfDays"].ToString();
                    string Price = Convert.ToDecimal(drw["Price"].ToString()).ToString("#,##0.00");
                    string ShortDescription = drw["ShortDescription"].ToString();
                    lstSubscriptionCards.Add(new ServiceAreaSubscriptionToSell
                    {
                        Origin = Origin,
                        Destination = Destination,
                        ServiceAreaSubscriptionID = ServiceAreaSubscriptionID,
                        ServiceAreaSubscriptionIDE = ServiceAreaSubscriptionIDE,
                        IsRecommended=IsRecommended,
                        SubscriptionName=SubscriptionName,
                        IsUnlimited=IsUnlimited,
                        ValidForNumberOfDays=ValidForNumberOfDays,
                        Price=Price,
                        ShortDescription=ShortDescription
                    });
                }
                return lstSubscriptionCards;
            }
        }
        [WebMethod]
        public static List<ServiceAreaSubscriptionLocation> PopulateOriginDestinationInfo(String pSubscription)
        {
            using (ClsSubscription objSubscriptionCards = new ClsSubscription())
            {
                List<ServiceAreaSubscriptionLocation> lstSubscriptionLocation = new List<ServiceAreaSubscriptionLocation>();
                DataTable tbl = new DataTable();
                objSubscriptionCards.FillServiceAreaSubscriptionLocation(pSubscription);
                tbl = objSubscriptionCards.TblServiceAreaSubscriptionLocation;
                DataView dv = new DataView(tbl);
                dv.Sort = "ServiceAreaLocationID ASC";
                tbl = dv.ToTable(); 
                foreach (DataRow drw in tbl.Rows)
                {

                    string ServiceAreaLocationID = drw["ServiceAreaLocationID"].ToString();
                    string SubscriptionName = drw["SubscriptionName"].ToString();
                    string OriginLocation = drw["Origin"].ToString();
                    string DestinationLocation = drw["Destination"].ToString();

                    lstSubscriptionLocation.Add(new ServiceAreaSubscriptionLocation
                    {
                        SubscriptionName=SubscriptionName,
                        OriginLocation = OriginLocation,
                        DestinationLocation = DestinationLocation,
                        ServiceAreaLocationID = ServiceAreaLocationID
                    });
                }
                return lstSubscriptionLocation;
            }
        }

        public class ServiceAreaSubscriptionLocationMajorStop
        {
            private string _MajorStopID;
            public string MajorStopID
            {
                get { return _MajorStopID; }
                set { _MajorStopID = value; }
            }

            private string _MajorStopAddress;
            public string MajorStopAddress
            {
                get { return _MajorStopAddress; }
                set { _MajorStopAddress = value; }
            }
       }

        [WebMethod]
        public static List<ServiceAreaSubscriptionLocationMajorStop> PopulateOriginDestinationInfoMajorStop(String pSubscription)
        {
            using (ClsSubscription objSubscriptionCards = new ClsSubscription())
            {
                List<ServiceAreaSubscriptionLocationMajorStop> lstSubscriptionLocationMajorStop = new List<ServiceAreaSubscriptionLocationMajorStop>();

                //DataTable tbl1 = new DataTable();
                //objSubscriptionCards.FillServiceAreaSubscriptionLocation(pSubscription);
                //tbl1 = objSubscriptionCards.TblServiceAreaSubscriptionLocation;
                //DataView dv1 = new DataView(tbl1);
                //dv1.Sort = "Origin ASC";
                //tbl1 = dv1.ToTable();
                //foreach (DataRow drw1 in tbl1.Rows)
                //{

                    DataTable tbl = new DataTable();
                    objSubscriptionCards.FillServiceAreaSubscriptionLocationMajorStop(pSubscription);
                    tbl = objSubscriptionCards.TblServiceAreaSubscriptionLocationMajorStop;
                    DataView dv = new DataView(tbl);
                    dv.Sort = "PositionOrder ASC";
                    tbl = dv.ToTable();
                    foreach (DataRow drw in tbl.Rows)
                    {
                        string MajorStopID = drw["MajorStopID"].ToString();
                        string MajorStopAddress = drw["MajorStopAddress"].ToString();

                        lstSubscriptionLocationMajorStop.Add(new ServiceAreaSubscriptionLocationMajorStop
                        {
                            MajorStopID = MajorStopID,
                            MajorStopAddress = MajorStopAddress
                        });
                    }
                //}
                return lstSubscriptionLocationMajorStop;
            }
        }


    }
}