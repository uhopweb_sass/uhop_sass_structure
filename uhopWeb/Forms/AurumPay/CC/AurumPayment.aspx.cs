﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using uhopWeb.Classes;


namespace uhopWeb.Forms.AurumPay.CreditCard
{
    public partial class AurumPayment11 : System.Web.UI.Page
    {
        public static string strMemberID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Form["MID"] != null)
                {
                strMemberID = Request.Form["MID"];
                ClsMembers objMembers = new ClsMembers();
                ClsSubscription objSubscription = new ClsSubscription(Request.Form["SID"]);
                if (ViewState["TblMembers"] == null)
                {
                    objMembers.Fill(strMemberID);
                    ViewState["TblMembers"] = objMembers.TblMembers;
                }
                if (ViewState["TblServiceAreaSubscription"] == null)
                {
                    objSubscription.FillServiceAreaSubscription();
                    ViewState["TblServiceAreaSubscription"] = objSubscription.TblServiceAreaSubscription;
                }

                var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                if (MemberDetails.Status == "0")
                {
                    Response.Redirect("../Maintenance/UserProfile.aspx");
                }
                }
           }
        }

      public string url()
        {
            string z = Request.Form["MID"];
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            string ipaddress = "";
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipaddress = ip.ToString();
                }
            }
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                ClsSubscription objSubscription = new ClsSubscription(Request.Form["SID"]);
                objSubscription.TblServiceAreaSubscription=(DataSets.DSTransactions.ServiceAreaSubscriptionToSellDataTable)ViewState["TblServiceAreaSubscription"];

                decimal unitPrice = Convert.ToDecimal(objSubscription.GetSubscriptionDetails("Price"));
                decimal OnlinePaymentFee = Convert.ToDecimal("0.048");
                decimal OFee = unitPrice * OnlinePaymentFee;
                decimal TotalAmount = Convert.ToDecimal(Convert.ToDouble(OFee) + Convert.ToDouble(unitPrice));

                string numValue = TotalAmount.ToString();
                //decimal decValue = 1;
                var amountInCents = numValue.ConvertToDecimal() * 100;  //Convert.ToDecimal(numValue) > decValue ? Convert.ToInt32(numValue.ConvertToDecimal()) : Convert.ToInt32(numValue.ConvertToDecimal()) * 100;
                values["order_desc"] = Request.Form["order_desc"];
                values["first_name"] = Request.Form["first_name"];
                values["last_name"] = Request.Form["last_name"];
                values["address1"] = Request.Form["address1"];
                values["city"] = Request.Form["city"];
                values["state"] = Request.Form["state"];
                values["zip_code"] = Request.Form["zip_code"];
                values["country"] = Request.Form["country"];
                values["phone"] = Request.Form["phone"];
                values["email"] = Request.Form["email"];
                values["amount"] = Convert.ToDecimal(numValue).ToString("###0.00");
                //values["server_callback_url"] = "https://u-hop.org:8088/Forms/AurumPay/CC/SC.aspx?S=${status}&M=${merchant_order}&O=${orderid}&I=" + z; //original
                values["server_callback_url"] = UHopCore.GetPageURL() + "Forms/AurumPay/CC/SC.aspx?S=${status}&M=${merchant_order}&O=${orderid}&I=" + z;
                values["client_orderid"] = Request.Form["client_orderid"];
                values["ipaddress"] = ipaddress;
                
                values["currency"] = "PHP";
                values["endPointId"] = "35";
                values["merchantControl"] = "8EDB07F1-E00C-4F23-B8DB-3CD7242C423A";

                DateTime dtSubscription = Convert.ToDateTime(ClsEncryptor.Decrypt(Request.Form["DateStart"].Replace(" ", "+"), "u-H0p"));
                values["redirect_url"] = UHopCore.GetPageURL() + "Forms/AurumPay/AurumPaymentStatus.aspx?R=" + dtSubscription.ToString("yyyy-MM-dd") + "&X=" + Request.Form["transaction_type"] + "&T=" + z;
                //values["redirect_url"] = "http://localhost:4803/Forms/AurumPay/AurumPaymentStatus.aspx?R=" + dtSubscription.ToString("yyyy-MM-dd") + "&X=" + Request.Form["transaction_type"] + "&T=" + z;
            
                string decrypted = FormsAuthentication.HashPasswordForStoringInConfigFile("35" + Request.Form["client_orderid"] + amountInCents.ToString("#") + Request.Form["email"] + "8EDB07F1-E00C-4F23-B8DB-3CD7242C423A", "SHA1");

                values["control"] = decrypted;

                var response = client.UploadValues("https://sandbox.aurumpay.com/paynet/api/v2/sale-form/35", values);

                var responseString = Encoding.Default.GetString(response);
                var c = responseString.Split('&');
                foreach (string i in c)
                {
                    if (i.Contains("redirect-url"))
                    {
                        var t = i.Split('=');
                        responseString = t[1];
                    }
                }
                

                return Server.UrlDecode(responseString);

            }
        }
        }
    
}