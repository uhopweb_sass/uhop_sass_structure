﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using uhopWeb.Classes;
using System.Data;
using System.Net.Http;


namespace uhopWeb.Forms.AurumPay.CreditCard
{
    public partial class ServerCallback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    string strMSG = "";
                    string strStatus = Request.QueryString["S"];
                    string strMerchantOrderID = Request.QueryString["M"];
                    string strOrderID = Request.QueryString["O"];
                    string strMemberID = Request.QueryString["I"];
                    ClsSendMail objSendMail = new ClsSendMail();

                    if (strStatus != "" && strMerchantOrderID != "" & strOrderID != "" && strMemberID != "")
                    {
                        if (strStatus.ToLower().Trim() == "filtered")
                        {
                            strMSG = "Your payment status: <b>Filtered</b> (Credit Card)<br /> There was a problem processing your transaction. A transaction with identical amount and card <br />information has been submitted. Please try another transaction. If the problem persists, contact <br />your credit/debit card company for assistance or call the membership hotline (654-3390) for inquiries.";
                        }
                        else if (strStatus.ToLower().Trim() == "error")
                        {
                            strMSG = "Your payment status: <b>Error</b> (Credit Card)<br /> An unexpected error has prevented your transaction from being processed. Please call the membership hotline (654-3390) for inquiries.";
                        }
                        else if (strStatus.ToLower().Trim() == "declined")
                        {
                            strMSG = "Your payment status: <b>Declined</b> (Credit Card)<br /> Your transaction has not been completed. Your credit/debit card information has been declined <br />by your credit/debit card company. Please contact your credit/debit card company for assistance, or try another transaction.";
                        }
                        else if (strStatus.ToLower().Trim() == "approved")
                        {
                            using (ClsMembers objMembers = new ClsMembers())
                            {
                                objMembers.Fill(strMemberID);
                                //objMembers.FillAddress();
                                objSendMail.SendMailAurumPayment(CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(objMembers.GetMemberDetailsByFieldName(strMemberID, "Email").ToString()), CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(objMembers.GetMemberDetailsByFieldName(strMemberID, "Firstname").ToString()), "Credit Card", CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(strStatus), strOrderID, strMerchantOrderID);
                                Response.StatusCode = 200;
                                Response.End();
                            }
                            return;
                        }
                        else
                        {
                            strMSG = "Your payment status: <b>" + strStatus.ToUpper() + "</b> (Over-the-counter)<br /> Kindly Email us or call to our hotline (654-3390)!";
                        }

                        using (ClsMembers objMembers = new ClsMembers())
                        {
                            objMembers.Fill(strMemberID);
                            //objMembers.FillAddress();
                            objSendMail.SendEmailNotify(objMembers.GetMemberDetailsByFieldName(strMemberID, "Email").ToString(),
                                CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(objMembers.GetMemberDetailsByFieldName(strMemberID, "Firstname").ToString()),
                                "U-Hop Purchased Subscription | " + strOrderID, strMSG);
                        }

                        Response.StatusCode = 200;
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                //ClsSendMail objSendMail = new ClsSendMail();
                //objSendMail.SendEmailNotify("ross.colas@gmail.com",
                //               "Ross",
                //               "U-Hop Purchased Subscription | " + "Error", ex.Message);
            }
            }
    }
}