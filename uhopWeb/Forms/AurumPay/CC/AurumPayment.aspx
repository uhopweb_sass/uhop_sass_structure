﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Site1.Master" AutoEventWireup="true" CodeBehind="AurumPayment.aspx.cs" Inherits="uhopWeb.Forms.AurumPay.CreditCard.AurumPayment11" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
         <style type="text/css">
        html {
            overflow: auto !important;
            height: 100% !important;
        }

        body {
            background-image: -webkit-gradient(linear, left top, left bottom, from(#dbecf4 ), to(#FFFFFF)) !important;
            background-image: -webkit-linear-gradient(top,#dbecf4  , #FFFFFF) !important;
            background-image:    -moz-linear-gradient(top, #dbecf4 , #FFFFFF) !important;
            background-image:      -o-linear-gradient(top, #dbecf4 , #FFFFFF) !important;
            background-image:         linear-gradient(to bottom, #dbecf4 , #FFFFFF) !important;
        }

        iframe {
            margin: 0px;
            padding: 0px;
            height: calc(100% - 64px);
            top: 64px;
            border: none;
            display: block;
            width: 100%;
            border: none;
            overflow-y: auto;
            overflow-x: hidden;
            position: absolute;
            
        }
    </style> 
    

    <iframe id="tree" name="tree" src="<% Response.Write(url()); %>" frameborder="0" marginheight="0" marginwidth="0" width="100%" height="100%" scrolling="auto"></iframe>
 
</asp:Content>
