﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Site1.Master" AutoEventWireup="true" CodeBehind="PolicyAgreement.aspx.cs" Inherits="uhopWeb.Forms.AurumPay.PolicyAgreement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
     
     <style>
        body {
            background-color: white !important;
        }
    
    </style>

      <!-- Modal Structure -->
      <div id="refundModal" class="modal modal-fixed-footer">
        <div class="modal-content">
          <h5>U-Hop Cancellation of Subscription</h5>
          <p>1. Call Membership Hotline (654-3390).</p>
          <p>2. Member will receive an e-mail of confirmation for the cancelled transaction.</p>
          <p>3. Members can rebook the trip or ask for a refund.</p>
            <ul style="margin-top: -10px;margin-left:20px;">
                <li><b>•</b> Call the Membership hotline to rebook the trip.</li>
                <li><b>•</b> To refund, see refund policy below.</li>
            </ul>
          <p>
              <b>Note:</b> Should a member wish to cancel their subscription with us, a formal, written request must be received at member@u-hop.com at least 7 days prior to the date of travel,
              at which point they are entitled to a 100% refund per member. Cancellations made later than 7 days in advance are ineligible for refunds.
          </p><br />
          
          <h5>U-Hop Refund OTC, Bank and Credit/Debit Card</h5>
          <p>1. Call Membership Hotline (654-3390).</p>
          <p>2. Members will be notified upon successful refund.</p>
          <p>3. Mode of payment:</p>
          <ul style="margin-top: -10px;margin-left:20px;">
            <li><b>•</b> OTC/ Bank Deposit (10 working days process)<br />
                <span style="margin-left: 20px;">- Member has only 30 days (from the day of pick-up specified at the e-mail) to claim the refund at the nearest u-Hop Branch / HO.</span>
            </li>
            <li><b>•</b> Credit/ Debit Card<br />
                <span style="margin-left: 20px;">- 10 working days before credits are reverted.</span>
            </li>
          </ul>
          <p>
              <b>Note:</b>
              <ul style="margin-top: -10px;margin-left:20px;">
                <li><b>•</b> Subscription should be unused and if used:<br />
                    <span style="margin-left: 20px;">- For <b>WEEKLY</b> subscription = if partly used, no refund is allowed.</span><br />
                    <span style="margin-left: 20px;">- For <b>MONTHLY</b> subscription = if partly used, the only refundable amount will be the complete week unused.</span>
                </li>
                <li><b>•</b>Refund will be less of service charge.                
                </li>
              </ul>
          </p>
        </div>
        <div class="modal-footer">
          <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Dismiss</a>
        </div>
      </div>
      <!-- END of Modal -->

     <div class="container" style="margin-top: 1%;">
         <div class="row">
            <%--<div class="col s12">
                <button onclick="JavaScript:window.history.back(1);return false;" class="btn-flat right waves-effect waves-light red-text"> <i class="large material-icons">&#xE042; </i> Back to Payment Options</button>
            </div>--%>
             <br />
             <div class="col s12">
                 <div class="divider"></div>
             </div>
         </div>
        <div class="row">
            <div class="col s12">
                <h3 style="margin: auto" class="paymentOptHeader">Subscription Details</h3>
            </div>
        </div>
        <div class="divider black" style="height:2px;"></div> 

         <br />
        <asp:HiddenField runat="server" ID="hdfQueryString" />
            <!--<div class="card grey lighten-3 wow rubberBand hoverable" id="paymentCardDetails">
                <div class="card-content">-->
                    <asp:HiddenField ID="hdfFirstname" runat="server" />
                    <asp:HiddenField ID="hdfLastname" runat="server" />
                    <asp:HiddenField ID="hdfAddress" runat="server" />
                    <asp:HiddenField ID="hdfCity" runat="server" />
                    <asp:HiddenField ID="hdfState" runat="server" />
                    <asp:HiddenField ID="hdfCountry" runat="server" />
                    <asp:HiddenField ID="hdfZipcode" runat="server" />
                    <asp:HiddenField ID="hdfPhone" runat="server" />
                    <asp:HiddenField ID="hdfEmail" runat="server" />
                    <asp:HiddenField ID="hdfOrderDesc" runat="server" />
                    <asp:HiddenField ID="hdfClientOrderID" runat="server" />
                    <asp:HiddenField ID="hdfOTCType" runat="server" />
                    
                    <div class="row">
                        <div class="col l7 s12">
                        <table class="striped bordered">
                            <tr>
                                <td>Subscription:</td>
                                <td><asp:Label ID="lblSubscriptionName" runat="server" Text="Label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Price:</td>
                                <td><asp:Label ID="lblPrice" runat="server" Text="Label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Online Payment Fee:</td>
                                <td><asp:Label ID="lblOFee" runat="server" Text="Label"></asp:Label></br></td>
                            </tr>
                            <tr>
                                <td>Total :</td>
                                <td><h5><asp:Label ID="lblTotal" runat="server" Text="Label"></asp:Label></h5></br></td>
                            </tr>
                            <tr>
                                <td>Days:</td>
                                <td><asp:Label ID="lblDays" runat="server" Text="Label"></asp:Label></br></td>
                            </tr>
                            <tr>
                                <td>Description:</td>
                                <td><asp:Label ID="lblShortDesc" runat="server" Text="Label"></asp:Label></br></td>
                            </tr>
                            <tr>
                                <td>Payment Form:</td>
                                <td><%  
                                string x = Request.QueryString["a"];
                                var footerVar = x == "mastercardCC" ? "(MasterCard and Visa Credit Card)"
                                    : x == "debitcard" ? "Payment Form (BancNet / ATM)"
                                    : x == "otcounter" ? "Payment Form (Over-the-counter)"
                                    : x == "mastercardDC" ? "Payment Form (MasterCard and Visa Debit Card)" : "";

                                Response.Write(footerVar);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                            %></td>
                            </tr>
                        </table>
                        </div>
                        <!--<asp:Label ID="lblID" runat="server" Text="Label" Style="display:none;"></asp:Label></br>-->
                        <!--<asp:Label ID="lblIsRecommended" runat="server" Text="Label" Style="display:none;"></asp:Label></br>-->
                        <!--<asp:Label ID="lblIsUnlimited" runat="server" Text="Label" Style="display:none;"></asp:Label></br>-->
                    </div>
                    
                    <div class="row">
                        <div class="col l2 s12">
                            <%--<i class="material-icons prefix">&#xE878;</i> <b>DATE:</b>--%>
                            <p class="noMargin"><b>Subscription Date:</b></p>
                        </div>                        
                        <div class="col l5 s12">
                            <div class="col s12">
                                <asp:TextBox class="expirationDatepicker txtSubscribeDate errorLabel" ID="txtSubscribeDate" runat="server" MaxLength="1" placeholder="Effectivity date of Subscription"></asp:TextBox>
                                <span runat="server" id="spnRequired" class="red-text" style="display:none;"></span>
                            </div>
                            <div class="col s12">
                                <span runat="server" id="spnSubscribe" class="red-text" style="display:none;"></span>
                            </div>
                            <div class="col l6 m6 s12">
                                <%--<asp:Button ID="btnProceed" CssClass="btn btn-primary btn btn-lg  green accent-2" runat="server" Text="Proceed" ValidationGroup="PurchasedSubscription" style="display:none;" OnClick="btnProceed_Click"/>--%>
                                
                                <a runat="server" id="btnProceed" class="btn btn-primary btn btn-lg  blue darken-2" ValidationGroup="PurchasedSubscription" style="display:none;" onserverclick="btnProceed_Click">PROCEED</a>
                                <br />
                            </div>
                            <div class="col l6 m6 s12">
                                <a runat="server" id="btnCancel" href="../Transactions/Subscription.aspx" class="btn btn-primary btn btn-lg  red accent-2" style="display:none;">Cancel</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col s6">
                            
                        </div>
                    </div>
                <!--</div> 
            </div> <!-- END of card -->
                <div class="row">
                    <div class="col s12 l8">
                        <h3 style="margin: auto" class="paymentOptHeader">FAQS</h3>
                    </div>
                </div>
                <div class="divider black" style="height:2px;"></div> 
                    <div class="row noMargin">
                        <div class="col l6 s12">
                            <h4 class="faqHeader paymentOptHeader"><b>How we protect your information</b></h4>
                            <h5 class="faqDetails policyDetails">We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information stored on our site.</h5>
                            <h5 class="faqInfo policyDetails">Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures.</h5>
                            <h5 class="faqInfo policyDetails">&nbsp;User confirms that the above information is correct and accurate, and acknowledges that the information entered above will be strictly and only used for the purpose of processing your transaction.<br></h5>
                        </div>

                         <!-- Modal Trigger -->
                        <!-- <a class="waves-effect waves-light btn modal-trigger black" data-wow-duration="3s" href="#modalProtect">Continue?</a>

                          <!-- Modal Structure -->
                          <!--<div id="modalProtect" class="modal">
                            <div class="modal-content">
                              <h4>"Your Security is our Responsibility"</h4>
                              <div class="divider"></div>
                              <div style="font-size:20px;">
                                  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information stored on our site.</p>
                                  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures.</p>
                                  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;User confirms that the above information is correct and accurate, and acknowledges that the information entered above will be strictly and only used for the purpose of processing your transaction.<br></p>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">OKAY</a>
                            </div>
                          </div>-->
                          <div class="col l6 s12">
                              <h4 class="faqHeader paymentOptHeader"><b>Refund and Cancellation </b></h4>
                              <h5 class="faqDetails policyDetails">Click here for the <a class="modal-trigger" target="_blank" href="#refundModal">Refund and Cancellation Policy.</a></h5>
                          </div>
                    </div>
                    
                    <div class="row noMargin">
                        <div class="col s12"><h4 class="faqHeader paymentOptHeader"><b>Payment Disclosure</b></h4></div>

                        <div class="col s10">
                            <%  
                                string y = Request.QueryString["a"];
                                var footerVar1 = x == "mastercardCC" ? "<h5 class='faqDetails policyDetails'>Please take note that the Payment Descriptor that you will see in your Credit Card Billing Statement will be <b><i>AurumPay® Philippines Corporation.</i></h5></b>"
                                                    : "";
                                Response.Write(footerVar1);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                            %>
                        </div>

                         <div class="col s12">
                            <input type="checkbox" name="required-checkbox" id="required-checkbox" onclick="CheckIfChecked()">
                            <label for="required-checkbox">Click here if you accept and agree</label>
                        </div>
                        
                        <div class="col m2 s12 center" style="padding: 10px;padding-left: 20px;">
                            <div class="control-group" id="submit-button-container" style="display: none;">
                                <asp:Button ID="btnSubmit1" CssClass="btn btn-primary btn btn-lg  pink accent-2" runat="server" Text="Submit" ValidationGroup="PurchasedSubscription" OnClick="btnSubmit1_Click" />
                            </div>
                        </div>
                  </div>
        

         


               <br /><br />
              <div class="row">
               <div class="col s12">
                  <footer id="paymentFooter" class="center">
	                    <div class="divider black" style="height:2px;"></div><br />
                         Powered by: <br>
		                 <!--Aurumpay-->
		                <a href="http://www.aurumpay.com/" target="_blank"><img src="../../Images/index/Aurumpay-min.jpg" id="aurum"></a>
		                <!--Aurumpay-->
                        <p class="grey-text center" style="font-size: 12px;">The GOLD STANDARD in online payment services 2012</p>
                  </footer>
              </div>
           </div>
        </div>

        <br>
        <br>
        <br>
        <br>
        <script>
            $(document).ready(function () {
                new WOW().init();
                $('select').material_select();
                $('.parallax').parallax();
            });

            function CheckIfChecked() {
                var CheckboxID = "required-checkbox";
                var SubmitButtonContainerID = "submit-button-container";
                if (document.getElementById(CheckboxID).checked) { document.getElementById(SubmitButtonContainerID).style.display = "block"; }
                else { document.getElementById(SubmitButtonContainerID).style.display = "none"; }
            }


            //$("#btnSubmit").click(function () {
            //    $('#form1').attr("action", "CreditCard/AurumPayment.aspx");
            //});


        </script>

</asp:Content>
    