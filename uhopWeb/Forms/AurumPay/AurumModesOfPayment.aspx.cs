﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Web.Services;
using uhopWeb.Classes;

namespace uhopWeb.Forms.AurumPay
{
    public partial class AurumModesOfPayment : System.Web.UI.Page
    {
        public string strMemberID;

        protected void Page_Load(object sender, EventArgs e)
        {
            clsAuthenticator.Authenticate();
            strMemberID = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
            string strID = ClsEncryptor.Decrypt(Request.QueryString["ND54653"].ToString().Replace(" ", "+"), "u-h0p");
            
            ViewState["MemberID"] = strMemberID;

            if(!IsPostBack){
                UHopCore.Title = "Choose payment option";
            
                ClsMembers objMembers = new ClsMembers();
                ClsSubscription objSubscription = new ClsSubscription(strID);

                if (ViewState["TblMembers"] == null)
                {
                    objMembers.Fill(ViewState["MemberID"].ToString());
                    ViewState["TblMembers"] = objMembers.TblMembers;
                }
                if (ViewState["TblServiceAreaSubscription"] == null)
                {
                    objSubscription.FillServiceAreaSubscription();
                    ViewState["TblServiceAreaSubscription"] = objSubscription.TblServiceAreaSubscription;
                }
                

                int intDays = Convert.ToInt32(objSubscription.GetSubscriptionDetails("ValidForNumberOfDays"));
                spnSubscription.InnerText = " " + objSubscription.GetSubscriptionDetails("SubscriptionName") + ", " + (intDays > 1 ? intDays + " days" : intDays + " day") + ", for only " + Convert.ToDecimal(objSubscription.GetSubscriptionDetails("Price")).ToString("#,##0.00");

                if (objMembers.TblMembers.Rows[0].GetString("Status") == "0")
                {
                    Response.Redirect("../Maintenance/UserProfile.aspx");
                }
            }






                
                //LIMITED RIDE, 7 DAYS, for only 693.00 PHP.
                
            
        }

        public void ProvideSubscription()
        {
            //string strID = clsEncryptor.Decrypt(Request.QueryString["ND54653"].Replace(" ", "+"), "u-h0p");
            string strID = Request.QueryString["ND54653"];
            Response.Write(strID);
        }
    }
}