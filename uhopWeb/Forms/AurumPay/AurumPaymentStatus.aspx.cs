﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using uhopWeb.Classes;
using System.Data;

namespace uhopWeb.Forms.AurumPay
{
    public partial class AurumPaymentStatus : System.Web.UI.Page
    {
        public static string MemberID;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    HttpCookie objCookie = Request.Cookies["Uhop"];
                    if (objCookie != null)
                    {
                        string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                        MemberID = strDecrypted; 
                        ClsMembers objMembers = new ClsMembers(MemberID);

                        if(ViewState["TblMembers"]==null)
                        {
                            objMembers.Fill(MemberID);
                            ViewState["TblMembers"]=objMembers.TblMembers;
                        }

                        objMembers.TblMembers=(DataSets.DSConfigs.MemberDataTable)ViewState["TblMembers"];

                        var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                        if (MemberDetails.Status == "0")
                        {
                            Response.Redirect("../Maintenance/UserProfile.aspx");
                        }
                        string strStatus = Request.Form["Status"].ToString();
                        string strMemberID = Request.QueryString["T"].ToString();
                        string strModesOfPayment = Request.QueryString["X"].ToString();
                        string strTransactionID;
                        string strServiceAreaSubscriptionID = Request.Form["client_orderid"].ToString();
                        string strOrderID = Request.Form["orderid"].ToString();
                        ClsTransaction objTransaction = new ClsTransaction();

                        lblStatus.InnerHtml = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(strStatus);
                        lblMerchantOrderID.InnerHtml = strServiceAreaSubscriptionID;
                        lblTransactionID.InnerHtml = strOrderID;

                        objTransaction.FillTransactionSubscriptionDetails(strServiceAreaSubscriptionID);
                        DataTable tbl = objTransaction.TblTransactionSubscriptionDetails;
                        foreach (DataRow drw in tbl.Rows)
                        {
                            if (strStatus == "approved")
                            {
                                //Saving on the Transaction Table
                                ClsMemberSubscription objMemberSubscription = new ClsMemberSubscription();
                                DateTime dtSubcribeDate = Convert.ToDateTime(Request.QueryString["R"].ToString());
                                DataRow drwTransaction = objTransaction.TblTransaction.NewRow();
                                DataRow drwTransactionDetail = objTransaction.TblTransactionDetails.NewRow();
                                DataRow drwModesOfPaymentDetails = objTransaction.TblModesOfPaymentDetails.NewRow();
                                DataRow drwMemberSubscription = objMemberSubscription.TblMemberSubscription.NewRow();
                                decimal decAmount = 0;
                                if (strModesOfPayment == "20150923000002" || strModesOfPayment == "20150923000003") //for Credit Card and Debit card
                                {
                                    drwModesOfPaymentDetails.SetString("CardHolderName", Request.Form["card-holder-name"].ToString());
                                    drwModesOfPaymentDetails.SetString("Last4Digits", Request.Form["last-four-digits"].ToString());

                                    decimal unitPrice = Convert.ToDecimal(drw["Price"].ToString());
                                    decimal OnlinePaymentFee = 0;
                                    if (strModesOfPayment == "20150923000002")
                                    {
                                        OnlinePaymentFee = Convert.ToDecimal("0.048");
                                    }
                                    else { OnlinePaymentFee = Convert.ToDecimal("0.044"); }
                                    decimal OFee = unitPrice * OnlinePaymentFee;
                                    decimal TotalAmount = Convert.ToDecimal(Convert.ToDouble(OFee) + Convert.ToDouble(unitPrice));
                                    decAmount = TotalAmount;

                                }
                                else
                                {
                                    drwModesOfPaymentDetails.SetString("CardHolderName", "");
                                    drwModesOfPaymentDetails.SetString("Last4Digits", "");
                                }

                                drwTransaction.SetString("TransactionTypeID", "20150923000002");
                                drwTransaction.SetString("MemberID", strMemberID);
                                //drwTransaction.SetString("AurumPayTransactionID", Request.Form["processor-tx-id"].ToString());
                                drwTransaction.SetString("ReferenceNumber", strOrderID);
                                drwTransaction.SetString("PaymentChannelID", "20151030000001"); //aurum Pay
                                //drwTransaction.SetString("AurumPayControlCode", Request.Form["control"].ToString());
                                drwTransaction.SetString("ModesOfPaymentID", strModesOfPayment);
                                drwTransaction.SetDecimal("GrandTotal", decAmount);//Convert.ToDecimal()); 
                                drwTransaction.SetString("Status", strStatus);
                                drwTransaction.SetString("IsPaid", "1");
                                drwTransaction.SetDateTime("CreatedOn", DateTime.Now);
                                //Saving on the Transaction Details Table
                                drwTransactionDetail.SetString("ServiceAreaSubscriptionID", strServiceAreaSubscriptionID);
                                drwTransactionDetail.SetInt32("Quantity", 1);
                                drwTransactionDetail.SetDecimal("Amount", decAmount);
                                drwTransactionDetail.SetDecimal("SubTotal", decAmount);

                                //Transaction Table
                                ClsAutoNumber objAutoNumber = new ClsAutoNumber();
                                strTransactionID = objAutoNumber.GenerateAutoNumber("TransactionID", "000000").ToString();
                                drwTransaction.SetString("TransactionID", strTransactionID);
                                objTransaction.TblTransaction.Rows.Add(drwTransaction);
                                objTransaction.UpdateTransaction();
                                //Transaction Details Table
                                drwTransactionDetail.SetString("TransactionDetailsID", objAutoNumber.GenerateAutoNumber("TransactionDetailsID", "000000").ToString());
                                drwTransactionDetail.SetString("TransactionID", strTransactionID);
                                objTransaction.TblTransactionDetails.Rows.Add(drwTransactionDetail);
                                objTransaction.UpdateTransactionDetails();
                                //Saving on the Modes Of Payment Details Table
                                drwModesOfPaymentDetails.SetString("ModesOfPaymentDetailsID", objAutoNumber.GenerateAutoNumber("ModesOfPaymentDetailsID", "000000").ToString());
                                drwModesOfPaymentDetails.SetString("TransactionID", strTransactionID);
                                objTransaction.TblModesOfPaymentDetails.Rows.Add(drwModesOfPaymentDetails);
                                objTransaction.UpdateModesOfPaymentTransactionDetails();
                                //Saving on the Member Subscription Details
                                drwMemberSubscription.SetString("MemberSubscriptionID", objAutoNumber.GenerateAutoNumber("MemberSubscriptionID", "000000").ToString());
                                drwMemberSubscription.SetString("ServiceAreaSubscriptionID", strServiceAreaSubscriptionID);
                                drwMemberSubscription.SetString("TransactionID", strTransactionID);
                                drwMemberSubscription.SetString("MemberID", strMemberID);
                                drwMemberSubscription.SetDateTime("DateSubscribed", dtSubcribeDate);
                                drwMemberSubscription.SetDateTime("DateExpired", dtSubcribeDate.AddDays(Convert.ToDouble(drw["ValidForNoOfDays"].ToString()) -1)); // dtsubscribe + days
                                drwMemberSubscription.SetInt32("IsUnlimited", Convert.ToInt32(drw["IsUnlimited"].ToString()));
                                drwMemberSubscription.SetInt32("RemainingRebook", Convert.ToInt32(drw["RebookingCount"].ToString()));
                                drwMemberSubscription.SetInt32("IsUsed", 0);
                                drwMemberSubscription.SetInt32("Enabled", 1);
                                drwMemberSubscription.SetDateTime("CreatedOn", DateTime.Now);
                                objMemberSubscription.TblMemberSubscription.Rows.Add(drwMemberSubscription);
                                objMemberSubscription.Update();

                                //aReceipt.HRef = "https://gate.payneteasy.com/paynet/view-receipt/35/" + Request.Form["Receipt-id"].ToString();
                            }
                            else
                            {
                                //Saving on the Transaction Table
                                if (strModesOfPayment != "20150923000003" && strStatus.ToLower() != "processing")
                                {
                                    ClsAutoNumber objAutoNumber = new ClsAutoNumber();
                                    strTransactionID = objAutoNumber.GenerateAutoNumber("TransactionID", "000000").ToString();
                                    DataRow drwTransaction = objTransaction.TblTransaction.NewRow();
                                    drwTransaction.SetString("TransactionID", strTransactionID);
                                    drwTransaction.SetString("TransactionTypeID", "20150923000002");
                                    drwTransaction.SetString("MemberID", strMemberID);
                                    drwTransaction.SetString("PaymentChannelID", "20151030000001");
                                    drwTransaction.SetString("ReferenceNumber", Request.Form["orderid"].ToString());
                                    drwTransaction.SetString("AurumPayControlCode", Request.Form["control"].ToString());
                                    drwTransaction.SetString("ModesOfPaymentID", strModesOfPayment);
                                    drwTransaction.SetDecimal("GrandTotal", Convert.ToDecimal(drw["Price"].ToString()));
                                    drwTransaction.SetString("IsPaid", "0");
                                    drwTransaction.SetString("Status", strStatus);
                                    drwTransaction.SetDateTime("CreatedOn", DateTime.Now);
                                    objTransaction.TblTransaction.Rows.Add(drwTransaction);
                                    objTransaction.UpdateTransaction();
                                }
                            }
                        }

                    }
                else
                {
                    Response.Write("<script>");
                    Response.Write("window.open('" + UHopCore.GetPageURL() + "index.aspx','_top')");
                    Response.Write("</script>");
                }

                }

            }
                catch (Exception ex)
                {
                    Response.Write("<script>");
                    Response.Write("window.open('" + UHopCore.GetPageURL() + "index.aspx','_top')");
                    Response.Write("</script>");
                    //Response.Redirect("../../index.aspx");
                }

        
            }
    }
}