﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;

namespace uhopWeb.Forms.AurumPay.OTC
{
    public partial class OTC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("<script>");
            Response.Write("window.open('" + UHopCore.GetPageURL() + "/Forms/Transactions/Subscription.aspx','_top')");
            Response.Write("</script>");
        }
    }
}