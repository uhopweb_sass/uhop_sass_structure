﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;
using System.IO;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.Data;
using System.Web.Services;
using System.Data.SqlClient;
namespace uhopWeb.Forms.AurumPay.OTC
{
    public partial class Mlhuillier : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void insertvalues()
        {
            if (Request.QueryString["MID"] != null && Request.QueryString["SID"] != null)
            {
                string strTransactionID;
                ClsAutoNumber objAutoNumber = new ClsAutoNumber();
                strTransactionID = objAutoNumber.GenerateAutoNumber("TransactionID", "000000").ToString();
                string strMemberID = Request.QueryString["MID"];
                string strSubscriptionID = Request.QueryString["SID"];

                ClsSubscription objSubscription = new ClsSubscription(strSubscriptionID);
                objSubscription.FillServiceAreaSubscription();
                decimal unitPrice = Convert.ToDecimal(objSubscription.GetSubscriptionDetails("Price"));
                decimal OnlinePaymentFee = Convert.ToDecimal("0.044");
                decimal OFee = unitPrice * OnlinePaymentFee;
                decimal TotalAmount = Convert.ToDecimal(Convert.ToDouble(OFee) + Convert.ToDouble(unitPrice));
                string numValue = TotalAmount.ToString("##0.00");
                                
                ClsTransaction objTransaction = new ClsTransaction(strMemberID);
                objTransaction.FillAurumPayMemberDetails(strMemberID);
                NameValueCollection collections = new NameValueCollection();
                collections.Add("order_desc", strSubscriptionID);
                collections.Add("first_name", objTransaction.GetMemberDetails("Firstname"));
                collections.Add("last_name", objTransaction.GetMemberDetails("Lastname"));
                collections.Add("address1", objTransaction.GetMemberDetails("Address"));
                collections.Add("city", objTransaction.GetMemberDetails("Province"));
                collections.Add("state", "XX");
                collections.Add("zip_code", objTransaction.GetMemberDetails("Zipcode"));
                collections.Add("country", "PH");
                collections.Add("phone", objTransaction.GetMemberDetails("Phone"));
                collections.Add("email", objTransaction.GetMemberDetails("Email"));
                collections.Add("client_orderid", strTransactionID);
                collections.Add("amount", numValue);
                collections.Add("merchant_name", "u-Hop");
                //collections.Add("login", "BancNetProdtest"); //login for testing
                //collections.Add("endpoint", "246"); // end point for testing
                //collections.Add("merchant_control", "7FC664C3-0E1A-4E9E-9FF7-FBB9A1304754"); //controlcode for testing
                collections.Add("login", "u-Hop");
                collections.Add("endpoint", "710");
                collections.Add("merchant_control", "D00EF8ED-74BC-4A0E-816B-A89B87538300");
                //collections.Add("server_callback_url", "http://121.97.91.74/Forms/AurumPay/OTC/SC.aspx?S=${orderid}&O=${status}&T=" + strTransactionID); //&M=${merchant_order}&O=${orderid}&I=" + strMemberID);
                collections.Add("server_callback_url", UHopCore.GetPageURL() + "Forms/AurumPay/OTC/SC.aspx?S=${orderid}&O=${status}&T=" + strTransactionID); //&M=${merchant_order}&O=${orderid}&I=" + strMemberID);
                collections.Add("merchant_redirect", UHopCore.GetPageURL() + "Forms/AurumPay/OTC/OTC.aspx");
                string html = "";
                foreach (string key in collections.Keys)
                {
                    html += string.Format("<input name='{0}' type='hidden' value='{1}'>", key, collections[key]);
                }


                string strServiceAreaSubscriptionID = objSubscription.GetSubscriptionDetails("ServiceAreaSubscriptionID");

                objTransaction.FillTransactionSubscriptionDetails(strSubscriptionID);
                DataTable tbl = objTransaction.TblTransactionSubscriptionDetails;

                foreach (DataRow drw in tbl.Rows)
                {
                        //Saving on the Transaction Table
                        DateTime dtSubcribeDate = Convert.ToDateTime(ClsEncryptor.Decrypt(Request.QueryString["R"].Replace(" ", "+"), "u-H0p"));
                        DataRow drwTransaction = objTransaction.TblTransaction.NewRow();
                        DataRow drwTransactionDetail = objTransaction.TblTransactionDetails.NewRow();
                        ClsMemberSubscription objMemberSubscription = new ClsMemberSubscription();
                        DataRow drwMemberSubscription = objMemberSubscription.TblMemberSubscription.NewRow();
                                           
                        drwTransaction.SetString("TransactionTypeID", "20150923000002");
                        drwTransaction.SetString("MemberID", strMemberID);
                        drwTransaction.SetString("ReferenceNumber", "");
                        drwTransaction.SetString("PaymentChannelID", "20151030000001"); //aurum Pay
                        drwTransaction.SetString("ModesOfPaymentID", "20150923000004"); //OTC
                        drwTransaction.SetDecimal("GrandTotal", TotalAmount);
                        drwTransaction.SetString("Status", "pending");
                        drwTransaction.SetString("IsPaid", "0");
                        drwTransaction.SetDateTime("CreatedOn", DateTime.Now);
                        //Saving on the Transaction Details Table
                        drwTransactionDetail.SetString("ServiceAreaSubscriptionID", strServiceAreaSubscriptionID );
                        drwTransactionDetail.SetInt32("Quantity", 1);
                        drwTransactionDetail.SetDecimal("Amount", TotalAmount);
                        drwTransactionDetail.SetDecimal("SubTotal", TotalAmount);
                        //Transaction Table
                        drwTransaction.SetString("TransactionID", strTransactionID);
                        objTransaction.TblTransaction.Rows.Add(drwTransaction);
                        objTransaction.UpdateTransaction();
                        //Transaction Details Table
                        drwTransactionDetail.SetString("TransactionDetailsID", objAutoNumber.GenerateAutoNumber("TransactionDetailsID", "000000").ToString());
                        drwTransactionDetail.SetString("TransactionID", strTransactionID);
                        objTransaction.TblTransactionDetails.Rows.Add(drwTransactionDetail);
                        objTransaction.UpdateTransactionDetails();
                        //Saving on the Member Subscription Details
                        drwMemberSubscription.SetString("MemberSubscriptionID", objAutoNumber.GenerateAutoNumber("MemberSubscriptionID", "000000").ToString());
                        drwMemberSubscription.SetString("ServiceAreaSubscriptionID", strServiceAreaSubscriptionID);
                        drwMemberSubscription.SetString("TransactionID", strTransactionID);
                        drwMemberSubscription.SetString("MemberID", strMemberID);
                        drwMemberSubscription.SetDateTime("DateSubscribed", dtSubcribeDate);
                        drwMemberSubscription.SetDateTime("DateExpired", dtSubcribeDate.AddDays(Convert.ToDouble(drw["ValidForNoOfDays"].ToString()) -1)); // dtsubscribe + days
                        drwMemberSubscription.SetInt32("IsUnlimited", Convert.ToInt32(drw["IsUnlimited"].ToString()));
                        drwMemberSubscription.SetInt32("RemainingRebook", Convert.ToInt32(drw["RebookingCount"].ToString()));
                        drwMemberSubscription.SetInt32("IsUsed", 0);
                        drwMemberSubscription.SetInt32("Enabled", 0);
                        drwMemberSubscription.SetDateTime("CreatedOn", DateTime.Now);
                        objMemberSubscription.TblMemberSubscription.Rows.Add(drwMemberSubscription);
                        objMemberSubscription.Update();

                }
                    Response.Write(html);
            }
        }


    }
}