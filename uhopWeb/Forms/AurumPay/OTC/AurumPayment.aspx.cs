﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using uhopWeb.Classes;

namespace uhopWeb.Forms.AurumPay.OTC
{
    public partial class AurumPayment : System.Web.UI.Page
    {
        public static string strMemberID;
        public string ProvideData()
        {
            string strMOP = Request.Form["MOP"];
            if (strMOP == "")
            {
                Response.Redirect("../../Transactions/Subscription.aspx");
            }
            return  strMOP + "?MID=" + Request.Form["MID"] + "&SID=" + Request.Form["SID"] + "&R=" + Request.Form["DateStart"];
        }

        public bool IsRefresh { get; private set; }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (Session["IsRefresh" + Page.GetType().Name] == null)
            {
                Session["IsRefresh" + Page.GetType().Name] = true;
                IsRefresh = false;
            }
            else
            {
                IsRefresh = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Form["MID"] != null || Request.Form["MID"] != "" )
                {
                    strMemberID = Request.Form["MID"];
                    ClsMembers objMembers = new ClsMembers();
                    objMembers.Fill(strMemberID);
                    var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                    if (MemberDetails.Status == "0")
                    {
                        Response.Redirect("../Maintenance/UserProfile.aspx");
                    }
                }

                if (IsRefresh)
                {
                    Response.Redirect("../../Transactions/Subscription.aspx");
                }

            }
            else
            {

            }

               
        }

    }
}