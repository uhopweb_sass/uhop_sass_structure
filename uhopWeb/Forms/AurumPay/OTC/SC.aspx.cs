﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using uhopWeb.Classes;
using System.Data;


namespace uhopWeb.Forms.AurumPay.Over_The_Counter
{
    public partial class ServerCallback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    string strMSG = "";
                    string strOrderID = Request.QueryString["S"];
                    string strStatus = Request.QueryString["O"];
                    string strTransactionID = Request.QueryString["T"];
                    string strMemberID = "";

                    //string strOrderID = "12345";//Request.QueryString["S"];
                    //string strStatus = "filtered";//Request.QueryString["O"];
                    //string strTransactionID = "20151109000001"; //Request.QueryString["T"];
                    //string strMemberID = "20151107000003";


                    if (strOrderID != "" && strStatus != "" && strTransactionID != "")
                    {
                        ClsMemberSubscription objMemberSubscription = new ClsMemberSubscription();
                        ClsTransaction objTransaction = new ClsTransaction();
                        objTransaction.FillTransaction();
                        DataRow drw = objTransaction.TblTransaction.Select("TransactionID='" + strTransactionID + "'")[0];
                        strMemberID = drw["MemberID"].ToString();
                        if (strStatus.ToLower().Trim() == "filtered")
                        {
                            strMSG = "Your payment status: <b>Filtered</b> (Over-the-counter)<br /> There was a problem processing your transaction. A transaction with identical amount and card <br />information has been submitted. Please try another transaction. If the problem persists, contact <br />your credit/debit card company for assistance or call the membership hotline (654-3390) for inquiries.";
                        }
                        else if (strStatus.ToLower().Trim() == "error")
                        {
                            strMSG = "Your payment status: <b>Error</b> (Over-the-counter)<br /> An unexpected error has prevented your transaction from being processed. Please call the membership hotline (654-3390) for inquiries.";
                        }
                        else if (strStatus.ToLower().Trim() == "declined")
                        {
                            strMSG = "Your payment status: <b>Declined</b> (Over-the-counter)<br /> Your transaction has not been completed. Your credit/debit card information has been declined <br />by your credit/debit card company. Please contact your credit/debit card company for assistance, or try another transaction.";
                        }
                        else if (strStatus.ToLower().Trim() == "approved")
                        {
                            strMSG = "Your payment status: <b>Approved</b> (Over-the-counter)<br /> Order ID: " + strOrderID + "<br /> You can now use your subscription, transaction paid! ";
                            DataRow drwMemberSubscription = objMemberSubscription.TblMemberSubscription.Select("TransactionID='" + strTransactionID + "'")[0];
                            drwMemberSubscription.BeginEdit();
                            drwMemberSubscription["Enabled"] = "1";
                            drwMemberSubscription.EndEdit();
                            objMemberSubscription.Update();
                        }
                        else
                        {
                            strMSG = "Your payment status: <b>" + strStatus.ToUpper() + "</b> (Over-the-counter)<br /> Kindly Email us or call to our hotline (654-3390)!";
                        }

                        if (strMSG != "")
                        {
                            drw.BeginEdit();
                            drw["ReferenceNumber"] = strOrderID;
                            drw["Status"] = strStatus;
                            drw["IsPaid"] = strStatus.ToLower().Trim() == "approved" ? "1" : "0";
                            drw.EndEdit();
                            objTransaction.UpdateTransaction();
                        }

                        using (ClsMembers objMembers = new ClsMembers())
                        {
                            ClsSendMail objSendMail = new ClsSendMail();
                            objMembers.FillAddress();
                            objSendMail.SendEmailNotify(objMembers.GetMemberDetailsByFieldName(strMemberID, "Email").ToString(),
                                CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(objMembers.GetMemberDetailsByFieldName(strMemberID, "Firstname").ToString()),
                                "U-Hop Purchased Subscription | " + strTransactionID, strMSG);
                        }

                        Response.StatusCode = 200;
                        Response.End();
                    }
                }
            }
            catch
            {

            }
        }
    }
}