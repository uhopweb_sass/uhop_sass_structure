﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AurumPaymentStatus.aspx.cs" Inherits="uhopWeb.Forms.AurumPay.AurumPaymentStatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Images/uhop_icon_beta.png" type="image/png" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

    <asp:PlaceHolder ID="PlaceHolder2" runat="server">
        <%: Styles.Render("~/bundle/style") %>
    </asp:PlaceHolder>
    
    <title>Payment Status</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row">
              <div class="col s12">
                <div class="card-panel white blackHover">
                   <div class="container">
                    <div class="row center">
                        <div class="col s12">
                            <img src="../../Images/u-hop_logo.png" class="responsive-img" alt="u-Hop" height="110"/></div>
                    </div><div class="divider"></div>
                    <div class="row">
                        <div class="col s12">
                            <blockquote>
                                 <h5 class="subscribeFinished">Payment Statuses : <span id="lblStatus" runat="server"></span></h5>
                       
                                <h5 class="subscribeFinished">Copy these code for your reference</h5>
	                    
                                <h5 class="subscribeFinished">Merchant Order ID : <span id="lblMerchantOrderID" runat="server"></span></h5>
                                
                                <h5 class="subscribeFinished">Transaction ID : <span id="lblTransactionID" runat="server"></span></h5>
                            </blockquote> <br />

                                <div class="center">
                                    <%--<h5><a id="aReceipt" runat="server" target="_blank">Print Receipt</a></h5>--%>
                                    <h5 class="subscribeFinished">Thank you for transacting!</h5>

	                                <h5 class="subscribeFinished">For Declined transactions, Kindly contact your issuing bank.</h5>

	                                <h5 class="subscribeFinished">For questions, please call u-Hop customer service at 02 (504-47-57)</h5>

	                                <h5 class="subscribeFinished"><i>email: technical1@aurumpay.com</i></h5>
                                </div>
                        </div>
                   </div><div class="divider"></div>
                    <br />
                   <div class="row center">
                        <!--<a href="../Transactions/Subscription.aspx" class="btn center-align waves-effect waves-light red-text black"><i class="large material-icons">replay</i> Back to Subscription</a>-->
                        <a href="../Maintenance/UserProfile.aspx"  target="_top" class="btn waves-effect waves-light blue darken-2" style="margin-bottom: 10px;">Profile &nbsp; <i class="material-icons right" style="margin:0px;">web</i></a>
                        <a href="../Transactions/Subscription.aspx"  target="_top" class="btn waves-effect waves-light pink accent-2" style="margin-bottom: 10px;">Subcribe &nbsp; <i class="material-icons right" style="margin:0px;">replay</i></a>
                   </div>
                </div>
              </div> <!-- END of Container -->
           </div>
         </div>
      </div>
     </form>
     
    <div class="row">
        <div class="col s12 center">
            <footer id="paymentFooter">
	            <div class="divider black" style="height:2px;"></div><br />
                    <span class="grey-text">Powered by</span><br> 
		                <!--Aurumpay-->
		                <a href="http://www.aurumpay.com/" target="_blank"><img src="../../Images/aurumpay1.png" id="aurum"></a>
		                <!--Aurumpay-->
                    <p class="grey-text center">The GOLD STANDARD in online payment services 2012</p>
           </footer>
        </div>
    </div>

    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/script") %>
    </asp:PlaceHolder>
</body>
</html>
