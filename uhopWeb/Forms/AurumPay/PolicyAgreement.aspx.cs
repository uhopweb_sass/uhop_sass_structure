﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;
using System.IO;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.Data;

namespace uhopWeb.Forms.AurumPay
{
    public partial class PolicyAgreement : System.Web.UI.Page
    {
        public static string strMemberID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clsAuthenticator.Authenticate();
                UHopCore.Title = "Policy Agreement";
                strMemberID = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                ViewState["MemberID"] = strMemberID;

                    ClsMembers objMembers = new ClsMembers(strMemberID);
                    if (ViewState["TblMembers"] == null)
                    {
                        objMembers.Fill(strMemberID);
                        ViewState["TblMembers"] = objMembers.TblMembers;
                    }


                    var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                    if (MemberDetails.Status == "0")
                    {
                        Response.Redirect("../Maintenance/UserProfile.aspx");
                    }
                    
                    string c = Request.QueryString["a"];
                    string postUrl = c == "mastercardCC" ? "CC/AurumPayment.aspx"
                          : c == "debitcard" ? "DC/AurumPayment.aspx"
                          : c == "otcounter" ? "OTC/AurumPayment.aspx"
                          : c == "mastercardDC" ? "DC/AurumPaymentMC.aspx"
                          : c == "otcounterBC" ? "OTC/AurumPayment.aspx" : "";
                    hdfQueryString.Value = postUrl;

                    ClsTransaction objTransaction = new ClsTransaction(strMemberID);
                    objTransaction.FillAurumPayMemberDetails(strMemberID);
                    if (objTransaction.TblAurumPayMemberDetails.Count > 0 && !string.IsNullOrEmpty(objTransaction.GetMemberDetails("Phone")))
                    {
                        hdfFirstname.Value = objTransaction.GetMemberDetails("Firstname");
                        hdfLastname.Value = objTransaction.GetMemberDetails("Lastname");
                        hdfAddress.Value = objTransaction.GetMemberDetails("Address");
                        hdfCity.Value = objTransaction.GetMemberDetails("Province");
                        hdfState.Value = ""; //PH
                        hdfCountry.Value = objTransaction.GetMemberDetails("Country");
                        hdfZipcode.Value = objTransaction.GetMemberDetails("Zipcode");
                        hdfPhone.Value = objTransaction.GetMemberDetails("Phone");
                        hdfEmail.Value = objTransaction.GetMemberDetails("Email");

                        string MOP = c == "otcounter" ? "Mlhuillier.aspx"
                               : c == "otcounterBC" ? "BayadCenter.aspx" : "";

                        hdfOTCType.Value = MOP;

                        string strID = ClsEncryptor.Decrypt(Request.QueryString["WZO234"].ToString().Replace(" ", "+"), "u-h0p");
                        ClsSubscription objSubscription = new ClsSubscription(strID);
                        objSubscription.FillServiceAreaSubscription();
                        decimal unitPrice = Convert.ToDecimal(objSubscription.GetSubscriptionDetails("Price"));//.ToString("#,##0.00");

                        var OF = c == "mastercardCC" ? ".048"
                               : c == "debitcard" ? ".044"
                               : c == "otcounter" ? ".044"
                               : c == "mastercardDC" ? ".044"
                               : c == "otcounterBC" ? ".044" : "0";
                        decimal OnlinePaymentFee = Convert.ToDecimal(OF);

                        decimal OFee = unitPrice * OnlinePaymentFee;
                        decimal TotalAmount = Convert.ToDecimal(Convert.ToDouble(OFee) + Convert.ToDouble(unitPrice));
                        lblOFee.Text = OFee.ToString("#,##0.00") + " Php";
                        lblTotal.Text = TotalAmount.ToString("#,##0.00") + " Php";

                        lblID.Text = objSubscription.GetSubscriptionDetails("ServiceAreaSubscriptionID");
                        lblIsRecommended.Text = objSubscription.GetSubscriptionDetails("IsRecommended");
                        lblSubscriptionName.Text = objSubscription.GetSubscriptionDetails("SubscriptionName");
                        lblIsUnlimited.Text = objSubscription.GetSubscriptionDetails("IsUnlimited");
                        lblDays.Text = objSubscription.GetSubscriptionDetails("ValidForNumberOfDays");
                        lblPrice.Text = unitPrice + " Php";
                        lblShortDesc.Text = objSubscription.GetSubscriptionDetails("ShortDescription");



                        hdfOrderDesc.Value = lblSubscriptionName.Text;
                        hdfClientOrderID.Value = lblID.Text;
                    }
                    else {
                        Response.Redirect("../Maintenance/UserProfile.aspx");
                    }
            }
        }

        public void ProvideSubscription()
        {
            string strID = Request.QueryString["WZO234"];
            Response.Write(strID);
        }

        protected void btnSubmit1_Click(object sender, EventArgs e)
        {
            if (txtSubscribeDate.Text.Trim() != "")
            {
                spnRequired.Attributes.Add("style", "display:none;margin-bottom:10px");
                using (ClsMemberSubscription objMemberSubscriptionSubscription = new ClsMemberSubscription())
                {
                    //ViewState["MemberID"] = Session["MemberID"].ToString();
                    objMemberSubscriptionSubscription.FillMemberSubscriptionValidation(strMemberID, lblID.Text, txtSubscribeDate.Text);
                    DataTable tbl = objMemberSubscriptionSubscription.TblMemberSubscriptionValidation;


                    if (tbl.Rows.Count <= 0)
                    {
                        spnSubscribe.Attributes.Add("style", "display:none");
                        btnCancel.Attributes.Add("style", "display:none");
                        btnProceed.Attributes.Add("style", "display:none");
                        PurchaseSubscription();
                    }
                    else
                    {
                        foreach (DataRow drw in tbl.Rows)
                        {
                            spnSubscribe.Attributes.Add("style", "display:block;margin-bottom:10px");
                            btnCancel.Attributes.Add("style", "display:block");
                            btnProceed.Attributes.Add("style", "display:block");
                            spnSubscribe.InnerText = "Currently you have existing subscription with the same Service Area and will be expire on : " + Convert.ToDateTime(drw["DateExpired"]).ToLongDateString();
                            spnSubscribe.Focus();
                        }
                    }

                }
            }
            else {

                spnRequired.Attributes.Add("style", "display:block;margin-bottom:10px");
                spnRequired.InnerText = "Subscription effectivity date is required!";
                spnRequired.Focus();
            }
        }


        public void PurchaseSubscription()
        {

            NameValueCollection collections = new NameValueCollection();
            collections.Add("order_desc", hdfOrderDesc.Value);
            collections.Add("first_name", hdfFirstname.Value);
            collections.Add("last_name", hdfLastname.Value);
            collections.Add("address1", hdfAddress.Value);
            collections.Add("city", hdfCity.Value);
            collections.Add("state", "XX");
            collections.Add("zip_code", hdfZipcode.Value);
            collections.Add("country", "PH");
            collections.Add("phone", hdfPhone.Value);
            collections.Add("email", hdfEmail.Value);
            collections.Add("SID", lblID.Text);
            collections.Add("client_orderid", hdfClientOrderID.Value);
            collections.Add("DateStart", ClsEncryptor.Encrypt(txtSubscribeDate.Text, "u-H0p"));
            collections.Add("MID", strMemberID);
            string c = Request.QueryString["a"];
            string postUrl = c == "mastercardCC" ? "CC/AurumPayment.aspx"
                                      : c == "debitcard" ? "DC/AurumPayment.aspx"
                                      : c == "otcounter" ? "OTC/AurumPayment.aspx"
                                      : c == "mastercardDC" ? "DC/AurumPaymentMC.aspx"
                                      : c == "otcounterBC" ? "OTC/AurumPayment.aspx" : "";

            string TransactionType = c == "mastercardCC" ? "20150923000002"
                  : c == "debitcard" ? "20150923000003"
                  : c == "otcounter" ? "20150923000004"
                  : c == "mastercardDC" ? "20150923000003" : "";

            collections.Add("MOP", hdfOTCType.Value);
            collections.Add("transaction_type", TransactionType);
            string html = "<html><head>";
            html += "</head><body onload='document.forms[0].submit()'>";
            html += string.Format("<form name='PostForm' method='POST' action='{0}'>",postUrl);
            foreach (string key in collections.Keys)
            {
                html += string.Format("<input name='{0}' type='hidden' value='{1}'>", key, collections[key]);
            }
            html += "</form></body></html>";
            Response.Clear();
            Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1");
            Response.HeaderEncoding = Encoding.GetEncoding("ISO-8859-1");
            Response.Charset = "ISO-8859-1";
            Response.Write(html);
            Response.End();
        }

        protected void btnProceed_Click(object sender, EventArgs e)
        {
            PurchaseSubscription();
        }

    }
}