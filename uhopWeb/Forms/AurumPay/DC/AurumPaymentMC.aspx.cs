﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using uhopWeb.Classes;

namespace uhopWeb.Forms.AurumPay.DebitCard
{
    public partial class AurumPaymentMC : System.Web.UI.Page
    {
        public static string strMemberID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Form["MID"] != null)
                {
                    strMemberID = Request.Form["MID"];
                    ClsMembers objMembers = new ClsMembers();
                    
                    if (ViewState["TblMembers"] == null)
                    {
                        objMembers.Fill(strMemberID);
                        ViewState["TblMembers"] = objMembers.TblMembers;
                    }
                    objMembers.TblMembers = (DataSets.DSConfigs.MemberDataTable)ViewState["TblMembers"];
                    var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                    if (MemberDetails.Status == "0")
                    {
                        Response.Redirect("../Maintenance/UserProfile.aspx");
                    }
                }
            }
        }

        public string url()
        {
            string z = Request.Form["MID"];
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            string ipaddress = "";
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipaddress = ip.ToString();
                }
            }
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                ClsSubscription objSubscription = new ClsSubscription(Request.Form["SID"]);
                objSubscription.FillServiceAreaSubscription();
                string numValue = objSubscription.GetSubscriptionDetails("Price");
                var amountInCents = numValue.ConvertToDecimal() * 100;  //Convert.ToDecimal(numValue) > decValue ? Convert.ToInt32(numValue.ConvertToDecimal()) : Convert.ToInt32(numValue.ConvertToDecimal()) * 100;
                values["order_desc"] = Request.Form["order_desc"];
                values["first_name"] = Request.Form["first_name"];
                values["last_name"] = Request.Form["last_name"];
                values["address1"] = Request.Form["address1"];
                values["city"] = Request.Form["city"];
                values["state"] = Request.Form["state"];
                values["zip_code"] = Request.Form["zip_code"];
                values["country"] = Request.Form["country"];
                values["phone"] = Request.Form["phone"];
                values["email"] = Request.Form["email"];
                values["amount"] = Convert.ToDecimal(numValue).ToString("###0.00");
                //values["server_callback_url"] = "http://121.97.91.74/Forms/AurumPay/Creditcard/ServerCallback.aspx?status=${status}&merchant_order=${merchant_order}&orderid=${orderid}&type=${type}&amount=${amount}&descriptor=${descriptor}&error_message=${error_message}&name=${name}&email=${email}&last-four-digits=${last-four-digits}&bin=${bin}&card-type=${card-type}&gate-partial-reversal=${gate-partial-reversal}&gate-partial-capture=${gate-partial-capture}&reason-code=${reason-code}";

                //values["server_callback_url"] = "http://121.97.91.74/Forms/AurumPay/DC/SCMC.aspx?S=${status}&M=${merchant_order}&O=${orderid}&I=" + z;
                values["server_callback_url"] = UHopCore.GetPageURL() + "Forms/AurumPay/DC/SCMC.aspx?S=${status}&M=${merchant_order}&O=${orderid}&I=" + z;
                values["control"] = Request.Form["control"];



                values["client_orderid"] = Request.Form["client_orderid"];
                values["ipaddress"] = ipaddress;

                values["currency"] = "PHP";
                values["endPointId"] = "35";
                values["merchantControl"] = "8EDB07F1-E00C-4F23-B8DB-3CD7242C423A";
                values["redirect_url"] = UHopCore.GetPageURL() + "Forms/AurumPay/AurumPaymentStatus.aspx?R=" + ClsEncryptor.Decrypt(Request.Form["DateStart"].Replace(" ", "+"), "u-H0p") + "&X=" + Request.Form["transaction_type"] + "&T=" + z;
                                
                string decrypted = FormsAuthentication.HashPasswordForStoringInConfigFile("35" + Request.Form["client_orderid"] + amountInCents.ToString("#") + Request.Form["email"] + "8EDB07F1-E00C-4F23-B8DB-3CD7242C423A", "SHA1");

                values["control"] = decrypted;
            https://gate.aurumpay.com/paynet/api/v2/sale-form/142 
                var response = client.UploadValues("https://sandbox.aurumpay.com/paynet/api/v2/sale-form/35", values);

                var responseString = Encoding.Default.GetString(response);
                var c = responseString.Split('&');
                foreach (string i in c)
                {
                    if (i.Contains("redirect-url"))
                    {
                        var t = i.Split('=');
                        responseString = t[1];
                    }
                }


                return Server.UrlDecode(responseString);

            }
        }

    }
}