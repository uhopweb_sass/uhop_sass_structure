﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;
using System.Data;
using System.Globalization;

namespace uhopWeb.Forms.AurumPay.DebitCard
{
    public partial class ServerCallback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    string strMSG = "";
                    string strStatus = Request.QueryString["S"];
                    string strMerchantOrderID = Request.QueryString["M"];
                    string strOrderID = Request.QueryString["O"];
                    string strMemberID = Request.QueryString["I"];
                    ClsSendMail objSendMail=new ClsSendMail();
                    if (strStatus != "" && strMerchantOrderID != "" & strOrderID != "" && strMemberID != "")
                    {
                        if (strStatus.ToLower().Trim() == "filtered")
                        {
                            strMSG = "Your payment status: <b>Filtered</b> (Debit Card)<br /> There was a problem processing your transaction. A transaction with identical amount and card <br />information has been submitted. Please try another transaction. If the problem persists, contact <br />your credit/debit card company for assistance or call the membership hotline (654-3390) for inquiries.";
                        }
                        else if (strStatus.ToLower().Trim() == "error")
                        {
                            strMSG = "Your payment status: <b>Error</b> (Debit Card)<br /> An unexpected error has prevented your transaction from being processed. Please call the membership hotline (654-3390) for inquiries.";
                        }
                        else if (strStatus.ToLower().Trim() == "declined")
                        {
                            strMSG = "Your payment status: <b>Declined</b> (Debit Card)<br /> Your transaction has not been completed. Your credit/debit card information has been declined <br />by your credit/debit card company. Please contact your credit/debit card company for assistance, or try another transaction.";
                        }
                        else if (strStatus.ToLower().Trim() == "approved")
                        {
                            using (ClsMembers objMembers = new ClsMembers())
                            {
                                objMembers.Fill(strMemberID);
                                objSendMail.SendMailAurumPayment(CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(objMembers.GetMemberDetailsByFieldName(strMemberID, "Email").ToString()), CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(objMembers.GetMemberDetailsByFieldName(strMemberID, "Firstname").ToString()), "Credit Card", CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(strStatus), strOrderID, strMerchantOrderID);
                                Response.StatusCode = 200;
                                Response.End();
                            }
                            return;
                        }
                        else
                        {
                            strMSG = "Your payment status: <b>" + strStatus.ToUpper() + "</b> (Over-the-counter)<br /> Kindly Email us or call to our hotline (654-3390)!";
                        }

                        using (ClsMembers objMembers = new ClsMembers())
                        {
                            objMembers.Fill(strMemberID);
                            objSendMail.SendEmailNotify(objMembers.GetMemberDetailsByFieldName(strMemberID, "Email").ToString(),
                                CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(objMembers.GetMemberDetailsByFieldName(strMemberID, "Firstname").ToString()),
                                "U-Hop Purchased Subscription | " + strOrderID, strMSG);
                        }

                        Response.StatusCode = 200;
                        Response.End();
                    }
                }
            }
            catch 
            {

            }
        }
    }
}