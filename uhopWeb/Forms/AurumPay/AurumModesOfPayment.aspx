﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Site1.Master" AutoEventWireup="true" CodeBehind="AurumModesOfPayment.aspx.cs" Inherits="uhopWeb.Forms.AurumPay.AurumModesOfPayment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
        body {
            background-color: white !important;
        }
    </style>

    <div class="container">

        <div class="row">
     	    <div class="col s12 l8">
                <p class="subscriptionQuote">Your current subscription details:<b><span id="spnSubscription" runat="server"></span></b></p>
            </div>
            <!--<div class="col s12 l4">
                <a href="../Transactions/Subscription.aspx" class="btn-flat right waves-effect waves-light red-text"><i class="large material-icons">&#xE042;</i> Back to Subscription</a>
            </div> -->
            <div class="col s12">
                <div class="divider"></div>
            </div>
        </div> 

        <div class="row">
            <div class="col s12">
	        <!--<h2>Welcome to u-Hop Subscription Payment Page</h2><br><br>-->
     	        <div class="col s12">
                    <h3 class="paymentOptHeader">Please choose payment option:</h3>
                    <h5 style="color:grey;" class="subscriptionQuote">(Click the icon on your preferred mode of payment to continue)</h5>
                </div>
            </div> 
                <div class="divider black" style="height:2px;"></div>
       </div>
        
			
		<table class="centered bordered responsive-table">
		    <tr>
		        <td class="subscriptionTHeader"><b>CREDIT CARD</b></td>
		        <td class="subscriptionTHeader"><b>DEBIT CARD</b></td>
		        <td class="subscriptionTHeader"><b>OVER-THE-COUNTER</b></td>
		    </tr>
		
		    <tr>
		        <td><a href='PolicyAgreement.aspx?a=mastercardCC&WZO234=<% ProvideSubscription(); %>'>
                    <img src="../../Images/VisaMaster.jpg" id="visamaster"/></a></td>
		        <td><a href='PolicyAgreement.aspx?a=debitcard&WZO234=<% ProvideSubscription(); %>'>
                    <img src="../../Images/bancnet.png" id="bancnet"/></a></td>
		        <td><a href='PolicyAgreement.aspx?a=otcounter&WZO234=<% ProvideSubscription(); %>'>
                    <img src="../../Images/mlhuillier.png" id="mlhuiller"/></a></td>
		    </tr>
		    

		    <tr>
		        <td>&nbsp;</td>
		        <td><a href='PolicyAgreement.aspx?a=mastercardDC&WZO234=<% ProvideSubscription(); %>' >
                    <img src="../../Images/mastervisadebit.png" id="visamasterdebit"/></a></td>
                <td><a href='PolicyAgreement.aspx?a=otcounterBC&WZO234=<% ProvideSubscription(); %>'>
                    <img src="../../Images/bayad-center.png" id="bayadcenter"/></a></td>
                 <%--<td><a href='personal_information.aspx?a=' > <center><Font color="#000000"> Gcash</a>
		        <a href='smartmoney/forsmartmoney.php' > <center><Font color="#000000">Smart Money </a></td>--%>
		    </tr>
		</table>
        <br /><br />
		<!--<a href="test/forcreditcard.php">test recurrent </a>!-->
           <div class="row">
               <div class="col s12 center">
                  <footer id="paymentFooter">
	                    <div class="divider black" style="height:2px;"></div><br />
                        <span class="grey-text">Powered by</span><br> 
		                <!--Aurumpay-->
		                <a href="http://www.aurumpay.com/" target="_blank"><img src="../../Images/index/Aurumpay-min.jpg" id="aurum"></a>
		                <!--Aurumpay-->
                        <p class="grey-text center" style="font-size: 12px;">The GOLD STANDARD in online payment services 2012</p>
                  </footer>
              </div>
           </div>
    </div> <!-- END of container -->
</asp:Content>
