﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Operators/Operators.Master" AutoEventWireup="true" CodeBehind="DriversMaintenance.aspx.cs" Inherits="uhopWeb.Forms.Operators.Maintenance.DriverMaintenance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div id="divRemoveVehicle" class="center white-text" style="background-color: rgb(2, 2, 99);display:none;padding:10px;">
            <span class="Red-text">Are you sure to want to disable [<label class="lblVehicleNameEdit cyan-text" style="font-size: 15px;"></label> - <label id="Label1" class="lblModelName cyan-text" style="font-size: 15px;"></label>]?</span><br />
            <asp:Button ID="btnRemove" runat="server" Text="YES" CssClass="btn green darken-2" Style="height: 35px; width: 90px;" OnClick="btnRemove_Click" />
            <a href="#" class="removeVehicleCancel  btn pink accent-2 white-text" style="height: 35px; width: 90px;">NO</a>
        </div>
    <div class="divAddDriver divCover" id="divAddDriver" runat="server" style="display: none; margin-top: -20px; background-color: rgba(2, 2, 99, 0.90); opacity: 1;">
        <div class="container white-text">
                <div class="row">
                    <div class="col s12">
                        <h4><i class="material-icons left">&#xE145;</i>Add New Driver</h4>

                    </div>
                     <div class="col s12">
                        <div class="divider"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 l4 m4">
                        <div class="input-field col s12 l12 m12">                            
                            <label for="txtFirstName">First Name </label>
                            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>                            
                            <asp:RequiredFieldValidator ID="rq1" style="color:red" runat="server"  ErrorMessage="First Name is required!" SetFocusOnError="True" ValidationGroup="addDriver" ControlToValidate="txtFirstName"></asp:RequiredFieldValidator>
                        </div>
                    </div> 
                    <asp:HiddenField ID="hfDriverID" runat="server" />
                    <div class="col s12 l4 m4">
                        <div class="input-field col s12 l12 m12">
                            <label for="txtLastName">Last Name</label>
                            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rq2" style="color:red" runat="server"  ErrorMessage="Last Name is required!" SetFocusOnError="True" ValidationGroup="addDriver" ControlToValidate="txtLastName"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col s12 l4 m4">
                        <div class="input-field col s12 l12 m12">
                            <label for="txtMiddleName">Middle Name</label>
                            <asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox>                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 l4 m4">
                        <div class="input-field col s12 l12 m12">
                            <label for="txtEmail">Email Address</label>
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                            <span id="invalidEmail" style="color:red">Email address already exist!</span>                            
                            <asp:RequiredFieldValidator ID="rq3" style="color:red" runat="server"  ErrorMessage="E-mail is required!" SetFocusOnError="True" ValidationGroup="addDriver" ControlToValidate="txtEmail"></asp:RequiredFieldValidator><br />
                            <asp:RegularExpressionValidator ID="regEx2" style="color:red" runat="server" ErrorMessage="E-mail address is in invalid format" ValidationExpression="^[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+(\.[\w!#$%&amp;&#039;*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$" ControlToValidate="txtEmail" ValidationGroup="addDriver"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col s12 l4 m4">
                        <div class="input-field col s12 l12 m12">
                            <label for="txtNewPassword">New Password</label>
                            <asp:TextBox ID="txtNewPassword" type="password" runat="server"></asp:TextBox>                            
                            <asp:RequiredFieldValidator ID="rq4" style="color:red" runat="server"  ErrorMessage="Password is required!" SetFocusOnError="True" ValidationGroup="addDriver" ControlToValidate="txtNewPassword"></asp:RequiredFieldValidator><br />
                            <asp:RegularExpressionValidator ID="regEx3" runat="server" ErrorMessage="Password character must be greater than four(4)" style="color:red" ValidationExpression="^[^\s]{4,}$" ControlToValidate="txtNewPassword" ValidationGroup="addDriver"></asp:RegularExpressionValidator>                            
                        </div>
                    </div>
                    <div class="col s12 l4 m4">
                        <div class="input-field col s12 l12 m12">
                            <label for="txtConfirmPassword">Confirm Password</label>
                            <asp:TextBox ID="txtConfirmPassword" type="password" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rq5" style="color:red" runat="server"  ErrorMessage="Confirm Password is requried!" SetFocusOnError="True" ValidationGroup="addDriver" ControlToValidate="txtConfirmPassword"></asp:RequiredFieldValidator><br />
                            <asp:CompareValidator ID="cv1" style="color:red" runat="server" ErrorMessage="Password did not match!" ControlToValidate="txtConfirmPassword" ControlToCompare="txtNewPassword" ValidationGroup="addDriver"></asp:CompareValidator>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 l4 m4">
                        <div class="input-field col s12 l12 m12">
                            <label for="mobileNumber">Mobile Number</label>
                            <asp:TextBox ID="txtMobileNumber" runat="server" MaxLength="11"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rq6" style="color:red" runat="server"  ErrorMessage="Mobile Number is required" SetFocusOnError="True" ValidationGroup="addDriver" ControlToValidate="txtMobileNumber"></asp:RequiredFieldValidator><br />                            
                            <asp:RegularExpressionValidator ID="regEx1" runat="server" ErrorMessage="Contact Number must contain only numeric values!" ValidationExpression="\d+$" ValidationGroup="addDriver" ControlToValidate="txtMobileNumber"></asp:RegularExpressionValidator>                            
                        </div>
                    </div>
                    <div class="col s12 l4 m4">                        
                        <div class="input-field col s12 l12 m12">
                            <label for="txtBirthDay">Birthday</label>
                            <asp:TextBox ID="txtBirthDay" CssClass="datepicker txtBirthday" type="date" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rq7" style="color:red" runat="server"  ErrorMessage="Birthday is required!" SetFocusOnError="True" ValidationGroup="addDriver" ControlToValidate="txtBirthDay"></asp:RequiredFieldValidator><br />                            
                        </div>
                    </div>
                    <div class="col s12 l4 m4" style="margin-top:15px;">
                        <!-- Male Radio Button -->
                        <div class="col s3 offset-s1">
                            <input name="group1" type="radio" id="optMale" value="Male" />
                            <label for="optMale">Male</label>
                        </div>
                        <!-- END of Male Radio Button -->

                        <!-- Female Radio Button -->
                        <div class="col s3 offset-s3 left">
                            <input name="group1" type="radio" id="optFemale" value="Female" />
                            <label for="optFemale">Female</label>
                        </div><br /><br />
                        <%--<asp:RequiredFieldValidator ID="rq8" style="color:red" runat="server"  ErrorMessage="Gender is required!" SetFocusOnError="True" ValidationGroup="addDriver" ControlToValidate="txtGender"></asp:RequiredFieldValidator>--%><br />                            
                        <!-- END of Female Radio Button -->
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 white-text center">
                        <h5>UPLOAD THE DOCUMENTS OF YOUR DRIVER</h5>
                        <br />
                    </div>
                    <div class="col s12 center-align">
                        <h6><span class="red-text redNote">NOTE: Maximum file size for each file is <b>2 MB</b>.</h6>
                    </div>
                </div>
                 <div class="row">
                    <div class="col s12 l4">
                        <h5>Driver's Picture</h5>
                        <div class="divider"></div>
                        <br />
                        <div class="input-field col s12">
                            <div class="col s12 center">
                                <img id="prevUploadImageProfile"  width="200" height="250" class="prevUploadImageProfile responsive-img" src="<%=uhopWeb.Classes.UHopCore.GetPageURL() %>UploadedImages/Attachment/Default.gif" alt="your image" />
                            </div>
                            <div class="col s12">
                                <asp:RequiredFieldValidator style="color:red" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Driver Profile picture is requried!" ControlToValidate="profilePictureUpload" ValidationGroup="addDriver"></asp:RequiredFieldValidator>
                                <asp:FileUpload ID="profilePictureUpload" runat="server" class="white-text"/><br />                            
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$" ControlToValidate="profilePictureUpload" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                    Display="Dynamic" ValidationGroup="addDriver" />
                            </div>
                        </div>
                    </div>
                    <div class="col s12 l4">
                        <h5>Driver's License</h5>
                        <div class="divider"></div>
                        <br />
                        <div class="input-field col s12 center">
                            <div class="col s12 center">
                                <img id="prevUploadImageDriversLicense" width="200" height="250" class="prevUploadImageProfile responsive-img" src="<%=uhopWeb.Classes.UHopCore.GetPageURL() %>UploadedImages/Attachment/Default.gif" alt="your image" />
                            </div>
                            
                            <div class="col l12 m6 offset-m3 s12 center">
                                <asp:RequiredFieldValidator style="color:red" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Driver License image is requried!" ControlToValidate="driversLicenseUpload" ValidationGroup="addDriver"></asp:RequiredFieldValidator>
                                <asp:FileUpload ID="driversLicenseUpload" runat="server" class="white-text"/><br /> 
                                <asp:TextBox ID="driversLicenseUploadExpirationDate" type="date" CssClass="expirationDatepicker driversLicenseUploadExpirationDate" placeholder="Driver License Expiration Date" runat="server"></asp:TextBox>                           
                                <asp:RequiredFieldValidator style="color:red" ID="RequiredFieldValidator4" runat="server" ErrorMessage="Driver License Expiration Date is required!" ControlToValidate="driversLicenseUploadExpirationDate" ValidationGroup="addDriver"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$" ControlToValidate="driversLicenseUpload" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                    Display="Dynamic" ValidationGroup="addDriver" />
                            </div>
                        </div>
                    </div>
                    <div class="col s12 l4">
                        <h5>NBI Clearance</h5>
                        <div class="divider"></div>
                        <br />
                        <div class="input-field col s12 center">
                            <div class="col s12 center">
                                <img id="prevUploadImageNBIClearance" width="200" height="250" class="prevUploadImageProfile responsive-img" src="../../../UploadedImages/Attachment/Default.gif" alt="your image" />
                            </div>

                            <div class="col l12 m6 offset-m3 s12 center">
                                <asp:RequiredFieldValidator style="color:red" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Driver Profile picture is requried!" ControlToValidate="nbiClearanceUpload" ValidationGroup="addDriver"></asp:RequiredFieldValidator>
                                <asp:FileUpload ID="nbiClearanceUpload" runat="server" class="white-text" /><br />
                                            
                                 <asp:TextBox ID="nbiClearanceUploadExpirationDate" type="date" CssClass="expirationDatepicker nbiClearanceUploadExpirationDate" placeholder="NBI Expiration Date" runat="server"></asp:TextBox>          
                                <asp:RequiredFieldValidator style="color:red" ID="RequiredFieldValidator5" runat="server" ErrorMessage="NBI Expiration Date is required!" ControlToValidate="nbiClearanceUploadExpirationDate" ValidationGroup="addDriver"></asp:RequiredFieldValidator>               
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$" ControlToValidate="nbiClearanceUpload" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                    Display="Dynamic" ValidationGroup="addDriver" />
                            </div>
                        </div>
                    </div>
                     <div class="col s12 center">
                         <br />
                        <asp:LinkButton ID="addDriver" runat="server" CssClass="waves-effect waves-light btn blue darken-2" ValidationGroup="addDriver" OnClick="addDriver_Click">Add Driver</asp:LinkButton>
                         <a class="btn red darken-2 btnCancelDriver waves-effect waves-light">Cancel</a>
                    </div>
                </div>
        </div> <!-- END of container -->
    </div> <!-- END of divAddDriver -->

    <div class="container" style="color: white;padding:0px;">
        <br />
        <div class="row">
            <div class="col m7 s12">
                <h4 class="operatorMasterHeader noMargin"><i class="material-icons">&#xE55A;</i>&nbsp;&nbsp;&nbsp;Drivers Dashboard</h4>
            </div>
            <div class="col m5 s12 right-align">
                <a href="#" class="btn-large waves-effect waves-light pink accent-2 btnAddDriver hide-on-med-and-down" style="margin-top:-10px;"><i class="material-icons left">&#xE145;</i>Add Driver</a>
                <a href="#" class="btn waves-effect waves-light pink accent-2 btnAddDriver hide-on-large-only"><i class="material-icons left">&#xE145;</i>Add Driver</a>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <div class="divider"></div>
                <br />
            </div>
            <div class="col s12 right-align">
                <div class="col m4 s11">

                    <asp:HiddenField ID="txtGender" runat="server"></asp:HiddenField>
                    <%--<input id="searchBox" placeholder="Search..." type="text" class="validate" />--%>
                </div>
                <div class="col s1" style="padding-left: 0px;">
                    <%--<i class="small material-icons left" style="margin-top: 10px;">search</i>--%>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <table class="responsive-table bordered">
                    <thead>
                        <tr class="trHeader">
                            <th>Driver Name</th>
                            <th>Date Hired</th>
                            <th>Status</th>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <%Response.Write(listOperatorDriver());%>
                    </tbody>
                </table>
                <br />
            </div>
        </div>
        <!-- END of row -->
    </div> <!-- END of MAIN container -->

    <div class="modal" id="addNewDriverModal" style="height:80%">
        <div class="modal-content">
            
        </div>
        <div class="modal-footer">
              <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
        </div>
    </div>

    <script>        
        $(document).on("ready", function ()
        {
            //$("#divRemoveVehicle").hide();
            $("#invalidEmail").hide();

            $("#<%=txtEmail.ClientID%>").on("change", function ()
            {

                PageMethods.validateEmail($("#<%=txtEmail.ClientID%>").val(), function (result)
                {                    
                    if (result == "1") {
                        $("#invalidEmail").show();
                    } else
                    {
                        $("#invalidEmail").hide();
                    }
                });
            });       

            $('.btnCancelDriver').click(function () {
                $('.divAddDriver').fadeOut("fast");;
            });

            $('.btnAddDriver').click(function () {
                $('.divAddDriver').hide().fadeIn("fast").css({ "display": "block" });
            });

            $("label").on("click", function ()
            {
                var c = $(this).attr("for");
                $("#ContentPlaceHolder1_" + c).focus();
            });

            $('input#input_text, textarea#textarea1').characterCounter();

            $("input[type=radio]").on("click", function ()
            {
                $("#<%=txtGender.ClientID%>").val($(this).val());                
            });
            
            $("label").each(function ()
            {
                if ($(this).text() != "Male")
                {
                    if($(this).text() != "Female")
                    {
                        if ($(this).text() != "Middle Name")
                        {
                            $(this).append(' <span style="color:red;font-size:18px">*</span>');
                        }                        
                    }                    
                }
            });

            $(".remDriver").on("click", function ()
            {
                $("#<%=hfDriverID.ClientID%>").val($(this).attr("id"));                 
                $(".lblVehicleNameEdit").text($(this).attr("dName"));
                $("#divRemoveVehicle").slideDown();
            });

            $(".removeVehicleCancel").on("click", function ()
            {
                $("#divRemoveVehicle").slideUp();
            });

            $("#<%=profilePictureUpload.ClientID%>, #<%=driversLicenseUpload.ClientID%>, #<%=nbiClearanceUpload.ClientID%>").on("change", function ()
            {
                readURL(this, $(this).attr("id"));
            });

            function readURL(input, control) {
                if (input.files && input.files[0]) {
                    var contentPH = "ContentPlaceHolder1_"
                    var reader = new FileReader();
                    var newControl = "";
                    switch(control)
                    {
                        case contentPH + "profilePictureUpload":
                            newControl = "prevUploadImageProfile";
                            break;
                        case contentPH + "driversLicenseUpload":
                            newControl = "prevUploadImageDriversLicense";
                            break;
                        case contentPH + "nbiClearanceUpload":
                            newControl = "prevUploadImageNBIClearance";
                            break;
                    }

                    reader.onload = function (e) {
                        $("#" + newControl).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }


            $("#<%=txtMobileNumber.ClientID%>").on("keydown", function (e)
            {
                if ($.inArray(e.keyCode, [8, 9, 27, 13]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || e.keyCode == 46) {
                    e.preventDefault();
                }
            });
        });        


    </script>
</asp:Content>
