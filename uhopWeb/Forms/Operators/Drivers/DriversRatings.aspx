﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Operators/Operators.Master" AutoEventWireup="true" CodeBehind="DriversRatings.aspx.cs" Inherits="uhopWeb.Forms.Operators.Drvers.DriversRatings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col s12">
            <!-- Changes1 -->
            <br /><br />           
            <asp:GridView ID="GVRatings" runat="server" AutoGenerateColumns="False" CssClass="bordered responsive-table"> 
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2" runat="server" Text="Driver's Details"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDriverDetails" runat="server" Text='<%# "Driver Name: "+Eval("FirstName").ToString()+" "+Eval("MidleName").ToString()+" "+Eval("LastName").ToString() %>'></asp:Label>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label4" runat="server" Text="Rated By"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRatedBy" runat="server" Text='<%#Eval("MemberFirstName").ToString()+" "+Eval("MemberMiddleName").ToString()+" "+Eval("MemberLastName").ToString() %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label6" runat="server" Text="Rating"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRating" runat="server" Text='<%# Eval("Rating").ToString() %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label8" runat="server" Text="Remarks"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("xRemarks").ToString() %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>




</asp:Content>
