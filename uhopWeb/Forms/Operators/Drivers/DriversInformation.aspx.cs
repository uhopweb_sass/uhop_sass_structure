﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;
using uhopWeb.DataSets;
using System.IO;

namespace uhopWeb.Forms.Operators.Drivers
{
    public partial class DriversInformation : System.Web.UI.Page
    {
        public static string strMemberID;

        private static readonly DSConfigs _dsContext = new DSConfigs();

        protected void Page_Load(object sender, EventArgs e)
        {
            ClsDriver objDriver = new ClsDriver();
            if (ViewState["tblDrivers"] == null)
            {
                objDriver.Fill();
                ViewState["tblDrivers"] = objDriver.tblDrivers;
            }

            if (!IsPostBack)
            {
                UHopCore.Title = "Driver Information";
                clsAuthenticator.Authenticate();
                string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                    //ViewState["MemberID"] = Session["MemberID"].ToString();
                    strMemberID = strDecrypted;
                    
            }


            objDriver.Fill();
            objDriver.FillDriverAssigned();
           

            var redDriver = Request.QueryString["rd"].ToString();

            var driverList = objDriver.tblDrivers.Where(t => t.DriverID == redDriver).Count();
            if(driverList <= 0)
            {
                Response.Redirect("~/Forms/Operators/Drivers/DriversMaintenance.aspx");
                return;
            }

            lblFirstName.Text = cs().FirstName;
            lblMiddleName.Text = cs().MiddleName;
            lblLastName.Text = cs().LastName;
            lblContact.Text = cRow() == null ? "" : cRow().ContactNumber;
            lblGender.Text = cs().Gender;
            lblBirthday.Text = cs().Birthdate.ToString();            
            lblMail.Text = cs().Email;

            using(ClsMembers cAt = new ClsMembers())
            {
                cAt.FillAttachment();
                string driversID = Request.QueryString["rd"].ToString();
                string type = "";

                type = "20150923000001";
                var attachmentExpiry = cAt.TblAttachment.Where(t => t.ReferenceID == driversID && t.IsForDriver == "1" && t.AttachmentTypeID == type).FirstOrDefault();
                if (attachmentExpiry == null)
                {
                    expNBIClearance.Text = "";
                }
                else
                {
                    expNBIClearance.Text = attachmentExpiry.ExpirationDate.ToString("yyyy-MM-dd");
                }


                type = "20150923000002";
                attachmentExpiry = cAt.TblAttachment.Where(t => t.ReferenceID == driversID && t.IsForDriver == "1" && t.AttachmentTypeID == type).FirstOrDefault();
                if (attachmentExpiry == null)
                {
                    expDriversLicense.Text = "";
                }
                else
                {
                    expDriversLicense.Text = attachmentExpiry.ExpirationDate.ToString("yyyy-MM-dd");
                }
            }

            
            

        }

        public uhopWeb.DataSets.DSConfigs.ContactNumberRow cRow() 
        {

            ClsMembers tb = new ClsMembers();
            tb.FillContacts(Request.QueryString["rd"].ToString());
            var ret=tb.TblContact.Where(t => t.ReferenceID == Request.QueryString["rd"].ToString()).FirstOrDefault();
            return ret;
        }

        public uhopWeb.DataSets.DSConfigs.DriverRow cs() 
        {
            ClsDriver cs = new ClsDriver();
            cs.Fill();
            var cl = cs.tblDrivers.Where(t => t.DriverID == Request.QueryString["rd"].ToString()).FirstOrDefault();
            return cl;                    
        }

        public string profilPicture() 
        {
            string retString = "";

            ClsDriver clsM = new ClsDriver();
            
                clsM.Fill();
            

            var gg = clsM.tblDrivers.Where(t => t.DriverID == Request.QueryString["rd"].ToString()).FirstOrDefault();
            retString = gg.ProfilePicture;
            return retString;
        }

        public string attachmentFileNameString(string type) 
        {
            string retString = "";
            string driverID = Request.QueryString["rd"].ToString();

            using(ClsMembers cls = new ClsMembers())
            {
                cls.FillAttachment();
                var driversLicenseAttachment = cls.TblAttachment.FirstOrDefault();

                if (type == "license")
                {
                    driversLicenseAttachment = cls.TblAttachment.Where(t => t.ReferenceID == driverID && t.IsForDriver == "1" && t.AttachmentTypeID == "20150923000002").FirstOrDefault();
                }
                else if (type == "nbi")
                {
                    driversLicenseAttachment = cls.TblAttachment.Where(t => t.ReferenceID == driverID && t.IsForDriver == "1" && t.AttachmentTypeID == "20150923000001").FirstOrDefault();
                }


                if (driversLicenseAttachment != null)
                {
                    retString = driversLicenseAttachment.AttachmentFileName;
                }
                else
                {
                    retString = "Default.gif";
                }
            }

            

            return retString;
        }



        private static string GetEncyptedPassword(string pass)
        {
            string encryptedPassword = "";
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(pass));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {

                    sb.Append(b.ToString("x2"));
                }

                encryptedPassword = sb.ToString().ToLower();
            }
            return encryptedPassword;
        }

        public void ClearPasswordFields() 
        {
            txtNewPassword.Text = "";
            txtConfirmPassword.Text = "";
            txtOldPassword.Text = "";
        }

        protected void btnSavePassword_Click(object sender, EventArgs e)
        {
            string message = "";
            using(ClsDriver csd = new ClsDriver())
            {
                csd.Fill();
                

                var oldPassword = csd.tblDrivers.Where(t => t.DriverID == Request.QueryString["rd"].ToString()).FirstOrDefault();
                string encrypTedPassword = GetEncyptedPassword(txtOldPassword.Text);


                if (txtConfirmPassword.Text == "" || txtNewPassword.Text == "" || txtOldPassword.Text == "")
                {
                    message = "Fields for changing your password is required!";
                    goto EndLine;
                }

                if (encrypTedPassword != oldPassword.Password)
                {
                    message = "Invalid Old Password!";
                    goto EndLine;
                }

                var newPasword = csd.tblDrivers.Where(t => t.DriverID == Request.QueryString["rd"].ToString()).FirstOrDefault();
                newPasword.Password = GetEncyptedPassword(txtNewPassword.Text);
                csd.Update();
                message = "Password has been changed!";



            }

        EndLine:
            System.Text.StringBuilder sbs = new System.Text.StringBuilder();
            sbs.Append("<script type = 'text/javascript'>");
            sbs.Append("window.onload=function(){");
            sbs.Append("alert('");
            sbs.Append(message);
            sbs.Append("')};");
            sbs.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sbs.ToString());
        }        
      

        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            string message = "No files to upload!";

            if (driverProfPicFU.HasFile)
            {
                var iFileSize = driverProfPicFU.FileContent.Length;
                var iFileExtension = Path.GetExtension(driverProfPicFU.FileName);
                
                if(iFileExtension.ToLower() != ".jpeg" && iFileExtension.ToLower() != ".jpg" && iFileExtension.ToLower() != ".png")
                {
                    message = "Upload Profile Picture is not a valid image!";
                    goto EndLine;
                }

                if(iFileSize > 2000000)
                {
                    message = "Cannot Upload greater than 2mb in file size for Profile Picture!";
                    goto EndLine;
                }               
            }


            if (driversLicenseFileUpload.HasFile)
            {
                var iFileSize = driversLicenseFileUpload.FileContent.Length;
                var iFileExtension = Path.GetExtension(driversLicenseFileUpload.FileName);
                if (iFileSize > 2000000)
                {
                    message = "Cannot Upload greater than 2mb in file size for Driver\'s License!";
                    goto EndLine;
                }

                if (iFileExtension.ToLower() != ".jpeg" && iFileExtension.ToLower() != ".jpg" && iFileExtension.ToLower() != ".png")
                {
                    message = "Uploaded Driver\'s License is not a valid image!";
                    goto EndLine;
                }

                if(string.IsNullOrEmpty(expDriversLicense.Text))
                {
                    message = "Drivers License expiration date is required!";
                    goto EndLine;
                }
              
            }


            if (nbiClearanceFileUpload.HasFile)
            {
                var iFileSize = nbiClearanceFileUpload.FileContent.Length;
                var iFileExtension = Path.GetExtension(nbiClearanceFileUpload.FileName);
                if (iFileSize > 2000000)
                {
                    message = "Cannot Upload greater than 2mb in file size for NBI Clearance";
                    goto EndLine;
                }

                if (iFileExtension.ToLower() != ".jpeg" && iFileExtension.ToLower() != ".jpg" && iFileExtension.ToLower() != ".png")
                {
                    message = "Uploaded NBI Clearance file is not a valid image!";
                    goto EndLine;
                }

                if(string.IsNullOrEmpty(expNBIClearance.Text))
                {
                    message = "NBI Clearance Expiration date is required!";
                    goto EndLine;
                }
            }            

            #region Upload Images
            ClsAutoNumber objAutoNumber = new ClsAutoNumber();
            if (driverProfPicFU.HasFile)
            {
                ClsDriver objDriver = new ClsDriver();
                objDriver.Fill();
                string driverId = Request.QueryString["rd"].ToString();
                string CustomFilename = "DPROFPICT_" + Request.QueryString["rd"].ToString() + Path.GetExtension(driverProfPicFU.FileName).ToLower();
                var driverProfile = objDriver.tblDrivers.Where(t => t.DriverID == driverId).FirstOrDefault();                                
                driverProfile.ProfilePicture = CustomFilename;
                objDriver.Update();
                driverProfPicFU.SaveAs(Server.MapPath("../../../UploadedImages/ProfilePicture/" + CustomFilename));
                message = "File has been updated!";
            }

            if(driversLicenseFileUpload.HasFile)
            {
                string CustomFilename = "DDRVRLCNS_" + Request.QueryString["rd"].ToString() + Path.GetExtension(driversLicenseFileUpload.FileName).ToLower();
                using (ClsMembers clsM = new ClsMembers())
                {
                    clsM.FillAttachment();
                    var driverID = Request.QueryString["rd"].ToString();
                    var driverAttachment = clsM.TblAttachment.Where(t => t.ReferenceID == driverID && t.IsForDriver == "1" && t.AttachmentTypeID == "20150923000002").FirstOrDefault();
                    if (driverAttachment == null)
                    {
                        
                        var qc = clsM.TblAttachment.NewAttachmentRow();
                        qc.AttachmentID = objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString();
                        qc.AttachmentTypeID = "20150923000002";
                        qc.ReferenceID = driverID;
                        qc.AttachmentFileName = CustomFilename;
                        qc.Description = "Professional Drivers License";
                        qc.ExpirationDate = Convert.ToDateTime(expDriversLicense.Text);
                        qc.Status = "0";
                        qc.IsForDriver = "1";
                        clsM.TblAttachment.Rows.Add(qc);
                    }
                    else
                    {
                        driverAttachment.AttachmentFileName = CustomFilename;
                        driverAttachment.ExpirationDate = Convert.ToDateTime(expDriversLicense.Text);
                        driverAttachment.Status = "0";
                        clsM.UpdateAttachment();
                    }
                    var filePathToUpload = Server.MapPath("../../../UploadedImages/Attachment/" + CustomFilename);

                    driversLicenseFileUpload.SaveAs(filePathToUpload);
                    message = "File has been updated!";
                }
                
            }

            if(nbiClearanceFileUpload.HasFile)
            {
                string CustomFilename = "DNBICLRNC_" + Request.QueryString["rd"].ToString() + Path.GetExtension(nbiClearanceFileUpload.FileName).ToLower();

                using (ClsMembers clsM = new ClsMembers())
                {
                    clsM.FillAttachment();

                    var driverID = Request.QueryString["rd"].ToString();
                    var driverAttachment = clsM.TblAttachment.Where(t => t.ReferenceID == driverID && t.IsForDriver == "1" && t.AttachmentTypeID == "20150923000001").FirstOrDefault();
                    if (driverAttachment == null)
                    {
                        var qc = clsM.TblAttachment.NewAttachmentRow();
                        qc.AttachmentID = objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString();
                        qc.AttachmentTypeID = "20150923000001";
                        qc.ReferenceID = driverID;
                        qc.AttachmentFileName = CustomFilename;
                        qc.Description = "Professional Drivers License";
                        qc.ExpirationDate = Convert.ToDateTime(expNBIClearance.Text);
                        qc.Status = "0";
                        qc.IsForDriver = "1";
                        clsM.TblAttachment.Rows.Add(qc);
                    }
                    else
                    {
                        driverAttachment.AttachmentFileName = CustomFilename;
                        driverAttachment.ExpirationDate = Convert.ToDateTime(expNBIClearance.Text);
                        driverAttachment.Status = "0";
                        clsM.UpdateAttachment();
                    }

                    nbiClearanceFileUpload.SaveAs(Server.MapPath("../../../UploadedImages/Attachment/" + CustomFilename));
                    message = "File has been updated!";
                }
                
            }
            #endregion

        EndLine:
            System.Text.StringBuilder sbs = new System.Text.StringBuilder();
            sbs.Append("<script type = 'text/javascript'>");
            sbs.Append("window.onload=function(){");
            sbs.Append("alert('");
            sbs.Append(message);
            sbs.Append("')};");
            sbs.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sbs.ToString());
        }

    }
}