﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using uhopWeb.Classes;
using System.Web.Services;

namespace uhopWeb.Forms.Operators.Maintenance
{
    public partial class DriverMaintenance : System.Web.UI.Page
    {

        public string strMemberID ="";

        protected void Page_Load(object sender, EventArgs e)
        {

                UHopCore.Title = "Driver Maintenance";
                clsAuthenticator.Authenticate();
                string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                ViewState["MemberID"] = strDecrypted;
                strMemberID = strDecrypted;
                
            
        }

        public string listOperatorDriver() 
        {
            using(ClsDriver dr = new ClsDriver())            
            {
                dr.Fill();
                dr.FillDriverAssigned();
                var driverListConnector = dr.tblDriverAssigned.Where(t => t.MemberID == strMemberID).ToList();

                string elementContent = "";

                foreach (var a in driverListConnector)
                {
                    var driverList = dr.tblDrivers.Where(t => t.DriverID == a.DriverID).FirstOrDefault();
                    elementContent += "<tr>";
                    elementContent += "<td><a href=\"DriversInformation.aspx?rd=" + driverList.DriverID + "\">" + driverList.FirstName + " " + driverList.MiddleName + " " + driverList.LastName + "</a></td>";
                    elementContent += "<td>" + driverList.CreatedOn.ToString("yyyy-mmmm-dd") + "</td>";
                    elementContent += "<td>" + (a.Enabled == "0" ? "Disabled" : driverList.Status == "0" ? "For Approval" : "Approved") + "</td>";
                    if (a.Enabled == "1")
                    {
                        elementContent += "<td><a href=\"DriversInformation.aspx?rd=" + driverList.DriverID + "\">Edit</a>&nbsp;|&nbsp;<a dName=\"" + driverList.FirstName + " " + driverList.MiddleName + " " + driverList.LastName + "\" class=\"remDriver\" id=\"" + driverList.DriverID + "\" href='#'>Disable</a> </td>";
                    }
                    else
                    {
                        elementContent += "<td></td>";
                    }
                    elementContent += "</tr>";
                }

                return elementContent;    
            }
            
                    
        }

        


        


        public string upload_Image(FileUpload fileupload/*, string ImageSavedPath, string FileName*/)
        {

            FileUpload fu = fileupload;
          //  string imagepath = "";
            if (fileupload.HasFile)
            {
               // string filepath = Server.MapPath(ImageSavedPath);
                String fileExtension = System.IO.Path.GetExtension(fu.FileName).ToLower();
                String[] allowedExtensions = { ".png", ".jpeg", ".jpg", ".PNG", ".JPEG", "JPG" };

                if (fileupload.FileContent.Length > 2000000)
                {
                    return "2";
                }

                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        try
                        {
                            //string s_newfilename = FileName;
                            //fu.PostedFile.SaveAs(filepath + s_newfilename);
                            //imagepath = ImageSavedPath + s_newfilename;
                            return "1";
                        }
                        catch
                        {
                            Response.Write("File could not be uploaded.");
                        }
                    }
                }
            }
            return "0";
        }

        [WebMethod]
        public static string getDriverName(string driverID) 
        {
            string retStr = "";

            using(ClsDriver dr = new ClsDriver())
            {
                dr.Fill();
                var drName = dr.tblDrivers.Where(t => t.DriverID == driverID).FirstOrDefault();
                retStr = drName.FirstName + " " + drName.MiddleName + " " + drName.LastName;
            }



            return retStr;

        }

        [WebMethod]
        public static string validateEmail(string email) 
        {
            string retStr = "";

            using(ClsDriver dr = new ClsDriver())
            {
                dr.Fill();
                var checkEmail = dr.tblDrivers.Where(t => t.Email == email).Count();

                if (checkEmail > 0)
                {
                    retStr = "1";
                }
                else
                {
                    retStr = "0";
                }
                return retStr;
            }

            
        }

        protected void addDriver_Click(object sender, EventArgs e)
        {
            string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
            strMemberID = strDecrypted;
            if (strMemberID != "")
            {
                ClsMembers objMembers = new ClsMembers();
                var message = "";

                if (string.IsNullOrEmpty(txtFirstName.Text) || string.IsNullOrEmpty(txtLastName.Text)
                    || string.IsNullOrEmpty(txtGender.Value) || string.IsNullOrEmpty(txtBirthDay.Text)
                    || string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrEmpty(txtNewPassword.Text)
                    || string.IsNullOrEmpty(driversLicenseUploadExpirationDate.Text) || string.IsNullOrEmpty(nbiClearanceUploadExpirationDate.Text))
                {
                    message = "Required Fields must not be empty!";
                    goto lastLine;
                }



                switch (upload_Image(profilePictureUpload))
                {
                    case "0":
                        message = "Profile Picture Image is required!";
                        goto lastLine;
                    case "1":
                        message = "Registration is successful!";
                        break;
                    case "2":
                        message = "File size must be lest than 2mb in size!";
                        goto lastLine;
                }

                switch (upload_Image(driversLicenseUpload))
                {
                    case "0":
                        message = "Profile Picture Image is required!";
                        goto lastLine;
                    case "1":
                        message = "Registration is successful!";
                        break;
                    case "2":
                        message = "File size must be lest than 2mb in size!";
                        goto lastLine;
                }

                switch (upload_Image(nbiClearanceUpload))
                {
                    case "0":
                        message = "Profile Picture Image is required!";
                        goto lastLine;
                    case "1":
                        message = "Registration is successful!";
                        break;
                    case "2":
                        message = "File size must be lest than 2mb in size!";
                        goto lastLine;
                }

                
                using(ClsDriver cld = new ClsDriver())
                {
                    cld.Fill();
                    ClsAutoNumber objAutoNumber = new ClsAutoNumber();

                    var checkEmail = cld.tblDrivers.Where(t => t.Email == txtEmail.Text).Count();

                    if (checkEmail > 0)
                    {
                        message = "Email already exist! Please select input another email address!";
                        goto lastLine;
                    }


                    string driverID = objAutoNumber.GenerateAutoNumber("DriverID", "000000").ToString();
                    string strFileName = "D" + "PROFPICT" + "_" + driverID + ".jpg";


                    FileUpload fu = profilePictureUpload;
                    string filepath = Server.MapPath("../../../UploadedImages/ProfilePicture/");



                    //var cc = ClsDriver.tblDrivers.NewDriverRow();
                    DataRow cc = cld.tblDrivers.NewDriverRow();
                    cc.BeginEdit();
                    //cc.DriverID = driverID;
                    //cc.FirstName = txtFirstName.Text;
                    //cc.MiddleName = txtMiddleName.Text;
                    //cc.LastName = txtLastName.Text;
                    //cc.Gender = txtGender.Text;
                    //cc.Birthdate = Convert.ToDateTime(txtBirthDay.Text);
                    //cc.Email = txtEmail.Text;
                    //cc.Password = txtNewPassword.Text;
                    //cc.CreatedBy = 
                    //cc.Status = "0";
                    //cc.ModifiedBy = Session["MemberID"].ToString();
                    //cc.CreatedOn = DateTime.Now;
                    //cc.ModifiedBy = strMemberID;
                    //cc.ModifiedOn = DateTime.Now;
                    //cc.ProfilePicture = strFileName;

                    cc.SetString("DriverID", driverID);
                    cc.SetString("FirstName", txtFirstName.Text);
                    cc.SetString("MiddleName", txtMiddleName.Text);
                    cc.SetString("LastName", txtLastName.Text);
                    cc.SetString("Gender", txtGender.Value);
                    cc.SetDateTime("Birthdate", Convert.ToDateTime(txtBirthDay.Text));
                    cc.SetString("Email", txtEmail.Text);
                    cc.SetString("Password", txtNewPassword.Text);
                    cc.SetString("Enabled", "1");
                    cc.SetString("Status", "0");
                    cc.SetString("CreatedBy", strMemberID);
                    cc.SetDateTime("CreatedOn", DateTime.Now);
                    cc.SetString("ModifiedBy", strMemberID);
                    cc.SetDateTime("ModifiedOn", DateTime.Now);
                    cc.SetString("ProfilePicture", strFileName);

                    cc.EndEdit();
                    cld.tblDrivers.Rows.Add(cc);
                    cld.Update();
                    try { fu.PostedFile.SaveAs(filepath + strFileName); }
                    catch (Exception ex) { ex.ToString(); }

                    var con = objMembers.TblContact.NewContactNumberRow();
                    con.ContactID = objAutoNumber.GenerateAutoNumber("ContactNumberID", "000000").ToString();
                    con.ContactNumberTypeID = "20150923000002";
                    con.ReferenceID = driverID;
                    con.ContactNumber = txtMobileNumber.Text;
                    con.Enabled = "1";
                    con.IsForDriver = "1";
                    objMembers.TblContact.Rows.Add(con);
                    objMembers.UpdateContact();

                    var cg = cld.tblDriverAssigned.NewDriverAssignedRow();
                    cg.DriverAssignedID = objAutoNumber.GenerateAutoNumber("DriverAssignedID", "000000").ToString();
                    cg.MemberID = strMemberID;
                    cg.DriverID = driverID;
                    cg.Enabled = "1";
                    cg.CreatedBy = strMemberID;
                    cg.CreatedOn = DateTime.Now;
                    cg.ModifiedBy = strMemberID;
                    cg.ModifiedOn = DateTime.Now;
                    cld.tblDriverAssigned.Rows.Add(cg);
                    cld.UpdateDriverAssigned();

                    // for NBI Clearance attachment
                    strFileName = "D" + "NBICLRNC" + "_" + driverID + ".jpg";
                    var at = objMembers.TblAttachment.NewAttachmentRow();
                    at.AttachmentID = objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString();
                    at.AttachmentTypeID = "20150923000001";
                    at.ReferenceID = driverID;
                    at.AttachmentFileName = strFileName;
                    at.Description = "NBI Clearance";
                    at.ExpirationDate = Convert.ToDateTime(nbiClearanceUploadExpirationDate.Text);
                    at.Status = "0";
                    at.IsForDriver = "1";
                    objMembers.TblAttachment.Rows.Add(at);
                    objMembers.UpdateAttachment();
                    filepath = Server.MapPath("../../../UploadedImages/Attachment/");
                    fu = nbiClearanceUpload;
                    try { fu.PostedFile.SaveAs(filepath + strFileName); }
                    catch (Exception ex) { ex.ToString(); }

                    //for driver's license attachment
                    strFileName = "D" + "DRVRLCNS" + "_" + driverID + ".jpg";
                    var ac = objMembers.TblAttachment.NewAttachmentRow();
                    ac.AttachmentID = objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString();
                    ac.AttachmentTypeID = "20150923000002";
                    ac.ReferenceID = driverID;
                    ac.AttachmentFileName = strFileName;
                    ac.Description = "Driver's License";
                    ac.ExpirationDate = Convert.ToDateTime(driversLicenseUploadExpirationDate.Text);
                    ac.Status = "0";
                    ac.IsForDriver = "1";
                    objMembers.TblAttachment.Rows.Add(ac);
                    objMembers.UpdateAttachment();
                    filepath = Server.MapPath("../../../UploadedImages/Attachment/");
                    fu = driversLicenseUpload;
                    try { fu.PostedFile.SaveAs(filepath + strFileName); }
                    catch (Exception ex) { ex.ToString(); }


                }

            lastLine:
                System.Text.StringBuilder sbs = new System.Text.StringBuilder();
                sbs.Append("<script type = 'text/javascript'>");
                sbs.Append("window.onload=function(){");
                sbs.Append("alert('");
                sbs.Append(message);
                sbs.Append("')};window.location.href = window.location.protocol + '//' + window.location.host + window.location.pathname;");
                sbs.Append("</script>");
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sbs.ToString());
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            using(ClsDriver dr = new ClsDriver())
            {
                dr.Fill();
                dr.FillDriverAssigned();
                var message = "";
                var color = "";

                var ifDriverExistWithMember = dr.tblDriverAssigned.Where(t => t.MemberID == strMemberID && t.DriverID == hfDriverID.Value).Count();

                if (ifDriverExistWithMember <= 0)
                {
                    color = "red";
                    message = "Selected driver does not exist on your list!";
                    goto EndLine;
                }

                var driver = dr.tblDriverAssigned.Where(t => t.DriverID == hfDriverID.Value);

                if (driver.FirstOrDefault().Enabled == "0")
                {
                    color = "red";
                    message = "Selected driver is already disabled!";
                    goto EndLine;
                }
                driver.FirstOrDefault().BeginEdit();
                driver.FirstOrDefault().Enabled = "0";
                driver.FirstOrDefault().EndEdit();
                dr.UpdateDriverAssigned();
                color = "#59A55A";

                var driverMain = dr.tblDrivers.Where(t => t.DriverID == hfDriverID.Value).FirstOrDefault();
                driverMain.Status = "0";
                dr.Update();

                message = "Successful! Driver's account has been disabled!";

            EndLine:
                System.Text.StringBuilder sbs = new System.Text.StringBuilder();
                sbs.Append("<script type = 'text/javascript'>");
                sbs.Append("window.onload=function(){");
                sbs.Append("$(\"#main\").prepend(\"<div class='errorPrompt white-text' style='background-color:" + color + ";text-align:center;padding:5px'>" + message + "</div>\");");
                sbs.Append("$(\".errorPrompt\").show(0).delay(3000).fadeOut(\"slow\");");
                sbs.Append("};");
                sbs.Append("</script>");
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sbs.ToString());
            }            

            
        }
    }
} 