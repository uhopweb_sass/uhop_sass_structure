﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using uhopWeb.Classes;

namespace uhopWeb.Forms.Operators.Drvers
{
    public partial class DriversList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clsAuthenticator.Authenticate();
                UHopCore.Title = "Driver List";
                string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                ViewState["MemberID"] = strDecrypted;
                using (ClsDriver objDriver = new ClsDriver())
                {
                    objDriver.Fill();
                    GVDrivers.DataSource = objDriver.tblDrivers;
                    GVDrivers.DataBind();
                }

                

                foreach (GridViewRow gvr in GVDrivers.Rows)
                {
                    HiddenField hdf = (HiddenField)gvr.FindControl("hdfDriverID");
                    gvr.Attributes.Add("onclick", "redirectMe('"+ hdf.Value +"')");
                }


            }
        }
    }
}