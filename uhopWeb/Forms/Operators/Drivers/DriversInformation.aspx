﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Operators/Operators.Master" AutoEventWireup="true" CodeBehind="DriversInformation.aspx.cs" Inherits="uhopWeb.Forms.Operators.Drivers.DriversInformation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row profileRows white-text" id="userHeading">
            <br />
            <div class="col s12 l2">                
                <div class="col s12">
                    <img id="imgUserProfile" style="width: 200px; height: 200px;" class="prevUploadImageProfile" src="../../../UploadedImages/ProfilePicture/<% Response.Write(profilPicture());%>?r=<%=Guid.NewGuid()%>>" alt="your image" />
                </div>
                <div class="col s12 center">
                    <asp:FileUpload ID="driverProfPicFU" runat="server" class="black-text"  accept="image/*"/>
                </div>
            </div>
            <div class="col s12 l8" style="color: white">
                <span style="font-size: 25px;">
                    <asp:Label ID="lblFirstName" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblMiddleName" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblLastName" runat="server" Text=""></asp:Label>, 34 y/o
                </span>
                <br />
                Email:<asp:Label ID="lblMail" runat="server" Text="mail.mail@mail.com"></asp:Label>
                <br />
                Contact #: <asp:Label ID="lblContact" runat="server" Text="09998745828"></asp:Label>
            </div>
        </div>

        <br />
        <div class="divider"></div>
        <br />

        <div class="row white-text">
            <div class="col s12">
                <h5>Personal Infomation</h5>
                <p>Gender: <asp:Label ID="lblGender" runat="server" Text="Male"></asp:Label></p>
                <p>Born on: <asp:Label ID="lblBirthday" runat="server" Text="09-08-2015"></asp:Label></p>         
            </div>
            <div class="col s12">
                <br>
                <h5>Account Information</h5>
                <p>Password: **************************</p>
                <p><button id="btnChangePassword" type="button" class="waves-effect waves-light btn pink accent-2 large">Change Password</button></p>  
            </div>
        </div>

        <div class="row">
            <div class="col l8 s12">
                <div class="card" id="divChangePassword">
                    <div class="card-content black-text">
                        <p>Old Password:</p>
                        <p>
                            <asp:TextBox type="password" ID="txtOldPassword" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator Style="color: red" ID="reqf1" runat="server" ErrorMessage="Please enter Old password" ControlToValidate="txtOldPassword" ValidationGroup="btnSavePassword"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <span>New Password:</span>
                        </p>
                        <p>
                            <asp:TextBox type="password" ID="txtNewPassword" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator Style="color: red" ID="reqf2" runat="server" ErrorMessage="Please enter New password" ControlToValidate="txtNewPassword" ValidationGroup="btnSavePassword"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <span>Confirm Password:</span>
                        </p>
                        <p>
                            <asp:TextBox type="password" ID="txtConfirmPassword" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator Style="color: red" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter confirm password!" ControlToValidate="txtConfirmPassword" ValidationGroup="btnSavePassword"></asp:RequiredFieldValidator>
                            <asp:CompareValidator Style="color: red" ID="reqf3" runat="server" ErrorMessage="Password did not match!" ControlToValidate="txtConfirmPassword" ControlToCompare="txtNewPassword" ValidationGroup="btnSavePassword"></asp:CompareValidator>
                        </p>
                     </div>
                    <div class="card-action center-align">                    
                        <asp:Button ID="btnSavePassword" ValidationGroup="btnSavePassword" runat="server" Text="Save Password" OnClick="btnSavePassword_Click" CssClass="btn blue darken-2" />
                        <button id="btnCancelChangePassword" type="button" class="btn red darken-2">Cancel</button>                    
                    </div>
                </div> <!-- END of card -->
            </div>
        </div> <!-- END of row -->

        <div class="divider"></div>
        <div class="row white-text">
            <h4>Driver's Documents</h4>
        </div>
        <div class="row white-text">
            <div class="col s12 l6 center">
                <h5>Driver's License</h5>
                <img id="Img1" style="width: 300px; height: 300px;" class="responsive-img prevUploadImageProfile" src="../../../UploadedImages/Attachment/<%Response.Write(attachmentFileNameString("license") + "?r=" + Guid.NewGuid());%>" alt="your image" />                     
                <asp:TextBox ID="expDriversLicense" type="date" runat="server" class="datepicker"></asp:TextBox>                                                           
                <asp:FileUpload ID="driversLicenseFileUpload" runat="server"/>             
            </div>
            <div class="col s12 l6">
                <div class="col s12 center">
                    <h5 >NBI Clearance</h5>
                    <img id="Img2" style="width: 300px; height: 300px;" class="responsive-img prevUploadImageProfile" src="../../../UploadedImages/Attachment/<%Response.Write(attachmentFileNameString("nbi") + "?r=" + Guid.NewGuid());%>" alt="your image" />
                    <asp:TextBox ID="expNBIClearance" type="date" runat="server" style="color:white" CssClass="datepicker"></asp:TextBox>
                    <asp:FileUpload ID="nbiClearanceFileUpload" runat="server"/>
                </div>
            </div>
        </div>
        <div class="row">
            <br />
            <div class="col s12 center">
                <asp:Button ID="SaveChanges" runat="server" Text="Save Changes" OnClick="SaveChanges_Click" class="btn blue darken-2"/>
                <a href="DriversMaintenance.aspx" class="btn red darken-2">Cancel</a>
            </div> 
        </div>
    </div>

    <script>   

        $(document).ready(function ()
        {
            var validity = "";

            function readURL(input, control) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();                    
                    reader.onload = function (e) {                        
                        var dataURL = e.target.result;
                        var mimeType = dataURL.split(",")[0].split(":")[1].split(";")[0];
                        if (mimeType != "image/jpeg" && mimeType != "image/png")
                        {
                            alert("Invalid File Type!");
                            validity = "0";
                            return;                            
                        }
                        $(control).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#<%=driversLicenseFileUpload.ClientID%>").on("change", function ()
            {
                readURL(this, "#Img1");
            });

            $("#<%=nbiClearanceFileUpload.ClientID%>").on("change", function () {
                readURL(this, "#Img2");
            });            

            $("#<%=driverProfPicFU.ClientID%>").on("change", function ()
            {
                readURL(this, "#prevUploadImageProfile");
                if(validity == "0")
                {
                    validity = "";
                    return;
                }                 
            });

            $("#divChangePassword").hide();

            $("#btnChangePassword").on("click", function ()
            {
                $("#divChangePassword").show("slow");
            });

            $("#btnCancelChangePassword").on("click", function ()
            {                
                $("#divChangePassword").hide("slow");
                $("input[type=password]").val("");
                $("#<%=reqf1.ClientID%>, #<%=reqf2.ClientID%>, #<%=reqf3.ClientID%>").css("visibility", "hidden");
            });
        });

    </script>


</asp:Content>
