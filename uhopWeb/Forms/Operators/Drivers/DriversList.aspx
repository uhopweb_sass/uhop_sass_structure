﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Operators/Operators.Master" AutoEventWireup="true" CodeBehind="DriversList.aspx.cs" Inherits="uhopWeb.Forms.Operators.Drvers.DriversList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row">
        <div class="col s12">
            <asp:GridView ID="GVDrivers" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2" runat="server" Text="Driver's Details"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDriversDetals" runat="server" Text='<%# "Driver Name: "+Eval("FirstName").ToString()+" "+Eval("MiddleName").ToString()+" "+Eval("LastName").ToString() %>'></asp:Label>
                            <asp:HiddenField ID="hdfDriverID" runat="server" value='<%# Bind("DriverID") %>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>


        <script type="text/javascript">

            function redirectMe(DriverID) {
                location.href = "DriversRatings.aspx?driver=" + DriverID;

            }


    </script>
</asp:Content>
