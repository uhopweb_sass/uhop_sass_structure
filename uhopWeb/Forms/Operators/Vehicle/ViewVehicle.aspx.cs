﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Web.Services;
using uhopWeb.Classes;
using System.Text;

namespace uhopWeb.Forms.Operators.Vehicle
{
    public partial class ViewVehicle : System.Web.UI.Page
    {
        public static string strMemberID;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                clsAuthenticator.Authenticate();
                    LoadYearModel();
                    UHopCore.Title = "Vehicle List";
                    string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                    ViewState["MemberID"] = strDecrypted;
                    strMemberID = strDecrypted;
                    ClsMembers objMembers = new ClsMembers(strMemberID);
                    if (ViewState["TblMembers"] == null)
                    {
                        objMembers.Fill(strMemberID);
                        ViewState["TblMembers"] = objMembers.TblMembers;
                    }

                    objMembers.TblMembers = (DataSets.DSConfigs.MemberDataTable)ViewState["TblMembers"];
                    var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                    if (MemberDetails.Status == "0")
                    {
                        Response.Redirect("../Maintenance/OperatorProfile.aspx");
                    }
 
            }
        }

        public void LoadYearModel()
        {
            int y = 0;
            for (int x = 2013; x <= DateTime.Now.Year + 1; x++)
            {
                ddlYearModel.Items.Insert(y, new ListItem(Convert.ToString(x), Convert.ToString(x)));
                ddlYearModel.SelectedIndex = 0;
                y += 1;
            }
        }

        public class VehicleDetails
        {
            private string _BrandName;
            public string BrandName
            {
                get { return _BrandName; }
                set { _BrandName = value; }
            }

            private string _ModelName;
            public string ModelName
            {
                get { return _ModelName; }
                set { _ModelName = value; }
            }

            private string _PlateNumber;
            public string PlateNumber
            {
                get { return _PlateNumber; }
                set { _PlateNumber = value; }
            }

            private string _VehicleColor;
            public string VehicleColor
            {
                get { return _VehicleColor; }
                set { _VehicleColor = value; }
            }

            private string _YearModel;
            public string YearModel
            {
                get { return _YearModel; }
                set { _YearModel = value; }
            }

            private string _VehicleType;
            public string VehicleType
            {
                get { return _VehicleType; }
                set { _VehicleType = value; }
            }

            private string _MaxCapacity;
            public string MaxCapacity
            {
                get { return _MaxCapacity; }
                set { _MaxCapacity = value; }
            }
        }
        public class VehicleAttachmentDetails
        {
            private string _AttachmentID;
            public string AttachmentID
            {
                get { return _AttachmentID; }
                set { _AttachmentID = value; }
            }

            private string _AttachmentTypeID;
            public string AttachmentTypeID
            {
                get { return _AttachmentTypeID; }
                set { _AttachmentTypeID = value; }
            }

            private string _ReferenceID;
            public string ReferenceID
            {
                get { return _ReferenceID; }
                set { _ReferenceID = value; }
            }

            private string _AttachmentFileName;
            public string AttachmentFileName
            {
                get { return _AttachmentFileName; }
                set { _AttachmentFileName = value; }
            }

            private string _Description;
            public string Description
            {
                get { return _Description; }
                set { _Description = value; }
            }

            private string _ExpirationDate;
            public string ExpirationDate
            {
                get { return _ExpirationDate; }
                set { _ExpirationDate = value; }
            }

            private string _HasExpirationDate;
            public string HasExpirationDate
            {
                get { return _HasExpirationDate; }
                set { _HasExpirationDate = value; }
            }

            private string _AttachmentTypeDescription;
            public string AttachmentTypeDescription
            {
                get { return _AttachmentTypeDescription; }
                set { _AttachmentTypeDescription = value; }
            }
        }
        public class AttachmentTypeDetails
        {
            private string _AttachmentTypeDescription;
            public string AttachmentTypeDescription
            {
                get { return _AttachmentTypeDescription; }
                set { _AttachmentTypeDescription = value; }
            }

            private string _HasExpirationDate;
            public string HasExpirationDate
            {
                get { return _HasExpirationDate; }
                set { _HasExpirationDate = value; }
            }

            private string _HasDescription;
            public string HasDescription
            {
                get { return _HasDescription; }
                set { _HasDescription = value; }
            }
        }

        [WebMethod]
        public static List<AttachmentTypeDetails> GetVehicleRequiredAttachments(String pAttachmentTypeID)
        {
            using (ClsMembers objMembers = new ClsMembers(strMemberID))
            {
                List<AttachmentTypeDetails> lstRequiredVehicleAttachment = new List<AttachmentTypeDetails>();
                objMembers.FillAttachmentType();
                DataTable dtAttachment = objMembers.TblAttachmentType.Where(t => t.IsForVehicle == "1" && t.AttachmentTypeID == pAttachmentTypeID).CopyToDataTable();
                foreach (DataRow drw in dtAttachment.Rows)
                {
                    string AttachmentTypeDescription = drw["AttachmentTypeDescription"].ToString();
                    string HasExpirationDate = drw["HasExpirationDate"].ToString();
                    string HasDescription = drw["HasDescription"].ToString();

                    lstRequiredVehicleAttachment.Add(new AttachmentTypeDetails
                    {
                        AttachmentTypeDescription = AttachmentTypeDescription,
                        HasExpirationDate = HasExpirationDate,
                        HasDescription = HasDescription
                    });
                }
                return lstRequiredVehicleAttachment;
            }
        }

        [WebMethod]
        public static List<VehicleDetails> PopulateVehicleDetails(String pVehicleID)
        {
                List<VehicleDetails> lstVehicleDetails = new List<VehicleDetails>();
                DataTable tbl = new DataTable();
                ClsMemberVehicle objMemberVehicle = new ClsMemberVehicle();
                objMemberVehicle.FillViaID(objMemberVehicle.TblMemberVehicle, strMemberID);
                tbl = objMemberVehicle.TblMemberVehicle.Where(t => t.VehicleID == pVehicleID).CopyToDataTable();
                foreach (DataRow drw in tbl.Rows) 
                {
                    string BrandName = drw["Brandname"].ToString();
                    string ModelName = drw["Model"].ToString();
                    string PlateNumber = drw["PlateNumber"].ToString();
                    string VehicleType = drw["VehicleType"].ToString();
                    string YearModel = drw["YearModel"].ToString();
                    string MaxCapacity = drw["MaximumCapacity"].ToString();
                    string VehicleColor = drw["VehicleColor"].ToString();

                    lstVehicleDetails.Add(new VehicleDetails
                    {
                        BrandName = BrandName,
                        ModelName = ModelName,
                        PlateNumber = PlateNumber,
                        VehicleType=VehicleType,
                        MaxCapacity=MaxCapacity,
                        YearModel = YearModel,
                        VehicleColor = VehicleColor
                    });
                }
                return lstVehicleDetails;
        }

        [WebMethod]
        public static List<VehicleAttachmentDetails> EditVehicleAttachment(String pAttachmentID)
        {
                List<VehicleAttachmentDetails> lstVehicleAttachment = new List<VehicleAttachmentDetails>();
                ClsMemberVehicle objMemberVehicleAttachment = new ClsMemberVehicle();
                objMemberVehicleAttachment.FillViaReferenceID(objMemberVehicleAttachment.TblMemberVehicleAttachment, strMemberID);
                DataTable tbl = objMemberVehicleAttachment.TblMemberVehicleAttachment.Where(t => t.AttachmentID == pAttachmentID).CopyToDataTable();

                string AttachmentID = "";
                string AttachmentTypeID = "";
                string ReferenceID = "";
                string AttachmentFileName = "";
                string Description = "";
                string ExpirationDate = "";
                string HasExpirationDate = "";
                string AttachmentTypeDescription = "";

                if (tbl.Rows.Count > 0)
                {
                    foreach (DataRow drw in tbl.Rows)
                    {
                        AttachmentID = drw["AttachmentID"].ToString();
                        AttachmentTypeID = drw["AttachmentTypeID"].ToString();
                        ReferenceID = drw["ReferenceID"].ToString();
                        AttachmentFileName = uhopWeb.Classes.UHopCore.GetPageURL() + "UploadedImages/Attachment/" + drw["AttachmentFileName"].ToString() ?? uhopWeb.Classes.UHopCore.GetPageURL() + "UploadedImages/Attachment/Default.gif";
                        Description = drw["Description"].ToString();
                        ExpirationDate = drw["ExpirationDate"].ToString();
                        HasExpirationDate = drw["HasExpirationDate"].ToString();
                        AttachmentTypeDescription = drw["AttachmentTypeDescription"].ToString() + " Image";

                        lstVehicleAttachment.Add(new VehicleAttachmentDetails
                        {
                            AttachmentID = AttachmentID,
                            AttachmentTypeID = AttachmentTypeID,
                            ReferenceID = ReferenceID,
                            AttachmentFileName = AttachmentFileName,
                            Description = Description,
                            ExpirationDate = ExpirationDate,
                            HasExpirationDate = HasExpirationDate,
                            AttachmentTypeDescription = AttachmentTypeDescription
                        });
                    }
                }
                else { 
                    
                }
               return lstVehicleAttachment;
        }
        
        public void LoadVehicles()
        {
            using (ClsMembers objMembers = new ClsMembers(strMemberID))
            {
                string strReturn = "";
                string strHasDescription = "";
                string strHasExpirationDate = "";
                string strHasImage = "";
                string strImageMissing = "";
                string strViewLarge = "";
                string strAttachmentID = "";
                string strAttachmentIDEdit = "";
                string strID = "";
                DateTime dtFormattedDate;

                //Member vehicle Details
                ClsMemberVehicle objMemberVehicle = new ClsMemberVehicle();
                objMemberVehicle.FillViaID(objMemberVehicle.TblMemberVehicle, strMemberID);
                DataTable dtMemberVehicle = objMemberVehicle.TblMemberVehicle;

                //Member vehicle Attachment
                ClsMemberVehicle objMemberVehicleAttachment = new ClsMemberVehicle();
                objMemberVehicleAttachment.FillViaReferenceID(objMemberVehicleAttachment.TblMemberVehicleAttachment, strMemberID);
                DataTable dtMemberVehicleID = objMemberVehicleAttachment.TblMemberVehicleAttachment;

                //Vehicle Attachment Requirements
                objMembers.FillAttachmentType();

                DataTable dtAttachment = objMembers.TblAttachmentType.Where(t => t.IsForVehicle == "1").CopyToDataTable();
                ulVehicles.Controls.Clear();

                if (dtMemberVehicle.Rows.Count != 0)
                {
                    foreach (DataRow drwMemberVehicle in dtMemberVehicle.Rows)
                    {
                        strReturn += "<li><div class='collapsible-header active'><i class='material-icons oDashboardIcons'>&#xE5CF;</i><span class='collapsibleTextHeader'>" + System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(drwMemberVehicle["BrandName"].ToString()) + "</span><span> (" + System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(drwMemberVehicle["VehicleColor"].ToString()) + ")</span><span class='collapsiblePNumber'> - " + drwMemberVehicle["PlateNumber"].ToString().ToUpper() + "</span><a href='#' id='" + drwMemberVehicle["VehicleID"] + "' class='DeleteVehicleDetails right darken-2 red-text switch right tooltipped hide-on-small-only'  data-position='bottom' data-delay='100' data-tooltip='Click to Delete Vehicle'>DELETE</a><a href='#' id='" + drwMemberVehicle["VehicleID"] + "' class='DeleteVehicleDetails right darken-2 red-text switch right tooltipped hide-on-med-and-up oDashboardIcons'  data-position='bottom' data-delay='100' data-tooltip='Click to Delete Vehicle'><i class=\"material-icons oDashboardIcons\"  style='margin:0px;'>&#xE872;</i></a><span class='right'>&nbsp;|&nbsp;</span><a href='#' id='" + drwMemberVehicle["VehicleID"] + "' class='editVehicleDetails right darken-2 blue-text switch right tooltipped hide-on-small-only'  data-position='bottom' data-delay='100' data-tooltip='Click to edit Vehicle Details'>EDIT</a><a href='#' id='" + drwMemberVehicle["VehicleID"] + "' class='editVehicleDetails right darken-2 blue-text switch right tooltipped hide-on-med-and-up oDashboardIcons'  data-position='bottom' data-delay='100' data-tooltip='Click to edit Vehicle Details'><i class=\"material-icons oDashboardIcons\" style='margin:0px;'>&#xE254;</i></a></div> " +
                                    "<div class='collapsible-body collapsibleBodyMargin'>" +
                                    "<div class='col s12 white-text'>" +
                                    "<h4>Details</h4>" +
                                    "<div class='center-align'>" +
                                    "<div class='row'><div class='col l3 m6 s12 center'><p class='noPadding2 vehicleDesc center'><b>Model Name:</b> " + System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(drwMemberVehicle["Model"].ToString()) + "</div>" +
                                    "<div class='col l3 m6 s12'><p class='noPadding2 vehicleDesc center'><b>Vehicle Type:</b> " + System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(drwMemberVehicle["VehicleType"].ToString()) + "</p></div>" +
                                    "<div class='col l3 m6 s12'><p class='noPadding2 vehicleDesc center'><b>Year Model: </b> " + drwMemberVehicle["YearModel"].ToString() + " </p></div>" +
                                    "<div class='col l3 m6 s12'><p class='noPadding2 vehicleDesc center'><b>Capacity: </b> " + System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(drwMemberVehicle["Maximumcapacity"].ToString()) + "</p></div></div></div>" +
                                    "<h4>Documents</h4>" +
                                    "</div>";
                        int cnt = 0;
                        foreach (DataRow drw in dtAttachment.Rows)
                        {
                            DataRow[] drwMemberVehicleAttachment = dtMemberVehicleID.Select("ReferenceID ='" + drwMemberVehicle["VehicleID"].ToString() + "' AND AttachmentTypeID='" + drw["AttachmentTypeID"].ToString() + "'");

                            if (cnt == 4)
                            {
                                cnt = 0;
                                strReturn += "</div>";
                                strReturn += "<div class='row'>";
                            }
                            else if (cnt == 0)
                            {
                                strReturn += "<div class='row'>";
                            }
                            
                            if (drwMemberVehicleAttachment.Length > 0)
                            {
                                strID = drwMemberVehicleAttachment[0].GetString("AttachmentID");
                                strAttachmentID = "id='" + drwMemberVehicleAttachment[0].GetString("AttachmentID") + "' name='Img'";
                                strAttachmentIDEdit = drwMemberVehicleAttachment[0].GetString("AttachmentID");

                                if (drwMemberVehicleAttachment[0].GetString("AttachmentFileName") != null)
                                {
                                    strHasImage = uhopWeb.Classes.UHopCore.GetPageURL() + "UploadedImages/Attachment/" + drwMemberVehicleAttachment[0].GetString("AttachmentFileName");
                                    strViewLarge = "<span class='card-title blue-text'><a id='" + strHasImage + "' class='viewLarge modal-trigger pink-text' href='#modal1'><i class='material-icons medium'>&#xE8A0;</i></a></span>";
                                    strImageMissing = "";
                                }

                                if (drw["hasExpirationDate"].ToString() == "1" && drw["hasDescription"].ToString() == "1")
                                {
                                    dtFormattedDate = Convert.ToDateTime(drwMemberVehicleAttachment[0].GetString("ExpirationDate"));
                                    strHasExpirationDate = "<div class='col s12'><p class='noPadding2' style='margin-top:10px;'><span><b>Expiration Date:</b> " + dtFormattedDate.ToString("MMM dd, yyyy") + "</span><br><br>";
                                    strHasDescription = "<b>Description: </b>" + drw["AttachmentTypeDescription"].ToString().ToUpper() + " IMAGE</p></div>";
                                }
                                else if (drw["hasExpirationDate"].ToString() == "1" && drw["hasDescription"].ToString() == "0")
                                {
                                    dtFormattedDate = Convert.ToDateTime(drwMemberVehicleAttachment[0].GetString("ExpirationDate"));
                                    strHasDescription = "";
                                    strHasExpirationDate = "<div class='col s12'><p class='noPadding2' style='margin-top:10px;'><span><b>Expiration Date:</b> " + dtFormattedDate.ToString("MMM dd, yyyy") + "</span></p></div>";
                                }
                                else if (drw["hasExpirationDate"].ToString() == "0" && drw["hasDescription"].ToString() == "1")
                                {
                                    strHasExpirationDate = "";
                                    strHasDescription = "<div class='col s12'><p class='noPadding2' style='margin-top:10px;'><b>Description: </b>" + drw["AttachmentTypeDescription"].ToString().ToUpper() + " IMAGE</p></div>";
                                }

                                if (drwMemberVehicleAttachment[0].GetString("ExpirationDate") != "" && drw["hasExpirationDate"].ToString() == "1")
                                {
                                    DateTime dtExpirationDate = Convert.ToDateTime(drwMemberVehicleAttachment[0].GetString("ExpirationDate"));
                                    if (dtExpirationDate < DateTime.Now)
                                    {
                                        strHasExpirationDate += "<div class='col s12'><p class='noPadding2 red-text' style='margin-top:10px;'>• " + drw["AttachmentTypeDescription"].ToString() + " is expired!</p></div>";
                                    }
                                }
                            }
                            else
                            {
                                strID = "";
                                strAttachmentID = "id='" + drw["AttachmentTypeID"].ToString() + "' name='NoImg'";
                                strViewLarge = "";
                                strHasDescription = "";
                                strHasExpirationDate = "";
                                strHasImage = uhopWeb.Classes.UHopCore.GetPageURL() + "Images/no-image.gif";
                                strImageMissing = "<p class='red-text center' style='padding:1rem'>YOUR " + drw["AttachmentTypeDescription"].ToString().ToUpper() + " IMAGE IS MISSING.</p>";
                                if (drw["hasExpirationDate"].ToString() == "1")
                                {
                                    strHasExpirationDate = "<div class='col s12'><p class='noPadding2 red-text' style='margin-top:10px;'>• Expiration Date is missing!</p></div>";
                                }
                            }

                            strReturn += "<div class='col l3 m6 s12'>" +
                                        "<div class='card'>" +
                                        "<div class='card-image'>" +
                                        "<img id='img" + strAttachmentIDEdit + "' class='responsive-img' src='" + strHasImage + "' style='height:300px'/>" +
                                        strViewLarge +
                                        "</div>" +
                                        "<div class='card-content noPadding2' style='height: 150px;'>" +
                                        strImageMissing +
                                        strHasExpirationDate +
                                        strHasDescription +
                                        "</div>" +
                                        "<div class='card-action'>" +
                                        "<a href='#modalEditImg' " + strAttachmentID + " class='editVehicle modal-trigger btn blue darken-2 white-text' innerhtml='" + drwMemberVehicle["VehicleID"] + "'>EDIT</a>" +
                                        "</div>" +
                                        "</div>" +
                                        "</div>";
                            cnt += 1;
                        }
                        strReturn += "</div></li>";
                    }
                }
                else { strReturn = "<center><h5 class='white-text'>No Vehicle found</h5></center>"; }
                Response.Write(strReturn);
            }
            
        }

        public void UpdateVehicleDetails()
        {
            if (txtBrandName.Text != "" && txtMaxCap.Text != "" && txtModelName.Text != "" && txtPlateNo.Text != "" && hdfVehicleID.Value != "")
            {
                ClsMemberVehicle objMemberVehicleType = new ClsMemberVehicle();
                objMemberVehicleType.Fill(objMemberVehicleType.TblMemberVehicle);
                DataRow drw1 = objMemberVehicleType.TblMemberVehicle.Select("VehicleID='" + hdfVehicleID.Value + "'")[0];
            
            string strVehicleTypeID = "";
            if (txtMaxCap.Text != "")
            {
                objMemberVehicleType.FillType();
                DataRow[] drwVehicleType = objMemberVehicleType.TblVehicleType.Select("MinimumCapacity<='" + txtMaxCap.Text + "' AND MaximumCapacity >='" + txtMaxCap.Text + "'");
                if (drwVehicleType.Length > 0)
                {
                    strVehicleTypeID = drwVehicleType[0].GetString("VehicleTypeID");
                }
                else
                {
                    DataRow[] drwVehicleTypeMax = objMemberVehicleType.TblVehicleType.Select("", "MaximumCapacity DESC");
                    strVehicleTypeID = drwVehicleTypeMax[0].GetString("VehicleTypeID");
                }
            }

            drw1.BeginEdit();
            drw1["BrandName"] = txtBrandName.Text;
            drw1["Model"] = txtModelName.Text;
            drw1["VehicleColor"] = txtVehicleColor.Text;
            drw1["PlateNumber"] = txtPlateNo.Text;
            drw1["VehicleType"] = ddlVehicelType.Text;
            drw1["YearModel"] = ddlYearModel.Text;
            drw1["MaximumCapacity"] = txtMaxCap.Text;
            drw1["Status"] = 0;
            drw1["VehicleTypeID"] = strVehicleTypeID;
            drw1.EndEdit();
            ClsMembers objMembers = new ClsMembers(strMemberID);
            objMembers.TblMembers = (DataSets.DSConfigs.MemberDataTable)ViewState["TblMembers"];

            DataRow drw = objMembers.TblMembers.Rows[0];
            drw.BeginEdit();
            drw["ModifiedBy"] = strMemberID;
            drw["ModifiedOn"] = DateTime.Now;
            drw.EndEdit();
            objMembers.Update();
            objMemberVehicleType.Update();

            string cc = "$(\".informChanges\").append(\"<span class='white-text'>Update successful!</span>\").css({ \"display\": \"block\" }).addClass(\"green\").delay(2000).fadeOut(\"slow\");";
            System.Text.StringBuilder sbs = new System.Text.StringBuilder();
            sbs.Append("<script type = 'text/javascript'>");
            sbs.Append("window.onload=function(){");
            sbs.Append(cc);
            sbs.Append("}");
            sbs.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sbs.ToString());                   

            }

        }

        protected void btnProceed_Click(object sender, EventArgs e)
        {
            if (strMemberID != "")
            {
                UpdateVehicleDetails();

            }

        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            if (strMemberID != "")
            {
                ClsMemberVehicle objMemberVehicle = new ClsMemberVehicle();
                ClsMemberVehicle objMemberVehicleAttachment = new ClsMemberVehicle();
                objMemberVehicle.Fill(objMemberVehicle.TblMemberVehicle);
                DataRow drw1 = objMemberVehicle.TblMemberVehicle.Select("VehicleID='" + hdfVehicleID.Value + "'")[0];
                drw1.BeginEdit();
                drw1["Enabled"] = 0;
                drw1["ModifiedBy"] = strMemberID;
                drw1["ModifiedOn"] = DateTime.Now;
                drw1.EndEdit();
                objMemberVehicle.Update();
            }
        }

        protected void btnUpdateAttachment_Click1(object sender, EventArgs e)
        {
            using (ClsMembers objMembers = new ClsMembers(strMemberID))
            {
                objMembers.FillAttachmentType();
                
                DataTable dtAttachmentType = objMembers.TblAttachmentType;
                DateTime dtExpirationDate;
                DataRow drwRequiredAttachment = dtAttachmentType.Select("AttachmentTypeID ='" + hdfAttachmentTypeID.Value + "' AND IsForVehicle = 1")[0];
                ClsMemberVehicle objMemberVehicle = new ClsMemberVehicle();
                string strClear = "";

                if (drwRequiredAttachment.GetString("HasExpirationDate") == "1")
                    {
                        if (expirationDate.Value != "")
                        {
                            dtExpirationDate = Convert.ToDateTime(expirationDate.Value);
                        }
                    }

                        if (hdfAttachmentID.Value == "")
                        {
                            ClsAutoNumber objAutoNumber = new ClsAutoNumber();

                            DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='" + hdfAttachmentTypeID.Value + "'");
                            DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                            string strFileName = "O" + strMemberID + drwAttachmentType[0].GetString("Prefix") + "." + uploadAttachment.PostedFile.FileName.Split('.')[(uploadAttachment.PostedFile.FileName.Split('.').Length - 1)];
                            drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                            drwAttachment.SetString("ReferenceID", hdfVehicleID.Value);
                            if (expirationDate.Value != "") { drwAttachment.SetDateTime("ExpirationDate", Convert.ToDateTime(expirationDate.Value)); }
                            drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                            drwAttachment.SetString("Status", "1");
                            drwAttachment.SetString("IsForDriver", "0");
                            drwAttachment.SetString("AttachmentTypeID", hdfAttachmentTypeID.Value);
                            if(ValidateFile())
                            {
                                drwAttachment.SetString("AttachmentFilename", strFileName);
                                uploadAttachment.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                            }
                            objMembers.TblAttachment.Rows.Add(drwAttachment);
                            objMembers.UpdateAttachment();

                        }
                        else
                        {
                            strClear = "";
                            objMembers.FillAttachment();
                            objMemberVehicle.FillViaReferenceID(objMemberVehicle.TblMemberVehicleAttachment, strMemberID);
                            DataRow drwVehicleAttachment = objMemberVehicle.TblMemberVehicleAttachment.Select("AttachmentID='" + hdfAttachmentID.Value + "'")[0];
                            //DataRow drwVehicleAttachment = ClsMemberVehicle.TblMemberVehicleAttachment.Where(t => t.AttachmentID == hdfAttachmentID.Value).FirstOrDefault();
                            drwVehicleAttachment.BeginEdit();
                            if (ValidateFile())
                            {
                                uploadAttachment.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + drwVehicleAttachment["AttachmentFileName"]);
                                strClear = drwVehicleAttachment["AttachmentFileName"].ToString();
                            }
                            if (expirationDate.Value != "") { drwVehicleAttachment.SetDateTime("ExpirationDate", Convert.ToDateTime(expirationDate.Value)); }
                            drwVehicleAttachment.EndEdit();
                            objMemberVehicle.UpdateVehicleAttachment();
                            
                        }


                        objMemberVehicle.FillViaID(objMemberVehicle.TblMemberVehicle, strMemberID);
                        var MemberVehicle = objMemberVehicle.TblMemberVehicle.Where(t => t.VehicleID == hdfVehicleID.Value).FirstOrDefault();
                        MemberVehicle.BeginEdit();
                        MemberVehicle.Status = "0";
                        MemberVehicle.EndEdit();
                        objMemberVehicle.Update();


                        //UploadedImages/Attachment/O20151112000003REARVIEW.png
                        if (strClear != "")
                        {
                            System.Text.StringBuilder sbs = new System.Text.StringBuilder();
                            sbs.Append("<script type = 'text/javascript'>");
                            sbs.Append("window.onload=function(){");
                            sbs.Append("$('#" + "img" + hdfAttachmentID.Value + "').attr('src', '" + UHopCore.GetPageURL() + "UploadedImages/Attachment/" + strClear + "?" + DateTime.Now.Ticks.ToString() + "');");
                            //sbs.Append("$('#imgLarge').attr('src', '" + UHopCore.GetPageURL() + "UploadedImages/Attachment/" + strClear + "?" + DateTime.Now.Ticks.ToString() + "');");
                            sbs.Append("};");
                            sbs.Append("</script>");
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sbs.ToString());
                        }

                        string cc = "$(\".informChanges\").append(\"<span class='white-text'>Update successful!</span>\").css({ \"display\": \"block\" }).addClass(\"green\").delay(2000).fadeOut(\"slow\");";
                        System.Text.StringBuilder SB = new System.Text.StringBuilder();
                        SB.Append("<script type = 'text/javascript'>");
                        SB.Append("window.onload=function(){");
                        SB.Append(cc);
                        SB.Append("}");
                        SB.Append("</script>");
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", SB.ToString());       
                    }

        }


        public Boolean ValidateFile()
        {
            Boolean blnReturn = false;
            FileUpload fu = uploadAttachment;
            if (uploadAttachment.HasFile)
            {                
                String fileExtension = System.IO.Path.GetExtension(fu.FileName).ToLower();
                String[] allowedExtensions = { ".png", ".jpeg", ".jpg", ".PNG", ".JPEG", ".JPG",".jp2",".JP2",".jpx",".JPX",".j2k",".J2K",".j2c",".J2C" };

                if (uploadAttachment.FileContent.Length > 2000000)
                {
                    blnReturn = false;
                }

                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension.ToLower() == allowedExtensions[i].ToLower())
                    {
                        blnReturn = true;
                    }
                    
                }
            }
            return blnReturn;       
        }


    }
}