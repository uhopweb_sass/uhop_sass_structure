﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using uhopWeb.Classes;
using System.Web.UI.HtmlControls;

namespace uhopWeb.Forms.Operators.Vehicle
{
    public partial class AddNewVehicle : System.Web.UI.Page
    {
        public static string strMemberID = "";
        //private void GenerateVehicleList(string xMemberID)
        //{
        //    string VehicleList = "";

        //    foreach (DataRow drw in ClsMemberVehicle.TblMemberVehicle.Select("MemberID='" + xMemberID + "'"))
        //    {
        //        VehicleList += "<li><div class='collapsible-header'><i class='material-icons'>filter_drama</i>" + drw.GetString("BrandName") + " " + drw.GetString("Model") + "(" + drw.GetString("VahiclePlateNumber") + ")" + "</div>";
        //        VehicleList += "<div class='collapsible-body'><img data-caption='Front View of " + drw.GetString("BrandName") + " " + drw.GetString("Model") + "(" + drw.GetString("VahiclePlateNumber") + ")" + "' src='../../../UploadedImages/Operator/Vehicle/" + drw.GetString("FrontView") + "' class='VehicleImg materialboxed'/>";
        //        VehicleList += "<img data-caption='Rear View of " + drw.GetString("BrandName") + " " + drw.GetString("Model") + "(" + drw.GetString("VahiclePlateNumber") + ")" + "' src='../../../UploadedImages/Operator/Vehicle/" + drw.GetString("RearView") + "' class='VehicleImg materialboxed'/>";
        //        VehicleList += "<img data-caption='Left View of " + drw.GetString("BrandName") + " " + drw.GetString("Model") + "(" + drw.GetString("VahiclePlateNumber") + ")" + "' src='../../../UploadedImages/Operator/Vehicle/" + drw.GetString("LeftView") + "' class='VehicleImg materialboxed'/>";
        //        VehicleList += "<img data-caption='Right View of " + drw.GetString("BrandName") + " " + drw.GetString("Model") + "(" + drw.GetString("VahiclePlateNumber") + ")" + "' src='../../../UploadedImages/Operator/Vehicle/" + drw.GetString("RightView") + "' class='VehicleImg materialboxed'/>";
        //        VehicleList += "<img data-caption='Front Interior View of " + drw.GetString("BrandName") + " " + drw.GetString("Model") + "(" + drw.GetString("VahiclePlateNumber") + ")" + "' src='../../../UploadedImages/Operator/Vehicle/" + drw.GetString("FrontInterior") + "' class='VehicleImg materialboxed'/>";
        //        VehicleList += "<img data-caption='Rear Interior View of " + drw.GetString("BrandName") + " " + drw.GetString("Model") + "(" + drw.GetString("VahiclePlateNumber") + ")" + "' src='../../../UploadedImages/Operator/Vehicle/" + drw.GetString("RearInterior") + "' class='VehicleImg materialboxed'/>";
        //        VehicleList += "<img data-caption='Official Receipt of " + drw.GetString("BrandName") + " " + drw.GetString("Model") + "(" + drw.GetString("VahiclePlateNumber") + ")" + "' src='../../../UploadedImages/Operator/Vehicle/" + drw.GetString("xOR") + "' class='VehicleImg materialboxed'/>";
        //        VehicleList += "<img data-caption='Certificate of Registration of " + drw.GetString("BrandName") + " " + drw.GetString("Model") + "(" + drw.GetString("VahiclePlateNumber") + ")" + "' src='../../../UploadedImages/Operator/Vehicle/" + drw.GetString("xCR") + "' class='VehicleImg materialboxed'/>";
        //        VehicleList += "<img data-caption='Insurance of " + drw.GetString("BrandName") + " " + drw.GetString("Model") + "(" + drw.GetString("VahiclePlateNumber") + ")" + "' src='../../../UploadedImages/Operator/Vehicle/" + drw.GetString("Insurance") + "' class='VehicleImg materialboxed'/>";
        //        VehicleList += "<br style='clear:left;'/></div></li>";
        //    }


        //    MemberVehicleList.InnerHtml = VehicleList;
        //}


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clsAuthenticator.Authenticate();
                string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                strMemberID = strDecrypted;
                LoadYearModel();
                UHopCore.Title = "Add New Vehicle";
                ViewState["MemberID"] = strDecrypted;
                strMemberID = strDecrypted;

                ClsMembers objMembers = new ClsMembers(strMemberID);
                objMembers.Fill(strMemberID);
                var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                if (MemberDetails.Status == "0")
                {
                    Response.Redirect("../Maintenance/OperatorProfile.aspx");
                }
            }                
 
        }

        public void LoadYearModel()
        {
            int y = 0;
            for (int x = 2013; x <= DateTime.Now.Year + 1; x++)
            {
                ddlYearModel.Items.Insert(y, new ListItem(Convert.ToString(x), Convert.ToString(x)));
                ddlYearModel.SelectedIndex = 0;
                y += 1;
            }
        }

        public Boolean ValidateFile(FileUpload pfu)
        {
            Boolean blnReturn = false;
            FileUpload fu = pfu;
            if (pfu.HasFile)
            {
                String fileExtension = System.IO.Path.GetExtension(fu.FileName).ToLower();
                String[] allowedExtensions = { ".png", ".jpeg", ".jpg", ".PNG", ".JPEG", ".JPG", ".jp2", ".JP2", ".jpx", ".JPX", ".j2k", ".J2K", ".j2c", ".J2C" };

                if (pfu.FileContent.Length > 2000000)
                {
                    blnReturn = false;
                }

                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        blnReturn = true;
                    }

                }
            }
            return blnReturn;
        }

        protected void btnAddVehicle_Click(object sender, EventArgs e)
        {
            try
            {
                if (strMemberID != "")
                {
                    ClsMembers objMembers = new ClsMembers(strMemberID);
                    ClsAutoNumber objAutoNumber = new ClsAutoNumber();
                    string strVehicleID = objAutoNumber.GenerateAutoNumber("VehicleID", "000000").ToString();
                    objMembers.FillAttachmentType();
                    objMembers.FillAttachment();

                    if (ValidateFile(FUFrontView))
                    {
                        DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000003'");
                        DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                        string strFileName = "O" + strMemberID + drwAttachmentType[0].GetString("Prefix") + "." + FUFrontView.PostedFile.FileName.Split('.')[(FUFrontView.PostedFile.FileName.Split('.').Length - 1)];
                        drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                        drwAttachment.SetString("AttachmentTypeID", "20150923000003");
                        drwAttachment.SetString("ReferenceID", strVehicleID);
                        drwAttachment.SetString("AttachmentFileName", strFileName);
                        drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                        drwAttachment.SetString("Status", "1");
                        drwAttachment.SetString("IsForDriver", "0");
                        FUFrontView.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                        objMembers.TblAttachment.Rows.Add(drwAttachment);
                        objMembers.UpdateAttachment();
                    }


                    if (ValidateFile(FURearView))
                    {
                        DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000004'");
                        DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                        string strFileName = "O" + strMemberID + drwAttachmentType[0].GetString("Prefix") + "." + FURearView.PostedFile.FileName.Split('.')[(FURearView.PostedFile.FileName.Split('.').Length - 1)];
                        drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                        drwAttachment.SetString("AttachmentTypeID", "20150923000004");
                        drwAttachment.SetString("ReferenceID", strVehicleID);
                        drwAttachment.SetString("AttachmentFileName", strFileName);
                        drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                        drwAttachment.SetString("Status", "1");
                        drwAttachment.SetString("IsForDriver", "0");
                        FURearView.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                        objMembers.TblAttachment.Rows.Add(drwAttachment);
                        objMembers.UpdateAttachment();
                    }

                    if (ValidateFile(FULeftView))
                    {
                        DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000005'");
                        DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                        string strFileName = "O" + strMemberID + drwAttachmentType[0].GetString("Prefix") + "." + FULeftView.PostedFile.FileName.Split('.')[(FULeftView.PostedFile.FileName.Split('.').Length - 1)];
                        drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                        drwAttachment.SetString("AttachmentTypeID", "20150923000005");
                        drwAttachment.SetString("ReferenceID", strVehicleID);
                        drwAttachment.SetString("AttachmentFileName", strFileName);
                        drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                        drwAttachment.SetString("Status", "1");
                        drwAttachment.SetString("IsForDriver", "0");
                        FULeftView.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                        objMembers.TblAttachment.Rows.Add(drwAttachment);
                        objMembers.UpdateAttachment();
                    }

                    if (ValidateFile(FURightView))
                    {
                        DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000006'");
                        DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                        string strFileName = "O" + strMemberID + drwAttachmentType[0].GetString("Prefix") + "." + FURightView.PostedFile.FileName.Split('.')[(FURightView.PostedFile.FileName.Split('.').Length - 1)];
                        drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                        drwAttachment.SetString("AttachmentTypeID", "20150923000006");
                        drwAttachment.SetString("ReferenceID", strVehicleID);
                        drwAttachment.SetString("AttachmentFileName", strFileName);
                        drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                        drwAttachment.SetString("Status", "1");
                        drwAttachment.SetString("IsForDriver", "0");
                        FURightView.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                        objMembers.TblAttachment.Rows.Add(drwAttachment);
                        objMembers.UpdateAttachment();
                    }

                    if (ValidateFile(FUFrontInterior))
                    {
                        DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000007'");
                        DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                        string strFileName = "O" + strMemberID + drwAttachmentType[0].GetString("Prefix") + "." + FUFrontInterior.PostedFile.FileName.Split('.')[(FUFrontInterior.PostedFile.FileName.Split('.').Length - 1)];
                        drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                        drwAttachment.SetString("AttachmentTypeID", "20150923000007");
                        drwAttachment.SetString("ReferenceID", strVehicleID);
                        drwAttachment.SetString("AttachmentFileName", strFileName);
                        drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                        drwAttachment.SetString("Status", "1");
                        drwAttachment.SetString("IsForDriver", "0");
                        FUFrontInterior.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                        objMembers.TblAttachment.Rows.Add(drwAttachment);
                        objMembers.UpdateAttachment();
                    }

                    if (ValidateFile(FURearInterior))
                    {
                        DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000008'");
                        DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                        string strFileName = "O" + strMemberID + drwAttachmentType[0].GetString("Prefix") + "." + FURearInterior.PostedFile.FileName.Split('.')[(FURearInterior.PostedFile.FileName.Split('.').Length - 1)];
                        drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                        drwAttachment.SetString("AttachmentTypeID", "20150923000008");
                        drwAttachment.SetString("ReferenceID", strVehicleID);
                        drwAttachment.SetString("AttachmentFileName", strFileName);
                        drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                        drwAttachment.SetString("Status", "1");
                        drwAttachment.SetString("IsForDriver", "0");
                        FURearInterior.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                        objMembers.TblAttachment.Rows.Add(drwAttachment);
                        objMembers.UpdateAttachment();
                    }

                    if (ValidateFile(FUOr))
                    {
                        DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000009'");
                        DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                        string strFileName = "O" + strMemberID + drwAttachmentType[0].GetString("Prefix") + "." + FUOr.PostedFile.FileName.Split('.')[(FUOr.PostedFile.FileName.Split('.').Length - 1)];
                        drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                        drwAttachment.SetString("AttachmentTypeID", "20150923000009");
                        drwAttachment.SetString("ReferenceID", strVehicleID);
                        drwAttachment.SetString("AttachmentFileName", strFileName);
                        drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                        drwAttachment.SetString("Status", "1");
                        drwAttachment.SetString("IsForDriver", "0");
                        drwAttachment.SetString("ExpirationDate", txtExpiryDateOR.Text);
                        FUOr.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                        objMembers.TblAttachment.Rows.Add(drwAttachment);
                        objMembers.UpdateAttachment();
                    }

                    if (ValidateFile(FUCR))
                    {
                        DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000010'");
                        DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                        string strFileName = "O" + strMemberID + drwAttachmentType[0].GetString("Prefix") + "." + FUCR.PostedFile.FileName.Split('.')[(FUCR.PostedFile.FileName.Split('.').Length - 1)];
                        drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                        drwAttachment.SetString("AttachmentTypeID", "20150923000010");
                        drwAttachment.SetString("ReferenceID", strVehicleID);
                        drwAttachment.SetString("AttachmentFileName", strFileName);
                        drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                        drwAttachment.SetString("Status", "1");
                        drwAttachment.SetString("IsForDriver", "0");
                        FUCR.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                        objMembers.TblAttachment.Rows.Add(drwAttachment);
                        objMembers.UpdateAttachment();
                    }

                    if (ValidateFile(FUInsurance))
                    {
                        DataRow[] drwAttachmentType = objMembers.TblAttachmentType.Select("AttachmentTypeID='20150923000011'");
                        DataRow drwAttachment = objMembers.TblAttachment.NewRow();
                        string strFileName = "O" + strMemberID + drwAttachmentType[0].GetString("Prefix") + "." + FUInsurance.PostedFile.FileName.Split('.')[(FUInsurance.PostedFile.FileName.Split('.').Length - 1)];
                        drwAttachment.SetString("AttachmentID", objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString());
                        drwAttachment.SetString("AttachmentTypeID", "20150923000011");
                        drwAttachment.SetString("ReferenceID", strVehicleID);
                        drwAttachment.SetString("AttachmentFileName", strFileName);
                        drwAttachment.SetString("Description", drwAttachmentType[0].GetString("AttachmentTypeDescription"));
                        drwAttachment.SetString("Status", "1");
                        drwAttachment.SetString("IsForDriver", "0");
                        drwAttachment.SetString("ExpirationDate", txtExpiryDateInsurance.Text);
                        FUInsurance.PostedFile.SaveAs(Server.MapPath(UHopCore.GetUploadImagesRoot()) + strFileName);
                        objMembers.TblAttachment.Rows.Add(drwAttachment);
                        objMembers.UpdateAttachment();
                    }


                    ClsMemberVehicle objMemberVehicle = new ClsMemberVehicle();
                    DataRow drwVehicle = objMemberVehicle.TblMemberVehicle.NewRow();

                    string strVehicleTypeID = "";
                    if (txtMaxCap.Text != "")
                    {
                        objMemberVehicle.FillType();
                        DataRow[] drwVehicleType = objMemberVehicle.TblVehicleType.Select("MinimumCapacity<='" + txtMaxCap.Text + "' AND MaximumCapacity >='" + txtMaxCap.Text + "'");
                        if (drwVehicleType.Length > 0)
                        {
                            strVehicleTypeID = drwVehicleType[0].GetString("VehicleTypeID");
                        }
                        else
                        {
                            DataRow[] drwVehicleTypeMax = objMemberVehicle.TblVehicleType.Select("", "MaximumCapacity DESC");
                            strVehicleTypeID = drwVehicleTypeMax[0].GetString("VehicleTypeID");
                        }
                    }


                    drwVehicle.SetString("VehicleID", strVehicleID);
                    drwVehicle.SetString("MemberID", strMemberID);
                    drwVehicle.SetString("VehicleTypeID", strVehicleTypeID);
                    drwVehicle.SetString("BrandName", txtBrandName.Text);
                    drwVehicle.SetString("Model", txtModelName.Text);
                    drwVehicle.SetString("VehicleColor", txtVehicleColor.Text);
                    drwVehicle.SetString("YearModel", ddlYearModel.SelectedValue.ToString());
                    drwVehicle.SetString("PlateNumber", txtPlateNo.Text);
                    drwVehicle.SetString("VehicleType", ddlVehicelType.SelectedValue.ToString());
                    drwVehicle.SetInt32("MaximumCapacity", Convert.ToInt32(txtMaxCap.Text));
                    drwVehicle.SetString("Enabled", "1");
                    drwVehicle.SetString("Status", "1");
                    drwVehicle.SetString("CreatedBy", strMemberID);
                    drwVehicle.SetString("ModifiedBy", strMemberID);
                    drwVehicle.SetDateTime("ModifiedOn", DateTime.Now);
                    drwVehicle.SetDateTime("CreatedOn", DateTime.Now);

                    //saving member vehicle
                    objMemberVehicle.TblMemberVehicle.Rows.Add(drwVehicle);
                    objMemberVehicle.Update();

                    Response.Redirect("ViewVehicle.aspx");
                    //MemberVehicleList.Style["display"] = "block";
                }
            }
            catch
            {

            }
        }

    }
}