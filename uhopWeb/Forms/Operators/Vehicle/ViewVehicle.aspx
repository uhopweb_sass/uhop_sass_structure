﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Operators/Operators.Master" AutoEventWireup="true" CodeBehind="ViewVehicle.aspx.cs" Inherits="uhopWeb.Forms.Operators.Vehicle.ViewVehicle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:HiddenField ID="hdfAttachmentID" runat="server" />
    <asp:HiddenField ID="hdfAttachmentTypeID" runat="server" />
    <asp:HiddenField ID="hdfVehicleID" runat="server" />
    <asp:HiddenField ID="hdfBrandName" runat="server" />
    <asp:HiddenField ID="hdfModel" runat="server" />
    <asp:HiddenField ID="hdfPlateNumber" runat="server" />
    <asp:HiddenField ID="hdfVehicleType" runat="server" />
    <asp:HiddenField ID="hdfVehicleColor" runat="server" />
    <asp:HiddenField ID="hdfMaxCapacity" runat="server" />
    <asp:HiddenField ID="hdfYearModel" runat="server" />
    <asp:HiddenField ID="hdfImg" runat="server" />

    <%-- View Large Modal --%>
    <div id="modal1" class="modal">
        <div class="modal-content viewLargeModal" style="padding: 0px;">
            <img class="responsive-img" id="imgLarge"/>
        </div>
        <div class="modal-footer center">
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn pink accent-2">Close</a>
        </div>
    </div>
    <%-- End View Large Modal --%>

    <%-- Div Proceed --%>
    <div id="divRemoveVehicle" class="divRemoveVehicle divErrorTop center white-text" runat="server" style="background-color: rgb(2, 2, 99);">
        <span class="Red-text">Are you sure to want to delete [<label class="lblVehicleNameEdit cyan-text" style="font-size: 15px;"></label> - <label id="Label1" class="lblModelName cyan-text" style="font-size: 15px;"></label>]?</span><br />
        <asp:Button ID="btnRemove" runat="server" Text="YES" CssClass="btn green darken-2" OnClick="btnRemove_Click" Style="height: 35px; width: 90px;" />
        <a href="#" class="removeVehicleCancel  btn pink accent-2 white-text" style="height: 35px; width: 90px;">NO</a>
    </div>
    <%-- End Div Proceed --%>

    <div class="informChanges divErrorTop center white-text"></div>

    <!-- Div Cover-->
    <div class="divVehicleDetails divCover" id="divVehicleDetails" runat="server" style="display: none; background-color: rgba(2, 2, 99, 0.90); opacity: 1;">

        <%-- Div Proceed --%>
        <div id="divEnabled" class="divEnabled divErrorTop center red-text" runat="server" style="margin-bottom: 10px; background-color: black;">
            <span style="margin-bottom: 5px;"><b>Saving changes will set your vehicle [<label class="lblVehicleNameEdit cyan-text" style="font-size: 15px;"></label> - <label id="lblModelName" class="lblModelName cyan-text" style="font-size: 15px;"></label>] for approval.</b></span><br />
            <asp:Button ID="btnProceed" runat="server" Text="UPDATE" CssClass="btnProceed btn btn-small btnEditV green darken-2" OnClick="btnProceed_Click" />
            <a href="#" class="editVehicleCancel  btn btn-small btnEditV pink accent-2 white-text">CANCEL</a>
        </div>
        <%-- End Div Proceed --%>

        <div class="divHider">

            <div class="container" style="margin-top: 20px;">
                <div class="row">
                    <div class="col s12 center">
                        <h5 class="white-text">EDIT VEHICLE</h5>
                        <br />
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col l4 s12 white-text">
                        <i class="material-icons prefix">&#xE88E;</i>
                        <asp:TextBox ID="txtBrandName" TextMode="SingleLine" CssClass="large validate" runat="server" MaxLength="20"></asp:TextBox>
                        <label for="txtBrandName" id="BrandName" class="lblFor">
                            Brand Name
                       
                        </label>
                        <div class="red-text center">
                            <asp:RequiredFieldValidator ControlToValidate="txtBrandName" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Brand name is required" CssClass="Validators" Display="Dynamic" ValidationGroup="EditVehicle">Brand name is required</asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="input-field col l4 s12 white-text">
                        <i class="material-icons prefix">&#xE88E;</i>
                        <asp:TextBox ID="txtModelName" TextMode="SingleLine" CssClass="large validate" runat="server" MaxLength="20"></asp:TextBox>
                        <label for="txtModelName" id="ModelName" class="lblFor">
                            Model Name
                       
                        </label>
                        <div class="red-text center">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Model name is required" SetFocusOnError="true" ControlToValidate="txtModelName" ValidationGroup="EditVehicle" CssClass="Validators">Model name is required</asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="input-field col l4 s12 white-text">
                        <i class="material-icons prefix">&#xE88E;</i>
                        <asp:TextBox ID="txtPlateNo" TextMode="SingleLine" CssClass="large validate" runat="server" MaxLength="8"></asp:TextBox>
                        <label for="txtPlateNo" id="PlateNo" class="lblFor">
                            Plate Number
                       
                        </label>
                        <div class="red-text center">
                            <asp:RequiredFieldValidator ControlToValidate="txtPlateNo" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Plate number is required" CssClass="Validators" Display="Dynamic" ValidationGroup="EditVehicle">Plate number is required</asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="row white-text">

                    <div class="input-field col l3 m6 s12" style="margin-bottom:20px;">
                        <h6 class="type left grey-text">Vehicle type</h6>
                        <asp:DropDownList ID="ddlVehicelType" runat="server" CssClass="browser-default black-text">
                            <asp:ListItem Text="Car" Value="Car"></asp:ListItem>
                            <asp:ListItem Text="Van" Value="Van"></asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <!-- Vehicle year model -->
                    <div class="input-field col l3 m6 s12" style="margin-bottom:20px;">
                        <h6 class="type left grey-text">Model year</h6>
                        <asp:DropDownList ID="ddlYearModel" runat="server" CssClass="browser-default black-text">
                        </asp:DropDownList>
                    </div>
                     <!-- End of Vehicle year model -->

                    <div style="padding-top: 20px;">
                        <div class="input-field col l3 m6 s12 white-text">
                            <i class="material-icons prefix">&#xE88E;</i>
                            <asp:TextBox ID="txtMaxCap" TextMode="SingleLine" CssClass="large validate" runat="server" MaxLength="2"></asp:TextBox>
                            <label for="txtMaxCap" id="MaxCap" class="lblFor">
                                Capacity                               
                            </label>
                            <div class="red-text center">
                                <asp:RequiredFieldValidator ControlToValidate="txtMaxCap" ID="RequiredFieldValidator4" runat="server" ErrorMessage="Max capacity is required" CssClass="Validators" Display="Dynamic" ValidationGroup="EditVehicle">Max Capacity is required</asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>

                        <div class="input-field col l3 m6 s12 white-text">
                            <i class="material-icons prefix">&#xE88E;</i>
                            <asp:TextBox ID="txtVehicleColor" TextMode="SingleLine" CssClass="large validate" runat="server" MaxLength="30"></asp:TextBox>
                            <label for="txtVehicleColor" id="Label2" class="lblFor">
                                Vehicle Color                               
                            </label>
                            <div class="red-text center">
                                <asp:RequiredFieldValidator ControlToValidate="txtVehicleColor" ID="RequiredFieldValidator5" runat="server" ErrorMessage="Vehicle color is required" CssClass="Validators" Display="Dynamic" ValidationGroup="EditVehicle">Vehicle color is required</asp:RequiredFieldValidator>
                            </div>
                        </div>

                </div>
                <div class="row center">
                    <%--<asp:Button ID="btnSave" ValidationGroup="EditVehicle" runat="server" Text="SAVE" CssClass="btn btn-large Green darken-2" OnClick="btnSave_Click" />--%>
                    <a href="#" id="aSaveChanges" class="editVehicleSave btn blue darken-2 white-text">SAVE</a>
                    <a href="#" class="editVehicleBack btn pink accent-2 white-text">BACK</a>
                </div>
            </div>
            <!-- END of Container -->
        </div>
        <!-- END of divHider -->
    </div>
    <!-- END of div cover-->

    <div class="container col s12">
        <br />
        <div class="row">
            <div class="col m7 s12 ">
                <h4 class="operatorMasterHeader white-text noMargin"><i class="material-icons small">&#xE531;</i>&nbsp;&nbsp;&nbsp; Vehicle Dashboard</h4>
            </div>
            <div class="col m5 s12 right-align">
                <%--<ul class="tabs" id="vehicleTabs">
                    <li class="tab col s3"><a class="active" href="<%=uhopWeb.Classes.UHopCore.GetPageURL() %>Forms/Operators/Vehicle/ViewVehicle.aspx">View Vehicle/s</a></li>
                    <li class="tab col s3"><a href="<%=uhopWeb.Classes.UHopCore.GetPageURL() %>Forms/Operators/Vehicle/AddNewVehicle.aspx" target="_blank">Add Vehicle</a></li>
                </ul>--%>
                <a href="<%=uhopWeb.Classes.UHopCore.GetPageURL() %>Forms/Operators/Vehicle/AddNewVehicle.aspx" class="waves-effect waves-light btn-large pink accent-2 hide-on-med-and-down" style="margin-top: -10px;"><i class="material-icons left">&#xE145;</i>Add Vehicle</a>
                <a href="<%=uhopWeb.Classes.UHopCore.GetPageURL() %>Forms/Operators/Vehicle/AddNewVehicle.aspx" class="waves-effect waves-light btn pink accent-2 hide-on-large-only"><i class="material-icons left">&#xE145;</i>Add Vehicle</a>
            </div>
        </div>
        <div class="divider"></div>
        <br />


        <!-- Modal -->
        <div id="modalEditImg" class="modal" style="width: 500px; overflow-x: hidden;">
            <div class="modal-content" style="padding: 0px;">
                <div class="col s12 center">
                    <h5>Upload Your Attachment</h5>
                </div>
                <div class="row center">
                    <div class="col s12">
                        <img id="imgEdit" class="imgEdit responsive-img" src="<%=uhopWeb.Classes.UHopCore.GetPageURL() %>Images/no-image.gif" style="height: 300px" />
                    </div>
                    <div class="col s12">
                        <asp:FileUpload ID="uploadAttachment" runat="server" class="fuAttachment uploadAttachment black-text" Style="margin-left: 84px;" />
                    </div>
                    <asp:RequiredFieldValidator ControlToValidate="uploadAttachment" ID="rfvRequiredAttachment" runat="server" ErrorMessage="" CssClass="Validators red-text" Display="Dynamic" ValidationGroup="UpdateAttachment">Image is required!</asp:RequiredFieldValidator><br />
                    <asp:CustomValidator runat="server" ID="CustomValidator1" ForeColor="Red" ControlToValidate="uploadAttachment" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="UpdateAttachment" />
                    <asp:RegularExpressionValidator ID="revFileType" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$" ControlToValidate ="uploadAttachment" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file." Display="Dynamic" ValidationGroup="UpdateAttachment" />
                </div>
                <div class="divEditAttachmentDateExpiry row center" style="margin-left: 20px; margin-right: 20px; margin-bottom: 0px;">
                    <div id="expirationDateDiv">
                        <div class="col m4 s12">
                            <p>Expiration Date:</p>
                        </div>
                        <div class="col m8 s12">
                            <input type="date" id="expirationDate" runat="server" name="expirationDate" />
                            <asp:HiddenField ID="hdfED" runat="server" />
                        </div>
                        <span id="dateValidator" class="dateValidator red-text" style="display:none;">Date must be greater than today.</span>
                        <asp:RequiredFieldValidator ControlToValidate="expirationDate" ID="rfvExpirationDate" runat="server" ErrorMessage="" CssClass="Validators red-text" Display="Dynamic" ValidationGroup="UpdateAttachment">Expiration Date is required!</asp:RequiredFieldValidator><br />
                    </div>
                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" Display="Static"></asp:RegularExpressionValidator>--%>
                </div>
                <div class="row" style="margin-left: 20px; margin-right: 20px; margin-bottom: 0px;">
                    <div class="col m4 s12">
                        <p>Description:</p>
                    </div>
                    <div class="col m8 s12" style="margin-top: 15px;">
                        <%--<label id="lblAttachmentDescription" class="lblAttachmentDescription" style="font-size: 15px; color: black;"></label>--%>
                        <span id="lblAttachmentDescription" class="lblAttachmentDescription" style="font-size: 15px; color: black;"></span>
                    </div>
                </div>
            </div>
            <br />
            <div class="divider"></div>
            <div class="modal-footer">
                <a href="#!" class="left modal-action modal-close waves-effect waves-red btn red accent-2">Cancel</a>
                <asp:LinkButton runat="server" ID="btnUpdateAttachment" class="btnUpdateAttachment modal-action waves-effect waves-green btn blue darken-2" ValidationGroup="UpdateAttachment"  OnClick="btnUpdateAttachment_Click1">Update Attachment</asp:LinkButton>
                
            </div>
        </div>
        <!-- END of modal -->

        <ul class="collapsible" id="ulVehicles" runat="server" data-collapsible="accordion">

            <% LoadVehicles(); %>
        </ul>
        <!-- END of <ul> collapsible-->
    </div>
    <!-- END of container -->
    <script src="../../../Scripts/jquery-2.1.4.min.js"></script>
    <script>

        $('.btnUpdateAttachment').click(function () {
            var blnreturn = true;
            
            //if (document.getElementById("dateValidator").style.display == "hidden")
            if ($("input[id*='hdfED']").val() == "True")
            {
                blnreturn = false;
            }

            $.each(Page_Validators, function (index, validator) {
                if (!validator.isvalid) {
            
                }

            });
            return blnreturn;
        });

        function Upload(sender, args) {
            
            
            var fileUpload = document.getElementById(sender.controltovalidate);
            if (typeof (fileUpload.files) != "undefined") {
                var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
                var maxFileSize = 1000
                if (size < maxFileSize) {
                    args.IsValid = true;
                } else {
                    $(fileUpload).closest("div").css({ "display": "block" });
                    args.IsValid = false;
                    return;
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        }

        $(".editVehicleDetails").click(function () {
            var VehicleID = $(this).attr('id');
            $("input[id*='hdfVehicleID']").val(VehicleID);
            $.ajax({
                type: "POST",
                url: "ViewVehicle.aspx/PopulateVehicleDetails",
                data: "{'pVehicleID':'" + VehicleID + "'}",
                contentType: "application/json",
                dataType: "json",
                success: function (response) {
                    try {
                        var res = response.d;
                        $.each(res, function (index, VehicleDetails) {
                            $("#ContentPlaceHolder1_txtBrandName").val(VehicleDetails.BrandName);
                            $("#ContentPlaceHolder1_txtModelName").val(VehicleDetails.ModelName);
                            $("#ContentPlaceHolder1_txtPlateNo").val(VehicleDetails.PlateNumber);
                            $("#ContentPlaceHolder1_txtMaxCap").val(VehicleDetails.MaxCapacity);
                            $("#ContentPlaceHolder1_txtVehicleColor").val(VehicleDetails.VehicleColor);
                            $("#ContentPlaceHolder1_ddlVehicelType").val(VehicleDetails.VehicleType);
                            $("#ContentPlaceHolder1_ddlYearModel").val(VehicleDetails.YearModel);
                            
                            $("input[id*='hdfMaxCapacity']").val(VehicleDetails.MaxCapacity);
                            $("input[id*='hdfYearModel']").val(VehicleDetails.YearModel);
                            $("input[id*='hdfVehicleColor']").val(VehicleDetails.VehicleColor);
                            $("input[id*='hdfVehicleType']").val(VehicleDetails.VehicleType);
                            $("input[id*='hdfPlateNumber']").val(VehicleDetails.PlateNumber);
                            $("input[id*='hdfModel']").val(VehicleDetails.ModelName);
                            $("input[id*='hdfBrandName']").val(VehicleDetails.BrandName);

                            if ($("#ContentPlaceHolder1_txtModelName").val() == "") {
                                var $rfv = $("#ContentPlaceHolder1_RequiredFieldValidator2");
                                $rfv.prop('isvalid', 'false');
                                $rfv.removeAttr('style');
                                $rfv.css('visibility', 'show');
                            }
                            if ($("#ContentPlaceHolder1_txtBrandName").val() == "") {
                                var $rfv = $("#ContentPlaceHolder1_RequiredFieldValidator1");
                                $rfv.prop('isvalid', 'false');
                                $rfv.removeAttr('style');
                                $rfv.css('visibility', 'show');
                            }
                            if ($("#ContentPlaceHolder1_txtPlateNo").val() == "") {
                                var $rfv = $("#ContentPlaceHolder1_RequiredFieldValidator3");
                                $rfv.prop('isvalid', 'false');
                                $rfv.removeAttr('style');
                                $rfv.css('visibility', 'show');
                            }
                            if ($("#ContentPlaceHolder1_txtMaxCap").val() == "") {
                                var $rfv = $("#ContentPlaceHolder1_RequiredFieldValidator4");
                                $rfv.prop('isvalid', 'false');
                                $rfv.removeAttr('style');
                                $rfv.css('visibility', 'show');
                            }
                            if ($("#ContentPlaceHolder1_txtVehicleColor").val() == "") {
                                var $rfv = $("#ContentPlaceHolder1_RequiredFieldValidator5");
                                $rfv.prop('isvalid', 'false');
                                $rfv.removeAttr('style');
                                $rfv.css('visibility', 'show');
                            }

                            
                            $("#ContentPlaceHolder1_txtVehicleColor").focus();
                            $("#ContentPlaceHolder1_txtModelName").focus();
                            $("#ContentPlaceHolder1_txtPlateNo").focus();
                            $("#ContentPlaceHolder1_txtMaxCap").focus();
                            $("#ContentPlaceHolder1_txtBrandName").focus();
                        });
                    }
                    catch (e) {
                        alert(e);
                    }
                }
            });


            $(".divVehicleDetails").css({ "display": "block" });
        });

        $(".editVehicleCancel").click(function () {
            //$(".divVehicleDetails").css({ "display": "none" });
            $(".divEnabled").css({ "display": "none" });
            $(".divHider").css({ "display": "block" });
        });

        $(".editVehicleBack").click(function () {
            $(".divVehicleDetails").css({ "display": "none" });
            $(".divEnabled").css({ "display": "none" });
            $(".divHider").css({ "display": "block" });
        });

        $(".DeleteVehicleDetails").click(function () {
            var VehicleID = $(this).attr('id');
            $("input[id*='hdfVehicleID']").val(VehicleID);
            $(".divRemoveVehicle").css({ "display": "block" });

            $.ajax({
                type: "POST",
                url: "ViewVehicle.aspx/PopulateVehicleDetails",
                data: "{'pVehicleID':'" + VehicleID + "'}",
                contentType: "application/json",
                dataType: "json",
                success: function (response) {
                    try {
                        var res = response.d;
                        $.each(res, function (index, VehicleDetails) {
                            $(".lblVehicleNameEdit").text(VehicleDetails.BrandName);
                            $(".lblModelName").text(VehicleDetails.PlateNumber);
                        });
                    }
                    catch (e) {
                        alert(e);
                    }
                }
            });
          
          
        });

        $(".editVehicle").click(function () {
            
            //var fileUpload = document.getElementById("ContentPlaceHolder1_uploadAttachment");
            //$(fileUpload).closest("div").css({ "display": "none" });
            
            $("#ContentPlaceHolder1_CustomValidator1").css({ "visibility": "hidden" });
            $("input[id*='hdfED']").val("False")
            var valName = document.getElementById("<%=rfvExpirationDate.ClientID%>");
            ValidatorEnable(valName, true);
            $("input[id*='hdfVehicleID']").val($(this).attr('innerhtml'));
            
            var pAttachmentID = $(this).attr('id');
            //$("input[id*='hdfAttachmentID']").val("");
            
            $("#ContentPlaceHolder1_rfvRequiredAttachment").css({ "display": "none" });
            $("#ContentPlaceHolder1_cvFileSize").css({ "display": "none" });
            $("#ContentPlaceHolder1_revFileType").css({ "display": "none" });
            $("#ContentPlaceHolder1_rfvExpirationDate").css({ "display": "none" });
            
            //$("#expirationDate").text('');
            var Imag;
            if ($(this).attr('name') == "Img") {
                 
                Imag = $("#img" + pAttachmentID).attr('src');
                $.ajax({
                    type: "POST",
                    url: "ViewVehicle.aspx/EditVehicleAttachment",
                    data: "{'pAttachmentID':'" + pAttachmentID + "'}",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (response) {
                        try {
                            $("input[id*='hdfAttachmentID']").val(pAttachmentID);
                            $("input[id*='hdfAttachmentTypeID']").val(response.d[0].AttachmentTypeID);
                            
                            var valName = document.getElementById("<%=rfvRequiredAttachment.ClientID%>");
                            ValidatorEnable(valName, false);

                            $(".lblAttachmentDescription").text(response.d[0].AttachmentTypeDescription + " Image");
                            if (response.d[0].HasExpirationDate == "" || response.d[0].HasExpirationDate == "0") {
                                $(".divEditAttachmentDateExpiry").css({ "display": "none" });
                                var valName = document.getElementById("<%=rfvExpirationDate.ClientID%>");
                                ValidatorEnable(valName, false);
                            }
                            else {
                                //var valName = document.getElementById("<%=rfvExpirationDate.ClientID%>");
                                //ValidatorEnable(valName, true);
                                $(".divEditAttachmentDateExpiry").css({ "display": "block" });
                            }
                            //$("#expirationDate").text(response.d[0].ExpirationDate);
                            
                        }
                        catch (e) {
                            alert(e);
                        }
                    }
                });
            }
            else {
                Imag = "/Images/no-image.gif";
                $("input[id*='hdfAttachmentID']").val("");
                $.ajax({
                    type: "POST",
                    url: "ViewVehicle.aspx/GetVehicleRequiredAttachments",
                    data: "{'pAttachmentTypeID':'" + pAttachmentID + "'}",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (response) {
                        try {
                            var res = response.d;
                            $.each(res, function (index, AttachmentTypeDetails) {
                                $("input[id*='hdfAttachmentTypeID']").val(pAttachmentID);
                                if (AttachmentTypeDetails.HasExpirationDate == "0")
                                {
                                    //var $rfv = $("#ContentPlaceHolder1_rfvExpirationDate");
                                    var valName = document.getElementById("<%=rfvExpirationDate.ClientID%>");
                                    ValidatorEnable(valName, false);
                                    $(".divEditAttachmentDateExpiry").css({ "display": "none" });
                                }
                                else {
                                    //var valName = document.getElementById("<%=rfvExpirationDate.ClientID%>");
                                    //ValidatorEnable(valName, true);
                                    $(".divEditAttachmentDateExpiry").css({ "display": "block" });
                                }
                                $("#expirationDate").text(response.d[0].ExpirationDate);
                                $(".lblAttachmentDescription").text(AttachmentTypeDetails.AttachmentTypeDescription + " Image");
                            });
                        }
                        catch (e) {
                            alert(e);
                        }
                    }
                });
            }
            $("#imgEdit").attr('src', Imag);
            
            var fileUpload = $("[id*=ContentPlaceHolder1_uploadAttachment]");
            var id = fileUpload.attr("id");
            var name = fileUpload.attr("name");
            var newFileUpload = $("<input type = 'file' class='uploadAttachment black-text' Style='margin-left: 84px;'/>");
            fileUpload.after(newFileUpload);
            fileUpload.remove();
            newFileUpload.attr("id", id);
            newFileUpload.attr("name", name);

            $(".uploadAttachment").on("change", function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imgEdit').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });

        });

        $("#ContentPlaceHolder1_expirationDate").on("change", function () {
            var CurrentDate = new Date();
            var ExpDate = new Date($("input[id*='expirationDate']").val());
            if (CurrentDate > ExpDate) {
                $(".dateValidator").css({ "display": "block" });
                $("input[id*='hdfED']").val("True")
            }
            else {
                $(".dateValidator").css({ "display": "none" });
                $("input[id*='hdfED']").val("False")
            }
        });

        

        $('#aSaveChanges').click(function () {

            var blnreturn = true;

            $.each(Page_Validators, function (index, validator) {
                if (!validator.isvalid) {
                    $(validator).css({ "visibility": "show" });
                }

            });


            return blnreturn;

        });


        $(".viewLarge").click(function () {
            var imgSrc = $(this).attr('id');
            $("#imgLarge").attr('src', imgSrc + "?<%= DateTime.Now.Ticks.ToString()%>");
        });

        $(".removeVehicleCancel").click(function () {
            $(".divRemoveVehicle").css({ "display": "none" });
        });

        $(".editVehicleSave").click(function () {

            if ($("#ContentPlaceHolder1_txtBrandName").val() != "" && $("#ContentPlaceHolder1_txtVehicleColor").val() != "" &&  $("#ContentPlaceHolder1_txtMaxCap").val() != "" && $("#ContentPlaceHolder1_txtModelName").val() != "" && $("#ContentPlaceHolder1_txtPlateNo").val() != "" && $("#ContentPlaceHolder1_ddlVehicelType").val() != "" && $("#ContentPlaceHolder1_ddlYearModel").val() != "") {
                if ($("input[id*='hdfVehicleColor']").val() != $("#ContentPlaceHolder1_txtVehicleColor").val() || $("input[id*='hdfBrandName']").val() != $("#ContentPlaceHolder1_txtBrandName").val() || $("input[id*='hdfMaxCapacity']").val() != $("#ContentPlaceHolder1_txtMaxCap").val() || $("input[id*='hdfModel']").val() != $("#ContentPlaceHolder1_txtModelName").val() || $("input[id*='hdfPlateNumber']").val() != $("#ContentPlaceHolder1_txtPlateNo").val() || $("input[id*='hdfVehicleType']").val() != $("#ContentPlaceHolder1_ddlVehicelType").val() || $("input[id*='hdfYearModel']").val() != $("#ContentPlaceHolder1_ddlYearModel").val()) {
                    $(".lblVehicleNameEdit").text($("#ContentPlaceHolder1_txtBrandName").val());
                    $(".lblModelName").text($("#ContentPlaceHolder1_txtPlateNo").val());
                    $(".divEnabled").css({ "display": "block" });
                    $(".divHider").css({ "display": "none" });
                    // $(".informChanges").append("<span class=\"white-text\">Update successful!</span>").css({ "display" : "block" }).addClass("green").delay(2000).fadeOut(1000);
                }
                else {
                    $(".divEnabled").css({ "display": "none" });
                    $(".divVehicleDetails").css({ "display": "none" });
                    $(".informChanges").append("<span class=\"white-text\">No changes made.</span>").css({ "display": "block" }).addClass("red").delay(2000).fadeOut("slow");
                }
            }

        });

        $(".btnProceed").click(function () {
            $(".divEnabled").css({ "display": "none" });
            $(".divVehicleDetails").css({ "display": "none" });
            $(".informChanges").append("<span class=\"white-text\">Update successful!</span>").css({ "display": "block" }).addClass("green").delay(2000).fadeOut("slow");
        });


        //Numbers Only (Mobile, Telephone) Validation
        $("#ContentPlaceHolder1_txtMaxCap").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if (e.keyCode == 189 || e.keyCode == 109) {
                return;
            }
            else if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
                // Ensure that it is a number and stop the keypress
            else {
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }

        });
        //End Validation

    </script>
</asp:Content>
