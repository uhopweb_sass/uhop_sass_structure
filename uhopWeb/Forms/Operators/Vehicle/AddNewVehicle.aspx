﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Operators/Operators.Master" AutoEventWireup="true" CodeBehind="AddNewVehicle.aspx.cs" Inherits="uhopWeb.Forms.Operators.Vehicle.AddNewVehicle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container col s12">
        <br />
        <!--<asp:Button ID="btnViewVehicle" runat="server" Text="View Vehicle" Style="display: none;" />-->
        <div class="row">
            <div class="col m7 s12 ">
                <h4 class="operatorMasterHeader white-text noMargin"><i class="small material-icons">&#xE8F1;</i>&nbsp;&nbsp;&nbsp; Vehicle Dashboard</h4>
            </div>
            <div class="col m5 s12 right-align">
                <!--<ul class="tabs" id="vehicleTabs">
                    <li class="tab col s3">
                        <a id="aViewVehicle" runat="server" >View Vehicle</a>
                    </li>
                    <li class="tab col s3">
                        <a class="active" runat="server">Add Vehicle</a>
                    </li>
                </ul>-->
                <a href="<%=uhopWeb.Classes.UHopCore.GetPageURL() %>Forms/Operators/Vehicle/ViewVehicle.aspx" class="waves-effect waves-light btn-large pink accent-2 hide-on-med-and-down" style="margin-top:-10px;"><i class="material-icons large left">&#xE8A0;</i>View Vehicle/s</a>
                <a href="<%=uhopWeb.Classes.UHopCore.GetPageURL() %>Forms/Operators/Vehicle/ViewVehicle.aspx" class="waves-effect waves-light btn  pink accent-2 hide-on-large-only"><i class="material-icons large left">&#xE8A0;</i>View Vehicle/s</a>
            </div>
        </div>
        <div class="divider"></div>
        <br />

        <div class="col s12 right">
            <span class="red-text">NOTE: All fields are required!</span>
        </div>

        <div class="row">
            <div class="col s12 center">
                <h5 class="white-text">ADD A VEHICLE</h5>
                <br />
            </div>
        </div>
        <div class="row">
            <div class="input-field col l4 m6 s12 white-text">
                <i class="material-icons prefix">&#xE88E;</i>
                <asp:TextBox ID="txtBrandName" TextMode="SingleLine" CssClass="large validate" runat="server" MaxLength="20"></asp:TextBox>
                <label for="txtBrandName" id="BrandName" class="lblFor">
                    Brand Name
                </label>
                <div class="red-text center">
                    <asp:RequiredFieldValidator ControlToValidate="txtBrandName" ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true" ErrorMessage="Brand name is required" CssClass="Validators" Display="Dynamic" ValidationGroup="AddVehicle">Brand name is required</asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="input-field col l4 m6 s12 white-text">
                <i class="material-icons prefix">&#xE88E;</i>
                <asp:TextBox ID="txtModelName" TextMode="SingleLine" CssClass="large validate" runat="server" MaxLength="20"></asp:TextBox>
                <label for="txtModelName" id="ModelName" class="lblFor">
                    Model Name
                </label>
                <div class="red-text center">
                    <asp:RequiredFieldValidator ControlToValidate="txtModelName" ID="RequiredFieldValidator2" runat="server" SetFocusOnError="true" ErrorMessage="Model name is required" CssClass="Validators" Display="Dynamic" ValidationGroup="AddVehicle">Model name is required</asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="input-field col l4 m6 s12 white-text">
                <i class="material-icons prefix">&#xE88E;</i>
                <asp:TextBox ID="txtPlateNo" TextMode="SingleLine" CssClass="large validate" runat="server" MaxLength="8"></asp:TextBox>
                <label for="txtPlateNo" id="PlateNo" class="lblFor">
                    Plate Number
                </label>
                <div class="red-text center">
                    <asp:RequiredFieldValidator ControlToValidate="txtPlateNo" ID="RequiredFieldValidator3" runat="server" SetFocusOnError="true" ErrorMessage="Plate number is required" CssClass="Validators" Display="Dynamic" ValidationGroup="AddVehicle">Plate number is required</asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="row white-text">
            <div class="input-field col l3 m6 s12"  style="margin-bottom: 25px;">
                <h6 class="type left grey-text">Vehicle type</h6>
                <asp:DropDownList ID="ddlVehicelType" runat="server" CssClass="browser-default black-text">
                    <asp:ListItem Text="Car" Value="Car"></asp:ListItem>
                    <asp:ListItem Text="Van" Value="Van"></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="input-field col l3 m6 s12" style="margin-bottom: 25px;">
                <h6 class="type left grey-text">Model year</h6>
                <asp:DropDownList ID="ddlYearModel" runat="server" CssClass="browser-default black-text">
                </asp:DropDownList>
            </div>

            <div style="padding-top: 20px;">
                <div class="input-field col l3 m6 s12 white-text">
                    <i class="material-icons prefix">&#xE88E;</i>
                    <asp:TextBox ID="txtMaxCap" TextMode="SingleLine" CssClass="large validate" runat="server" MaxLength="2"></asp:TextBox>
                    <label for="txtMaxCap" id="MaxCap" class="lblFor">
                        Capacity
                    </label>
                    <div class="red-text center">
                        <asp:RequiredFieldValidator ControlToValidate="txtMaxCap" ID="RequiredFieldValidator4" runat="server" SetFocusOnError="true" ErrorMessage="Max Capacity is required" CssClass="Validators" Display="Dynamic" ValidationGroup="AddVehicle">Max Capacity is required</asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>

                <div class="input-field col l3 m6 s12 white-text">
                    <i class="material-icons prefix">&#xE88E;</i>
                    <asp:TextBox ID="txtVehicleColor" TextMode="SingleLine" CssClass="large validate" runat="server" MaxLength="30"></asp:TextBox>
                    <label for="txtVehicleColor" id="Label1" class="lblFor">
                        Vehicle Color
                    </label>
                    <div class="red-text center">
                        <asp:RequiredFieldValidator ControlToValidate="txtVehicleColor" ID="RequiredFieldValidator16" runat="server" SetFocusOnError="true" ErrorMessage="Vehicle color is required" CssClass="Validators" Display="Dynamic" ValidationGroup="AddVehicle">Vehicle color is required</asp:RequiredFieldValidator>
                    </div>
                </div>
        </div>

        <div class="row">
            <div class="col s12 white-text center">
                <br />
                <h5>UPLOAD THE IMAGE OF THE FOLLOWING</h5>
                <br />
            </div>
            <div class="col s12 right-align">
                <h6><span class="red-text redNote">NOTE: Maximum file size for each file is <b>2 MB</b>.</h6>
            </div>
        </div>
        <div class="row">

            <div class="col l4 s12">
              <ul class="collapsible" data-collapsible="accordion">
                <li>
                <!-- Front View -->
                  <div class="collapsible-header"><i class="material-icons">&#xE88F;</i>Front View</div>
                  <div class="collapsible-body white-text">
                      <p>
                          <asp:FileUpload ID="FUFrontView" runat="server" /><br />
                          <asp:RequiredFieldValidator ControlToValidate="FUFrontView" ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass="Validators red-text" Display="Dynamic" ValidationGroup="AddVehicle">Front View image is required!</asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator3" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                            ControlToValidate="FUFrontView" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                            Display="Dynamic" ValidationGroup="AddVehicle" />
                          <asp:CustomValidator runat="server" ID="CustomValidator1" ForeColor="Red" ControlToValidate="FUFrontView" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="AddVehicle" />
                      </p>
                  </div>
                  <!-- END of Front view -->
                </li>

                <li>
                    <!-- Rear View -->
                  <div class="collapsible-header"><i class="material-icons">&#xE88F;</i>Rear View</div>
                  <div class="collapsible-body white-text">
                      <p>
                        <asp:FileUpload ID="FURearView" runat="server" /><br />
                        <asp:RequiredFieldValidator ControlToValidate="FURearView" ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass="Validators red-text" Display="Dynamic" ValidationGroup="AddVehicle">Rear View image is required!</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                            ControlToValidate="FURearView" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                            Display="Dynamic" ValidationGroup="AddVehicle" />
                        <asp:CustomValidator runat="server" ID="CustomValidator2" ForeColor="Red" ControlToValidate="FURearView" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="AddVehicle" />
                      </p>
                  </div>
                    <!-- END of Rear View -->
                </li>

                <li>
                   <!-- Left view -->
                  <div class="collapsible-header"><i class="material-icons">&#xE88F;</i>Left View</div>
                  <div class="collapsible-body white-text">
                      <p>
                        <asp:FileUpload ID="FULeftView" runat="server" /><br />

                        <asp:RequiredFieldValidator ControlToValidate="FULeftView" ID="RequiredFieldValidator7" runat="server" ErrorMessage="" CssClass="Validators red-text" Display="Dynamic" ValidationGroup="AddVehicle">Left View image is required!</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                            ControlToValidate="FULeftView" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                            Display="Dynamic" ValidationGroup="AddVehicle" />
                        <asp:CustomValidator runat="server" ID="CustomValidator3" ForeColor="Red" ControlToValidate="FULeftView" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="AddVehicle" />
                      </p>
                  </div>
                  <!-- END of left view -->
                </li>
              </ul>
            </div>

            <div class="col l4 s12">
              <ul class="collapsible" data-collapsible="accordion">

                <li>
                  <!-- RIght view -->
                  <div class="collapsible-header"><i class="material-icons">&#xE88F;</i>Right View</div>
                  <div class="collapsible-body white-text">
                      <p>
                         <asp:FileUpload ID="FURightView" runat="server" /><br />
                        <asp:RequiredFieldValidator ControlToValidate="FURightView" ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass="Validators red-text" Display="Dynamic" ValidationGroup="AddVehicle">Right View image is required!</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                            ControlToValidate="FURightView" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                            Display="Dynamic" ValidationGroup="AddVehicle" />
                        <asp:CustomValidator runat="server" ID="CustomValidator4" ForeColor="Red" ControlToValidate="FURightView" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="AddVehicle" />
                      </p>
                  </div>
                   <!-- END of right view -->
                </li>

                <li>
                   <!-- Front interior view -->
                  <div class="collapsible-header"><i class="material-icons">&#xE88F;</i>Front Interior View</div>
                  <div class="collapsible-body white-text">
                      <p>
                         <asp:FileUpload ID="FUFrontInterior" runat="server" /><br />
                        <asp:RequiredFieldValidator ControlToValidate="FUFrontInterior" ID="RequiredFieldValidator9" runat="server" ErrorMessage="" CssClass="Validators red-text" Display="Dynamic" ValidationGroup="AddVehicle">Front Interior View image is required!</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                            ControlToValidate="FUFrontInterior" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                            Display="Dynamic" ValidationGroup="AddVehicle" />
                        <asp:CustomValidator runat="server" ID="CustomValidator5" ForeColor="Red" ControlToValidate="FUFrontInterior" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="AddVehicle" />
                      </p>
                  </div>
                    <!-- END front interior view -->
                </li>

                <li>
                    <!-- Rear interior view -->
                  <div class="collapsible-header"><i class="material-icons">&#xE88F;</i>Rear Interior View</div>
                  <div class="collapsible-body white-text">
                      <p>
                         <asp:FileUpload ID="FURearInterior" runat="server" /><br />
                        <asp:RequiredFieldValidator ControlToValidate="FURearInterior" ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass="Validators red-text" Display="Dynamic" ValidationGroup="AddVehicle">Rear Interior View image is required!</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                            ControlToValidate="FURearInterior" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                            Display="Dynamic" ValidationGroup="AddVehicle" />
                        <asp:CustomValidator runat="server" ID="CustomValidator6" ForeColor="Red" ControlToValidate="FURearInterior" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="AddVehicle" />
                      </p>
                  </div>
                    <!-- END of rear interior view -->
                </li>
              </ul>
            </div>

            <div class="col l4 s12">
              <ul class="collapsible" data-collapsible="accordion">
                <li>
                  <div class="collapsible-header"><i class="material-icons">&#xE88F;</i>Official Receipt</div>
                  <div class="collapsible-body white-text">
                      <p>
                        <asp:FileUpload ID="FUOr" runat="server" /><br />
                        <asp:RequiredFieldValidator ControlToValidate="FUOr" ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="Validators red-text" Display="Dynamic" ValidationGroup="AddVehicle">Official Receipt image is required!</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                            ControlToValidate="FUOr" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                            Display="Dynamic" ValidationGroup="AddVehicle" />
                        <asp:CustomValidator runat="server" ID="CustomValidator7" ForeColor="Red" ControlToValidate="FUOr" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="AddVehicle" />

                            <asp:TextBox class="expirationDatepicker dpexpiration txtExpiryDateOR errorLabel" ID="txtExpiryDateOR" runat="server" MaxLength="1" placeholder="Official Receipt Expiration Date"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ForeColor="Red" runat="server" ErrorMessage="Official Receipt Expiration date is required" ValidationGroup="AddVehicle" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtExpiryDateOR" Text="Official Receipt Expiration date is required"></asp:RequiredFieldValidator>

                      </p>
                  </div>
                </li>
                <li>
                  <div class="collapsible-header"><i class="material-icons">&#xE88F;</i>Cert. of Registration</div>
                  <div class="collapsible-body white-text">
                      <p>
                         <asp:FileUpload ID="FUCR" runat="server" /><br />
                        <asp:RequiredFieldValidator ControlToValidate="FUCR" ID="RequiredFieldValidator12" runat="server" ErrorMessage="" CssClass="Validators red-text" Display="Dynamic" ValidationGroup="AddVehicle">Cert. of Registration image is required!</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                            ControlToValidate="FUCR" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                            Display="Dynamic" ValidationGroup="AddVehicle" />
                        <asp:CustomValidator runat="server" ID="CustomValidator8" ForeColor="Red" ControlToValidate="FUCR" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="AddVehicle" />
                      </p>
                  </div>
                </li>
                <li>
                  <div class="collapsible-header"><i class="material-icons">&#xE88F;</i>Insurance</div>
                  <div class="collapsible-body white-text">
                      <p>
                        <asp:FileUpload ID="FUInsurance" runat="server" /><br />
                        <asp:RequiredFieldValidator ControlToValidate="FUInsurance" ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass="Validators red-text" Display="Dynamic" ValidationGroup="AddVehicle">Insurance image is required!</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG|.jp2|.JP2|.jpx|.JPX|.j2k|.J2K|.j2c|.J2C)$"
                            ControlToValidate="FUInsurance" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                            Display="Dynamic" ValidationGroup="AddVehicle" />
                        <asp:CustomValidator runat="server" ID="CustomValidator9" ForeColor="Red" ControlToValidate="FUInsurance" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="AddVehicle" />
                        <asp:TextBox class="expirationDatepicker dpexpiration txtExpiryDateInsurance errorLabel" ID="txtExpiryDateInsurance" runat="server" MaxLength="1" placeholder="Insurance Expiration Date"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ForeColor="Red" runat="server" ErrorMessage="Insurance Expiration date is required" ValidationGroup="AddVehicle" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtExpiryDateInsurance" Text="Insurance Expiration date is required"></asp:RequiredFieldValidator>
                      </p>
                  </div>
                </li>
              </ul>
            </div>
        </div> <!-- END of main row -->
        <div class="row center-align">
            <asp:Button ID="btnSubmit" ValidationGroup="AddVehicle" runat="server" Text="Add Vehicle" OnClick="btnAddVehicle_Click" class="btnSubmit btn btn-large blue darken-2" />
        </div>
    </div>
    <!-- END of container -->

<%--    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/script") %>
    </asp:PlaceHolder>--%>

    <script src="../../../Scripts/jquery-2.1.4.min.js"></script>
    <script>
        //Numbers Only (Mobile, Telephone) Validation
        $("#ContentPlaceHolder1_txtMaxCap").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if (e.keyCode == 189 || e.keyCode == 109) {
                return;
            }
            else if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
                // Ensure that it is a number and stop the keypress
            else {
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }

        });
        //End Validation

        $('.btnSubmit').click(function () {
            var blnreturn = true;
            
            $.each(Page_Validators, function (index, validator) {
                if (!validator.isvalid) {
                    blnreturn = false;
                    $(validator).closest("div").css({ "display": "block" });
                }

            });
            return blnreturn;
        });

        function Upload(sender, args) {


            var fileUpload = document.getElementById(sender.controltovalidate);
            if (typeof (fileUpload.files) != "undefined") {
                var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
                var maxFileSize = 1000
                if (size < maxFileSize) {
                    args.IsValid = true;
                } else {
                    $(fileUpload).closest("div").css({ "display": "block" });
                    args.IsValid = false;
                    return;
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        }
    </script>
</asp:Content>
