﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;

namespace uhopWeb.Forms.Operators
{
    public partial class Operators : System.Web.UI.MasterPage
    {
        public static string strMemberID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                clsAuthenticator.Authenticate();
                string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                using (ClsMembers objMembers = new ClsMembers(strDecrypted))
                    {
                        strMemberID = strDecrypted;

                        objMembers.Fill(strMemberID);
                        if (objMembers.TblMembers.Rows.Count > 0)
                        {

                            hdfMemberStatus.Value = objMembers.TblMembers.Rows[0].GetString("Status");
                            string[] nameSplitter = objMembers.GetMemberDetails("FirstName").Split(' ');
                            string strname = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(nameSplitter[0]);
                            lblNavBarName.Text = strname;
                            lblNavBarName2.Text = strname;
                        }
                    }
                
            }
        }
    }
}