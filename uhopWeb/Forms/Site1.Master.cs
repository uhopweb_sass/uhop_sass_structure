﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using uhopWeb.Classes;
using System.Web.UI.HtmlControls;

namespace uhopWeb.Forms
{
    public partial class Site1 : System.Web.UI.MasterPage
    {

        public static string strMemberID;

        public void CreateDashboardLink()
        {
            try
            {
                clsAuthenticator.Authenticate();

                string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");

                using (ClsMembers objMembers = new ClsMembers(strDecrypted))
                {

                    objMembers.Fill(strDecrypted);
                    strMemberID = strDecrypted;
                    var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                    if (objMembers.GetMemberDetails("IsOperator") == "1")
                    {
                        liProfileOperator.Visible = true;
                        liProfile.Visible = false;
                        liProfileOperator.HRef = UHopCore.GetPageURL() + "Forms/Operators/Maintenance/OperatorProfile.aspx";
                    }
                    else
                    {
                        liProfile.InnerText = "Be an operator";
                        liProfileOperator.Visible = false;
                        liProfile.Visible = true;
                    }
                    }
            }
            catch
            {
                Response.Redirect(UHopCore.GetPageURL() + "index.aspx");
            }
        }

        public void CreateDashboardLinkSideNav()
        {
            try
            {
                 clsAuthenticator.Authenticate();
                    string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");

                    using (ClsMembers objMembers = new ClsMembers(strDecrypted))
                    {

                        objMembers.Fill(strDecrypted);
                        strMemberID = strDecrypted;
                        var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                        if (objMembers.GetMemberDetails("IsOperator") == "1")
                        {
                            liSidenavOperator.Visible = true;
                            liSidenav.Visible = false;
                            liSidenavOperator.HRef = UHopCore.GetPageURL() + "Forms/Operators/Maintenance/OperatorProfile.aspx";
                       }
                        else
                        {
                            liSidenavOperator.Visible = false;
                            liSidenav.Visible = true;
                        }
                   }
            }
            catch
            {
                Response.Redirect(UHopCore.GetPageURL() + "index.aspx");
            }
        }

        protected void BecomeAnOperator_Click(object sender, EventArgs e)
        {
            try
            {
                clsAuthenticator.Authenticate();
                string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                ClsMembers objMembers = new ClsMembers(strDecrypted);
                objMembers.Fill(strDecrypted);
                DataRow drwMember = objMembers.TblMembers.Select("MemberID='" + strDecrypted + "'")[0];

                drwMember.BeginEdit();
                drwMember.SetString("IsOperator", "1");
                drwMember.EndEdit();
                objMembers.Update();
                Response.Redirect(UHopCore.GetPageURL() + "Forms/Operators/Maintenance/OperatorProfile.aspx",false);
            }
            catch (Exception ex)
            {
                Response.Redirect(UHopCore.GetPageURL() + "index.aspx");
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    clsAuthenticator.Authenticate();
                    if (string.IsNullOrEmpty(Session["IsOperator"].ToString()))
                    {
                        HttpContext.Current.Response.Redirect(UHopCore.GetPageURL() + "index.aspx");
                    }
                    
                    string strDecrypted = ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                    
                    using (ClsMembers objMembers = new ClsMembers(strDecrypted))
                    {

                        
                        objMembers.Fill(strDecrypted);
                        strMemberID = strDecrypted;
                        var MemberDetails = objMembers.TblMembers.FirstOrDefault();
                        hdfMemberStatus.Value = MemberDetails.Status;

                        string[] nameSplitter = objMembers.GetMemberDetails("FirstName").Split(' ');

                        string strname = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(nameSplitter[0]);
                        lblNavBarName.Text = strname;
                        lblNavBarName2.Text = strname;
                        CreateDashboardLink();
                        CreateDashboardLinkSideNav();
                    }
                }
                else
                {
                    //comment by Ross error on aurum pay
                    //if (Request.Form["__EVENTTARGET"].ToString() == "BecomeAnOperator")
                    //{
                    //    BecomeAnOperator();
                    //}
                }
            }
            catch
            {
                Response.Redirect(UHopCore.GetPageURL() + "index.aspx");
            }
       }
    }
}