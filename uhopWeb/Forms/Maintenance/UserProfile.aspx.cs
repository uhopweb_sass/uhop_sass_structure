using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using uhopWeb.Classes;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using uhopWeb.DataSets;
using System.Web.Hosting;

namespace uhopWeb.Forms
{
    public partial class UserProfile : System.Web.UI.Page
    {

        public class ContactInfo
        {
            public string contactType { get; set; }
            public string ContactDescription { get; set; }
            public string ContactID { get; set; }
        }


        public static string strMemberID;

        public static readonly DSConfigs _dsContext = new DSConfigs();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clsAuthenticator.Authenticate();
                UHopCore.Title = "Welcome u-Hop Member!";
                string strDecrypted = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                strMemberID = strDecrypted;

                ClsMembers objMembers = new ClsMembers(strMemberID);
                if (ViewState["TblMembers"] == null)
                {
                    objMembers.Fill(strDecrypted);
                    ViewState["TblMembers"] = objMembers.TblMembers;
                }

                ClsContactType objContactType = new ClsContactType();
                objContactType.Fill();
                lblFName.Text = objMembers.GetMemberDetails("LastName") + ", " + objMembers.GetMemberDetails("FirstName") + " " + objMembers.GetMemberDetails("MiddleName");
                lblFirstName.Text = objMembers.GetMemberDetails("FirstName");
                txtEmail.Text = objMembers.GetMemberDetails("Email");
                DateTime dt = Convert.ToDateTime(objMembers.GetMemberDetailsDate("Birthdate"));
                lblBirthdate.Text = dt.ToString("MMMM dd, yyyy");
                txtPassword.Text = "������������"/*objMembers.GetMemberDetails("Password")*/;

                ddlContactType.DataSource = objContactType.TblContactType;
                ddlContactType.DataTextField = "Description";
                ddlContactType.DataValueField = "ContactNumberID";
                ddlContactType.DataBind();



                //imgUserProfile.Src = UHopCore.ProfilePictureView() + (string.IsNullOrEmpty(objMembers.GetMemberDetails("ProfilePicture")) ? "Default_Sidebar.png" : getProfpic.ProfilePicture + "?" + GuidString);
                userStatus2.Disabled = true;
                userStatus.Disabled = true;
                
                var getIsverified = objMembers.TblMembers.FirstOrDefault();                

                if (getIsverified.Status.Equals("1"))
                {
                    userStatus2.Checked = true;
                    userStatus.Checked = true;
                }
                else
                {
                    userStatus2.Checked = false;
                    userStatus.Checked = false;
                }
            }
        }

        public void InvalidateAccount()
        {
            clsAuthenticator.Authenticate();
            ClsMembers obj = new ClsMembers();
            obj.Fill(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));
            //obj.TblMembers = (DataSets.DSConfigs.MemberDataTable)ViewState["TblMembers"];

            var memberValidity = obj.TblMembers.FirstOrDefault();
            memberValidity.Status = "0";
            //userStatus.Checked = false;
            obj.Update();
        }

        public string PopulateContactNo()
        {
            clsAuthenticator.Authenticate();

            string trContent = "";

            using (ClsMembers objMembers = new ClsMembers())
            {
                objMembers.FillContacts(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));
                foreach (DataRow drw in objMembers.TblContact.Rows)
                {
                    trContent += "<tr class='removableTR'><td>" + drw.GetString("TypeDescription") + "</td><td>" + drw.GetString("ContactNumber") + "</td><td><a class='removeNum' numberID='" + drw.GetString("ContactID") + "' href='#deleted'><i class=\"material-icons black-text small\">&#xE5CD;</i></a></td></tr>";
                }
            }

            
            return trContent;
        }

        [System.Web.Services.WebMethod]
        public static string PopulateContactNos()
        {
            clsAuthenticator.Authenticate();
            string trContent = "";

            using (ClsMembers objMembers = new ClsMembers())
            {
                objMembers.FillContacts(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));
                foreach (DataRow drw in objMembers.TblContact.Rows)
                {
                    trContent += "<tr class='removableTR'><td>" + drw.GetString("TypeDescription") + "</td><td>" + drw.GetString("ContactNumber") + "</td><td><a class='removeNum' numberID='" + drw.GetString("ContactID") + "' href='#deleted'><i class=\"material-icons black-text\">&#xE5CD;</i></a></td></tr>";
                }
            }

            
            return trContent;
        }
        public string loadProfilePicture()
        {
            clsAuthenticator.Authenticate();
            ClsMembers objMembers = new ClsMembers();
            objMembers.TblMembers = (DataSets.DSConfigs.MemberDataTable)ViewState["TblMembers"];
            var getProfpic = objMembers.TblMembers.FirstOrDefault();
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());
            GuidString = GuidString.Replace("=", "");
            GuidString = GuidString.Replace("+", "");
            GuidString = GuidString.Replace("/x`", "");


            
            return "<img src=\"" + UHopCore.ProfilePictureView() + getProfpic.ProfilePicture + "?r=" + GuidString + "\" id=\"imgUserProfile\" class=\"responsive-img\" alt=\"Alternate Text\" width=\"100\" height=\"100\" style=\"margin-top: 12px;\" />";
        }

        [System.Web.Services.WebMethod]
        public static string listAllAddresses()
        {
            clsAuthenticator.Authenticate();
            string addressesListJson = "";
            using (ClsMembers objMembers = new ClsMembers())
            {
                objMembers.FillAddress(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));

                
                var addressString = objMembers.TblAddress.Where(t => t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") & t.IsForDriver == "0").ToList();


                foreach (var a in addressString)
                {

                    var addressType = clsAddressType.TblAddressType.Where(t => t.AddressTypeID == a.AddressTypeID).FirstOrDefault();

                    var remAddress = addressType.AddressTypeDescription.Split(' ');

                    // "<div class='col s12'><span style='font-size: 12px;padding:5px;border-radius: 5%;margin-bottom:5px;' class='white-text red hide-on-med-and-down'>Your work address is not valid</span></div>" +

                    addressesListJson += "<div class='row allAddressEnt' style='padding-left: 45px;'>" +

                                         "<div class='col l1 s12' >" +
                                            "<p class='noPadding2'>" + remAddress[0] + ":</p>" +
                                        "</div>" +
                                        "<div class='col l10 s12'>" +
                                            "<p class='noPadding2'>" +
                                                "<span>" + a.Street + " " + a.Barangay + " " + a.CityMunicipality + " " + a.Province + " " + a.ZipCode + ", " + a.Country + "</span>" +
                        //"<asp:Label ID = 'lblHome' runat='server' class='left white-text lblHome'></asp:Label>" +
                                            "</p>" +
                                        "</div>" +
                                    "</div>";

                }
            }
            using (clsAddressType obAddress = new clsAddressType())
            {
                obAddress.Fill();
            }

            
            return addressesListJson;
        }

       

        protected void btnUpdateProfilePick_Click(object sender, EventArgs e)
        {
            clsAuthenticator.Authenticate();
            string message = "";
            if (!profilePictureUpload.HasFile)
            {
                message = "Please select profile picture!";
                goto EndLine2;
            }

            //var getFileName = ClsMembers.TblAttachmentType.Where(t => t.AttachmentTypeID == hdfFileID.Value).FirstOrDefault();
            string strFileName = "MPROFPICT_" + ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") + ".jpg";
            
            string uploadResult = upload_Image(profilePictureUpload, "../../UploadedImages/ProfilePicture/", strFileName);

            if (uploadResult == "0")
            {
                message = "Invalid image format!. Please upload a valid image!";
                goto EndLine2;
            }

            if (uploadResult == "2")
            {
                message = "Image must be less than 2mb in size!";
                goto EndLine2;
            }

            message = "Your profile picture has been updated!";
            InvalidateAccount();
            ClsMembers obj = new ClsMembers();
            obj.Fill(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));

            var updateProfileMember = obj.TblMembers.FirstOrDefault();
            updateProfileMember.ProfilePicture = strFileName;
            obj.Update();

        EndLine2:
            System.Text.StringBuilder sbs = new System.Text.StringBuilder();
            sbs.Append("<script type = 'text/javascript'>");
            sbs.Append("window.onload=function(){");
            sbs.Append("$(\"#errorPrompt\").text('");
            sbs.Append(message);
            sbs.Append("');");
            sbs.Append("$(\"#errorPrompt\").show(0).delay(3000).fadeOut(\"slow\");");
            sbs.Append("};");
            sbs.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sbs.ToString());
            expirationDateHidden.Value = "";
        }


        protected void btnUpdateImage_Click(object sender, EventArgs e)
        {
            clsAuthenticator.Authenticate();
            ClsMembers objMembers = new ClsMembers();
            ClsAutoNumber objAutoNumber = new ClsAutoNumber();
            objMembers.FillAttachmentType();
            objMembers.FillAttachment();
            var getFileName = objMembers.TblAttachmentType.Where(t => t.AttachmentTypeID == hdfFileID.Value).FirstOrDefault();
            string strFileName = "M" + getFileName.Prefix + "_" + ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") + ".jpg";

            var getIfExist = objMembers.TblAttachment.Where(t => t.AttachmentTypeID == hdfFileID.Value && t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p")).Count();


            string message = "";

            DateTime dt;

            if (!uploadAttachment.HasFile)
            {
                message = "Please input a valid file.";
                goto EndLine;
            }

            if (getFileName.HasExpirationDate == "1")
            {
                if (expirationDateHidden.Value == "" || expirationDateHidden.Value == null)
                {
                    message = "Please input an Expirate Date";
                    goto EndLine;
                }

                if (!DateTime.TryParse(expirationDateHidden.Value, out dt))
                {
                    message = "Please Input a valid date and time";
                    goto EndLine;
                }
                else
                {
                    if (Convert.ToDateTime(expirationDateHidden.Value) < DateTime.Now)
                    {
                        message = "Your attachment is already expired!, Please attach a valid one.";
                        goto EndLine;
                    }
                }
            }

            if (getFileName.HasDescription == "1")
            {
                if (attachmentDescriptionHidden.Value == "" || attachmentDescriptionHidden == null)
                {
                    message = "Please input a description for the attachment";
                    goto EndLine;
                }
            }



            string uploadResult = upload_Image(uploadAttachment, "../../UploadedImages/Attachment/", strFileName);

            if (uploadResult == "0")
            {
                message = "Please upload a valid image!";
            }
            else if (uploadResult == "2")
            {
                message = "Image must be less than 2mb in size.";
            }
            else
            {
                if (getIfExist == 0)
                {
                    var am = objMembers.TblAttachment.NewAttachmentRow();
                    am.AttachmentID = objAutoNumber.GenerateAutoNumber("AttachmentID", "000000").ToString();
                    am.AttachmentTypeID = hdfFileID.Value;
                    am.ReferenceID = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                    am.AttachmentFileName = strFileName;
                    am.Description = attachmentDescriptionHidden.Value;
                    if (hdfFileID.Value != "20150923000012" && hdfFileID.Value != "20150923000013")
                    {
                        am.ExpirationDate = Convert.ToDateTime(expirationDateHidden.Value);
                    }
                    else
                    {
                        am.ExpirationDate = Convert.ToDateTime("12/12/9999");
                    }
                    am.Status = "0";
                    am.IsForDriver = "0";
                    objMembers.TblAttachment.Rows.Add(am);
                    objMembers.UpdateAttachment();

                    InvalidateAccount();

                    message = "Member Attachment has been uploaded";
                }
                else
                {
                    var am = objMembers.TblAttachment.Where(t => t.AttachmentTypeID == hdfFileID.Value && t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p")).FirstOrDefault();
                    if (hdfFileID.Value != "20150923000012" && hdfFileID.Value != "20150923000013")
                    {
                        am.ExpirationDate = expirationDateHidden.Value == "" ? Convert.ToDateTime("9999-9-9") : Convert.ToDateTime(expirationDateHidden.Value);
                    }
                    am.Description = attachmentDescriptionHidden.Value;
                    objMembers.UpdateAttachment();
                    InvalidateAccount();
                    message = "Member Attachment has been updated";
                }
            }
        EndLine:
            // uploadAttachment.PostedFile.SaveAs(Server.MapPath("../../UploadedImages/Attachment/" + strFileName));
            System.Text.StringBuilder sbs = new System.Text.StringBuilder();
            sbs.Append("<script type = 'text/javascript'>");
            sbs.Append("window.onload=function(){");
            sbs.Append("$(\"#errorPrompt\").text('");
            sbs.Append(message);
            sbs.Append("');");
            sbs.Append("$(\"#errorPrompt\").show(0).delay(3000).fadeOut(\"slow\");");
            sbs.Append("};");
            sbs.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sbs.ToString());
            expirationDateHidden.Value = "";
            hdfFileID.Value = "";
            attachmentDescriptionHidden.Value = "";
        }
        
        public string upload_Image(FileUpload fileupload, string ImageSavedPath, string FileName)
        {
            clsAuthenticator.Authenticate();
            FileUpload fu = fileupload;
            string imagepath = "";
            if (fileupload.HasFile)
            {
                string filepath = Server.MapPath(ImageSavedPath);
                String fileExtension = System.IO.Path.GetExtension(fu.FileName).ToLower();
                String[] allowedExtensions = { ".png", ".jpeg", ".jpg", ".PNG", ".JPEG", "JPG" };

                if (fileupload.FileContent.Length > 2000000)
                {
                    return "2";
                }

                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        try
                        {
                            string s_newfilename = FileName;
                            fu.PostedFile.SaveAs(filepath + s_newfilename);
                            imagepath = ImageSavedPath + s_newfilename;
                            return "1";
                        }
                        catch (Exception ex)
                        {
                            Response.Write("File could not be uploaded.");
                        }
                    }
                }
            }
            return "0";
        }



        [System.Web.Services.WebMethod]
        public static string getHomeAddressView()
        {
            clsAuthenticator.Authenticate();
            ClsMembers objMembers = new ClsMembers();
           
                objMembers.FillAddress(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));


                var addressString = objMembers.TblAddress.Where(t => t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") & t.AddressTypeID == "20150923000001" & t.IsForDriver == "0").FirstOrDefault();
            return addressString.Street + " " + addressString.Barangay + " " + addressString.CityMunicipality + " " + addressString.Province + " " + addressString.ZipCode + ", " + addressString.Country;
        }


        [System.Web.Services.WebMethod]
        public static string getBillingAddressView()
        {
            clsAuthenticator.Authenticate();
            ClsMembers objMembers = new ClsMembers();
            
                objMembers.FillAddress(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));


                var addressString = objMembers.TblAddress.Where(t => t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") & t.AddressTypeID == "20150923000003" & t.IsForDriver == "0").FirstOrDefault();
            return addressString.Street + " " + addressString.Barangay + " " + addressString.CityMunicipality + " " + addressString.Province + " " + addressString.ZipCode + ", " + addressString.Country;
        }

        public void populateUhopLocation()
        {
            using (ClsMembers objMembers = new ClsMembers())
            {
                objMembers.FilluHopLocation();
            }
        }

        [System.Web.Services.WebMethod]
        public static string getProvinces()
        {
            UserProfile up = new UserProfile();
            ClsMembers objMembers = new ClsMembers();
            objMembers.FilluHopLocation();
            up.populateUhopLocation();

            var provincesString = objMembers.TbluHopLocation.AsEnumerable().GroupBy(r => r.Field<string>("Province")).Select(g => new { Province = g.Key }).ToList();

            string stringConcat = "";
            stringConcat = "<option class='prov1' value='none' selected disabled>Select Province</option>";
            foreach (var q in provincesString)
            {
                stringConcat += "<option value='" + q.Province + "'>" + q.Province + "</option>";
            }
            return stringConcat;
        }

        [System.Web.Services.WebMethod]
        public static string checkAddress(string addressTypeID)
        {
            clsAuthenticator.Authenticate();
            string jsonString = "";

            ClsMembers popadd = new ClsMembers();
            
                popadd.FillAddress(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));


                var populateAddress = popadd.TblAddress.Where(t => t.AddressTypeID == addressTypeID && t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") && t.IsForDriver == "0").FirstOrDefault();

            if (populateAddress != null)
            {
                jsonString += "{";
                jsonString += "\"zipCode\":\"" + populateAddress.ZipCode + "\",";
                jsonString += "\"street\":\"" + populateAddress.Street + "\",";
                jsonString += "\"barangay\":\"" + populateAddress.Barangay + "\"";
                jsonString += "}";
            }

            return jsonString;
        }

        [System.Web.Services.WebMethod]
        public static string getCityMunicipality(string provinceName)
        {
            UserProfile up = new UserProfile();
            ClsMembers objMembers = new ClsMembers();
            objMembers.FilluHopLocation();
            up.populateUhopLocation();

            string cityMunicipalitList = "";
            var cityMunicipalityString = objMembers.TbluHopLocation.Where(t => t.Province == provinceName).ToList();

            cityMunicipalitList = "<option class='muni1' value='none' selected disabled>Select City Municipality</option>";
            foreach (var c in cityMunicipalityString)
            {
                cityMunicipalitList += "<option class='muni1' value='" + c.MunicipalityID + "'>" + c.Description + "</option>";
            }
            return cityMunicipalitList;
        }

        public string getAddressType()
        {
            clsAddressType at = new clsAddressType();
            at.Fill();

            string addressTypes = "";
            var addressTypesList = clsAddressType.TblAddressType.ToList();
            addressTypes += "<option value='none' selected disabled>Select Address Type</option>";
            foreach (var a in addressTypesList)
            {
                addressTypes += "<option value='" + a.AddressTypeID + "'>" + a.AddressTypeDescription + "</option>";
            }
            return addressTypes;
        }

        [System.Web.Services.WebMethod]
        public static string getCityMunicipality2(string provinceName)
        {
            UserProfile up = new UserProfile();
            ClsMembers objMembers = new ClsMembers();
            objMembers.FilluHopLocation();
            up.populateUhopLocation();

            string cityMunicipalitList = "";
            var cityMunicipalityString = objMembers.TbluHopLocation.Where(t => t.Province == provinceName).ToList();

            cityMunicipalitList = "<option class='muni2' value='none' selected disabled>Select City Municipality</option>";
            foreach (var c in cityMunicipalityString)
            {
                cityMunicipalitList += "<option class='muni2' value='" + c.MunicipalityID + "'>" + c.Description + "</option>";
            }
            return cityMunicipalitList;
        }

        [System.Web.Services.WebMethod]
        public static string getZipCode(string municipalityID)
        {
            UserProfile up = new UserProfile();
            ClsMembers objMembers = new ClsMembers();
            objMembers.FilluHopLocation();
            up.populateUhopLocation();

            string zipCode = "";
            var cityMunicipalityString = objMembers.TbluHopLocation.Where(t => t.MunicipalityID == Convert.ToInt32(municipalityID)).FirstOrDefault();

            zipCode = cityMunicipalityString.ZipCode;

            return zipCode;
        }


        [System.Web.Services.WebMethod]
        public static string getProvinces(string zipCodeReq)
        {
            UserProfile up = new UserProfile();
            ClsMembers objMembers = new ClsMembers();
            objMembers.FilluHopLocation();
            up.populateUhopLocation();

            string provinceID = "", municipalityID = "";

            var cityMunicipalityString = objMembers.TbluHopLocation.Where(t => t.ZipCode == zipCodeReq).FirstOrDefault();

            if (cityMunicipalityString == null)
            {
                return null;
            }

            if (cityMunicipalityString != null)
            {
                provinceID = cityMunicipalityString.Province;
                municipalityID = Convert.ToString(cityMunicipalityString.MunicipalityID);
            }

            return "{\"provinceID\":\"" + provinceID + "\",\"municipalityID\":\"" + municipalityID + "\"}";
        }

        private static string GetEncyptedPassword(string pass)
        {
            string encryptedPassword = "";
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(pass));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {

                    sb.Append(b.ToString("x2"));
                }

                encryptedPassword = sb.ToString().ToLower();
            }
            return encryptedPassword;
        }


        [System.Web.Services.WebMethod]
        public static string removeSelectedContact(string selectedContact)
        {
            clsAuthenticator.Authenticate();
            ClsMembers objMembers = new ClsMembers();
            objMembers.FillContacts(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));
            var checkIfValidToRemove = objMembers.TblContact.Where(t => t.ContactID == selectedContact && t.IsForDriver == "0").Count();
            if (checkIfValidToRemove < 1)
            {
                return "Selected contact number does not exist please refresh your page and remove again.";
            }

            var checkMobileInfo = objMembers.TblContact.Where(t => t.ContactID == selectedContact && t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") && t.IsForDriver == "0").FirstOrDefault();

            if (checkMobileInfo.ContactNumberTypeID == "20150923000002")
            {
                var checkMobileCount = objMembers.TblContact.Where(t => t.ContactNumberTypeID == "20150923000002" && t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") && t.IsForDriver == "0").Count();
                if (checkMobileCount <= 1)
                {
                    return "Mobile number is required";
                }
            }

            var selectedContactRem = objMembers.TblContact.Where(t => t.ContactID == selectedContact && t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") && t.IsForDriver == "0").FirstOrDefault();
            selectedContactRem.Delete();
            objMembers.UpdateContact();

            return "Selected contact number has been removed!";
        }

        [System.Web.Services.WebMethod]
        public static int UpdatePassword(string currentPass, string newPass, string confirmPass)
        {
            clsAuthenticator.Authenticate();
            int strReturn = 0;
            ClsMembers objMembers = new ClsMembers();
            objMembers.Fill(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));
            var dtr = objMembers.TblMembers.FirstOrDefault();
            string dbPass = dtr.Password;
            if (dbPass == GetEncyptedPassword(currentPass))
            {
                if (!string.IsNullOrEmpty(newPass.Trim()))
                {
                    if (newPass == confirmPass)
                    {
                        DataRow drw = objMembers.TblMembers.Rows[0];
                        drw.BeginEdit();
                        drw.SetString("Password", GetEncyptedPassword(newPass));
                        drw.EndEdit();
                        objMembers.Update();
                        strReturn = 4;
                    }
                    else
                    {
                        strReturn = 1;
                        //strReturn = "Your new password does not matched your confirmed password";
                    }
                }
                else
                {
                    strReturn = 2;
                    //strReturn = "Please enter a new password";
                }

            }
            else
            {
                strReturn = 3;
                //strReturn = "Incorrect password";
            }

            return strReturn;
        }


        [System.Web.Services.WebMethod]
        public static void UpdateMemberDetail(string FieldName, string FieldValue)
        {
            clsAuthenticator.Authenticate();
            ClsMembers objMembers = new ClsMembers();
            objMembers.Fill(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));
            DataRow drw = objMembers.TblMembers.Rows[0];
            drw.BeginEdit();
            drw[FieldName] = FieldValue;
            drw.EndEdit();
            objMembers.Update();
        }



        [System.Web.Services.WebMethod]
        public static string getAllRequiredAttachments()
        {
            clsAuthenticator.Authenticate();
            string attachmentListJson = "";
            using (ClsMembers clsAttachmentFill = new ClsMembers())
            {
                clsAttachmentFill.FillAttachment();
                clsAttachmentFill.FillAttchmentType();

                
                var listAllAttachmentTypes = clsAttachmentFill.TblAttachmentType.Where(t => t.IsForMember == "1").ToList();

                foreach (var a in listAllAttachmentTypes)
                {
                    var attachmentDetails = clsAttachmentFill.TblAttachment.Where(t => t.AttachmentTypeID == a.AttachmentTypeID && t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") && t.IsForDriver == "0").FirstOrDefault();

                    if (attachmentDetails == null)
                    {
                        //<span style='font-size: 12px;padding:5px;border-radius: 5%;margin-bottom:5px;' class='white-text red'>Your document is not valid</span>
                        attachmentListJson += "<div class=\"col s12 l6\"> " + a.AttachmentTypeDescription + "&nbsp;&nbsp;" +
                                                "<div class=\"card grey lighten-5\">" +
                                                        "<div class=\"card-image\">" +
                                                            "<img src=\"../../UploadedImages/Attachment/Default.gif\" height=\"300\" />" +
                                                            "<span class=\"card-title\" style='color:#f06292;'><b>" + a.AttachmentTypeDescription + "</b></span>" +
                                                        "</div>" +
                                                        "<div class=\"card-content\">" +
                                                        "<p class=\"black-text attachmentTxt noPadding2\"><b>Status: </b><span class='red-text attachmentTxt'>PLEASE UPDATE YOUR ATTACHMENT." + (a.HasExpirationDate == "1" ? " Expiration Date is required!" : "") + " " + (a.HasDescription == "1" ? " Description is requried!" : "") + "</span></p>" +
                                                        "</div>" +
                                                        "<div class=\"card-action userPanel\">" +
                                                            "<a id=\"" + a.AttachmentTypeID + "\" href=\"#ModalEdit\" class=\"editAttach right notpwdsenior blue-text btn-flat waves-effect waves-dark modal-trigger mod1\">Edit</a>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>";
                    }
                }

                var checkSeniorPWD = clsAttachmentFill.TblAttachment.ToList();

                foreach (var q in checkSeniorPWD) //FOR SENIOR PWD
                {
                    if (q.ReferenceID.Contains(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p")) && q.IsForDriver == "0" && q.AttachmentFileName[0].ToString() == "M")/*q.AttachmentTypeID == "20150923000012" || q.AttachmentTypeID == "20150923000013" ||*/
                    {
                        var attachmentTypeFields = clsAttachmentFill.TblAttachmentType.Where(t => t.AttachmentTypeID == q.AttachmentTypeID).FirstOrDefault();

                        Guid g = Guid.NewGuid();
                        string GuidString = Convert.ToBase64String(g.ToByteArray());
                        GuidString = GuidString.Replace("=", "");
                        GuidString = GuidString.Replace("+", "");


                        string fileName = q.AttachmentFileName + "?r=" + GuidString,
                               attachmentName = q.Description,
                               expirationDate = q.ExpirationDate.ToString(),
                               status = q.Status;



                        attachmentListJson += "<div class=\"col s12 l6\"> " + attachmentTypeFields.AttachmentTypeDescription +
                                    "<div class=\"card grey lighten-5\">" +
                                        "<div class=\"card-image\">" +
                                            "<img src=\"../../UploadedImages/Attachment/" + fileName + "\" height=\"300\" />" +
                                            "<span class=\"card-title\" style=\"color: #0d47a1;\"><b>" + attachmentTypeFields.AttachmentTypeDescription + "</b></span>" +
                                        "</div>" +
                                        "<div class=\"card-content\">" +
                                            "<p class=\"black-text noPadding2\"><b>" + attachmentName + "</b></p>";
                        if (attachmentTypeFields.HasExpirationDate == "1")
                        {
                            attachmentListJson += "<p class=\"black-text noPadding2\"><b>Expiration date: </b><br>" + expirationDate.ToString() + "</p>";
                        }

                        attachmentListJson += "<p class=\"black-text noPadding2\"><br><b>Status: </b><br>" + (status == "0" ? "<span style='color:red'>NOT YET VALIDATED</span>" : "VALIDATED") + "</p>" +
                                         "</div>" +
                                         "<div class=\"card-action userPanel\">";
                        if (attachmentTypeFields.IsForMember == "0")
                        {
                            attachmentListJson += "<a id=\"" + q.AttachmentTypeID + "\" href=\"#\" class=\"deleditAttach right red-text btn-flat waves-effect waves-dark\">Delete</a>";
                        }

                        if (attachmentTypeFields.IsForSeniorCitizen == "1" || attachmentTypeFields.IsForPWD == "1")
                        {
                            attachmentListJson += "<a id=\"" + q.AttachmentTypeID + "\" href=\"#ModalEdit\" class=\"editAttach pwdsenior right blue-text btn-flat waves-effect waves-dark modal-trigger mod1\">Edit</a>";
                        }
                        else
                        {
                            attachmentListJson += "<a id=\"" + q.AttachmentTypeID + "\" href=\"#ModalEdit\" class=\"editAttach notpwdsenior right blue-text btn-flat waves-effect waves-dark modal-trigger mod1\">Edit</a>";
                        }



                        attachmentListJson += "</div>" +
                                     "</div>" +
                                 "</div>";
                    }


                }
            }

            

            return attachmentListJson;
        }

        [System.Web.Services.WebMethod]
        public static string removeAttachment(string AttachmentTypeID)
        {
            clsAuthenticator.Authenticate();
            ClsMembers objMembers = new ClsMembers();
            objMembers.FillAttachment();
            if (AttachmentTypeID == "20150923000012" || AttachmentTypeID == "20150923000013")
            {
                var memberAttachment = objMembers.TblAttachment.Where(t => t.AttachmentTypeID == AttachmentTypeID).FirstOrDefault();
                if (memberAttachment == null)
                {
                    return "1";
                }
                if (memberAttachment.Status == "1")
                {
                    return "1";
                }
                else
                {

                    var memberRemove = objMembers.TblAttachment.Where(t => t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") && t.AttachmentTypeID == AttachmentTypeID).FirstOrDefault();
                    string fileName = "";
                    //fileName = "../../UploadedImages/Attachment/" + memberRemove.AttachmentFileName;     
                    fileName = HostingEnvironment.ApplicationPhysicalPath + "/UploadedImages/Attachment/" + memberRemove.AttachmentFileName;

                    if (System.IO.File.Exists(fileName))
                    {
                        System.IO.File.Delete(fileName);
                    }


                    memberRemove.Delete();
                    objMembers.UpdateAttachment();
                    return "0";
                }
            }
            else
            {
                return "2";
            }

        }

        [System.Web.Services.WebMethod]
        public static string AddNewAddresInformation(string addressTypeID, string street, string zipCode, string barangay, string cityMunicipality, string province)
        {
            clsAuthenticator.Authenticate();
            string returnStr = "";
            string country = "Philippines";

            using (ClsMembers objMembers = new ClsMembers())
            {
                objMembers.FillAddress(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));
                objMembers.FilluHopLocation();
                var validateAddress = objMembers.TbluHopLocation.Where(t => t.ZipCode == zipCode).ToList();
                var validateAddressType = clsAddressType.TblAddressType.Where(t => t.AddressTypeID == addressTypeID).FirstOrDefault();

                if (validateAddressType == null)
                {
                    return "Invalid Address Type. Please select the correct one.";
                }

                if (validateAddress == null)
                {
                    return "No address found for the specified Zipcode!";
                }

                var validOnZip = "0";

                foreach (var q in validateAddress)
                {
                    if (q.Description == cityMunicipality)
                    {
                        validOnZip = "1";
                        break;
                    }
                    validOnZip = "0";
                }

                if (validOnZip == "0")
                {
                    return "Invalid City/Municipality or Province specified on the current zipcode.";
                }

                //if (validateAddress.Province != province)
                //{
                //    return "Invalid City/Municipality or Province specified on the current zipcode.";
                //}


                var dtAddress = objMembers.TblAddress.Where(t => t.ReferenceID == ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p") && t.AddressTypeID == addressTypeID);

                if (dtAddress.Count() > 0)
                {
                    var dt = dtAddress.FirstOrDefault();
                    dt.AddressTypeID = addressTypeID;
                    dt.ReferenceID = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                    dt.Street = street;
                    dt.ZipCode = Convert.ToInt32(zipCode);
                    dt.Barangay = barangay;
                    dt.CityMunicipality = cityMunicipality;
                    dt.Province = province;
                    dt.Country = country;
                    dt.IsForDriver = "0";
                    objMembers.UpdateAddress();
                    uhopWeb.Forms.UserProfile ss = new uhopWeb.Forms.UserProfile();
                    //ss.InvalidateAccount();
                    returnStr = "Address has been updated";
                }
                else
                {
                    ClsAutoNumber objAutoNumber = new ClsAutoNumber();
                    var dt = objMembers.TblAddress.NewAddressRow();
                    dt.AddressID = objAutoNumber.GenerateAutoNumber("AddressID", "000000").ToString();
                    dt.AddressTypeID = addressTypeID;
                    dt.ReferenceID = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
                    dt.Street = street;
                    dt.ZipCode = Convert.ToInt32(zipCode);
                    dt.Barangay = barangay;
                    dt.CityMunicipality = cityMunicipality;
                    dt.Province = province;
                    dt.Country = country;
                    dt.IsForDriver = "0";
                    objMembers.TblAddress.Rows.Add(dt);
                    objMembers.UpdateAddress();
                    uhopWeb.Forms.UserProfile ss = new uhopWeb.Forms.UserProfile();
                    //ss.InvalidateAccount();
                    returnStr = "New address has been added";
                }
            }

            

            return returnStr;
        }



        [System.Web.Services.WebMethod]
        public static string AddNewContactInformation(string typeID, string contactNumber)
        {
            clsAuthenticator.Authenticate();
            ClsMembers objMembers = new ClsMembers();
            ClsContactType objContactType = new ClsContactType();
            objContactType.Fill();
            objMembers.FillContacts(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));
            if (contactNumber.Length < 7)
            {
                return "Invalid Contact number";
            }


            var countContact = objMembers.TblContact.Where(t => t.IsForDriver == "0").Count();
            if (countContact > 5) //MUST BE ON SETTINGS
            {
                return "5 contact numbers only!";
            }

            var referenceIDs = objContactType.TblContactType.Where(t => t.ContactNumberID == typeID).FirstOrDefault();
            if (referenceIDs == null)
            {
                return "Unknown Contact Type";
            }

            try
            {
                long contactInt = 0;
                contactInt = Convert.ToInt64(contactNumber.Trim());
            }
            catch (Exception e)
            {
                return "Specified contact number is not valid!";
            }


            var checkDuplicate = objMembers.TblContact.Where(t => t.ContactNumber == contactNumber).Count();
            if (checkDuplicate > 0)
            {
                return "Duplicate contact number!";
            }

            ClsAutoNumber objAutoNumber=new ClsAutoNumber();
            var dtContact = objMembers.TblContact.NewContactNumberRow();
            dtContact.ContactID = objAutoNumber.GenerateAutoNumber("ContactNumberID", "000000").ToString();
            dtContact.ContactNumberTypeID = typeID;
            dtContact.ReferenceID = ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p");
            dtContact.ContactNumber = contactNumber;
            dtContact.Enabled = "1";
            dtContact.IsForDriver = "0";
            objMembers.TblContact.Rows.Add(dtContact);
            objMembers.UpdateContact();
            uhopWeb.Forms.UserProfile ss = new uhopWeb.Forms.UserProfile();
            ss.InvalidateAccount();

            return "New contact information added successfully!";
        }

        protected void modifyBirthdate_ServerClick(object sender, EventArgs e)
        {
            clsAuthenticator.Authenticate();
            string message = "";
            string appendColor = "";

            using (ClsMembers mbr = new ClsMembers())
            {
                mbr.Fill(ClsEncryptor.Decrypt(HttpContext.Current.Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p"));


                DateTime dt;

                if (!DateTime.TryParse(txtModifyBirthday.Text, out dt))
                {
                    message = "Invalid DateFormat for your Birthdate!";
                    goto EndLine;
                }

                if (string.IsNullOrEmpty(txtModifyBirthday.Text))
                {
                    message = "Please input a valid birthdate.";
                    goto EndLine;
                }

                if (Convert.ToDateTime(Convert.ToDateTime(txtModifyBirthday.Text).ToString("yyyy-MM-dd")) > Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")))
                {
                    message = "Selected Birthdate is Invalid. Please input or select a valid Birthdate";
                    goto EndLine;
                }

                var selectMember = mbr.TblMembers.FirstOrDefault();
                selectMember.Birthdate = Convert.ToDateTime(txtModifyBirthday.Text);
                selectMember.Status = "0";
                appendColor = "$(\"#errorPrompt\").css(\"background-color\",\"green\");";
                mbr.Update();

                message = "Birthdate has been updated! Your account has been disabled for revalidation.";
                lblBirthdate.Text = Convert.ToDateTime(txtModifyBirthday.Text).ToString("MMMM dd, yyyy");
            
            }

        EndLine:
            System.Text.StringBuilder sbs = new System.Text.StringBuilder();
            sbs.Append("<script type = 'text/javascript'>");
            sbs.Append("window.onload=function(){");
            sbs.Append("$(\"#errorPrompt\").text('");
            sbs.Append(message);
            sbs.Append("');");
            sbs.Append("$(\"#errorPrompt\").show(0).delay(3000).fadeOut(\"slow\");");
            sbs.Append(appendColor);
            sbs.Append("};");
            sbs.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sbs.ToString());
            expirationDateHidden.Value = "";
            hdfFileID.Value = "";
            attachmentDescriptionHidden.Value = "";

        }
    }
}