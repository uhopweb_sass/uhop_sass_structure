﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Site1.Master" AutoEventWireup="true" CodeBehind="UnderConstruction.aspx.cs" Inherits="uhopWeb.Forms.Maintenance.UnderConstruction" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <link rel="shortcut icon" href="Images/uhop_icon_beta.png" type="image/png" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />    
    <link href="Content/materialize/css/materialize.min.css" rel="stylesheet" />
    <link href="Content/wow/animate.css" rel="stylesheet" />
    <link href="Styles/alter.css" rel="stylesheet" />

    <title>u-Hop Transport Network Vehicle System Inc.</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <center>
            <img style="margin-top:50px;" class="responsive-img wow pulse animated center-align" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="3s" src="../../Images/u-hop_logo.png" alt="Alternate Text" />
        </center>
        <img src="../../Images/finallast.png" class="responsive-img"/>
        <div class="center" style="margin-top:-120px;">
            <h3 class="pink-text">This page is currently under construction.</h3>
            <h4 class="blue-text">We're sorry for the inconvenience.</h4>
        </div>
    </div>

    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundle/script") %>
    </asp:PlaceHolder>

    <script>
        /* WOW.js Initialization */
        new WOW().init();
        /* END of WOW.js initialization */
    </script>

</asp:Content>
