﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/Site1.Master" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="uhopWeb.Forms.UserProfile" %>

<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="messagePrompt"></div>    
    <script src="/../Scripts/jquery-2.1.4.min.js"></script>
    <div id="errorPrompt" style="background-color:red;color:white;text-align:center;padding:10px"><i class="material-icons right" id="I1"></i></div>
    <br />

    <div id="statusUpdate" class="statusUpdate center white-text" style="display:none;">
        <div class="col s12 right"><a href="#" class="white-text divClose"><i class="material-icons right" style="margin-top:0px;">&#xE5CD;</i></a> </div>
        <div class="col s12 center">You're account was already checked and we found some information inconsistencies. Scroll down to see the details.</div>
    </div>

    <div class="container hide-on-large-only">
        <div class="row">
            <!-- Switch -->
            <div class="switch right">
                <span><em>Status:</em> &nbsp;&nbsp;&nbsp;
                </span>
                <label>
                    <span id="inactiveUser2">INACTIVE</span>
                    <input disabled id="userStatus2" runat="server" type="checkbox" />
                    <%-- <input disabled id="userStatus" runat="server" type="checkbox"/>--%>
                    <span class="lever"></span>
                    <span id="activeUser2"><span class="wow tada" data-wow-iteration="infinite" data-wow-duration="2s"><u>ACTIVATE?</u></span></span>
                </label>
            </div>
        </div>
    </div>
    <div class="container user" style="padding:0px;">
        <!-- USER PROFILE HEADING -->
        <div class="row profileRows" id="userHeading">
            <div class="col l5 m8 s12">
                <div class="col s12 m4">
                    <%-- %> <%Response.Write(loadProfilePicture()); %> --%>
                   <img id="imgUserProfile" class="responsive-img tooltipped" data-position="top" data-delay="10" data-tooltip="Click to update picture" src="<% Guid g = Guid.NewGuid(); string GuidString = Convert.ToBase64String(g.ToByteArray()); GuidString = GuidString.Replace("=", ""); GuidString = GuidString.Replace("+", ""); Response.Write(uhopWeb.Classes.UHopCore.GetProfilePic(uhopWeb.Classes.ClsEncryptor.Decrypt(Request.Cookies["Uhop"]["Token"].Replace(" ", "+"), "Uh0p")) + "?r=" + GuidString); %>" alt="Profile picture"/>
                </div>
                <div class="col s12 m8">
                    <h5 style="font-size: 25px;">
                        <asp:Label ID="lblFirstName" runat="server" Text="Label"></asp:Label>
                    </h5>
                    <asp:Label ID="lblFName" runat="server" Text="Label"></asp:Label><br />
                    Born on
                    <a href="#"><asp:Label class="lblBirthdate" ID="lblBirthdate" runat="server" Style="margin-top: 100px;"></asp:Label></a>
                </div>
            </div>
            <div class="col l7 s12 right hide-on-med-and-down">
                <!-- Switch -->
                <div class="switch right">
                    <span><em>Status:</em> &nbsp;&nbsp;&nbsp;</span>
                    <label>
                        <span id="inactiveUser">INACTIVE</span>
                        <input disabled id="userStatus" runat="server" type="checkbox" />
                        <%-- <input disabled id="userStatus" runat="server" type="checkbox"/>--%>
                        <span class="lever"></span>
                        <span id="activeUser"><span class="wow tada" data-wow-iteration="infinite" data-wow-duration="2s"><u>ACTIVATE?</u></span></span>
                    </label>
                </div>
            </div>
            <!--<div class="col s4 right"><span class="red-text">The quick brown fox jumps over the lazy dog.</span></div>-->
            <div class="col s12">
                <br />
                <div class="divider"></div>
            </div>
        </div>
        <!-- END OF USER PROFILE HEADING -->

        <br />
        <div class="col 12">
            <ul class="collapsible" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header userDetails active" id="headerAddress" style="color: #eeeeee;"><b><i class="material-icons">&#xE313;</i>Account Information</b></div>
                    <div class="collapsible-body">

                        <div class="row profileRows" style="position: relative; margin: 0px;">

                            <!-- row noMargin -->
                            <div class="row noMargin noPadding">
                                <br /><br />
                                <div class="col l12">
                                    <p class="noPadding pInfoHeader" style="font-size: 20px;">MAILING ADDRESS</p>
                                </div>

                                <div class="col l1 s12">
                                    <p class="noPadding">Email:</p>
                                </div>
                                <div class="col l5 s12">
                                    <p class="noPadding" style="padding-bottom: 0;" >
                                        <asp:TextBox class="txtEmail" ID="txtEmail" type="Text" runat="server" ReadOnly="true" ForeColor="Black" MaxLength="50"></asp:TextBox>
                                    </p>
                                    <br /><br />
                                </div>
                                <!--<div class="col s12 right"  style="padding-bottom:12px;">
                                    <p class="noPadding2 right"><a class="waves-effect waves-purple btn pink accent-2 lnkEmailEdit hide-on-med-and-down">Edit</a></p>
                                    <p class="noPadding2 right"><a class="waves-effect waves-purple btn pink accent-2 lnkEmailEdit show-on-medium-and-down hide-on-large-only" style="padding:10px;">Edit</a></p>
                                </div>-->

                                <div class="col l12">
                                    <p class="noPadding pInfoHeader" style="font-size: 20px;">CURRENT PASSWORD</p>
                                </div>
                                <div class="col l1 s12">
                                    <p class="noPadding" style="padding-bottom: 0;">Password:</p>
                                </div>
                                <div class="col l11 s12">
                                    <p style="padding-bottom: 0;" class="noPadding">
                                        <asp:Label ID="txtPassword" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div class="col s12 right" style="padding-bottom: 12px;">
                                    <p class="noPadding2 right"><a class="waves-effect waves-purple btn pink accent-2 lnkEditPassword hide-on-med-and-down">EDIT</a></p>
                                    <p class="noPadding2 right"><a class="waves-effect waves-purple btn pink accent-2 lnkEditPassword show-on-medium-and-down hide-on-large-only" style="padding: 10px;">EDIT</a></p>
                                </div>
                            </div>
                            <br /><br />

                            <!-- row noMargin -->
                            <div class="row noMargin noPadding">
                                <div class="passwordFadein valign-wrapper" style="padding: 5px;">
                                    <div class="row profileRows" style="width: 100%; padding-left: 25px;">

                                        <div class="changePw" style="position:relative;">
                                            <div class="row">
                                                <div class="col s12">
                                                    <br /><br />
                                                    <p class="fadeHeader center white-text" style="font-size: 30px; margin-bottom: -20px; margin-top: 0px; padding:0;">Change your password</p>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="row noMargin">
                                                <div class="col l2 s12">
                                                    <p class="white-text noPadding2">Old Password:</p>
                                                </div>
                                                <div class="col l3 s12 white-text">
                                                    <input type="password" id="txtOldPassword" name="txtOldPassword" class="passwordField" />
                                                </div>
                                            </div>

                                            <div class="row noMargin">
                                                <div class="col l2 s12">
                                                    <p class="white-text noPadding2">New Password:</p>
                                                </div>
                                                <div class="col l3 s12 white-text">
                                                    <input type="password" id="txtNewPassword2" name="txtNewPassword2" class="passwordField" />
                                                </div>
                                                <div class="col l2 offset-l1 s12">
                                                    <p class="white-text noPadding2">Confirm New Password:</p>
                                                </div>
                                                <div class="col l3 s12 white-text">
                                                    <input type="password" id="txtConfirmPassword" name="txtConfirmPassword" class="passwordField" />
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col s12 center">
                                                    <a id="btnSubmitPassword" class="waves-effect waves-light btn btnSubmitPassword btn blue darken-2 hide-on-med-and-down" style="margin-top: 30px;">Update</a>
                                                    <a id="Button1" class="waves-effect waves-light btn lnkEditPasswordCancel red darken-2 hide-on-med-and-down" style="margin-top: 30px;">Cancel</a>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col s12 center">
                                                    <a id="Button2" class="waves-effect waves-light btn btnSubmitPassword btn blue darken-2 show-on-medium-and-down hide-on-large-only" style="margin-top: 30px; padding: 10px;">Update</a>
                                                    <a id="Button3" class="waves-effect waves-light btn lnkEditPasswordCancel btn red darken-2 show-on-medium-and-down hide-on-large-only" style="margin-top: 30px; padding: 10px;">Cancel</a>
                                                </div>
                                            </div>
                                        </div> <!-- ChangePW -->
                                        <!--<button type="button" id="btnTest">submit</button>
                                        <button type="button" id="btnHide1">cancel</button> -->
                                    </div>
                                </div>
                            </div>
                            <!-- END of row noMargin in passwordFadeIn -->
                        </div>
                        <!-- END of main row [profileRows] -->
                    </div>
                    <!-- END of collapsible body -->
                </li>
                <li>
                    <div class="collapsible-header userDetails active" style="color: #eeeeee;"><i class="material-icons">&#xE313;</i><b>Personal Information</b></div>
                    <div class="collapsible-body">
                        <div class="row profileRows" style="position: relative; margin: 0px;">

                            <!-- row noMargin -->
                            <div class="row noMargin noPadding">
                                <div class="addressFadein valign-wrapper" style="padding: 5px; overflow: auto;">
                                    <div class="row profileRows white-text" style="width: 100%; padding-left: 25px;">

                                        <div class="divUpdateAddress">
                                            <!-- Home address section -->
                                            <div class="row">
                                                <div class="col s12">
                                                    <p class="fadeHeader center" style="font-size: 30px; margin-top: 0px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px;">UPDATE YOUR ADDRESS</p>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col l4 s12">
                                                    <label>Address Type<span class="red-text">*</span></label>
                                                    <select class="browser-default ddlAddressType" id="ddlAddressType" style="color: black"><%Response.Write(getAddressType()); %></select>
                                                    <%--<asp:RequiredFieldValidator ID="rfvOProvince" runat="server" ErrorMessage="Origin Province is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtOriginProvince" Text="Origin Province is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col l4 s12">
                                                    <label>Province<span class="red-text">*</span></label>
                                                    <select class="browser-default ddlOriginProvince" id="ddlProvinceHome" style="color: black"><%Response.Write(getProvinces()); %></select>
                                                    <asp:TextBox ID="txtOriginProvince" CssClass="large validate text-color" runat="server" MaxLength="1" Style="display: none;"></asp:TextBox>
                                                    <asp:HiddenField ID="hdftxtOriginProvince" runat="server" />
                                                    <%--<asp:RequiredFieldValidator ID="rfvOProvince" runat="server" ErrorMessage="Origin Province is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtOriginProvince" Text="Origin Province is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>--%>
                                                </div>

                                                <div class="col l4 s12">
                                                    <label>City / Municipality<span class="red-text">*</span></label>
                                                    <select class="browser-default ddlCityMunicipalityHome" id="ddlCityMunicipalityHome" style="color: black">
                                                        <option class='muni1' value='none' selected disabled>Select City Municipality</option>
                                                    </select>
                                                    <asp:TextBox ID="txtOriginCityMunicipality" CssClass="large validate text-color" runat="server" MaxLength="1" Style="display: none;"></asp:TextBox>
                                                    <asp:HiddenField ID="hdfOriginCityMunicipality" runat="server" />
                                                    <%--<asp:RequiredFieldValidator ID="rfvOCity" runat="server" ErrorMessage="Origin City / Municipality is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="txtOriginCityMunicipality" Text="Origin City / Municipality is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>--%>
                                                </div>

                                                <div class="input-field col l4 s12 center">
                                                    <input id="Ozipcode" type="text" class="validate Ozipcode" maxlength="5" style="margin: 0; margin-top: 19px;">
                                                    <label class="active lblOzipcode" for="Ozipcode" style="margin: 0; margin-top: 19px;">Zip Code<span class="red-text spnOZipcode">*</span></label>
                                                    <div class="active lblOzipcodeError red-text" for="Ozipcode" style="margin: 0; margin-top: 5px; display: none">Invalid Zipcode!</div>
                                                    <%--<asp:RequiredFieldValidator ID="rfvOzipcode" runat="server" style="margin:0; margin-top:19px;" ErrorMessage="Zipcode is required" ValidationGroup="SignUp" Display="Dynamic"  SetFocusOnError="true" ControlToValidate="Ozipcode" Text="Zipcode is required" CssClass="ValidationSummary"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>

                                            <div class="row noMargin">
                                                <div class="input-field col l8 s12">
                                                    <input id="Ostreet" type="text" class="validate Ostreet" style="text-transform: capitalize;" maxlength="150">
                                                    <label class="active" for="Ostreet" style="margin: 0">Street</label>
                                                </div>
                                                <div class="input-field col l4 s12">
                                                    <input id="Obarangay" type="text" class="validate Obarangay" maxlength="100">
                                                    <label class="active" for="Obarangay" style="margin: 0;">Barangay</label>
                                                </div>
                                            </div>
                                            <!-- END of Home address section -->

                                            <div class="row">
                                                <div class="col s12 center">
                                                    <a class="waves-effect waves-light btn lnkUpdateAddress blue accent-2 hide-on-med-and-down" style="margin-top: 30px;">Update</a>
                                                    <a class="waves-effect waves-light btn lnkEditAddressCancel red darken-2 hide-on-med-and-down" style="margin-top: 30px;">Cancel</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s12 center">
                                                    <a class="waves-effect waves-light btn lnkUpdateAddress blue accent-2 show-on-medium-and-down hide-on-large-only" style="padding: 10px;">Update</a>
                                                    <a class="waves-effect waves-light btn lnkEditAddressCancel red darken-2 show-on-medium-and-down hide-on-large-only" style="padding: 10px;">Cancel</a>
                                                </div>
                                            </div> <!-- END of divUpdateAddress -->
                                        </div>
                                    </div> <!-- END of profile rows -->
                                    <br />
                                </div>
                                <!-- END of addressFadeIn -->
                            </div>
                            <!-- END of row noMargin -->

                            <br /><br /><br />
                            <!-- ADDRESS Display -->
                            <div class="col s12">
                                <p class="noPadding pInfoHeader" style="font-size: 20px;">ADDRESS</p>
                            </div>

                            <div class="row profileRows noMargin noPadding2 popAllAddress">
                                <% Response.Write(listAllAddresses());%>
                            </div>

                            <div class="row profileRows noMargin noPadding">

                                <div class="col s12 right">
                                    <p class="noPadding2 right"><a class="waves-effect waves-light btn pink accent-2 lnkEditAddress hide-on-med-and-down">Edit</a></p>
                                    <p class="noPadding2 right"><a class="waves-effect waves-light btn pink accent-2 lnkEditAddress show-on-medium-and-down hide-on-large-only" style="padding: 10px;">Edit</a></p>
                                </div>
                            </div>
                            <!-- END of ADDRESS Display -->

                            <br /><br />

                            <div class="col l12">
                                <p class="noPadding pInfoHeader" style="font-size: 20px;">CONTACT NUMBER/S</p>
                            </div>

                            <!-- row profileRows -->
                            <div class="row profileRows noMargin noPadding">
                                <div class="divContactTbl" style="position:relative;">
                                    <div class="col l8 s12" id="tableContactInformation" style="padding-left: 40px;">
                                        <table class="responsive-table hoverable" id="tblContactList">
                                            <thead>
                                                <tr>
                                                    <th>Type</th>
                                                    <th>Contact #</th>
                                                    <th><a class="btn-floating btn pink accent-2 waves-effect waves-light modal-trigger-newContact lnkContactNo"><i class="material-icons">&#xE145;</i></a></th>
                                                </tr>
                                            </thead>
                                            <tbody id="contactBody">
                                                <% Response.Write(PopulateContactNo()); %>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <br /><br /><br />
                            </div>
                            <!-- END of row ProfileRows -->

                            <!-- row noMargin -->
                            <div class="row noMargin noPadding white-text">
                                <div class="contactFadeIn valign-wrapper" style="padding: 5px;">
                                    <!-- Inner Row -->
                                    <div class="row profileRows" style="width: 100%; padding-left: 25px;">

                                        <div class="divcontactNumbers">
                                            <div class="row">
                                                <div class="col s12">
                                                    <p class="noPadding2 fadeHeader center" style="font-size: 30px;">ADD NEW CONTACT</p>
                                                </div>
                                            </div>

                                            <div class="row center">
                                                <div class="input-field col s12 l3 offset-l3 center">
                                                    <asp:DropDownList ID="ddlContactType" runat="server" CssClass="browser-default black-text">
                                                    </asp:DropDownList>
                                                    <br />
                                                </div>

                                                <div class="input-field col s12 l3" id="phoneInputHolder">
                                                    <i class="material-icons prefix">&#xE0CD;</i>
                                                    <asp:TextBox ID="txtProfileContactInformation" CssClass="txtProfileContactInformation center" runat="server" MaxLength="11"></asp:TextBox>
                                                    <label for="txtProfileContactInformation" id="lblContactINformation">Contact #</label>
                                                </div>
                                            </div>

                                            <div class="col s12 center">
                                                <a class="waves-effect waves-light btn blue hide-on-med-and-down lnkAddNewContact" style="width: 119px;">Add</a>
                                                <a class="waves-effect waves-light btn btnContactCancel red darken-2 hide-on-med-and-down">Cancel</a>
                                            </div>

                                            <div class="col s12 center">
                                                <a class="waves-effect waves-light btn lnkAddNewContact blue accent-2 show-on-medium-and-down hide-on-large-only" style="padding: 10px;">Add</a>
                                                <a class="waves-effect waves-light btn btnContactCancel red darken-2 show-on-medium-and-down hide-on-large-only" style="padding: 10px;">Cancel</a>
                                            </div>
                                        </div> <!-- END of divcontactNumbers -->
                                    </div>
                                    <!-- END of profile rows -->
                                </div>
                            </div>
                            <!-- END of row noMargin -->
                            <br />
                            <br />
                            <br />
                        </div><!-- END of Main Div ProfileRows -->
                    </div>
                    <!-- END of collapsible body -->
                </li>
                <li>
                    <asp:HiddenField ID="hdfFileID" runat="server" />
                  <div class="collapsible-header userDetails active" style="color: #eeeeee;"><i class="material-icons">&#xE313;</i><b>Documents</b></div>
                    <div class="collapsible-body">
                        <div class="container" style="padding:0;">
                            <div class="row" id="" style="padding:0;">
                                <% Response.Write(getAllRequiredAttachments()); %>
                            </div>
                            <div class="row">
                                <div class="col s12 center">
                                    <a id="20150923000012" href="#ModalEdit" class="btn pink accent-2 editAttach pwdsenior white-text waves-effect waves-dark modal-trigger tooltipped"  data-position="bottom" data-delay="10" data-tooltip="(Senior Citizen ID)" style="margin-bottom:10px;">Upload Senior ID</a>
                                    <a id="20150923000013" href="#ModalEdit" class="btn pink accent-2 editAttach pwdsenior white-text waves-effect waves-dark modal-trigger tooltipped" data-position="bottom" data-delay="10" data-tooltip="(Person with Disabilities)" style="margin-bottom:10px;">Upload PWD ID</a>
                                </div>
                            </div>
                        </div>  
                    </div>
                </li>
            </ul>
            <br /><br /><br /><br />

            <div id="ModalEdit" class="modal" style="width:450px;overflow-x:hidden;bottom:0;right:0;margin-top:0px;">
                <div class="modal-content" style="padding:0px;">
                    <div class="col s12 center">                       
                        <h5>Upload Your Attachment</h5>
                    </div> 
                    <div class="divider"></div><br />
                    <div class="row center">  
                        <div class="col s12">
                            <img id="prevUploadImage" class="prevUploadImage" style="height:300px;width:300px" src="../../UploadedImages/Attachment/Default.gif" alt="your image" />
                        </div>
                        <div class="col s12">
                            <asp:FileUpload ID="uploadAttachment" runat="server" class="black-text" style="margin-left:84px;" accept="image/jpeg,image/png"/>
                        </div>
                    </div>
                    <div class="row center" style="margin-left:20px;margin-right:20px;margin-bottom:0px;">
                        <div id="expirationDateDiv">
                            <div class="col m4 s12">
                                <p>Expiration Date:</p>
                            </div>
                            <div class="col m8 s12">
                                <input type ="date" id="expirationDate" name="expirationDate" />
                            </div>
                        </div>
                    </div>
                    <div class="row center" style="margin-left:20px;margin-right:20px;margin-bottom:0px;">
                        <div class="col m4 s12">
                            <p>Description:</p>
                        </div>
                        <div class="col m8 s12">
                            <input type ="text" id="attachmentDescription" name="attachmentDescription" />
                        </div>
                    </div>
                    <div class="row center">
                        <div class="col s12 center">
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$" ControlToValidate="uploadAttachment" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                                        Display="Dynamic" ValidationGroup="SignUp" />
                            <asp:CustomValidator runat="server" ID="custPrimeCheck" ForeColor="Red" ControlToValidate="uploadAttachment" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="SignUp" />--%>
                            <asp:HiddenField ID="attachmentID" runat="server" />
                            <asp:HiddenField ID="expirationDateHidden" runat="server" />
                            <asp:HiddenField ID="attachmentDescriptionHidden" runat="server" />
                        </div> 
                    </div>                                        
                </div> <!-- END of modal-content -->
                <div class="divider"></div>
                <div class="modal-footer">
                    <a id="uploadImageToServer" runat="server" class="modal-action modal-close waves-effect waves-green btn pink accent-2" onserverclick="btnUpdateImage_Click">Upload Image</a>
                </div>
            </div> <!-- END of modal edit -->

            <div id="Div1" class="modal" style="width:450px;overflow-x:hidden;">
                <div class="modal-content" style="padding:0px;">
                    <div class="col s12 center">                       
                        <h5>Update Profile Picture</h5>
                        <p class="red-text center" style="margin-top:0px;">Your account will be reverified if you upload a new picture.</p>
                        <div class="divider"></div><br />
                        <div class="col s12 center">
                            <img id="prevUploadImageProfile" style="height:300px;width:300px" class="prevUploadImageProfile" src="../../Images/no-image.gif" alt="your image" />
                        </div>
                        <asp:FileUpload ID="profilePictureUpload" runat="server" class="black-text" style="margin-left:84px;" accept="image/jpeg,image/png"/><br />
                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" SetFocusOnError="true" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$" ControlToValidate="profilePictureUpload" runat="server" ForeColor="Red" ErrorMessage="Please select a valid image file."
                                                    Display="Dynamic" ValidationGroup="SignUp" />
                        <asp:CustomValidator runat="server" ID="CustomValidator1" ForeColor="Red" ControlToValidate="profilePictureUpload" SetFocusOnError="true" ClientValidationFunction="Upload" ErrorMessage="Files is too large" ValidationGroup="SignUp" />--%>
                    </div>                   
                </div>
                <div class="divider"></div>
                <div class="modal-footer">
                    <a id="A1" runat="server" class="modal-action waves-effect waves-green btn pink accent-2" onserverclick="btnUpdateProfilePick_Click">Upload Image</a>
                </div>
            </div>
            
            <div id="Div2" class="modal" style="width: 450px; overflow-x: hidden;">
                <div class="modal-content" style="padding: 0px;">
                    <div class="row center">
                        <div class="col s12">
                            <h5>Update Birthday</h5>
                            <p class="red-text center" style="margin-top: 0px;">Your account will be reverified if you modified your birthday.</p>
                        </div>
                        <div class="divider"></div>
                        <div class="col s12">
                            Birthdate:
                        </div>
                        <div class="col m6 offset-m3 s8 offset-s2">
                            <asp:TextBox ID="txtModifyBirthday" type="date" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="modal-footer">
                    <a id="modifyBirthdate" runat="server" class="modal-action waves-effect waves-green btn pink accent-2" onserverclick="modifyBirthdate_ServerClick">Update Birthdate</a>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        $(document).ready(function () {

            $("#I1").on("click", function () {
                $("#errorPrompt").fadeOut("slow");
                $("#errorPrompt").text("");
            });

            $(".lblBirthdate").on("click", function ()
            {
                $("#Div2").openModal();
            });

            
            $(".divClose").click(function () {
                $(".statusUpdate").css({ "display": "none" });
            })
            var inputString = "";

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var dataURL = e.target.result;
                        var mimeType = dataURL.split(",")[0].split(":")[1].split(";")[0];
                        if (mimeType != "image/jpeg" && mimeType != "image/png")
                        {
                            var control = $("#<%=uploadAttachment.ClientID%>");                            
                            control.replaceWith(control = control.clone(true));
                            $("#prevUploadImage").attr("src", "../../Images/no-image.gif");
                            alert("Invalid File Type!");
                            return;
                        }
                        
                        $('#prevUploadImage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURL2(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var dataURL = e.target.result;
                        var mimeType = dataURL.split(",")[0].split(":")[1].split(";")[0];
                        if (mimeType != "image/jpeg" && mimeType != "image/png") {
                            var control = $("#<%=profilePictureUpload.ClientID%>");
                            control.replaceWith(control = control.clone(true));
                            $("#prevUploadImageProfile").attr("src", "../../Images/no-image.gif");
                            alert("Invalid File Type!");
                            return;
                        }
                        $('#prevUploadImageProfile').attr('src', e.target.result);  
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#expirationDate").on("change", function () {              
                $("#<%= expirationDateHidden.ClientID %>").val($(this).val());
            });

            $("#attachmentDescription").on("change", function () {
                $("#<%= attachmentDescriptionHidden.ClientID %>").val($(this).val());
            });

            $("#imgUserProfile").on("click", function ()
            {
                $("#Div1").openModal();
            });

            function Upload(sender, args) {
                var fileUpload = document.getElementById(sender.controltovalidate);
                if (typeof (fileUpload.files) != "undefined") {
                    var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
                    var maxFileSize = 2000
                    if (size < maxFileSize) {
                        args.IsValid = true;
                    } else {
                        $(fileUpload).closest("div").css({ "display": "block" });
                        args.IsValid = false;
                        return;
                    }
                } else {
                    alert("This browser does not support HTML5.");
                }
            }

            $(".deleditAttach").on("click", function ()
            {
                $(this).parent().parent().parent().remove();
                PageMethods.removeAttachment($(this).attr("id"),function (result)
                {
                    var message = "";
                    if (result == "1") {
                        message = "Can't remove Verified Attachments!";
                    }else if(result == "2")
                    {
                        message = "Can't remove specified attachment type id";
                    }
                    else
                    {
                        message = "Attachment has been removed!";                        
                    }
                    alert(message);
                    window.location.href = window.location.protocol + '//' + window.location.host + window.location.pathname;
                });

            });

            $(".pwdsenior").on("click", function ()
            {
                $("#expirationDateDiv").hide();
            });

            $(".notpwdsenior").on("click", function ()
            {
                $("#expirationDateDiv").show();
            });

            $("#ContentPlaceHolder1_uploadAttachment").on("change", function () {
                readURL(this);
            });

            $("#<%= profilePictureUpload.ClientID %>").on("change", function () {
                readURL2(this);
            });0

            $(".editAttach").on("click", function ()
            {
                inputString = $(this).attr("id");
                $("input[id*='hdfFileID']").val(inputString);
                $("#<%= hdfFileID.ClientID %>").val(inputString);
            });


            $('.mod1').leanModal({  
                complete: function () {
                    $('#prevUploadImage').attr('src', "../../UploadedImages/Attachment/Default.gif");
                    $("#expirationDate").val("");
                    $("#attachmentDescription").val("");
                }
            }
            );

            //Home
            $("#ddlProvinceHome").on("change", function () {
                $("#Ozipcode").val(null);
                $("#Ozipcode").focusout();
                PageMethods.getCityMunicipality($("#ddlProvinceHome").val(), function (result) {
                    $(".muni1").remove();
                    $("#ddlCityMunicipalityHome").append(result);
                    $("#Ostreet, #Obarangay").val(null).focusout();
                });
            });

            $("#ddlCityMunicipalityHome").on("change", function () {
                PageMethods.getZipCode($(this).val(), function (result) {
                    $("#Ozipcode").val(result);
                    $("#Ozipcode").focus();
                });
            });

            $("#Ozipcode").on("change", function () {
                PageMethods.getProvinces($(this).val(), function (result) {
                    if (result == null) {
                        alert("No specified Area for this ZipCode");
                        clearAddressFields();
                    }
                    var objResult = $.parseJSON(result);
                    $("#ddlProvinceHome").val(objResult.provinceID);
                    PageMethods.getCityMunicipality($("#ddlProvinceHome").val(), function (result) {
                        $(".muni1").remove();
                        $("#ddlCityMunicipalityHome").append(result);
                        $("#ddlCityMunicipalityHome option").each(function () {
                            if ($(this).val() == objResult.municipalityID) {
                                $(this).attr("selected", "selected");
                                return false;
                            }
                        });
                    });
                });
            });

            $(".lnkUpdateAddress").on("click", function () {

                if ($("#ddlAddressType option:selected").val() == "none") {
                    $('.divUpdateAddress').css('margin-top', '30px');
                    var parentContainer = $('.lnkUpdateAddress').parent().parent().parent().parent();
                    $('#msg').remove();
                    $(parentContainer).hide().prepend("<div id='msg' class='white-text center-align' style='left:0;top:0;min-width:100%;background-color:red;padding:10px;position:absolute;'><h6>Please specify your address type.</h6></div>").fadeToggle(1000);
                    $('.addressFadein #msg').delay(4000).fadeOut();
                    //alert("Please specify your address type!");
                    return false;
                }

                PageMethods.AddNewAddresInformation($("#ddlAddressType option:selected").val(), $("#Ostreet").val(), $("#Ozipcode").val(), $("#Obarangay").val(), $("#ddlCityMunicipalityHome option:selected").text(), $("#ddlProvinceHome").val(), function (result) {
                    if (result == 'none') {
                        //alert("Please specify your address type!");
                        return false;
                    }

                    $('.divUpdateAddress').css('margin-top', '30px');
                    var parentContainer = $('.lnkUpdateAddress').parent().parent().parent().parent();
                    $('#msg').remove();

                    if (result == 'Address has been updated') {
                        $(parentContainer).hide().prepend("<div id='msg' class='white-text center-align' style='left:0;top:0;min-width:100%;background-color:green;padding:10px;position:absolute;'><h6>" + result + "</h6></div>").fadeToggle(1000);
                        $('.addressFadein #msg').delay(4000).fadeOut();
                        $('.addressFadein').delay(3000).fadeOut();
                    } else {
                        $(parentContainer).hide().prepend("<div id='msg' class='white-text center-align' style='left:0;top:0;min-width:100%;background-color:red;padding:10px;position:absolute;'><h6>" + result + "</h6></div>").fadeToggle(1000);
                        $('.addressFadein #msg').delay(4000).fadeOut();
                    }

                    /*$(".addressFadein").fadeOut("slow");*/
                    $(".allAddressEnt").remove();
                    PageMethods.listAllAddresses(function (result) {
                        $(".popAllAddress").append(result);
                    });

                    clearAddressFields();
                    $("#ddlAddressType").val("none");
                });
            });

            $("#ddlAddressType").on("change", function () {
                PageMethods.checkAddress($(this).val(), function (result) {
                    if (result == "") {
                        clearAddressFields();
                        return false;
                    }
                    var parsedResult = $.parseJSON(result);
                    $("#Ozipcode").val(parsedResult.zipCode).trigger("change");
                    $("#Ostreet").val(parsedResult.street).focus();
                    $("#Obarangay").val(parsedResult.barangay).focus();
                });
            });


            function clearAddressFields() {
                $("#Ozipcode, #Ostreet, #Obarangay").val(null);
                $(".muni1").remove();
                $("#ddlProvinceHome").val("none");
                $("#ddlCityMunicipalityHome").append("<option class='muni1' value='none' selected disabled>Select City Municipality</option>");
            }

            function clearPasswordFields() {
                $(".passwordField").val("");
            }

            $(".btnSubmitPassword").click(function () {
                PageMethods.UpdatePassword($("#txtOldPassword").val(), $("#txtNewPassword2").val(), $("#txtConfirmPassword").val(), function (result) {
                    switch (result) {
                        case 1:
                            $('.changePw').css('margin-top', '60px');
                            var parentContainer = $('.btnSubmitPassword').parent().parent().parent().parent();
                            $('#msg').remove();
                            $(parentContainer).hide().prepend("<div id='msg' class='white-text center-align' style='left:0;top:0;min-width:100%;background-color:red;padding:10px;position:absolute;'><h6>Your new password does not match your confirm password.</h6></div>").fadeToggle(1000);
                            $('.passwordFadein #msg').delay(4000).fadeOut();
                            //alert("Your new password does not matched your confirmed password.");
                            clearPasswordFields();
                            $('#txtOldPassword').focus();
                            break;
                        case 2:
                            $('.changePw').css('margin-top', '60px');
                            var parentContainer = $('.btnSubmitPassword').parent().parent().parent().parent();
                            $('#msg').remove();
                            $(parentContainer).hide().prepend("<div id='msg' class='white-text center-align' style='left:0;top:0;min-width:100%;background-color:red;padding:10px;position:absolute;'><h6>Please enter a new password.</h6></div>").fadeToggle(1000);
                            $('.passwordFadein #msg').delay(4000).fadeOut();
                            //alert("Please enter a new password.");
                            clearPasswordFields();
                            $('#txtOldPassword').focus();
                            
                            break;
                        case 3:
                            $('.changePw').css('margin-top', '60px');
                            var parentContainer = $('.btnSubmitPassword').parent().parent().parent().parent();
                            $('#msg').remove();
                            $(parentContainer).hide().prepend("<div id='msg' class='white-text center-align' style='left:0;top:0;min-width:100%;background-color:red;padding:10px;position:absolute;'><h6>Incorrect password.</h6></div>").fadeToggle(1000);
                            $('.passwordFadein #msg').delay(4000).fadeOut();
                            //Materialize.toast("<p>Incorrect password. The quick brown fox jumps over the lazy dog.</p>");
                            clearPasswordFields();
                            $('#txtOldPassword').focus();
                            break;
                        case 4:
                            /*var parentContainer = $('.btnSubmitPassword').parent().parent().parent().parent();*/
                            $('.changePw').css('margin-top', '60px');
                            var parentContainer = $('.btnSubmitPassword').parent().parent().parent().parent();
                            $(parentContainer).hide().prepend("<div id='msg' class='white-text center-align' style='left:0;top:0;min-width:100%;background-color:green;padding:10px;position:absolute;'><h6>You've successfully updated your password!</h6></div>").fadeToggle(1000);
                            $('.passwordFadein #msg').delay(4000).fadeOut();
                            $('.passwordFadein').delay(3000).fadeOut();

                            clearPasswordFields();
                            $('#txtOldPassword').focus();
                            break;
                        default:
                            $('.changePw').css('margin-top', '60px');
                            var parentContainer = $('.btnSubmitPassword').parent().parent().parent().parent();
                            $(parentContainer).hide().prepend("<div id='msg' class='white-text center-align' style='left:0;top:0;min-width:100%;background-color:red;padding:10px;position:absolute;'><h6>Something went wrong, we're sorry for the inconvenience.</h6></div>").fadeToggle(1000);
                            $('.passwordFadein #msg').delay(4000).fadeOut();
                            //alert("Something went wrong, we're sorry for the inconvenience.");
                            clearPasswordFields();
                    }
                });
            });

            $('#phoneInputHolder').on('keydown', '.txtProfileContactInformation', function (e) { -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault() });

            $(".lnkAddNewContact").click(function () {
                PageMethods.AddNewContactInformation($("#ContentPlaceHolder1_ddlContactType").val(), $("#ContentPlaceHolder1_txtProfileContactInformation").val(), function (result) {
                    $("#ContentPlaceHolder1_txtProfileContactInformation").val("");
                    PageMethods.PopulateContactNos(populateContacts_Result);

                    $('.divContactnumbers').css('margin-top', '30px');
                    var parentContainer = $('.lnkAddNewContact').parent().parent().parent().parent();
                    var color = "";

                    if (result == "New contact information added successfully!")
                    {
                        color = "green";
                        $(parentContainer).hide().prepend("<div id='msg' class='white-text center-align' style='left:0;top:0;min-width:100%;background-color:" + color + ";padding:10px;position:absolute;'><h6>" + result + "</h6></div>").fadeToggle(1000);
                        $('.contactFadeIn #msg').delay(4000).fadeOut();
                        $('.contactFadeIn').delay(3000).fadeOut();
                    }
                    else
                    {
                        color = "red"
                        $(parentContainer).hide().prepend("<div id='msg' class='white-text center-align' style='left:0;top:0;min-width:100%;background-color:" + color + ";padding:10px;position:absolute;'><h6>" + result + "</h6></div>").fadeToggle(1000);
                        $('.contactFadeIn #msg').delay(4000).fadeOut();
                    }
                    

                });
            });

            function populateContacts_Result(ResultString) {
                $(".removableTR").remove();
                $("#contactBody").append(ResultString);
            }

            $("body").on("click", ".removeNum", function () {
                PageMethods.removeSelectedContact($(this).attr("numberID"), function (result) {
                    PageMethods.PopulateContactNos(populateContacts_Result);
                    var parentContainer = $('.removeNum').parent().parent().parent().parent();
                    $(parentContainer).hide().prepend("<div id='msg' class='white-text center-align' style='left:0;top:0;min-width:100%;background-color:red;padding:10px;position:absolute;'><h6>" + result + "</h6></div>").fadeToggle(1000);
                    $('#msg').delay(3000).fadeOut();

                    var message = document.getElementById("msg");
                    if (message) {
                        $(message).delay(3000).fadeOut();
                    }
                    //alert(result);
                });
            });

            $(".contactFadeIn").hide();

            $(".btnContactCancel").click(function () {
                $(".contactFadeIn").fadeOut("fast");

                var xparentContainer = $(".btnContactCancel").parent().parent().parent().parent();
                $(xparentContainer).find("#msg").remove();
                $('.divContactNumbers').css('margin-top', '-30px');
            });

            $(".modal-trigger-newContact").click(function () {
                $(".contactFadeIn").fadeIn("fast");
                $(".txtProfileContactInformation").focus();
            });


            $(".addressFadein").hide();

            $(".lnkEditAddressCancel").click(function () {
                $("#ddlAddressType").val("none");
                clearAddressFields();
                $(".addressFadein").fadeOut("fast");

                var xparentContainer = $(this).parent().parent().parent().parent();;
                $(xparentContainer).find("#msg").remove();
                $('.divUpdateAddress').css('margin-top', '-30px');
            });

            $(".lnkEditAddress").click(function () {
                $(".addressFadein").fadeIn("fast");
                $('.txtHome').focus();
            });

            $(".passwordFadein").hide();

            $(".lnkEditPasswordCancel").click(function () {
                $(".passwordFadein").fadeOut("fast");

                var xparentContainer = $(this).parent().parent().parent().parent();;
                $(xparentContainer).find("#msg").remove();
                $('.changePw').css('margin-top', '-10px');
            });

            $(".lnkEditPassword").click(function () {
                $(".passwordFadein").fadeIn("fast");
                $('#txtOldPassword').focus();
            });

            $(".birthdateFadein").hide();

            $(".lnkEditBirthdateCancel").click(function () {
                $(".birthdateFadein").fadeOut("fast");
            });

            /*$(".lnkEditBirthdate").click(function () {
                $(".birthdateFadein").fadeIn("slow");
                $(".txtBirthdate").focus();
            });*/

            $("#lblContactINformation").click(function () {
                $("#ContentPlaceHolder1_txtProfileContactInformation").focus();
            });

        });




    </script>
</asp:Content>
