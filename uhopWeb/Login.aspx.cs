﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using uhopWeb.Classes;
using System.Data;
//using ASPSnippets.FaceBookAPI;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace uhopWeb
{
    public partial class Login : System.Web.UI.Page
    {

        public static string strFirstname;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

            if (!IsPostBack)
                {

                    if (Request.QueryString["error"] == "access_denied")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('User has denied access.')", true);
                        return;
                    }

                    string code = Request.QueryString["code"];
                    if (!string.IsNullOrEmpty(code))
                    {
                        //string data = FaceBookConnect.Fetch(code, "me");
                        //FaceBookUser faceBookUser = new JavaScriptSerializer().Deserialize<FaceBookUser>(data);
                        //faceBookUser.PictureUrl = string.Format("https://graph.facebook.com/{0}/picture", faceBookUser.Id);
                        ////pnlFaceBookUser.Visible = true;
                        //txtEmail1.Text = faceBookUser.Email;
                        //lblUserName.Text = faceBookUser.UserName;
                        //lblName.Text = faceBookUser.Name;
                        //lblEmail.Text = faceBookUser.Email;
                        //ProfileImage.ImageUrl = faceBookUser.PictureUrl;
                        //btnLogin.Enabled = false;
                    }



                    //txtEmail1.Text = "";
                    txtPassword2.Text = "";
                    String strQRYSTR = HttpContext.Current.Request.QueryString["UT"];
                    if (strQRYSTR != null)
                    {
                        spnLogin.InnerHtml = Request.QueryString["UT"].ToString() + ' ' + "Login";
                        spnLogin2.InnerHtml = "";
                        if (Request.QueryString["UT"].ToString().ToUpper() == "OPERATOR")
                        {
                            HdfIsOperatorLogIn.Value = "1";

                            spnLogin2.InnerHtml = "Member";
                        }
                        else
                        {
                            spnLogin2.InnerHtml = "Operator";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("index.aspx");
            }
        }
        protected void btnResendMail_Click(object sender, EventArgs e)
        {
            spnVerifyEmail.Style["display"] = "none";
            spnResendSuccess.InnerHtml = "Email successfully Re-send to your email address : <b>" + txtEmail1.Text + "</b>";
            spnResendSuccess.Style["display"] = "block";
            ClsSendMail objSendMail = new ClsSendMail();
            objSendMail.SendMailSignUp(txtEmail1.Text, strFirstname);
        }

        protected void BtnogIn_Click(object sender, EventArgs e)
        {
            spnResendSuccess.Style["display"] = "none";
            spnVerifyEmail.Style["display"] = "none";
            using (ClsMembers objMembers = new ClsMembers())
            {
                objMembers.FillByEmail(txtEmail1.Text);

                if (objMembers.TblMembers.Rows.Count > 0)
                {
                    strFirstname = objMembers.TblMembers.Rows[0].GetString("firstname");

                    if (objMembers.TblMembers.Rows[0].GetString("Isverified") != "0")
                    {
                        string encryptedPassword = "";
                        using (SHA1Managed sha1 = new SHA1Managed())
                        {
                            var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(txtPassword2.Text));
                            var sb = new StringBuilder(hash.Length * 2);

                            foreach (byte b in hash)
                            {

                                sb.Append(b.ToString("x2"));
                            }

                            encryptedPassword = sb.ToString().ToLower();
                        }

                        if (encryptedPassword == objMembers.TblMembers.Rows[0].GetString("Password"))
                        {

                            //Session["MemberID"] = drw[0].GetString("MemberID");
                            //temporary comment
                            string strMID = ClsEncryptor.Encrypt(objMembers.TblMembers.Rows[0].GetString("MemberID"), "Uh0p");
                            Response.Cookies["Uhop"]["Token"] = strMID;
                            Response.Cookies["Uhop"]["lastVisit"] = DateTime.Now.ToString();
                            Response.Cookies["Uhop"].Expires = DateTime.Now.AddDays(1);

                            Session["IsOperator"] = objMembers.TblMembers.Rows[0].GetString("IsOperator");

                            if (HdfIsOperatorLogIn.Value == "1" && objMembers.TblMembers.Rows[0].GetString("IsOperator") == "1")
                            {
                                //Response.Redirect("/UnderConstruction.aspx");
                                Response.Redirect("Forms/Operators/Maintenance/OperatorProfile.aspx");
                            }
                            else
                            {
                                Response.Redirect("Forms/Maintenance/UserProfile.aspx");
                                //Response.Redirect("/UnderConstruction.aspx");
                            }
                        }


                        else
                        {
                            Ulerr.InnerHtml = "<li>Incorrect E-mail or password</li>";
                        }
                    }
                    else
                    {
                        spnVerifyEmail.Style["display"] = "block";
                    }
                }
                else
                {
                    Ulerr.InnerHtml = "<li>Incorrect E-mail or password</li>";
                }
            }
            
        }

        public class FaceBookUser
        {
            //public string Id { get; set; }
            //public string Name { get; set; }
            //public string UserName { get; set; }
            //public string PictureUrl { get; set; }
            //public string Email { get; set; }
        }

        protected void btnLoginFacebook_Click(object sender, EventArgs e)
        {
            //FaceBookConnect.Authorize("user_photos,email", Request.Url.AbsoluteUri.Split('?')[0]);
        }
    }
}