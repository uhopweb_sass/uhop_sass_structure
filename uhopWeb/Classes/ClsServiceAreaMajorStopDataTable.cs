﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;

namespace uhopWeb.Classes
{
    public class ClsServiceAreaMajorStopDataTable : IDisposable
    {
        public DSConfigs.ServiceAreaMajorStopDataTable TblCityMajorStop { get; set; }

        public string GetStationName(string MajorStopID)
        {
            string strReturn = "";



                DataRow[] drw = TblCityMajorStop.Select("MajorStopID='" + MajorStopID + "'");

                if (drw.Length > 0)
                {
                    strReturn = drw[0].GetString("StationName");
                }
            
            return strReturn;
        }

        public ClsServiceAreaMajorStopDataTable()
        {
            TblCityMajorStop = new DSConfigs.ServiceAreaMajorStopDataTable();
        }

        public virtual int Fill(string ServiceAreaID,string ServiceAreaLocationID)
        {
            TblCityMajorStop.Rows.Clear();
            return new ServiceAreaMajorStopTableAdapter().Fill(TblCityMajorStop, ServiceAreaID, ServiceAreaLocationID);
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}