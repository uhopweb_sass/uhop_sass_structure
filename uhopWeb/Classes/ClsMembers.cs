﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;
using System.Data;
namespace uhopWeb.Classes
{
    public class ClsMembers:IDisposable
    {


        public DSConfigs.MemberDataTable TblMembers { get; set; }
        public DSConfigs.ContactNumberDataTable TblContact { get; set; }
        public DSConfigs.uHopLocationDataTable TbluHopLocation  { get; set; }
        public DSConfigs.AttachmentDataTable TblAttachment  { get; set; }
        public DSConfigs.AttachmentTypeDataTable TblAttachmentType { get; set; }
        public DSConfigs.AddressDataTable TblAddress { get; set; }

        private string _MemberID = "";

        public ClsMembers()
        {
            TblAddress = new DSConfigs.AddressDataTable();
            TblContact = new DSConfigs.ContactNumberDataTable();
            TbluHopLocation = new DSConfigs.uHopLocationDataTable();
            TblAttachment = new DSConfigs.AttachmentDataTable();
            TblAttachmentType = new DSConfigs.AttachmentTypeDataTable();
            TblMembers = new DSConfigs.MemberDataTable();
        }
        public ClsMembers(string MemberID):this()
        {
            _MemberID = MemberID;

        }

        public virtual int FillAttachment()
        {
            TblAttachment.Rows.Clear();
            return new AttachmentTableAdapter().Fill(TblAttachment);
        }

        public virtual int FillAttachmentType()
        {
            TblAttachmentType.Rows.Clear();
            return new AttachmentTypeTableAdapter().Fill(TblAttachmentType);
        }

        public virtual int FillAddress()
        {
            TblAddress.Rows.Clear();
            return new AddressTableAdapter().Fill(TblAddress,_MemberID);
        }

        public virtual int FilluHopLocation()
        {
            TbluHopLocation.Rows.Clear();
            return new uHopLocationTableAdapter().Fill(TbluHopLocation);
        }

        public string GetLocationDetails(string pZipcode,string FieldName)
        {
            DataRow drw = TbluHopLocation.Select("zipcode='" + pZipcode + "'")[0];
            string strReturn = drw[FieldName] == DBNull.Value ? "" : (string)drw[FieldName];
            return strReturn;
        }


        public virtual int Fill(string memberID)
        {
            TblMembers.Rows.Clear();
            return new MemberTableAdapter().Fill(TblMembers, memberID);
        }

        public virtual int FillByEmail(string email)
        {
            TblMembers.Rows.Clear();
            return new MemberTableAdapter().FillByEmail(TblMembers, email);
        }

        public virtual int FillAttchmentType()
        {
            TblAttachmentType.Rows.Clear();
            return new AttachmentTypeTableAdapter().Fill(TblAttachmentType);
        }
        
        public string GetMemberDetails(string FieldName)
        {
            DataRow drw = TblMembers.Select("MemberID='" + _MemberID + "'")[0];
            string strReturn = drw[FieldName] == DBNull.Value ? "" : (string)drw[FieldName];
            return strReturn;
        }

        public string GetMemberDetailsViaEmail(string pEmail, string pFieldName)
        {
            DataRow drw = TblMembers.Select("email='" + pEmail + "'")[0];
            string strReturn = drw[pFieldName] == DBNull.Value ? "" : (string)drw[pFieldName];
            return strReturn;
        }

        public DateTime? GetMemberDetailsDate(string FieldName)
        {
            if (TblMembers.Select("MemberID='" + _MemberID + "'")[0].IsNull(FieldName))
            {
                return DateTime.Now;
            }
            else
            {
                return (DateTime)TblMembers.Select("MemberID='" + _MemberID + "'")[0].Field<DateTime>(FieldName);
            }
            
        }

        public void Update()
        {
            using (MemberTableAdapter TA = new MemberTableAdapter())
            {
                TA.Update(TblMembers);
            }
        }

        public void UpdateContact() {
            using (ContactNumberTableAdapter TA = new ContactNumberTableAdapter()) 
            {
                TA.Update(TblContact);                
            }
        }

        public void UpdateAddress()
        {
            using (AddressTableAdapter TA = new AddressTableAdapter())
            {
                TA.Update(TblAddress);
            }
        }

        public void UpdateAttachment()
        {
            new AttachmentTableAdapter().Update(TblAttachment);
        }


        public virtual int FillContacts(string memberID)
        {
            TblContact.Rows.Clear();
            return new ContactNumberTableAdapter().Fill(TblContact, memberID);
        }

        public virtual int FillAddress(string memberID)
        {
            TblAddress.Rows.Clear();
            return new AddressTableAdapter().Fill(TblAddress, memberID);
        }

        public string GetMemberDetailsByFieldName(string pMemberID,string FieldName)
        {
            DataRow drw = TblMembers.Select("MemberID='" + pMemberID + "'")[0];
            string strReturn = drw[FieldName] == DBNull.Value ? "" : (string)drw[FieldName];
            return strReturn;
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}