﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;

namespace uhopWeb.Classes
{
    public class ClsServiceAreaSubscription:IDisposable
    {

        public DSConfigs.ServiceAreaSubscriptionDataTable TblServiceAreaSubscription { get; set; }

        public ClsServiceAreaSubscription() {
            TblServiceAreaSubscription = new DSConfigs.ServiceAreaSubscriptionDataTable();
        }

        public string GetServiceAreaSubscriptionID()
        {
            string strReturn = "";
            strReturn = ((TblServiceAreaSubscription.Rows.Count > 0) ? TblServiceAreaSubscription.Rows[0].GetString("ServiceAreaSubscriptionID") : "");
            return strReturn;
        }

        public virtual int Fill(string serviceAreaID)
        {
            return new ServiceAreaSubscriptionTableAdapter().Fill(TblServiceAreaSubscription,serviceAreaID);
        }



        public void Dispose() { GC.SuppressFinalize(this); }
    }
}