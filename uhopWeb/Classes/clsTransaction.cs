﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSTransactionsTableAdapters;
using System.Data;

namespace uhopWeb.Classes
{
    public class ClsTransaction : IDisposable
    {
        public DSTransactions.AurumPayMemberDetailsDataTable TblAurumPayMemberDetails { get; set; }
        public DSTransactions.TransactionDataTable TblTransaction { get; set; }
        public DSTransactions.TransactionDetailsDataTable TblTransactionDetails { get; set; }
        public DSTransactions.ModesOfPaymentDetailsDataTable TblModesOfPaymentDetails { get; set; }
        public DSTransactions.TransactionSubscriptionDetailsDataTable TblTransactionSubscriptionDetails { get; set; }
        private string _MemberID = "";

        public ClsTransaction()
        {
            TblAurumPayMemberDetails = new DSTransactions.AurumPayMemberDetailsDataTable();
            TblTransaction = new DSTransactions.TransactionDataTable();
            TblTransactionDetails = new DSTransactions.TransactionDetailsDataTable();
            TblModesOfPaymentDetails = new DSTransactions.ModesOfPaymentDetailsDataTable();
            TblTransactionSubscriptionDetails = new DSTransactions.TransactionSubscriptionDetailsDataTable();
        }

        public virtual int FillTransactionSubscriptionDetails(string pServiceAreaSubscriptionLocationID)
        {
            TblTransactionSubscriptionDetails.Rows.Clear();
            return new TransactionSubscriptionDetailsTableAdapter().Fill(TblTransactionSubscriptionDetails, pServiceAreaSubscriptionLocationID);
        }

        public virtual int FillTransaction()
        {
            TblTransaction.Rows.Clear();
            return new TransactionTableAdapter().Fill(TblTransaction);
        }



        public void UpdateModesOfPaymentTransactionDetails()
        {
            new ModesOfPaymentDetailsTableAdapter().Update(TblModesOfPaymentDetails);
        }

        public void UpdateTransactionDetails()
        {
            new TransactionDetailsTableAdapter().Update(TblTransactionDetails);
        }

        public void UpdateTransaction()
        {
            new TransactionTableAdapter().Update(TblTransaction);
        }

        public ClsTransaction(string MemberID):this()
        {
            _MemberID = MemberID;
        }
        
        public virtual int FillAurumPayMemberDetails(string pMemberID)
        {
            TblAurumPayMemberDetails.Rows.Clear();
            return new AurumPayMemberDetailsTableAdapter().Fill(TblAurumPayMemberDetails, pMemberID);
        }

        public string GetMemberDetails(string FieldName)
        {
            DataRow drw = TblAurumPayMemberDetails.Select("MemberID='" + _MemberID + "'")[0];
            string strReturn = drw[FieldName] == DBNull.Value ? "" : (string)Convert.ToString(drw[FieldName]);
            return strReturn;
        }

        public void Dispose() { GC.SuppressFinalize(this); }

    }
}