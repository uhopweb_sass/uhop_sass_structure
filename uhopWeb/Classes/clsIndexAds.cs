﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;
using System.Data;

namespace uhopWeb.Classes
{
    public class ClsIndexAds:IDisposable
    {
        private static DSConfigs.IndexAdsDataTable _tbl = new DSConfigs.IndexAdsDataTable();
        public static DSConfigs.IndexAdsDataTable TblIndexAds { get { return _tbl; } set { _tbl = value; } }

        private string _AdsID = "";

        public ClsIndexAds()
        {

        }

        public ClsIndexAds(string AdsID)
        {
            _AdsID = AdsID;
        }

        public virtual int Fill(DSConfigs.IndexAdsDataTable tbl)
        {
            tbl.Rows.Clear();
            return new IndexAdsTableAdapter().Fill(tbl);
        }

        public string GetBranchDetails(string FieldName)
        {
            DataRow drw = _tbl.Select("AdsID='" + _AdsID + "'")[0];
            string strReturn = drw[FieldName] == DBNull.Value ? "" : (string)drw[FieldName];
            return strReturn;
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}