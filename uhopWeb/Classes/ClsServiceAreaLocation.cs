﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;

namespace uhopWeb.Classes
{
    public class ClsServiceAreaLocation:IDisposable
    {
        public static string GetServiceAreaLocationID(string Origin, string Destination)
        {
            string strReturn = "";
            using (SqlConnection cn = new SqlConnection(UHopCore.GetConnectionString()))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandText = "SELECT ServiceAreaLocationID FROM ServiceArea.ServiceAreaLocation WHERE Origin=@Origin AND Destination=@Destination";
                    cmd.Parameters.AddWithValue("@Origin", Origin);
                    cmd.Parameters.AddWithValue("@Destination", Destination);
                    cn.Open();
                    try
                    {
                        strReturn = cmd.ExecuteScalar().ToString();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally
                    { cn.Close(); }
                }
            }

            return strReturn;
        }

        public static string GetServiceAreaID(string Origin, string Destination)
        {
            string strReturn = "";

            using (SqlConnection cn = new SqlConnection(UHopCore.GetConnectionString()))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandText = "SELECT ServiceArea.ServiceAreaID FROM ServiceArea.ServiceAreaLocation INNER JOIN ServiceArea.ServiceArea ON(ServiceAreaLocation.ServiceAreaLocationID=ServiceArea.ServiceAreaLocationID) WHERE ServiceAreaLocation.Origin=@Origin AND ServiceAreaLocation.Destination=@Destination";
                    cmd.Parameters.AddWithValue("@Origin", Origin);
                    cmd.Parameters.AddWithValue("@Destination", Destination);
                    cn.Open();
                    try
                    {
                        strReturn = cmd.ExecuteScalar().ToString();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally
                    { cn.Close(); }
                }
            }

            return strReturn;
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}