﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace uhopWeb.Classes
{
    public class ClsErrorMessages
    {
        public static string messageForDateIsInSchedule = "Schedule already booked";
        public static string messageForDateConflict = "Schedule has conflict";
        public static string messageForDateStartGreaterThanDateExpired = "Return date and time must be greater than departure date and time";
        public static string messageForDateIsOutOfBoundsOfSubscrptionDate = "Schedule is out of your subscription date";
    }
}