﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace uhopWeb.Classes
{
    public class clsForgotPassword: IDisposable
    {

        public static string RandomText()
        {
            char[] arrPossibleChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();
            int intPasswordLength = 6;
            string stringText = "";
            System.Random rand = new System.Random();
            for (int i = 0; i <= intPasswordLength-1; i++)
            {
                int intRandom = rand.Next(arrPossibleChars.Length);
                stringText = stringText + arrPossibleChars[intRandom].ToString();
            }
            return stringText;
        }
        
        
        
        
        public void Dispose() { GC.SuppressFinalize(this); }
    }
}