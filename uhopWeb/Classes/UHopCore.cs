﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;

namespace uhopWeb.Classes
{
    public class UHopCore
    {

        public static string Title;

        public static string GetUploadImagesRoot() {
            return System.Configuration.ConfigurationManager.AppSettings["UploadVehicleImagesRoot"].ToString();
        }

        public static string  GetProfilePic(string memberID) {

            string strReturn= System.Configuration.ConfigurationManager.AppSettings["UploadMembersProfilePic"].ToString();
            ClsMembers objMembers = new ClsMembers(memberID);
            objMembers.Fill(memberID);

            if (objMembers.TblMembers.Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(objMembers.TblMembers.Rows[0].GetString("ProfilePicture")))
                {
                    strReturn += objMembers.TblMembers.Rows[0].GetString("ProfilePicture");
                }
                else
                {
                    strReturn += "Default_Profile.png";
                }
            }

            return strReturn + "?";
        }

        public static string GetSMTPServer()
        {
            return System.Configuration.ConfigurationManager.AppSettings["SMTPServer"].ToString();
        }

        public static string GetSMTPPassword()
        {
            return System.Configuration.ConfigurationManager.AppSettings["SmtpPassword"].ToString();
        }

        public static string GetSMTPSenderMail()
        {
            return System.Configuration.ConfigurationManager.AppSettings["SmtpSenderMail"].ToString();
        }

          public static string GetPageURL() {
              return System.Configuration.ConfigurationManager.AppSettings["PageURL"].ToString();
          }

          public static string ProfilePictureView()
          {
              return System.Configuration.ConfigurationManager.AppSettings["PicUrlView"].ToString();
          }

          public static string IndexAdsView()
          {
              return System.Configuration.ConfigurationManager.AppSettings["IndexAdsImages"].ToString();
          }

          public static string GetConnectionString()
          {
              return System.Configuration.ConfigurationManager.ConnectionStrings["UHopWebConnectionString"].ToString();
          }
       
    }
}