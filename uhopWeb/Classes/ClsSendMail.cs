﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Web.Services;
using uhopWeb.Classes;
using System.Configuration;
using System.IO;

namespace uhopWeb.Classes
{
    public class ClsSendMail
    {


        public bool SendMailLuxuryReservation(string MsgEmailReceiver, string pCustomerName, string pMobileNumber, string pLandlineNumber, string pServiceType, string pDescription, string pDepartureDate, string pNumberOfPax, string pRemarks, string pEmail)
        {
            string MsgBody = PopulateLuxuryReservation(pCustomerName, pMobileNumber, pLandlineNumber, pServiceType,pDescription, pDepartureDate, pNumberOfPax, pRemarks, pEmail);
            try
            {
                SendHtmlFormattedEmail(UHopCore.GetSMTPSenderMail(), MsgEmailReceiver, "u-Hop Luxury Reservation : " + pServiceType, MsgBody);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool SendMailLuxuryConfirm(string MsgEmailReceiver, string pFullName, string pBookingDate, string pDepartureDate, string pServiceType, string pDescription, string pSpecification, string pNumberOfPax, string pLinkHref, string pURL, string pRemarks)
        {
            string MsgBody = PopulateLuxuryConfirm(pFullName, pBookingDate, pDepartureDate, pServiceType, pDescription, pSpecification, pNumberOfPax, pLinkHref, pURL,pRemarks);
            try
            {
                SendHtmlFormattedEmail(UHopCore.GetSMTPSenderMail(), MsgEmailReceiver, "u-Hop Luxury Confirmation : " + pServiceType, MsgBody);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool SendMailContactUs(string MsgSender, string MsgSubject, string MsgBody)
        {
            try
            {
                SendHtmlFormattedEmail(MsgSender,UHopCore.GetSMTPSenderMail(),MsgSubject,MsgBody);
                //SendHtmlFormattedEmail(MsgSender, "ross.colas@gmail.com;customer.support@u-hop.com", MsgSubject, MsgBody);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool SendRequestForgotPassword(string MsgSenderEmail, string MsgRecieverName)
        {
            string MsgBody = PopulateBodyRequestForgot(MsgRecieverName, "You (or someone pretending to be you) requested to reset your password.<br />" + 
            "If you didn't make this request then ignore the email; no changes will be made.",
           "Click here!",
           UHopCore.GetPageURL() + "ForgotPasswordApproved.aspx?xRT=" + ClsEncryptor.Encrypt(MsgSenderEmail, "uh0p"),
           "If you did make the request, then click the link to reset your password.");
            try
            {
                SendHtmlFormattedEmail(UHopCore.GetSMTPSenderMail(), MsgSenderEmail, "u-Hop Password Reset Request", MsgBody);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool SendMailSignUp(string MsgSenderEmail, string MsgSenderName)
        {
            string MsgBody = PopulateBodySignup(MsgSenderName, "You are receiving this email beacause you recently created a new account at u-Hop Transport Network Vehicle System Inc.",
           "<a href='" + UHopCore.GetPageURL() + "EmailVerification.aspx?ND54662=" + ClsEncryptor.Encrypt(MsgSenderEmail, "uh0p") + "'>Click here! u-Hop Email verification</a><br />",
           "Or copy and paste this <i>URL</i> to your browser"+UHopCore.GetPageURL() + "EmailVerification.aspx?ND54662=" + ClsEncryptor.Encrypt(MsgSenderEmail,"uh0p"),
           "After visiting the above link you can log into the site!<br />" +
           "If you have any problems verifying your account, please reply to this email to get assistance.");
            try
            {
                SendHtmlFormattedEmail(UHopCore.GetSMTPSenderMail(),MsgSenderEmail,"U-Hop Email Verification" , MsgBody);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }


        public bool SendMailAurumPayment(string MsgEmailReceiver, string MemberName, string ModeOfPayment, string Status, string OrderID, string MerchantOrderID)
        {
            string MsgBody = PopulateBodyEReceipt(MemberName, ModeOfPayment, Status, OrderID, MerchantOrderID);
            try
            {
                SendHtmlFormattedEmail(UHopCore.GetSMTPSenderMail(), MsgEmailReceiver, "U-Hop Purchased Subscription : " + OrderID, MsgBody);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }


        public bool SendEmailNotify(string pEmailReceiver, string pMemberName,string pTitle, string pBody)
        {
            string MsgBody = PopulateBodyNotify(pMemberName,pBody,pTitle);
            try
            {
                SendHtmlFormattedEmail(UHopCore.GetSMTPSenderMail(), pEmailReceiver,pTitle,pBody);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }


        //SendMailSignUp string MemberEmail, string MemberName

        public bool SendHtmlFormattedEmail(string sender,string recipientEmail, string subject, string body)
        {

            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(sender);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;

                string[] strRecipient = recipientEmail.Split(';');
                for (int x = 0; x <= strRecipient.Length -1; x++)
                {
                    if (strRecipient[x].ToString() != "" || !string.IsNullOrEmpty(strRecipient[x].ToString()))
                    {
                        mailMessage.To.Add(new MailAddress(strRecipient[x].ToString()));
                    }
                }
                

                SmtpClient smtp = new SmtpClient();
                smtp.Host = UHopCore.GetSMTPServer();
                smtp.EnableSsl = false;
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = UHopCore.GetSMTPSenderMail();
                NetworkCred.Password = UHopCore.GetSMTPPassword();
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                try
                {
                    smtp.Send(mailMessage);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        private string PopulateBodyNotify(string MemberName, string BodyText, string title)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailNotify.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{UserName}", MemberName);
            body = body.Replace("{BodyText}", BodyText);
            body = body.Replace("{Title}", title);
            return body;
        }

        private string PopulateBodySignup(string userName,string BodyText, string title, string url, string description)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailSignup.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{UserName}", userName);
            body = body.Replace("{BodyText}", BodyText);
            body = body.Replace("{Title}", title);
            body = body.Replace("{Url}", url);
            body = body.Replace("{Description}", description);
            return body;
        }

        private string PopulateBodyRequestForgot(string userName, string BodyText, string title, string url, string description)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailForgotPassword.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{UserName}", userName);
            body = body.Replace("{BodyText}", BodyText);
            body = body.Replace("{Title}", title);
            body = body.Replace("{Url}", url);
            body = body.Replace("{Description}", description);
            return body;
        }

        private string PopulateBodyEReceipt(string userName, string ModeOfPayment, string Status, string OrderID, string MerchantOrderID)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailReceipt.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{UserName}", userName);
            body = body.Replace("{ModeOfPayment}", ModeOfPayment);
            body = body.Replace("{Status}", Status);
            body = body.Replace("{AurumPayTransactionCode}", OrderID);
            body = body.Replace("{TransactionID}", MerchantOrderID);
            return body;
        }

        private string PopulateLuxuryConfirm(string pFullName, string pBookingDate, string pDepartureDate, string pServiceType, string pDescription, string pSpecification, string pNumberOfPax,string pLinkHref,string pURL,string pRemarks)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailLuxuryConfirm.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{FullName}", pFullName);
            body = body.Replace("{BookingDate}", pBookingDate);
            body = body.Replace("{DepartureDate}", pDepartureDate);
            body = body.Replace("{ServiceType}", pServiceType);
            body = body.Replace("{Description}", pDescription);
            body = body.Replace("{Specification}", pSpecification);
            body = body.Replace("{NumberOfPax}", pNumberOfPax);
            body = body.Replace("{LINKHref}", pLinkHref);
            body = body.Replace("{URL}", pURL);
            body = body.Replace("{Remarks}", pRemarks);
            return body;
        }

        private string PopulateLuxuryReservation(string pCustomerName, string pMobileNumber, string pLandlineNumber, string pServiceType, string pDescription, string pDepartureDate, string pNumberOfPax, string pRemarks,string pEmail)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailLuxuryReservation.html")))
            {
                body = reader.ReadToEnd();
            }
            //body = body.Replace("{FullName}", pFullName);
            body = body.Replace("{CustomerName}", pCustomerName);
            body = body.Replace("{EmailAddress}", pEmail);
            body = body.Replace("{MobileNumber}", pMobileNumber);
            body = body.Replace("{LandlineNumber}", pLandlineNumber);
            body = body.Replace("{ServiceType}", pServiceType);
            body = body.Replace("{Description}", pDescription);
            body = body.Replace("{DepartureDate}", pDepartureDate);
            body = body.Replace("{NumberOfPax}", pNumberOfPax);
            body = body.Replace("{Remarks}", pRemarks);

            return body;
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }


}