﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;
using System.Data;
namespace uhopWeb.Classes
{
    public class ClsServiceAreas:IDisposable
    {

        public DSConfigs.ServiceAreaDataTable TblServiceAreas { get; set; }

        public DSConfigs.ServiceAreaLocationOriginDataTable TblServiceAreaLocationOrigin { get; set; }
        public DSConfigs.ServiceAreaLocationDestinationDataTable TblServiceAreaLocationDestination { get; set; }


        public ClsServiceAreas() {
            TblServiceAreas = new DSConfigs.ServiceAreaDataTable();
            TblServiceAreaLocationOrigin = new DSConfigs.ServiceAreaLocationOriginDataTable();
            TblServiceAreaLocationDestination = new DSConfigs.ServiceAreaLocationDestinationDataTable();
        }

        public virtual int FillOrigin()
        {
            TblServiceAreaLocationOrigin.Rows.Clear();
            return new ServiceAreaLocationOriginTableAdapter().Fill(TblServiceAreaLocationOrigin);
        }
        
        public virtual int FillDestination(String pOriginCityProvince)
        {
            TblServiceAreaLocationDestination.Rows.Clear();
            return new ServiceAreaLocationDestinationTableAdapter().Fill(TblServiceAreaLocationDestination, pOriginCityProvince);
        }

        

        public virtual int Fill(DSConfigs.ServiceAreaDataTable tbl)
        {
            tbl.Rows.Clear();
            return new ServiceAreaTableAdapter().Fill(tbl);
        }


        public string GetServiceAreaID(string From, string To)   
        {
            DataRow[] drw = TblServiceAreas.Select("ServiceAreaFromID='" + From + "' AND ServiceAreaToID='" + To + "'");
            return drw.Length > 0 ? drw[0].GetString("ServiceAreaID") : "";
        }


        public void Dispose() { GC.SuppressFinalize(this); }
    }
}