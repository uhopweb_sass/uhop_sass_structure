﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;
namespace uhopWeb.Classes
{
    public class clsAddressType : IDisposable
    {

        public static DSConfigs.AddressTypeDataTable TblAddressType = new DSConfigs.AddressTypeDataTable();

        public virtual int Fill()
        {
            return new AddressTypeTableAdapter().Fill(TblAddressType);
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}