﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;
using System.Data;

namespace uhopWeb.Classes
{
    public class ClsBranch:IDisposable
    {

        public DSConfigs.BranchDataTable TblBranch { get; set; }


        public ClsBranch()
        {
            TblBranch = new DSConfigs.BranchDataTable();
        }

        public virtual int Fill(DSConfigs.BranchDataTable tbl)
        {
            tbl.Rows.Clear();
            return new BranchTableAdapter().Fill(tbl);
        }
        
        public string GetBranchDetails(string FieldName,string branchID)
        {
            DataRow drw = this.TblBranch.Select("BranchID='" + branchID + "'")[0];
            string strReturn = drw[FieldName] == DBNull.Value ? "" : (string)drw[FieldName];
            return strReturn;
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}