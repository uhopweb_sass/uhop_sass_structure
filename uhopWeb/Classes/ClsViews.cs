﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSViewsTableAdapters;

namespace uhopWeb.Classes
{
    public class ClsViews:IDisposable
    {
        internal class ViewBookingHistory:IDisposable {

            private DSViews.ViewBookingHistoryDataTable _tblBookingHistory;
            public DSViews.ViewBookingHistoryDataTable TblBookingHistory { get { return _tblBookingHistory; } }

            public ViewBookingHistory() {

                _tblBookingHistory = new DSViews.ViewBookingHistoryDataTable();
            }

            public ViewBookingHistory(string MemberID):this() {
                using (ViewBookingHistoryTableAdapter TA = new ViewBookingHistoryTableAdapter())
                {
                    TA.Fill(_tblBookingHistory,MemberID);
                }
            }

            public void Dispose() { GC.SuppressFinalize(this); }
        }

        internal class ViewServiceAreaDestination:IDisposable
        {

            public string ServiceAreaLocationID { get; set; }
            public string Destination { get; set; }
            public string CityProvince { get; set; }
            public decimal Latitude { get; set; }
            public decimal Longitude { get; set; }
            public DSViews.ViewDestinationLimitedRideDataTable TblServiceAreaDestination { get; set; }

            public ViewServiceAreaDestination()
            {
                ServiceAreaLocationID = "";
                Destination = "";
                CityProvince = "";
                Latitude = 0;
                Longitude = 0;
            }

            public ViewServiceAreaDestination(string MemberID, string Origin) : this()
            {

                TblServiceAreaDestination = new DSViews.ViewDestinationLimitedRideDataTable();
                using (ViewDestinationLimitedRideTableAdapter TA = new ViewDestinationLimitedRideTableAdapter())
                {
                    TA.Fill(TblServiceAreaDestination,Origin,MemberID);
                }
                if (TblServiceAreaDestination.Rows.Count > 0)
                { ServiceAreaLocationID = TblServiceAreaDestination.Rows[0].GetString("ServiceAreaLocationID"); }
                
            }

            public ViewServiceAreaDestination(string memberID,string origin,string destination) : this()
            {
                TblServiceAreaDestination = new DSViews.ViewDestinationLimitedRideDataTable();
                using (ViewDestinationLimitedRideTableAdapter TA = new ViewDestinationLimitedRideTableAdapter())
                {
                    TA.FillBy(TblServiceAreaDestination, origin, memberID, destination);
                }

                ServiceAreaLocationID = TblServiceAreaDestination.Rows[0].GetString("ServiceAreaLocationID");
                Destination = TblServiceAreaDestination.Rows[0].GetString("Destination");
                CityProvince = TblServiceAreaDestination.Rows[0].GetString("DestinationCityProvince");
                Latitude = TblServiceAreaDestination.Rows[0].GetDecimal("DestinationLatitude");
                Longitude = TblServiceAreaDestination.Rows[0].GetDecimal("DestinationLongitude");
            }

            

            public virtual int Fill(string MemberID,string Origin)
            {
                return new ViewDestinationLimitedRideTableAdapter().Fill(TblServiceAreaDestination, Origin, MemberID);
            }

            public void Dispose() { GC.SuppressFinalize(this); }
        }

        internal class ViewServiceAreaOrigin:IDisposable
        {
            public DSViews.ViewOriginLimitedRideDataTable TblServiceAreaOrigin { get; set; }
            public string ServiceAreaID { get; set; }
            public string ServiceAreaLocationID { get; set; }
            public string Origin { get; set; }
            public string CityProvince { get; set; }
            public decimal Latitude { get; set; }
            public decimal Longitude { get; set; }

            public ViewServiceAreaOrigin()
            {
                ServiceAreaID="";
                ServiceAreaLocationID="";
                Origin="";
                CityProvince="";
                Latitude=0;
                Longitude = 0;
            }

            public ViewServiceAreaOrigin(string MemberID) : this()
            {
                TblServiceAreaOrigin = new DSViews.ViewOriginLimitedRideDataTable();
                using (ViewOriginLimitedRideTableAdapter TA = new ViewOriginLimitedRideTableAdapter())
                {
                    TA.Fill(TblServiceAreaOrigin, MemberID);
                }
            }

            public ViewServiceAreaOrigin(string memberID,string origin) : this()
            {
                TblServiceAreaOrigin = new DSViews.ViewOriginLimitedRideDataTable();
                using (ViewOriginLimitedRideTableAdapter TA = new ViewOriginLimitedRideTableAdapter())
                {
                    TA.FillBy(TblServiceAreaOrigin, memberID, origin);
                }

                ServiceAreaID = TblServiceAreaOrigin.Rows[0].GetString("ServiceAreaID");
                ServiceAreaLocationID = TblServiceAreaOrigin.Rows[0].GetString("ServiceAreaLocationID");
                Origin = TblServiceAreaOrigin.Rows[0].GetString("Origin");
                CityProvince = TblServiceAreaOrigin.Rows[0].GetString("OriginCityProvince");
                Latitude = TblServiceAreaOrigin.Rows[0].GetDecimal("OriginLatitude");
                Longitude = TblServiceAreaOrigin.Rows[0].GetDecimal("OriginLongitude");
            }

            

            public virtual int Fill(string MemberID)
            {
                return new ViewOriginLimitedRideTableAdapter().Fill(TblServiceAreaOrigin, MemberID);
            }

            public void Dispose() { GC.SuppressFinalize(this); }
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}