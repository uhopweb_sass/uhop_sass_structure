﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using uhopWeb.Classes;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using uhopWeb.DataSets;
using System.Web.Hosting;

namespace uhopWeb.Classes
{
    public class clsAuthenticator
    {
        public static void Authenticate()
        {
            if (HttpContext.Current.Request.Cookies["Uhop"] == null)
            {
                HttpContext.Current.Response.Redirect(UHopCore.GetPageURL() + "index.aspx");
            }
            else if (HttpContext.Current.Request.Cookies["Uhop"]["Token"] == null || string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["Uhop"]["Token"]))
            {
                HttpContext.Current.Response.Redirect(UHopCore.GetPageURL() + "index.aspx");
            }

        }

    }
}