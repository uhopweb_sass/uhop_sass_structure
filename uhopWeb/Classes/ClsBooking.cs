﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSTransactionsTableAdapters;

namespace uhopWeb.Classes
{
    public class ClsBooking:IDisposable
    {


        public DSTransactions.BookingDataTable tblBooking { get; set; }
        public ClsBooking() {
            tblBooking = new DSTransactions.BookingDataTable();
        }
        public virtual int Fill(DSTransactions.BookingDataTable tbl,string memberID)
        {
            tbl.Rows.Clear();
            return new BookingTableAdapter().Fill(tbl,memberID);
        }

        public void Update()
        {
            using (BookingTableAdapter TA = new BookingTableAdapter())
            {
                TA.Update(tblBooking);
            }
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}