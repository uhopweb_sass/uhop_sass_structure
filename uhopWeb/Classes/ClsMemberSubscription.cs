﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSTransactionsTableAdapters;

namespace uhopWeb.Classes
{
    public class ClsMemberSubscription:IDisposable
    {

        public DSTransactions.MemberSubscriptionDataTable TblMemberSubscription { get; set; }
        public DSTransactions.MemberSubscriptionValidationDataTable TblMemberSubscriptionValidation { get; set; }

        public ClsMemberSubscription()
        {
            TblMemberSubscription = new DSTransactions.MemberSubscriptionDataTable();
            TblMemberSubscriptionValidation = new DSTransactions.MemberSubscriptionValidationDataTable();
        }

        public virtual int FillMemberSubscriptionValidation(string pMemberID, string pServiceAreaSubscriptionID,string pDateSubscribe)
        {
            TblMemberSubscriptionValidation.Rows.Clear();
            return new MemberSubscriptionValidationTableAdapter().Fill(TblMemberSubscriptionValidation, pMemberID, pServiceAreaSubscriptionID, pDateSubscribe);
        }

        public string GetLasSubscriptionIDByServiceAreaSubscription(string serviceAreaSubscriptionID)
        {
            string strReturn = "";
            DataRow[] drw = TblMemberSubscription.Select("ServiceAreaSubscriptionID='" + serviceAreaSubscriptionID + "'");
            if (drw.Length > 0)
            {
                strReturn = drw[0].GetString("MemberSubscriptionID");
            }

            return strReturn;
        }

        public DateTime GetExpirationDate(string MemberSubscriptionID)
        {
            DateTime dtReturn = new DateTime();
            DataRow drw = TblMemberSubscription.Select("MemberSubscriptionID='" + MemberSubscriptionID + "'")[0];
            dtReturn = drw.GetDateTime("DateExpired");
            return dtReturn;
        }

        public DateTime GetDateSubscribed(string MemberSubscriptionID)
        {
            DateTime dtReturn = new DateTime();
            DataRow drw = TblMemberSubscription.Select("MemberSubscriptionID='" + MemberSubscriptionID + "'")[0];
            dtReturn = drw.GetDateTime("DateSubscribed");
            return dtReturn;
        }

        public string GetMemberSubscriptionID(DateTime dtNow, string MemberID)
        {
            string strReturn = "";

            DataRow[] drw = TblMemberSubscription.Select("DateSubscribed<=#" + dtNow.Date + "# AND DateExpired>=#" + dtNow.Date + "#");
            if (drw.Length > 0)
            {
                strReturn = drw[0].GetString("MemberSubscriptionID");
            }

            return strReturn;
        }

        public virtual int Fill(DSTransactions.MemberSubscriptionDataTable tbl,string MemberID)
        {
            tbl.Rows.Clear();
            return new MemberSubscriptionTableAdapter().Fill(tbl,MemberID);
        }

        public void Update()
        {
            using (MemberSubscriptionTableAdapter TA = new MemberSubscriptionTableAdapter())
            {
                TA.Update(TblMemberSubscription);
            }
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}