﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;
namespace uhopWeb.Classes
{
    public class ClsContactType:IDisposable
    {

        public DSConfigs.ContactNumberTypeDataTable TblContactType = new DSConfigs.ContactNumberTypeDataTable();

        public virtual int Fill()
        {
            return new ContactNumberTypeTableAdapter().Fill(TblContactType);
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}