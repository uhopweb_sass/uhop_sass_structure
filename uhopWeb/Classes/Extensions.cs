﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using uhopWeb.DataSets;

namespace uhopWeb.Classes
{
    public static class Extensions
    {
        public static string ConvertToTitleCase(this string str)
        {
            return System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(str);
        }

        public static Decimal ConvertToDecimal(this string str)
        {
            return Convert.ToDecimal(str);
        }

        public static string ToStringOrEmpty(this string str) {
            return string.IsNullOrEmpty(str) ? "" : str;
        }

        public static decimal GetDecimal(this DataRow DR, string ColumnName)
        {
            return (DR.IsNull(ColumnName) ? 0 : Convert.ToDecimal(DR[ColumnName].ToString()));
        }

        public static void SetDecimal(this DataRow DR, string ColumnName, object Value)
        {
            DR[ColumnName] = (decimal)Value;
        }

        public static string GetString(this DataRow DR,string ColumnName)
        {
            return DR.IsNull(ColumnName)?"":DR[ColumnName].ToString();
        }
              
        public static void SetString(this DataRow DR, string ColumnName, object Value)
        {
            if (Value == null)
            { DR[ColumnName]=DBNull.Value;}
            else
            { DR[ColumnName] = (string)Value; }
        }

        public static void SetInt16(this DataRow DR, string ColumnName, object Value)
        {
            DR[ColumnName] = (Int16)Value;
        }

        public static int GetInt16(this DataRow DR, string ColumnName)
        {
            return Convert.ToInt16(DR[ColumnName].ToString());
        }

        public static void SetInt32(this DataRow DR, string ColumnName, object Value)
        {
            DR[ColumnName] = (Int32)Value;
        }

        public static int GetInt32(this DataRow DR, string ColumnName)
        {
            return Convert.ToInt32(DR[ColumnName].ToString());
        }

        public static void SetInt64(this DataRow DR, string ColumnName, object Value)
        {
            DR[ColumnName] = (Int64)Value;
        }

        public static Int64 GetInt64(this DataRow DR, string ColumnName)
        {
            return Convert.ToInt64(DR[ColumnName].ToString());
        }

        public static DateTime GetDateTime(this DataRow DR, string ColumnName)
        {
            return Convert.ToDateTime(DR[ColumnName].ToString());
        }
        public static void SetDateTime(this DataRow DR,string ColumnName, object Value)
        {
            if (Value == null)
            {
                DR[ColumnName] = DBNull.Value;
            }
            else
            {
                DR[ColumnName] = (DateTime)Value;
            }
        }
    }
}