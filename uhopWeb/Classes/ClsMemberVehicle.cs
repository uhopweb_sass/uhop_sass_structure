﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;

namespace uhopWeb.Classes
{
    public class ClsMemberVehicle:IDisposable
    {

        public DSConfigs.VehicleTypeDataTable TblVehicleType { get; set; }
        public DSConfigs.MemberVehicleDataTable TblMemberVehicle { get; set; }
        public DSConfigs.MemberVehicleAttachmentDataTable TblMemberVehicleAttachment { get; set; }

        public ClsMemberVehicle()
        {
            TblVehicleType = new DSConfigs.VehicleTypeDataTable();
            TblMemberVehicle = new DSConfigs.MemberVehicleDataTable();
            TblMemberVehicleAttachment = new DSConfigs.MemberVehicleAttachmentDataTable();
        }

        public virtual int Fill(DSConfigs.MemberVehicleDataTable tbl)
        {
            return new MemberVehicleTableAdapter().Fill(tbl);
        }

        public virtual int FillViaReferenceID(DSConfigs.MemberVehicleAttachmentDataTable tbl,string pMemberID)
        {
            return new MemberVehicleAttachmentTableAdapter().Fill(tbl,pMemberID);
        }

        public virtual int FillViaID(DSConfigs.MemberVehicleDataTable tbl, string pMemberID)
        {
            return new MemberVehicleTableAdapter().FillBy(tbl,pMemberID);
        }


        public virtual int FillType()
        {
            return new VehicleTypeTableAdapter().Fill(TblVehicleType);
        }
        
        public void Update()
        {
            new MemberVehicleTableAdapter().Update(TblMemberVehicle);
        }

        public void UpdateVehicleAttachment()
        {
            new MemberVehicleAttachmentTableAdapter().Update(TblMemberVehicleAttachment);
        }


        public void Dispose() { GC.SuppressFinalize(this); }
    }
}