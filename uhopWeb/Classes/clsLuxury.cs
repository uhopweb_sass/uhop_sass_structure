﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;
using System.Data;

namespace uhopWeb.Classes
{
    public class clsLuxury : IDisposable
    {
        public DSConfigs.LuxuryDataTable TblLuxury = new DSConfigs.LuxuryDataTable();
        public DSConfigs.EmailRecipientDataTable TblEmailRecipient = new DSConfigs.EmailRecipientDataTable();
        public DSConfigs.LuxuryTypeDataTable TblLuxuryType = new DSConfigs.LuxuryTypeDataTable();
        public DSConfigs.LuxuryBookingDataTable TblLuxuryBooking = new DSConfigs.LuxuryBookingDataTable();

        public clsLuxury()
        {
            TblLuxury = new DSConfigs.LuxuryDataTable();
            TblEmailRecipient = new DSConfigs.EmailRecipientDataTable();
            TblLuxuryType = new DSConfigs.LuxuryTypeDataTable();
            TblLuxuryBooking = new DSConfigs.LuxuryBookingDataTable();
        }

        public virtual int FillLuxury(DSConfigs.LuxuryDataTable tbl)
        {
            return new LuxuryTableAdapter().Fill(tbl);
        }

        public virtual int FillEmailRecipient()
        {
            return new EmailRecipientTableAdapter().Fill(TblEmailRecipient);
        }

        public virtual int FillLuxuryType(DSConfigs.LuxuryTypeDataTable tbl)
        {
            return new LuxuryTypeTableAdapter().Fill(tbl);
        }

        public virtual int FillLuxuryBooking(string luxuryBookingID)
        {
            return new LuxuryBookingTableAdapter().Fill(TblLuxuryBooking, luxuryBookingID);
        }

        public void UpdateLuxuryBooking()
        {
            new LuxuryBookingTableAdapter().Update(TblLuxuryBooking);
        }

        public string GetLuxuryTypeDetails(string pLuxuryTypeID,string pFieldName)
        {
            DataRow drw = TblLuxuryType.Select("LuxuryTypeID='" + pLuxuryTypeID + "'")[0];
            string strReturn = drw[pFieldName] == DBNull.Value ? "" : (string)drw[pFieldName];
            return strReturn;
        }

        public string GetLuxuryDetails(string pLuxuryID, string pFieldName)
        {
            DataRow drw = TblLuxury.Select("LuxuryID='" + pLuxuryID + "'")[0];
            string strReturn = drw[pFieldName] == DBNull.Value ? "" : (string)drw[pFieldName];
            return strReturn;
        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}