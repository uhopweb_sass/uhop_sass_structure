﻿using System;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace uhopWeb.Classes
{
    public class ClsEncryptor:IDisposable   
    {
       public static byte[] Encrypt(byte[] input, string password)
       {
       try {
	       TripleDESCryptoServiceProvider service = new TripleDESCryptoServiceProvider();
	       MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

	       byte[] key = md5.ComputeHash(Encoding.ASCII.GetBytes(password));
	       byte[] iv = md5.ComputeHash(Encoding.ASCII.GetBytes(password));

	       return Transform(input, service.CreateEncryptor(key, iv));
       } catch (Exception e) {
	       return new byte[-1 + 1];
       }
       }

       public static byte[] Decrypt(byte[] input, string password)
       {
           try
           {
               TripleDESCryptoServiceProvider service = new TripleDESCryptoServiceProvider();
               MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

               byte[] key = md5.ComputeHash(Encoding.ASCII.GetBytes(password));
               byte[] iv = md5.ComputeHash(Encoding.ASCII.GetBytes(password));

               return Transform(input, service.CreateDecryptor(key, iv));
           }
           catch (Exception e)
           {
               return new byte[-1 + 1];
           }
       }

       public static string Encrypt(string text, string password)
       {
        byte[] input = Encoding.UTF8.GetBytes(text);
        byte[] output = Encrypt(input,password);
        string strReturn = Convert.ToBase64String(output).ToString();
        return Convert.ToBase64String(output);
       }

       public static string Decrypt(string text, string password)
       {
           byte[] input = Convert.FromBase64String(text);
           byte[] output = Decrypt(input, password);
           return Encoding.UTF8.GetString(output);
       }

       private static byte[] Transform(byte[] input, ICryptoTransform CryptoTransform)
       {
           MemoryStream memStream = new MemoryStream();
           CryptoStream cryptStream = new CryptoStream(memStream, CryptoTransform, CryptoStreamMode.Write);

           cryptStream.Write(input, 0, input.Length);
           cryptStream.FlushFinalBlock();

           memStream.Position = 0;
           byte[] result = new byte[System.Convert.ToInt32(memStream.Length)];
           memStream.Read(result, 0, System.Convert.ToInt32(result.Length));

           memStream.Close();
           cryptStream.Close();

           return result;
       }
    
    public void Dispose() { GC.SuppressFinalize(this); }
    }
       
}