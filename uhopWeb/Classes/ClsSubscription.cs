﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;
using uhopWeb.DataSets.DSTransactionsTableAdapters;
using System.Data;

namespace uhopWeb.Classes
{
    public class ClsSubscription:IDisposable
    {

        public DSConfigs.SubscriptionDataTable TblSubscription { get; set; }
        public DSTransactions.ServiceAreaSubscriptionToSellDataTable TblServiceAreaSubscription { get; set; }
        public DSTransactions.ServiceAreaSubscriptionLocationDataTable TblServiceAreaSubscriptionLocation { get; set; }

        public DSTransactions.ServiceAreaSubscriptionLocationMajorStopDataTable TblServiceAreaSubscriptionLocationMajorStop { get; set; }

        public DSTransactions.MemberSubscriptionListDataTable TblMemberSubscriptionList { get; set; }



        private string _ServiceAreaSubscriptionID = "";

        public ClsSubscription()
        {
            TblSubscription = new DSConfigs.SubscriptionDataTable();
            TblServiceAreaSubscription = new DSTransactions.ServiceAreaSubscriptionToSellDataTable();
            TblServiceAreaSubscriptionLocation = new DSTransactions.ServiceAreaSubscriptionLocationDataTable();
            TblMemberSubscriptionList = new DSTransactions.MemberSubscriptionListDataTable();
            TblServiceAreaSubscriptionLocationMajorStop = new DSTransactions.ServiceAreaSubscriptionLocationMajorStopDataTable();
        }

        public ClsSubscription(string pServiceAreaSubscriptionID):this()
        {
            _ServiceAreaSubscriptionID = pServiceAreaSubscriptionID;
        }

        public virtual int FillServiceAreaSubscription()
        {
            TblServiceAreaSubscription.Rows.Clear();
            return new ServiceAreaSubscriptionToSellTableAdapter().Fill(TblServiceAreaSubscription);
        }

        public virtual int FillMemberSubscriptionList(string pMemberID)
        {
            TblMemberSubscriptionList.Rows.Clear();
            return new MemberSubscriptionListTableAdapter().Fill(TblMemberSubscriptionList,pMemberID);
        }


        public virtual int FillServiceAreaSubscription(string pOrigin, string pDestination)
        {
            TblServiceAreaSubscription.Rows.Clear();
            return new ServiceAreaSubscriptionToSellTableAdapter().FillByOriginDestination(TblServiceAreaSubscription, pOrigin,pDestination);
        }

        public virtual int FillServiceAreaSubscriptionLocation(string pServiceAreaSubscriptionLocationID)
        {
            TblServiceAreaSubscriptionLocation.Rows.Clear();
            return new ServiceAreaSubscriptionLocationTableAdapter().Fill(TblServiceAreaSubscriptionLocation, pServiceAreaSubscriptionLocationID);
        }

        public virtual int FillServiceAreaSubscriptionLocationMajorStop(string pServiceAreaLocationID)
        {
            TblServiceAreaSubscriptionLocationMajorStop.Rows.Clear();
            return new ServiceAreaSubscriptionLocationMajorStopTableAdapter().Fill(TblServiceAreaSubscriptionLocationMajorStop, pServiceAreaLocationID);
        }

        public virtual int Fill()
        {
            TblSubscription.Rows.Clear();
            return new SubscriptionTableAdapter().Fill(TblSubscription);
        }

        public string GetSubscriptionDetails(string FieldName)
        {
            DataRow drw = TblServiceAreaSubscription.Select("ServiceAreaSubscriptionID='" + _ServiceAreaSubscriptionID + "'")[0];
            string strReturn = drw[FieldName] == DBNull.Value ? "" : (string)Convert.ToString(drw[FieldName]);
            return strReturn;
        }


        public void Dispose() { GC.SuppressFinalize(this); }
    }
}