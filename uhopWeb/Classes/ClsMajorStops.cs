﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;
using System.Data;

namespace uhopWeb.Classes
{
    public class ClsMajorStops:IDisposable
    {
        private string _ID = "";
        private string _Address = "";
        private decimal _Latitude = 0;
        private decimal _Longitude = 0;
        
        public DSConfigs.MajorStopDataTable TblMajorStops = new DSConfigs.MajorStopDataTable();

        public string id { get { return _ID; } }
        public string address { get { return _Address; } }
        public decimal latitude { get { return _Latitude; } }
        public decimal longitude { get { return _Longitude; } }

        public ClsMajorStops() { 
        
        }

        public ClsMajorStops(string MajorStopID)
        {
            _ID = ""; ;
            _Address = "";
            _Latitude = 0;
            _Longitude = 0;
            using (MajorStopTableAdapter TA = new MajorStopTableAdapter())
            {
                TA.Fill(TblMajorStops);
            }
            DataRow[] drw = TblMajorStops.Select("MajorStopID='" + MajorStopID + "'");
            if (drw.Length > 0)
            {
                _ID = MajorStopID;
                _Address = drw[0].GetString("MajorStopAddress");
                _Latitude = drw[0].GetDecimal("Latitude");
                _Longitude = drw[0].GetDecimal("Longitude");
            }

        }

        public void Dispose() { GC.SuppressFinalize(this); }
    }
}