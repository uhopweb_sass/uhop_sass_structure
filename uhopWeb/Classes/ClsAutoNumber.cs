﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;

namespace uhopWeb.Classes
{
    public class ClsAutoNumber:IDisposable
    {

        public Int64 GenerateAutoNumber(string xKey,string Format)
        {
            Int64 intReturn = 0;
            int xVal = 0;
            using (KeysTableAdapter TA = new KeysTableAdapter())
            {
                using (DSConfigs.KeysDataTable tbl = new DSConfigs.KeysDataTable())
                {
                    TA.Fill(tbl, xKey);
                    if (tbl.Rows.Count > 0)
                    {
                        xVal = Convert.ToInt32(tbl.Rows[0]["xVal"].ToString())+1;
                        tbl.Rows[0].BeginEdit();
                        tbl.Rows[0]["xVal"] = xVal;
                        tbl.Rows[0].EndEdit();
                    }
                    else
                    {
                        DataRow[] drw = tbl.Select("xKey='" + xKey + "' AND xDate<='"+ DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") +"'");
                        if (drw.Length > 0)
                        { 
                            tbl.Rows.Remove(drw[0]); 
                            
                        }
                        xVal = 1;
                        tbl.Rows.Add(xKey, DateTime.Now, 1);                        
                    }
                    TA.Update(tbl);

                }                
            }

            intReturn = Convert.ToInt64(DateTime.Now.ToString("yyyyMMdd") + (Format + xVal.ToString().Trim()).Substring(xVal.ToString().Length));

            return intReturn;
        }


        public void Dispose() { GC.SuppressFinalize(this); }
    }
}