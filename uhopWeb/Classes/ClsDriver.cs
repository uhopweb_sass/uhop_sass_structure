﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uhopWeb.DataSets;
using uhopWeb.DataSets.DSConfigsTableAdapters;

namespace uhopWeb.Classes
{
    public class ClsDriver:IDisposable
    {

        public DSConfigs.DriverDataTable tblDrivers { get; set; }

        public DSConfigs.DriverAssignedDataTable tblDriverAssigned { get; set; }

        public ClsDriver() {
            tblDrivers = new DSConfigs.DriverDataTable();
            tblDriverAssigned = new DSConfigs.DriverAssignedDataTable();
        }

        public virtual int Fill() {
            return new DriverTableAdapter().Fill(tblDrivers);
        }

        public virtual int FillDriverAssigned() 
        {
            return new DriverAssignedTableAdapter().Fill(tblDriverAssigned);
        }

        public void Update() {
            using (DriverTableAdapter TA = new DriverTableAdapter())
            {
                TA.Update(tblDrivers);
            }
        }

        public void UpdateDriverAssigned()
        {
            using(DriverAssignedTableAdapter TA = new DriverAssignedTableAdapter())
            {
                TA.Update(tblDriverAssigned);
            }
        }


        public void Dispose() { GC.SuppressFinalize(this); }
    }
}